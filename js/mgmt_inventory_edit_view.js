$.getJSON(
	  json_url, 
	  function(data) {
	      var json_obj = data;	   
	      var transaction = json_obj.transaction;	 
 	
	      var TAX_RAT = 0.15; //after tax change
	

	      $(function() {
	      
		      var dom_name = $('#name');
		      var dom_branch = $('#branch');
		      var dom_id = $('#id');
		      var dom_subtotal = $('#subtotal'); 
		      var dom_gst = $('#gst'); 
		      var dom_total = $('#total'); 
		      var dom_qty = $('#qty'); 
		      var dom_low_stock = $('#low_stock'); 
		      var dom_price = $('#price'); 
		      var dom_pur_id = $('#pur_id'); 
		      var dom_category = $('#category'); 
		      var dom_detail= $('#detail'); 


		      dom_name.val(transaction.name);		      
		      dom_detail.val(transaction.detail);
		      dom_branch.val(transaction.branch);
		      dom_pur_id.val(transaction.pur_id);		      
		      dom_low_stock.val(transaction.low_stock);
		      dom_category.val(transaction.category);

 		      var subtotal, total, tax;
		      subtotal=total=tax = 0;

		      $('#close')
			  .click(function () { 
				  window.close();	     
			      }); 
	      
		      $('#save')
			  .click(function () { 
				  transaction.name = dom_name.val();
				  transaction.detail= dom_detail.val();
			    	  transaction.branch = dom_branch.val();
			    	  transaction.pur_id = dom_pur_id.val();
			    	  transaction.low_stock = dom_low_stock.val();
			    	  transaction.category = dom_category.val();

				  var post_json = new Object();
				  post_json.transaction = transaction;
 
			     
				  $.ajax({
					  type: 'POST',
					      url: post_url,
					      data: {'json' : JSON.stringify(post_json)},
					      success:  function(data) { 
					      top.window.resizeTo(720,300);
					      $('#save').hide();
					      $('#inner-text')
						  .hide()
						  .html(data)
						  .fadeIn(200, 
							  function () {
							      setTimeout(
									 'window.close()', 500);
							  });
					      window.opener.location.reload(true);
					  }
				      });
			      });

 
	      
	      
	       
		      dom_total      
			  .keyup(function() {
				  total = $(this).val();
				  calc_rvs();
				  display('total');
			      })
			  .bind('paste cut', function() {
				  setTimeout(function() { dom_total.keyup(); }, 300);				      
			      });
		      
		      
		      
		      dom_qty      
			  .keyup(function() {
				  transaction.qty = $(this).val(); 
				  calc();
				  display('qty');
			      })
			  .bind('paste cut', function() {
				  setTimeout(function() { dom_qty.keyup(); }, 300);				      
			      });
		      
		      
	      
	      	      dom_price      
			  .keyup(function() {
				  transaction.price = $(this).val(); 
				  calc();
				  display('price');
			      })
			  .bind('paste cut', function() {
				  setTimeout(function() { dom_price.keyup(); }, 300);				      
			      });
		 
	      
		      
	      
		      function display(hide) {

			  dom_id.val(transaction.id);
		  
			  dom_subtotal.val(parseFloat(subtotal).toFixed(2));
			  dom_gst.val(parseFloat(tax).toFixed(2)); 
			  if (hide != 'total')
			      dom_total.val(parseFloat(total).toFixed(2));
			  if (hide != 'price')
				  dom_price.val(parseFloat(transaction.price).toFixed(4));
			  if (hide != 'qty')
			      dom_qty.val(transaction.qty); 

		      }

	      
		 
	      
		      function calc(){
                         var item_total = transaction.qty * transaction.price;
		 
			  subtotal = item_total;
			  tax = subtotal * TAX_RAT;
			  total = subtotal * (1 + TAX_RAT);  
		      }
	      
		      function calc_rvs(){
			  subtotal = total / (1 + TAX_RAT);
			  tax = subtotal * TAX_RAT;
			  if (transaction.qty != 0)transaction.price = total / transaction.qty;
		  
		      }

		    
	            calc();
		    display();	

		      if(read_only){
			  $("input.cst")
			      .each(function(){
				      $(this).attr('readonly', 'readonly');
				      $(this).css('background','#f1f1f1');
				      $('tr').removeClass('bgc');
				      $('#save').hide();
				  });
		  
		      }


		  });//end doc ready


	  });// end getjson
 