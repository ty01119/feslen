$.getJSON(
    json_url, 
    function(data) {

	var json_obj = data;
	var transaction = json_obj.transaction;
	transaction.payment_meth = transaction.payment_meth.split(';');
	
	$(function() {

	      
	      $('#close')
		  .click(function () { 
			     window.close();	     
			 }); 
	      
	      $('#save')
		  .click(function () { 

			     if (transaction.payment_meth.length < 1)
			     {
				 $('#msg').html('Please choose payment method.');
				 $('#msgbox').fadeIn();
			     } 
			     else 
			     {

				 transaction.payment_meth 
				     = transaction.payment_meth.join(';');

				 $('#msgbox').fadeOut();
				 $(".cst")
				     .each(function(){
					       transaction[this.id] = $(this).val();	   
					   });	     
				 
				 var post_json = new Object();
				 post_json.transaction = transaction;
				 
				 $.ajax({
					    type: 'POST',
					    url: post_url,
					    data: {'json': JSON.stringify(post_json)},
					    success:  function(data) {

						var json = $.parseJSON(data); 
						if (json.close)
						{					
						    top.window.resizeTo(720,300);
						    $('#save').hide();	
						    $('#msgbox').fadeIn();
						    $('#msg').html(json.msg);
						    $('#inner-wrapper')
							.fadeOut(111, function() {	
								     setTimeout('window.close()', 500);
								 });
						    
						    window.opener.location.reload(true); 			
						}
						else
						{
						    $('#msgbox').fadeIn();
						    $('#msg').html(json.msg);
						}

					    }
					});
			     }
			     
			 });
	    
	    $('#pmt_cash, #pmt_eftpos, #pmt_cheque, #pmt_banks, #pmt_other')
		.click(function(){
		    if ($(this).attr('checked') &&
			$.inArray(this.value, transaction.payment_meth) == -1) {
			transaction.payment_meth.push(this.value);
		    }
		    else{
			transaction.payment_meth.splice(
			    transaction.payment_meth.indexOf(this.value), 1); 
		    }
		});
	      


	      if(read_only){
		  $(".cst")
		      .each(function(){
				$(this).val(transaction[this.id]);
				$(this).attr('readonly', 'readonly');
				$(this).css('background','#f1f1f1');
				$('tr').removeClass('bgc');
				$('#save').hide();
			    });
		  
	      } else {
		  $(".cst")
		      .each(function(){
				$(this).val(transaction[this.id]);
			    });
	      }
	      
	      $.each(transaction.payment_meth, function() {
		    if(this.toString()) 
			$('[value="' + this +'"]').attr('checked', true);
		});
		
	      $('.ui_buttonset').buttonset( "refresh" );
	      
	      


	  });//end doc ready

	
    });// end getjson