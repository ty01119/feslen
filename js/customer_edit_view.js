$.getJSON(
    json_url, 
    function(data) {

	var json_obj = data;
	var customer = json_obj.customer;
 
	$(function() {

     
      $('#close')
	  .click(function () { 
		     window.close();	     
		 }); 
      
      $('#save')
	  .click(function () { 
		     $('#msgbox').fadeOut();
		     $(".cst")
			 .each(function(){
				   customer[this.id] = $(this).val();	   
			       });	     
 
		     var post_json = new Object();
		     post_json.customer = customer;
		
		     $.ajax({
				type: 'POST',
				url: post_url,
				data: {'json': JSON.stringify(post_json)},
				success:  function(data) {
				    var json = $.parseJSON(data);
				    if (json.close)
				    {					
					top.window.resizeTo(720,300);
					$('#save').hide();	
					$('#msgbox').fadeIn();
					$('#msg').html(json.msg);
					$('#inner-wrapper')
					    .fadeOut(111, function() {	
							 setTimeout('window.close()', 500);
						    });
					if (window.opener.name == '')
					{ // openned by main dock
					   window.opener.location.reload(true);   
					}
					else
					{ 
					   window.opener.document.getElementById('customer').value = post_json.customer.name;
					}
				    }
				    else
				    {
					$('#msgbox').fadeIn();
					$('#msg').html(json.msg);
				    }
				}
			    });
			   
		 });
    
      if(read_only){
	  $(".cst")
	      .each(function(){
			$(this).val(customer[this.id]);
			$(this).attr('readonly', 'readonly');
			$(this).css('background','#f1f1f1');
			$('tr').removeClass('bgc');
			$('#save').hide();
		    });
	  
      } else {
	  $(".cst")
	      .each(function(){
			$(this).val(customer[this.id]);
		    });
      }
	
      
    
      


	  });//end doc ready

	
    });// end getjson