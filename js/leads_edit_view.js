$.getJSON(
    json_url, 
    function(data) {
	var json_obj = data;	      
	var transaction = json_obj.data;

// console.log(transaction);
 	     
	$(function() {
	      	     
	      $('#lead_id').val(transaction.id);       
	      $('#date').val(transaction.date);	      
	      $('#reminder_date').val(transaction.reminder_date);        
	      $('#reminder_notes').val(transaction.reminder_notes);	            
	      $('#owner').val(transaction.owner);	            	            
	      $('#follow').val(transaction.follow);	            
	      $('#client_company').val(transaction.company);	            
	      $('#client_contact').val(transaction.client_contact);	            
	      $('#client_email').val(transaction.client_email);	            
	      $('#client_phone').val(transaction.client_phone);          
	      $('#client_address').val(transaction.client_address);	            
	      $('#client_notes').val(transaction.client_notes);	            
	      $('#quote_id').val(transaction.quote_id);	            
	      $('#invoice_id').val(transaction.invoice_id);	            
	      $('#customer_id').val(transaction.customer_id);		            
	      $('#logs').val(transaction.logs);             
	     
 
	      $('[name="status_btn"]').each(function() {
					       if (transaction.status_btn.indexOf($(this).val()) > -1) 
						   $(this).prop("checked", true);
					   });
	     if (transaction.deleted) $('#deleted').prop("checked", true);
 
	    $('.ui_buttonset').buttonset( "refresh" );


	      $('#close')
		  .click(function () { 
			    window.close();	     
			 
			 }); 

	      
	      $('#save')
		  .click(function () {
			transaction.id = $('#lead_id').val();       
			transaction.date = $('#date').val();	      
			transaction.reminder_date = $('#reminder_date').val();        
			transaction.reminder_notes = $('#reminder_notes').val();	            
			transaction.owner = $('#owner').val();	            	            
			transaction.follow = $('#follow').val();	            
			transaction.company = $('#client_company').val();	            
			transaction.client_contact = $('#client_contact').val();	            
			transaction.client_email = $('#client_email').val();	            
			transaction.client_phone = $('#client_phone').val();          
			transaction.client_address = $('#client_address').val();	            
			transaction.client_notes = $('#client_notes').val();	            
			transaction.quote_id = $('#quote_id').val();	            
			transaction.invoice_id = $('#invoice_id').val();	            
			transaction.customer_id = $('#customer_id').val();
			transaction.log = $('#log').val();	              	
		//	if ($('#log').val()) transaction.logs = '\n- ' + $('#log').val() + '\n\n' + transaction.logs;  
			transaction.deleted = $('#deleted').is(':checked');

			   
			     transaction.status_btn = '';
			     $('[name="status_btn"]:checked').each(function() {
								      transaction.status_btn += $(this).val();   
								  });
			 

			   			   
			     var post_json = new Object();
			     post_json.transaction = transaction;
			     
 
			    var request =  $.ajax({
					type: 'POST',
					url: post_url,
					data: {'json' : JSON.stringify(post_json)},
					success:  function(data) {					 
					    // $('#msgbox').fadeIn();$('#msg').html(data);
					    var json = $.parseJSON(data);	
					    if (json.close)
					    {		 		
						top.window.resizeTo(720,300);
						$('#save').hide();	
						$('#msgbox').fadeIn();
						$('#msg').html(json.msg);
						
						$('#inner-wrapper')
						    .fadeOut(111, 
							     function () {
								 setTimeout(
								 'window.close()', 500);
							     });
						window.opener.location.reload(true); 
					    }
					    else
					    { 
						$('#msgbox').fadeIn();
						$('#msg').html(json.msg);
					    }
					}
				    });
			     
			     request.fail(function(jqXHR, textStatus) {
					      console.log(jqXHR.responseText);
					  });
			 });

	      
	      
	       


	  });//end doc ready


    });// end getjson