$.getJSON(
	  json_url, 
	  function(data) {
	      var json_obj = data; 
	      var obj_data = json_obj.obj_data; 
	      var contents = $.parseJSON(obj_data.contents);
 
	      obj_data.date = date("d/m/Y", strtotime(obj_data.date));
	 
	      $(function() {
		      
		      
		      var dom_inv_items = $("#invo-items");

		      $('#close')
			  .click(function () { 
				  window.close();	     
			      }); 
	      
		      $('#save')
			  .click(function () { 
			  	  var serialize 
				      = $("#invo-items").sortable("toArray");
				      
				  var serial_conts = new Array();
				  for (i in serialize){ 
				      serial_conts[i] = contents[serialize[i].substr(4)];
					 
				  } 
				  contents = serial_conts;
 
				  $('.cst')
				      .each(function(){
					      obj_data[this.id] = $(this).val();
					  });

				  var post_json = new Object();
			     
				  post_json.obj_data = obj_data;
				  post_json.contents = contents;
			     

				  $.ajax({
					  type: 'POST',
					      url: post_url,
					      data: {'json' : JSON.stringify(post_json)},
					      success:  function(data) {
					      top.window.resizeTo(720,300);
					      $('#save').hide();
					      $('#inner-text')
						  .hide()
						  .html(data)
						  .fadeIn(200, function () {
							  setTimeout('window.close()', 500);
						      });
					      window.opener.location.reload(true);
					  }
				      });
			      });
	      
 

		      display_ctns();
	      
              
		      if(read_only){
			  $(".cst")
			      .each(function(){
				      $(this).val(obj_data[this.id]);
				      $(this).attr('readonly', 'readonly');
				      $(this).css('background','#f1f1f1');
				      $('tr').removeClass('bgc');
				      $('#save').hide();
				  });
		  
		      } else {  
			  $(".cst")
			      .each(function(){
				      $(this).val(obj_data[this.id]); 
				  });

		      }
		      
		      
	       $("#item_add")
			  .click(function () { 
				  var i = contents.length;
				  contents[i] = new Object();
				  contents[i].quantity = 1;
				  contents[i].discount = 0;
				  contents[i].unit_price = 0;
				  append_item(i);		     
			      });
	      
	      
	      
		      function display_ctns(){ 
			  for (i in contents)
			      {
				  append_item(i);
				  $('#prod'+i).val(contents[i].product);
				  $('#qty'+i).val(contents[i].quantity);
				  $('#up'+i).val(contents[i].unit_price);
				  $('#dc'+i).val(contents[i].discount);
			      }
		      }
	      
	      

		      function append_item(i){
			  var html = invo_item_html(i);
			  dom_inv_items.append(html);
			  $('#item' + i).fadeIn("normal"); 
			  $('#prod' + i)
			      .keyup(function() {
				      contents[i].product = $(this).val();
				  })
			      .bind('paste cut', function() {
				      setTimeout(function() { $('#prod' + i).keyup(); }, 300);				      
				  });
			  
			  $('#qty' + i).keyup(function() {
				  contents[i].quantity = $(this).val();
				  calc();
			      })
			      .bind('paste cut', function() {
				      setTimeout(function() { $('#qty' + i).keyup(); }, 300);				      
				  });

			  $('#up' + i).keyup(function() {
				  contents[i].unit_price = $(this).val();
				  calc();
			      })
			      .bind('paste cut', function() {
				      setTimeout(function() { $('#up' + i).keyup(); }, 300);				      
				  });
			  
			  $('#dc' + i).keyup(function() {
				  contents[i].discount = $(this).val();
				  calc();
			      })
			      .bind('paste cut', function() {
				      setTimeout(function() { $('#dc' + i).keyup(); }, 300);				      
				  });
			  
			  $('.item_del', '#item' + i)
			      .click(function () {
				      var obj = $(this.parentNode);
				      obj.fadeOut("slow", function () {
					      obj.remove();
					      delete contents[i];  // set i -> undefined
					      calc();
 
					  });			     
				  });    
		      }
	      
		     function calc(){
		  
			  var item_total = calc_item_total();
			  obj_data.total = item_total;
			 
			  $('#total').val(obj_data.total);
		      }
	      
		 

		      function calc_item_total(){
		  
			  var item_total = 0;
			  for (i in contents){
			      if (contents[i] != undefined) {
				  item_total += parseFloat(contents[i].quantity)
				      * parseFloat(contents[i].unit_price);	  
			      } 
			  }
			  return item_total;
		      }
		      
		      
		  });//end doc ready


	  });// end getjson

function invo_item_html(id){

    var rt = '<li class="ui-helper-clearfix bgc" id="item' + id + '" style="display:none;">'
	+ '<span class="ui-icon ui-icon-arrowthick-2-n-s pt flt" style="margin:2px 5px 0 0;" title="Drag to sort"></span>'
	+ '<input type="text" name="2" value="" id="prod' + id + '" class="ui-corner-all prod"/>'
	+ '<input type="text" name="2" value="1" id="qty' + id + '" maxlength="5" class="ui-corner-all qty"/>'
	+ '<input type="text" name="2" value="0" id="up' + id + '"  class="ui-corner-all up"/>'
	+ '<input type="text" name="2" value="0" id="dc' + id + '" maxlength="5" class="ui-corner-all dc"/>'
	+ '<span class="ui-icon ui-icon-closethick pt flt item_del" style="margin:2px 0 0 20px;" title="Delete" id="del' + id + '"></span></li>';

    return rt;
}