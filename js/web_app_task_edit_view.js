$.getJSON(
    json_url, 
    function(data) {
	var json_obj = data;	      
	var transaction = json_obj.data;

	     
	$(function() {
	      
	      var dom_customer = $('#customer');
	      var dom_id = $('#id');
	      var dom_date = $('#date');
	      var dom_invo_id = $('#invo_id'); 

	      var dom_developer = $('#developer'); 
	      var dom_designer = $('#designer'); 
	      var dom_qa = $('#qa'); 
	      
	      var dom_paid = $('#paid'); 
	      var dom_finished = $('#finished');
	      var dom_delivered = $('#delivered'); 

	      var dom_notes = $('#notes');
	      var dom_new_cmts = $('#new_cmts');
	      var dom_logs = $('#logs');


	      dom_customer.val(transaction.cust_name);	  
	      dom_date.val(date("d/m/y", strtotime(transaction.date)));	  
	      dom_id.val(transaction.id);
  
              dom_invo_id.val(transaction.invo_id);
 
              dom_developer.val(transaction.developer);
 
	      dom_designer.val(transaction.designer);
 
	      dom_qa.val(transaction.qa); 
	      dom_notes.val(transaction.notes);
	     
	      dom_logs.html(transaction.logs);

	      

	      if (parseInt(transaction.finished)) 
		  dom_finished.attr('checked', true);	      
	      if (parseInt(transaction.delivered)) 
		  dom_delivered.attr('checked', true);
	      if (parseInt(transaction.paid)) 
		  dom_paid.attr('checked', true);
	      
	      $('.ui_buttonset').buttonset( "refresh" );


	      $('#close')
		  .click(function () { 
			     window.close();	     
			 }); 

	      
	      $('#save')
		  .click(function () {
			     
			    
			     transaction.logs = "<br />- " 
				 + $('#comments').val() 
				 + transaction.logs;
			     
			     transaction.cust_name = dom_customer.val();
			     transaction.invo_id = dom_invo_id.val();
			     transaction.developer = dom_developer.val();
			     transaction.designer = dom_designer.val();
			     transaction.qa = dom_qa.val();
			     transaction.notes = dom_notes.val();
			     
			     var post_json = new Object();
			     post_json.transaction = transaction;
			     
			     
			     $.ajax({
					type: 'POST',
					url: post_url,
					data: {'json' : JSON.stringify(post_json)},
					success:  function(data) {
					    // $('#msgbox').fadeIn();$('#msg').html(data);
					    var json = $.parseJSON(data);	
					    if (json.close)
					    {					
					    top.window.resizeTo(720,300);
					    $('#save').hide();	
					    $('#msgbox').fadeIn();
					    $('#msg').html(json.msg);
					    
					    $('#inner-wrapper')
						.fadeOut(111, 
							 function () {
							     setTimeout(
								 'window.close()', 500);
							 });
					    window.opener.location.reload(true); 
					    }
					    else
					    {
						$('#msgbox').fadeIn();
						$('#msg').html(json.msg);
					    }
					}
				    });

			 });

	      
	      
	      dom_finished
		  .click(function(){
			     transaction.finished = $(this).attr('checked') ? 1 : 0;
			 });
	      
	      dom_delivered
		  .click(function(){
			     transaction.delivered = $(this).attr('checked') ? 1 : 0;
			 });
	      
	      dom_paid
		  .click(function(){
			     transaction.paid = $(this).attr('checked') ? 1 : 0;
			 });
	      
	    
	      

	      
	      


	  });//end doc ready


    });// end getjson
