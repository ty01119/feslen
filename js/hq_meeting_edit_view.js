$.getJSON(
    json_url, 
    function(data) {
	var json_obj = data;	      
	var transaction = json_obj.data;

	     
	$(function() { 
	      var dom_id = $('#id');
	      var dom_date = $('#date'); 
 
	      var dom_approve = $('#approve');  

	      var dom_purpose = $('#purpose');
	      var dom_attendees = $('#attendees');
	      var dom_outcomes = $('#outcomes');
	      var dom_status = $('#status');
 
	      var dom_logs = $('#logs');

 	  
	      dom_date.val(date("d/m/y H:i:s", strtotime(transaction.date)));	  
	      dom_id.val(transaction.id);
   
	      dom_purpose.val(transaction.purpose);
	      dom_attendees.val(transaction.attendees);
	      dom_outcomes.val(transaction.outcomes);
	     
	      dom_logs.html(transaction.logs);

	      	      dom_status.html(transaction.status);
	      	      
	      dom_approve.attr('checked', false); 
	      $('.ui_buttonset').buttonset( "refresh" );


	      $('#close')
		  .click(function () { 
			     window.close();	     
			 }); 

	      
	      $('#save')
		  .click(function () {
			     
			    
			     transaction.logs = transaction.logs;
			       
			     transaction.purpose = dom_purpose.val();
			     transaction.attendees = dom_attendees.val();
			     transaction.outcomes = dom_outcomes.val();
			     
			     var post_json = new Object();
			     post_json.transaction = transaction;
			     
			     
			     $.ajax({
					type: 'POST',
					url: post_url,
					data: {'json' : JSON.stringify(post_json)},
					success:  function(data) {
					    // $('#msgbox').fadeIn();$('#msg').html(data);
					    var json = $.parseJSON(data);	
					    if (json.close)
					    {					
					    top.window.resizeTo(720,300);
					    $('#save').hide();	
					    $('#msgbox').fadeIn();
					    $('#msg').html(json.msg);
					    
					    $('#inner-wrapper')
						.fadeOut(111, 
							 function () {
							     setTimeout(
								 'window.close()', 500);
							 });
					    window.opener.location.reload(true); 
					    }
					    else
					    {
						$('#msgbox').fadeIn();
						$('#msg').html(json.msg);
					    }
					}
				    });

			 });

	      
	      
	      dom_approve
		  .click(function(){
			     transaction.approve = $(this).attr('checked') ? 1 : 0; 
			 });
	      

	    
	      

	      
	      


	  });//end doc ready


    });// end getjson
