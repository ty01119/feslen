$.getJSON(
    json_url, 
    function(data) {
	var json_obj = data;	      
	var invoice = json_obj.invoice;

	var contents = json_obj.contents;
	var key_logs = '';
	invoice.payment_meth = invoice.payment_meth.split(';');
	
	var invoice_date = mysqlParseDate(invoice.date);
	var tax_change_date = new Date("October 1, 2010, 00:00:00");
	
	if (tax_change_date > invoice_date)
	{
	    var STATIC_TAX_RAT = 0.125; //before tax change
	}
	else
	{
	    var STATIC_TAX_RAT = 0.15; //after tax change
	}

	var TAX_RAT = STATIC_TAX_RAT;

	$(function() {
	    var availablePrefix = [
		"Design / Layout: ",	
		"Digital Printing: ",
		"Business Cards: ",
		"Large Format Poster: ",
		"Finishing (Lamination): ",
		"Finishing (Binding): ", 
		"Finishing: ",		
		"Service (Open File Fee): ",
		"Printing: ",
		"Vinyl: ",
		"3D / Lightbox: ",
		"Installation: ",
		"Signage: "];
	    
	    var dom_customer = $('#customer');
	    var dom_id = $('#id');
	    var dom_date = $('#date');
	    var dom_subtotal = $('#subtotal'); 
	    var dom_gst = $('#gst'); 
	    var dom_total = $('#total'); 
	    var dom_discount = $('#discount'); 
	    var dom_paid = $('#paid'); 
	    var dom_debit = $('#debit'); 
	    var dom_finished = $('#finished');
	    var dom_delivered = $('#delivered'); 
	    var dom_bad_debt = $('#bad_debt'); 
	    var dom_tot_paid = $('#tot_paid'); 
	    var dom_outsourced = $('#outsourced'); 
	    var dom_pmt_cash = $('#pmt_cash'); 
	    var dom_pmt_eftpos = $('#pmt_eftpos'); 
	    var dom_pmt_cheque = $('#pmt_cheque'); 
	    var dom_pmt_banks = $('#pmt_banks'); 
	    var dom_pmt_other = $('#pmt_other'); 
	    var dom_new_cmts = $('#new_cmts');
	    var dom_logs = $('#logs');
	    var dom_info = $('#info');
	    var dom_inv_items = $("#invo-items");
	    var dom_taxex = $("#taxex");


	    dom_customer.val(invoice.cust_name);	  
	    dom_date.val(date("d/m/y", strtotime(invoice.date)));	  
	    dom_info.val(invoice.info);	  
	    dom_logs.html(invoice.logs);
	      
	    


	    $('#close')
		.click(function () { 
		    window.close();	     
		}); 


	    $('#cart')
		.click(function () { 
		    $.getJSON(cart_url, function(json_obj) {
			
			var cart = json_obj.cart;
			var j = contents.length;
			for (k in cart)
			{
			    var i = j + k;
			    contents[i] = cart[k];
			    append_item(i);
			    $('#prod'+i).val(contents[i].product);
			    $('#qty'+i).val(contents[i].quantity);
			    $('#up'+i).val(contents[i].unit_price);
			    $('#dc'+i).val(contents[i].discount);
			} 
			
			calc();
			display();				 
			
		    });
		    
		}); 
	    
	    $('#edit_customer')
		.click(function () { 
			invoice.cust_name = dom_customer.val(); 
			openView(base_url + 'customer/edit/' + escape(invoice.cust_name), 'edit-client');
		}); 
	    
	    $('#save')
		.click(function () {

		    if (dom_tot_paid.attr('checked') && (invoice.payment_meth.length < 2))
		    {
			$('#msg').html('Please choose payment method.');
			$('#msgbox').fadeIn();
		    } 
		    else 
		    {
			
			var serialize 
			    = $("#invo-items").sortable("toArray");
			var serial_conts = new Array();
			for (i in serialize){
			    serial_conts[i] = 
				contents[serialize[i].substr(4)];
			}
			contents = serial_conts;
			invoice.info = dom_info.val();
			invoice.logs = "<br />- Total: $" 
			    + parseFloat(invoice.total).toFixed(2) 
			    + " - Comment: " 
			    + $('#comments').val() 
			    + ((key_logs == '') ? '' 
			       : '<br /> - Click Records: ' 
			       + key_logs + '<br />') 
			    + invoice.logs;
			
			invoice.cust_name = dom_customer.val();
			invoice.payment_meth 
			    = invoice.payment_meth.join(';');
			
			var post_json = new Object();
			post_json.invoice = invoice;
			post_json.contents = contents;
			
			$.ajax({
			    type: 'POST',
			    url: post_url,
			    data: {'json' : JSON.stringify(post_json)},
			    success:  function(data) {
				var json = $.parseJSON(data);
				top.window.resizeTo(720,300);
				$('#save').hide();	
				$('#msgbox').fadeIn();
				$('#msg').html(json.msg);
				
				$('#inner-wrapper')
				    .fadeOut(111, 
					     function () {
						 setTimeout(
						     'window.close()', 500);
					     });
				window.opener.location.reload(true);
			    }
			});
		    }
		});
	    calc();
	    display();
	    display_ctns();
	    
	    
	    dom_taxex
	      .click(function(){
			 if (parseInt(tax_auth)) 
			 {			     
			     alert('Tax exemption is available to qualified companies ONLY!!!');
			     invoice.tax_exempt = $(this).attr('checked') ? 1 : 0;
			     key_logs += $(this).attr('checked') 
				 ? ' &lt;' + this.value + '&gt;  '
				 : ' &gt;' + this.value + '< '; 

			     calc();
			     display(); 
			 }
			 else
			 {
			     alert('Authorization Rejected!');
			     dom_taxex.attr('checked', parseInt(invoice.tax_exempt) ? true : false);	
			 } 
			
		});


	    dom_finished
		.click(function(){
		    invoice.finished = $(this).attr('checked') ? 1 : 0;
		    key_logs += $(this).attr('checked') 
			? ' &lt;' + this.value + '&gt;  '
			: ' &gt;' + this.value + '< ';
		});

	    dom_delivered
		.click(function(){
		    invoice.delivered = $(this).attr('checked') ? 1 : 0;
		    key_logs += $(this).attr('checked') 
			? '  &lt;' + this.value + '&gt;  '
			: '  &gt;' + this.value + '&lt;  ';
		});

	    dom_bad_debt
		.click(function(){
		    invoice.bad_debt = $(this).attr('checked') ? 1 : 0;
		    key_logs += $(this).attr('checked') 
			? '  &lt;' + this.value + '&gt;  '
			: '  &gt;' + this.value + '&lt;  ';
		});
	    
	    dom_tot_paid
		.click(function(){
		    invoice.paid = $(this).attr('checked') ? invoice.total : 0;
		    key_logs += $(this).attr('checked') 
			? '  &lt;' + this.value + '&gt;  '
			: '  &gt;' + this.value + '&lt;  ';
		    display();
		});
	    
	    dom_outsourced
		.click(function(){
		    invoice.outsourced = $(this).attr('checked') ? 1 : 0;
		    key_logs += $(this).attr('checked') 
			? '  &lt;' + this.value + '&gt;  '
			: '  &gt;' + this.value + '&lt;  ';
		});
	    
	    $('#pmt_cash, #pmt_eftpos, #pmt_cheque, #pmt_banks, #pmt_other')
		.click(function(){
		    if ($(this).attr('checked') &&
			$.inArray(this.value, invoice.payment_meth) == -1) {
			invoice.payment_meth.push(this.value);
			key_logs += '  &lt;' + this.value + '&gt;  ';
		    }
		    else{
			invoice.payment_meth.splice(
			    invoice.payment_meth.indexOf(this.value), 1); 
			key_logs += '  &gt;' + this.value + '&lt;  ';
		    }
		});

	    dom_total      
		.keyup(function() {
		    invoice.total = $(this).val();
		    calc_rvs();
		    display('total');
		})
		.bind('paste cut', function() {
		    setTimeout(function() { dom_total.keyup(); }, 300);				      
		});
	    
	    dom_discount      
		.keyup(function() {
		    invoice.discount = $(this).val();
		    calc();
		    display('discount');
		})
		.bind('paste cut', function() {
		    setTimeout(function() { dom_discount.keyup(); }, 300);				      
		});


	    dom_paid      
		.keyup(function() {
		    invoice.paid = $(this).val(); 
		    calc();
		    display('paid');
		})
		.bind('paste cut', function() {
		    setTimeout(function() { dom_paid.keyup(); }, 300);				      
		});
	    
	    
	    $('.item')
		.keyup(function() {
		    alert($(this).val());
		});
	    
	    $("#item_add")
		.click(function () { 
		    var i = contents.length;
		    contents[i] = new Object();
		    contents[i].quantity = 1;
		    contents[i].discount = 0;
		    contents[i].unit_price = 0;
		    append_item(i);		     
		});
	    
	    function display(hide) {

		dom_id.val(invoice.id);
		
		dom_subtotal.val(parseFloat(invoice.subtotal).toFixed(2));
		dom_gst.val(parseFloat(invoice.tax).toFixed(2)); 
		if (hide != 'total')
		    dom_total.val(parseFloat(invoice.total).toFixed(2));
		if (hide != 'discount')
		    dom_discount.val(parseFloat(invoice.discount).toFixed(2));
		if (hide != 'paid')
		    dom_paid.val(parseFloat(invoice.paid).toFixed(2));
		dom_debit.val((parseFloat(invoice.total).toFixed(2) 
			       - parseFloat(invoice.paid).toFixed(2)).toFixed(2));


		if (parseInt(invoice.tax_exempt)) 
		    dom_taxex.attr('checked', true);	     


		if (parseInt(invoice.finished)) 
		    dom_finished.attr('checked', true);	      
		if (parseInt(invoice.delivered)) 
		    dom_delivered.attr('checked', true);     
		if (parseInt(invoice.bad_debt)) 
		    dom_bad_debt.attr('checked', true);
		if ((parseFloat(invoice.total)-parseFloat(invoice.paid))<0.05
		    && parseFloat(invoice.total) != 0)
		    dom_tot_paid.attr('checked', true);
		if (parseInt(invoice.outsourced))
		    dom_outsourced.attr('checked', true); 	 	  
		/*
		  for each (var item in invoice.payment_meth) {
		  if (item)
		  $('[value="'+item+'"]').attr('checked', true);
		  } 
		*/
		
		$.each(invoice.payment_meth, function() {
		    if(this.toString()) 
			$('[value="' + this +'"]').attr('checked', true);
		});
		
		$('.ui_buttonset').buttonset( "refresh" );


	    }

	    
	    function display_ctns(){
		for (i in contents)
		{
		    append_item(i);
		    $('#prod'+i).val(contents[i].product);
		    $('#qty'+i).val(contents[i].quantity);
		    $('#up'+i).val(contents[i].unit_price);
		    $('#dc'+i).val(contents[i].discount);
		}
	    }
	    
	    function calc(){

		TAX_RAT  = parseInt(invoice.tax_exempt) ?  0 : STATIC_TAX_RAT;
		
		var item_total = calc_item_total();
		invoice.subtotal = item_total * (1 - invoice.discount / 100);
		invoice.tax = invoice.subtotal * TAX_RAT;
		invoice.total = invoice.subtotal * (1 + TAX_RAT);

		// Rounding
		// invoice.total = Math.floor(invoice.total * 100) / 100;
		
	    }
	    
	    function calc_rvs(){

		TAX_RAT  = parseInt(invoice.tax_exempt) ?  0 : STATIC_TAX_RAT;

		invoice.subtotal = invoice.total / (1 + TAX_RAT);
		invoice.tax = invoice.subtotal * TAX_RAT;
		var item_total = calc_item_total();
		invoice.discount = (1 - invoice.subtotal / item_total) * 100;
		
	    }

	    function calc_item_total(){
		
		var item_total = 0;
		for (i in contents){
		    if (contents[i] != undefined) {
			item_total += parseFloat(contents[i].quantity)
			    * parseFloat(contents[i].unit_price)
			    * (1 - parseFloat(contents[i].discount) / 100);	  
		    } 
		}
		return item_total;

	    }

	    function append_item(i){
		var html = invo_item_html(i);
		dom_inv_items.append(html);
		$('#item' + i).fadeIn("normal"); 
		
		$('#prod' + i).autocomplete({source: availablePrefix});

		$('#prod' + i)
		    .keyup(function() {
			contents[i].product = $(this).val();
		    })
		    .bind('paste cut', function() {
			setTimeout(function() { $('#prod' + i).keyup(); }, 300);				      
		    });	
		
		$('#qty' + i)
		    .keyup(function() {
			contents[i].quantity = $(this).val();
			calc();
			display();
		    })
		    .bind('paste cut', function() {
			setTimeout(function() { $('#qty' + i).keyup(); }, 300);				      
		    });
		
		$('#up' + i)
		    .keyup(function() {
			contents[i].unit_price = $(this).val();
			calc();
			display();
		    })
		    .bind('paste cut', function() {
			setTimeout(function() { $('#up' + i).keyup(); }, 300);				      
		    });
		
		
		$('#dc' + i)
		    .keyup(function() {
			contents[i].discount = $(this).val();
			calc();
			display();
		    })
		    .bind('paste cut', function() {
			setTimeout(function() { $('#dc' + i).keyup(); }, 300);				      
		    });
		
		$('.item_del', '#item' + i)
		    .click(function () {
			var obj = $(this.parentNode);
			obj.fadeOut("slow", function () {
			    obj.remove();
			    delete contents[i]; // set i -> undefined
			    calc();
			    display();
			});			     
		    });    
	    }
	    

	    
	    
	    
	    

	});//end doc ready


    });// end getjson

function invo_item_html(id){

    var rt = '<li class="ui-helper-clearfix bgc" id="item' + id + '" style="display:none;">'
	+ '<span class="ui-icon ui-icon-arrowthick-2-n-s pt flt" style="margin:2px 5px 0 0;" title="Drag to sort"></span>'
	+ '<input type="text" name="2" value="" id="prod' + id + '" class="ui-corner-all prod"/>'
	+ '<input type="text" name="2" value="1" id="qty' + id + '" maxlength="5" class="ui-corner-all qty"/>'
	+ '<input type="text" name="2" value="0" id="up' + id + '"  class="ui-corner-all up"/>'
	+ '<input type="text" name="2" value="0" id="dc' + id + '" maxlength="5" class="ui-corner-all dc"/>'
	+ '<span class="ui-icon ui-icon-closethick pt flt item_del" style="margin:2px 0 0 20px;" title="Delete" id="del' + id + '"></span></li>';

    return rt;
}