var dp_price_pt = new Object();
var dp_price_lf = new Object();
var dp_price_lm = new Object();
var dp_price_bd = new Object();
var dp_price_bc = new Object();

const TAX_RAT = 0.15;
const CONTACT_INFO = "[ contact manager ]";
const DP_PT_LEVELS = new Array(10,20,30,50,80,100,300,500,800,1000,1500,2000,4000,6000,8000,10000);
const DP_LF_LEVELS = new Array(5,10,20,30,50,100);
const DP_LM_LEVELS = new Array(5,10,20,30,50,100);
const DP_BD_LEVELS = new Array(5,10,20,30,50,100,200,300,500);

function calc_dp_price(lvl, price, qty){
    var fee_dp, fee_dp_base, fee_dp_upper, u_p_oddment, qty_oddments;
    fee_dp = fee_dp_base = fee_dp_upper = u_p_oddment = qty_oddments = 0;
    if(lvl.curr == lvl.next) {
      	fee_dp = price.curr * qty;
    } else {
	fee_dp_base = price.curr * lvl.curr;
	fee_dp_upper = price.next * lvl.next;
	
	u_p_oddment = (fee_dp_upper - fee_dp_base) / (lvl.next - lvl.curr);
	qty_oddments = qty - lvl.curr;

	fee_dp = fee_dp_base + qty_oddments * u_p_oddment;
    }
    return fee_dp;
}

function calc_dp_lvl(LEVELS, qty) {
    var lvl_curr, lvl_next;
    lvl_curr = lvl_next = LEVELS[0];
    for (level in LEVELS)
	{
	    lvl_next = LEVELS[level];
	    if (qty < lvl_next)
		break;
	    else
		lvl_curr = lvl_next;
	}
    return {curr:lvl_curr,next:lvl_next};
}

function calc_dp_price_pt(pt_opt){

    var qty, size, fee_ser, item_type, qty_a4, lvl, u_p_curr, u_p_next, 
	fee_pt, fee_cut, tot_ex_gst, tot_in_gst;

    qty = isNaN(qty = parseInt(pt_opt.qty)) ? 1 : qty;
    fee_ser = isNaN(fee_ser = parseFloat(pt_opt.ser)) ? 0 : fee_ser;
    size = pt_opt.size;
    item_type = pt_opt.type + '_a4_' + pt_opt.weight;
    
    switch (size)
	{
	case 'a4':
	    qty_a4 = parseInt(qty); 
	    fee_cut = 0;
	    break;
	case 'sra3':
	case 'a3':
	    // qty_a4 = (pt_opt.type == 'blk_sgl') ? parseInt(qty * 2.5) : parseInt(qty * 2); 
	    // further discuss needed, weight goes upto 256g price slg > dbl
	    qty_a4 = (pt_opt.type == 'blk_sgl' && pt_opt.weight != 'label' && pt_opt.weight != '256g' && pt_opt.weight != '300g') 
	    	? parseInt(qty * 2.7) : parseInt(qty * 2); 
	    fee_cut = 0;
	    break;
	case 'a5':
	    qty = (qty < 2) ? 2 : qty;
	    qty_a4 = Math.ceil(qty / 2);
	    fee_cut = ((1 + qty * 0.04) > 5) ? 5 : (1 + qty * 0.04);
	    break;
	case 'dle':
	    qty_a4 = Math.ceil(qty / 3); 
	    fee_cut = ((1 + qty * 0.04) > 5) ? 5 : (1 + qty * 0.04);
	    break;
	case 'a6':
	    qty_a4 = Math.ceil(qty / 4);
	    fee_cut = ((1 + qty * 0.04) > 5) ? 5 : (1 + qty * 0.04);
	    break;
	}
    
    qty_a4 = (isNaN(qty_a4)||!qty_a4) ? 1 : qty_a4;
    
    lvl = calc_dp_lvl(DP_PT_LEVELS, qty_a4);

    u_p_curr = u_p_next = 0;
    u_p_curr = parseFloat(dp_price_pt[item_type]['qty'+lvl.curr]);
    u_p_next = parseFloat(dp_price_pt[item_type]['qty'+lvl.next]);
 
    fee_pt = calc_dp_price(lvl,{curr:u_p_curr,next:u_p_next},qty_a4);
	
    tot_ex_gst = tot_in_gst = 0;

    tot_ex_gst = fee_pt + fee_cut + fee_ser;
    tot_in_gst = tot_ex_gst * (1 + TAX_RAT);
    $('#pt_total').val(tot_ex_gst.toFixed(2));
    $('#pt_unit').val((tot_ex_gst/qty).toFixed(4));
    $('#pt_cut_fee').val(fee_cut.toFixed(2));
    $('#pt_tot_gst').val(tot_in_gst.toFixed(2));
    $('#pt_unit_gst').val((tot_in_gst/qty).toFixed(4));

    //if (tot_ex_gst > 800) 	$('#pt_total, #pt_unit, #pt_tot_gst, #pt_unit_gst').val(CONTACT_INFO);
   
}


function calc_dp_price_lf(lf_opt){
    var qty, fee_ser, item_type, lvl, u_p_curr, u_p_next,
	fee_lf, tot_ex_gst, tot_in_gst;

    qty = isNaN(qty = parseInt(lf_opt.qty)) ? 1 : qty;
    fee_ser = isNaN(fee_ser = parseFloat(lf_opt.ser)) ? 0 : fee_ser;
    item_type = lf_opt.type + '_' + lf_opt.printing + '_' + lf_opt.size;   
    
  // alert(item_type);
    lvl = calc_dp_lvl(DP_LF_LEVELS, qty);
    
    u_p_curr = u_p_next = 0;
    u_p_curr = parseFloat(dp_price_lf[item_type]['qty'+lvl.curr]);
    u_p_next = parseFloat(dp_price_lf[item_type]['qty'+lvl.next]);
     
    fee_lf = calc_dp_price(lvl, {curr:u_p_curr,next:u_p_next}, qty);
    
    tot_ex_gst = tot_in_gst = 0;
    tot_ex_gst = fee_lf + fee_ser;
    tot_in_gst = tot_ex_gst * (1 + TAX_RAT);

    $('#lf_total').val(tot_ex_gst.toFixed(2));
    $('#lf_unit').val((tot_ex_gst/qty).toFixed(4));
    $('#lf_tot_gst').val(tot_in_gst.toFixed(2));
    $('#lf_unit_gst').val((tot_in_gst/qty).toFixed(4));
   
    if (tot_ex_gst > 800) 
	$('#lf_total, #lf_unit, #lf_tot_gst, #lf_unit_gst').val(CONTACT_INFO);
}


function calc_dp_price_bc(bc_opt){
    var qty, names, fee_ser, item_type, fee_bc, 
	fee_lam, fee_names, tot_ex_gst, tot_in_gst;
    qty = isNaN(qty = parseInt(bc_opt.qty)) ? 1 : qty;
    names = isNaN(names = parseInt(bc_opt.names)) ? 1 : names;
    fee_ser = isNaN(fee_ser = parseFloat(bc_opt.ser)) ? 0 : fee_ser;
    item_type = bc_opt.type;
    
    fee_bc = fee_lam = fee_names = 0;  
    fee_bc = parseFloat(dp_price_bc[item_type]['qty'+qty]);   
    fee_lam = (bc_opt.lam == 'lam') ? parseFloat(dp_price_bc.lam['qty'+qty]) : 0;
    fee_names = (names - 1) * 10;
 
    tot_ex_gst = tot_in_gst = 0;
    tot_ex_gst = fee_bc + fee_lam + fee_names + fee_ser;
    tot_in_gst = tot_ex_gst * (1 + TAX_RAT);
 
    $('#bc_total').val(tot_ex_gst.toFixed(2));
    $('#bc_tot_gst').val(tot_in_gst.toFixed(2));
    $('#bc_name_fee').val(fee_names.toFixed(4));
}

function calc_dp_price_bd(bd_opt){ 
    var qty, size, fee_ser, item_type, lvl, u_p_curr, u_p_next,
	fee_bd, tot_ex_gst, tot_in_gst;

    qty = isNaN(qty = parseInt(bd_opt.qty)) ? 1 : qty;
    fee_ser = isNaN(fee_ser = parseFloat(bd_opt.ser)) ? 0 : fee_ser;
    item_type = bd_opt.type;
    size = bd_opt.size;
    
    lvl = calc_dp_lvl(DP_BD_LEVELS, qty);
   
    u_p_curr = u_p_next = 0;
    u_p_curr = parseFloat(dp_price_bd[item_type]['qty'+lvl.curr]);
    u_p_next = parseFloat(dp_price_bd[item_type]['qty'+lvl.next]);
    
    fee_bd = calc_dp_price(lvl, {curr:u_p_curr,next:u_p_next}, qty);
    
    switch (size)
	{
	case 'a4':	    
	    break;
	case 'a3':
	    fee_bd = fee_bd * ((qty > 20) ? 1.6 : 1.8);
	    break;
	}
    
    tot_ex_gst = tot_in_gst = 0;
    tot_ex_gst = fee_bd + fee_ser;
    tot_in_gst = tot_ex_gst * (1 + TAX_RAT);

    $('#bd_total').val(tot_ex_gst.toFixed(2));
    $('#bd_unit').val((tot_ex_gst/qty).toFixed(4));
    $('#bd_tot_gst').val(tot_in_gst.toFixed(2));
    $('#bd_unit_gst').val((tot_in_gst/qty).toFixed(4));
   
    if (tot_ex_gst > 200) 
	$('#bd_total, #bd_unit, #bd_tot_gst, #bd_unit_gst').val(CONTACT_INFO);
}

function calc_dp_price_lm(lm_opt){ 
    var qty, size, fee_ser, item_type, lvl, u_p_curr, u_p_next,
	fee_lm, tot_ex_gst, tot_in_gst;

    qty = isNaN(qty = parseInt(lm_opt.qty)) ? 1 : qty;
    fee_ser = isNaN(fee_ser = parseFloat(lm_opt.ser)) ? 0 : fee_ser;
    item_type = lm_opt.type;
    
    lvl = calc_dp_lvl(DP_LM_LEVELS, qty);
   
    u_p_curr = u_p_next = 0;
    u_p_curr = parseFloat(dp_price_lm[item_type]['qty'+lvl.curr]);
    u_p_next = parseFloat(dp_price_lm[item_type]['qty'+lvl.next]);
    
    fee_lm = calc_dp_price(lvl, {curr:u_p_curr,next:u_p_next}, qty);
    
    tot_ex_gst = tot_in_gst = 0;
    tot_ex_gst = fee_lm + fee_ser;
    tot_in_gst = tot_ex_gst * (1 + TAX_RAT);

    $('#lm_total').val(tot_ex_gst.toFixed(2));
    $('#lm_unit').val((tot_ex_gst/qty).toFixed(4));
    $('#lm_tot_gst').val(tot_in_gst.toFixed(2));
    $('#lm_unit_gst').val((tot_in_gst/qty).toFixed(4));
   
    if (tot_ex_gst > 200) 
	$('#lm_total, #lm_unit, #lm_tot_gst, #lm_unit_gst').val(CONTACT_INFO);
}

$.getJSON( price_url + '/dp_price_pt', function(data) { dp_price_pt = data; }); 
$.getJSON( price_url + '/dp_price_lf', function(data) { dp_price_lf = data; }); 
$.getJSON( price_url + '/dp_price_bc', function(data) { dp_price_bc = data; }); 
$.getJSON( price_url + '/dp_price_bd', function(data) { dp_price_bd = data; }); 
$.getJSON( price_url + '/dp_price_lm', function(data) { dp_price_lm = data; }); 

$(function() {	
	
	$('#dp_price_pt, #dp_price_lf, #dp_price_bc, #dp_price_bd, #dp_price_lm')
	    .mouseenter(function() { 
	     	$('.pt_target').change();		
 	       	$('.bc_target').change();
 	        $('.lf_target').change();
 	       	$('.lm_target').change();
 	       	$('.bd_target').change();
	    });
	    
	    
	$('.pt_target').change(function() {
		var pt_opt = new Object();
		pt_opt.type = $('#pt_type').val();
		pt_opt.size = $('#pt_size').val();
		pt_opt.weight = $('#pt_weight').val();
		pt_opt.qty = $('#pt_qty').val();
		pt_opt.ser = $('#pt_ser').val();
		calc_dp_price_pt(pt_opt);
	    });
	$('.pt_target').keyup(function() {
	       	$('.pt_target').change();
	    });
	$('#pt_button_calc').click(function() {
		$('.pt_target').change();
	    });
	
	$('.bc_target').change(function() {
		var bc_opt = new Object();
		bc_opt.type = $('#bc_type').val();
		bc_opt.lam = $('#bc_lam').val();
		bc_opt.names = $('#bc_names').val();
		bc_opt.qty = $('#bc_qty').val();
		bc_opt.ser = $('#bc_ser').val();
		calc_dp_price_bc(bc_opt);
	    });
	$('.bc_target').keyup(function() {
	       	$('.bc_target').change();
	    });
	$('#bc_button_calc').click(function() {
		$('.bc_target').change();
	    });
	
	$('.lf_target').change(function() {
		var lf_opt = new Object();
		lf_opt.type = $('#lf_type').val();
		lf_opt.size = $('#lf_size').val();
		lf_opt.qty = $('#lf_qty').val();
		lf_opt.ser = $('#lf_ser').val();
		lf_opt.printing = $('#lf_printing').val(); 
		    // check logic
    if (lf_opt.type == 'blk' && lf_opt.printing == 'laser_120gsm')
    	$('#dp_price_lf_msg').html('no black prints on 120gsm');
    if (lf_opt.type == 'blk' && lf_opt.printing == 'ink')
    	$('#dp_price_lf_msg').html('no black prints on Inkjet (treated as color)');
     
		calc_dp_price_lf(lf_opt);
	    });
	$('.lf_target').keyup(function() {
		$('.lf_target').change();
	    });
	$('#lf_button_calc').click(function() {
		$('.lf_target').change();
	    });

	$('.bd_target').change(function() {
		var bd_opt = new Object();
		bd_opt.type = $('#bd_type').val();
		bd_opt.size = $('#bd_size').val();
		bd_opt.qty = $('#bd_qty').val();
		bd_opt.ser = $('#bd_ser').val();
		calc_dp_price_bd(bd_opt);
	    });
	$('.bd_target').keyup(function() {
		$('.bd_target').change();
	    });
	$('#bd_button_calc').click(function() {
		$('.bd_target').change();
	    });

	$('.lm_target').change(function() {
		var lm_opt = new Object();
		lm_opt.type = $('#lm_type').val();
		lm_opt.qty = $('#lm_qty').val();
		lm_opt.ser = $('#lm_ser').val();
		calc_dp_price_lm(lm_opt);
	    });
	$('.lm_target').keyup(function() {
		$('.lm_target').change();
	    });
	$('#lm_button_calc').click(function() {
		$('.lm_target').change();
	    });
    });