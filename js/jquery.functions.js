
$(document).ready(function(){
/** start doc ready **/

$(".buttonset").buttonset(); 
$(".button").button(); 

/** end doc ready **/
});



/*********************************************/
/* 2014 above */
/*********************************************/
$(document)
    .ready(function() 
	   {
	       
	       
	       /** jquery button **/
	       
	       $("button, input:submit, input:button, a", ".ui_button").button();
	       

	       $('#dialog_link, ul#icons li').hover(
		   function() { $(this).addClass('ui-state-hover'); }, 
		   function() { $(this).removeClass('ui-state-hover'); }
	       );


	       /** jquery button **/
	       $(".check").button();
	       $(".ui_radio, .ui_buttonset").buttonset(); 
	       



	       /** jquery icon hover **/
	       $('div.icons, ul.icons li').hover(
		   function() { $(this).addClass('ui-state-hover'); }, 
		   function() { $(this).removeClass('ui-state-hover'); }
	       );




	       /** START jquery input blur **/
	       
	       $('.blur')
		   .focus(function() {
			      if (this.value == 'Customer..' 
				  || this.value == 'Search..'
				  || this.value == 'Vendor..' || this.value == 'Name..'){ 
				  this.value = '';
			      } else {
				  this.select();
			      }
			  });
	       
	       $('.blur[name="customer"]')
		   .blur(function() {
			     if ($.trim(this.value) == ''){
				 this.value = 'Customer..';
			     }
			 });

	       $('.blur[name="vendor"]')
		   .blur(function() {
			     if ($.trim(this.value) == ''){
				 this.value = 'Vendor..';
			     }
			 });

	       $('.blur[name="search"]')
		   .blur(function() {
			     if ($.trim(this.value) == ''){
				 this.value = 'Search..';
			     }
			 });
	       $('.blur[name="name"]')
		   .blur(function() {
			     if ($.trim(this.value) == ''){
				 this.value = 'Name..';
			     }
			 });
	       /** END jquery input blur **/
	       
	       


	       /* end doc ready */
	   });
function openView(url, name) {
    window.open(url, name, "height=740,width=730,location=no, status=no, resizable=no, scrollbars=yes, toolbar=no");
    return true;
}

function mysqlParseDate(string) {
    var date = new Date();
    var parts = String(string).split(/[- :]/);

    date.setFullYear(parts[0]);
    date.setMonth(parts[1] - 1);
    date.setDate(parts[2]);
    date.setHours(parts[3]);
    date.setMinutes(parts[4]);
    date.setSeconds(parts[5]);
    date.setMilliseconds(0);

    return date;
}

var branch_names = ["3A Copy & Design (Epsom)", 
		    "3A Copy & Design (City)",
		    "3A Copy & Design (Newmarket)",
		    "3A Copy & Design (Onehunga)",
		    "3A Copy & Design (Albany)",
		    "3A Copy & Design (Takapuna)",
		    "3A Copy & Design (Penrose)",
		    "3A Copy & Design (Manukau)",
		    "3A Production Centre",
		    "3A Copy & Design",
		    "3A Web Solution",
		    "3A Energy Signs"];
		    
var cost_types = ["Wages", 
		    "Lunch and commission", 
		    "PAYE", 
		    "GST (pre)", 
		    "Inventory(Paper ect.)", 
		    "Machine rentals", 
		    "Metering ", 
		    "Out Source", 
		    "Rent and Rate (house)", 
		    "Electricity and water", 
		    "Advertisement", 
		    "Phone and Internet", 
		    "Car gas , WOF, ect,", 
		    "Machine maintenance", 
		    "Account service fee (pre)", 
		    "Daily others", 
		    "special 01-new machine", 
		    "special 02", 
		    "special 03"];
		    
var print_size = ["A0", "A1", "A2", "A3", "A4", "SRA1", "SRA2", "SRA3"];
		    
var finish_size = ["A0", "A1", "A2", "A3", "A4", "A5", "A6", "A7", "SRA3", "DL", "BC 90x55", "BC 90x50"];
			
var printers = ["DocuColor 8000",
		"DocuColor 5000",
		"Color 800",
		"DocuColor 250",
		"Xerox 4100",
		"FX DC 6000",
		"FX DC 750",
		"FX DC 1100",
		"DocuWide 3030",
		"Epson 7800"];

var printing_branches = ["epsom",
		"city",
		"newmarket",
		"onehunga",
		"albany"];
		
				