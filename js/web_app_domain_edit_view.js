$.getJSON(
	  json_url, 
	  function(data) {
	      var json_obj = data;
	      var domain = json_obj.domain;

	      domain.due_date = date("d/m/Y", strtotime(domain.due_date));
	      
	      $(function() {
		      
		      
		      $('#close')
			  .click(function () { 
				  window.close();	     
			      }); 
	      
		      $('#save')
			  .click(function () { 


				  $('.cst')
				      .each(function(){
					      domain[this.id] = $(this).val();
					  });

				  var post_json = new Object();
			     
				  post_json.domain = domain;

				  $.ajax({
					  type: 'POST',
					      url: post_url,
					      data: {'json' : JSON.stringify(post_json)},
					      success:  function(data) {
					      top.window.resizeTo(720,300);
					      $('#save').hide();
					      $('#inner-text')
						  .hide()
						  .html(data)
						  .fadeIn(200, function () {
							  setTimeout('window.close()'
								     , 500);
						      });
					      window.opener.location.reload(true);
					  }
				      });


			      });
	      
              
		      if(read_only){
			  $(".cst")
			      .each(function(){
				      $(this).val(domain[this.id]);
				      $(this).attr('readonly', 'readonly');
				      $(this).css('background','#f1f1f1');
				      $('tr').removeClass('bgc');
				      $('#save').hide();
				  });
		  
		      } else {
			  $(".cst")
			      .each(function(){
				      $(this).val(domain[this.id]); 
				  });

		      }
	      
	      
	      
	      


		  });//end doc ready

	
	  });// end getjson
