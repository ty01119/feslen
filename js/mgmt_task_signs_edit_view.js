$.getJSON(
    json_url, 
    function(data) {
	var json_obj = data;	      
	var transaction = json_obj.data;

//console.log(transaction);
 	     
	$(function() {
	      	     
	      $('#task_id').val(transaction.id);
	      $('#invo_id').val(transaction.invo_id);	      
	      $('#date').val(transaction.date);	      
	      $('#date_due').val(transaction.date_due);        
	      $('#cust_name').val(transaction.cust_name);	            
	      $('#staff_pre_press').val(transaction.staff_pre_press);	            
	      $('#file_name').val(transaction.file_name);	            
	     
	      $('[name="urgent"]').each(function() {
					       if (transaction.urgent) 
						   $(this).prop("checked", true);
					   });
					   
	      $('[name="bleed_btn"]').each(function() {
					       if (transaction.bleed_btn.indexOf($(this).val()) > -1) 
						   $(this).prop("checked", true);
					   });
	      $('#bleed_notes').val(transaction.bleed_notes);
	      
	      $('[name="sample_btn"]').each(function() {
					       if (transaction.sample_btn.indexOf($(this).val()) > -1) 
						   $(this).prop("checked", true);
					   });
	      $('#sample_notes').val(transaction.sample_notes);
	      
	      $('[name="image_btn"]').each(function() {
					       if (transaction.image_btn.indexOf($(this).val()) > -1) 
						   $(this).prop("checked", true);
					   });
	      $('#image_notes').val(transaction.image_notes);
	      
	      $('[name="text_btn"]').each(function() {
					       if (transaction.text_btn.indexOf($(this).val()) > -1) 
						   $(this).prop("checked", true);
					   });
	      $('#text_notes').val(transaction.text_notes);
	      
	      $('[name="printing_btn"]').each(function() {
					       if (transaction.printing_btn.indexOf($(this).val()) > -1) 
						   $(this).prop("checked", true);
					   });
	      $('#printing_notes').val(transaction.printing_notes);
	      
	      $('#printer').val(transaction.printer);
	      $('#printer_notes').val(transaction.printer_notes);
	      
	      $('[name="paper_btn"]').each(function() {
					       if (transaction.paper_btn.indexOf($(this).val()) > -1) 
						   $(this).prop("checked", true);
					   });
	      $('#paper_notes').val(transaction.paper_notes);

	      $('#print_qty').val(transaction.print_qty);
	      $('#print_size').val(transaction.print_size);
	      $('#print_notes').val(transaction.print_notes);

	      $('#finish_qty').val(transaction.finish_qty);
	      $('#finish_size').val(transaction.finish_size);
	      $('#finish_notes').val(transaction.finish_notes);
	      
	      $('[name="finishing_btn"]').each(function() {
					       if (transaction.finishing_btn.indexOf($(this).val()) > -1) 
						   $(this).prop("checked", true);
					   });
	      $('#finishing_notes').val(transaction.finishing_notes);
	      
	      $('[name="delivery_btn"]').each(function() {
					       if (transaction.delivery_btn.indexOf($(this).val()) > -1) 
						   $(this).prop("checked", true);
					   });
	      $('#delivery_notes').val(transaction.delivery_notes);
	      
	      $('[name="qa_btn"]').each(function() {
					       if (transaction.qa_btn.indexOf($(this).val()) > -1) 
						   $(this).prop("checked", true);
					   });
	      $('#qa_notes').val(transaction.qa_notes);
	      $('#qa').val(transaction.qa);

	      $('#staff_production').val(transaction.staff_production);
	      $('#staff_production_notes').val(transaction.staff_production_notes);
	      
	      $('[name="status_btn"]').each(function() {
					       if (transaction.status_btn.indexOf($(this).val()) > -1) 
						   $(this).prop("checked", true);
					   });
	      $('#status_notes').val(transaction.status_notes);

	    $('.ui_buttonset').buttonset( "refresh" );


	      $('#close')
		  .click(function () { 
			     window.close();	     
			 }); 

	      
	      $('#save')
		  .click(function () {
			     transaction.invo_id = $('#invo_id').val();
			     transaction.date_due = $('#date_due').val();
			     transaction.cust_name = $('#cust_name').val();
			     transaction.staff_pre_press = $('#staff_pre_press').val();
			     transaction.file_name = $('#file_name').val();

			     transaction.urgent = 0;
			     $('[name="urgent"]:checked').each(function() {
								      transaction.urgent = $(this).val();   
								  });
								  
			     transaction.bleed_btn = '';
			     $('[name="bleed_btn"]:checked').each(function() {
								      transaction.bleed_btn += $(this).val() + ';';   
								  });
			     transaction.bleed_notes = $('#bleed_notes').val();

			     transaction.sample_btn = '';
			     $('[name="sample_btn"]:checked').each(function() {
								      transaction.sample_btn += $(this).val() + ';';   
								  });
			     transaction.sample_notes = $('#sample_notes').val();

			     transaction.image_btn = '';
			     $('[name="image_btn"]:checked').each(function() {
								      transaction.image_btn += $(this).val() + ';';   
								  });
			     transaction.image_notes = $('#image_notes').val();

			     transaction.text_btn = '';
			     $('[name="text_btn"]:checked').each(function() {
								      transaction.text_btn += $(this).val() + ';';   
								  });
			     transaction.text_notes = $('#text_notes').val();

			     transaction.printing_btn = '';
			     $('[name="printing_btn"]:checked').each(function() {
								      transaction.printing_btn += $(this).val() + ';';   
								  });
			     transaction.printing_notes = $('#printing_notes').val();

			     transaction.printer = $('#printer').val();
			     transaction.printer_notes = $('#printer_notes').val();

			     transaction.paper_btn = '';
			     $('[name="paper_btn"]:checked').each(function() {
								      transaction.paper_btn += $(this).val() + ';';   
								  });
			     transaction.paper_notes = $('#paper_notes').val();

			     transaction.print_qty = $('#print_qty').val();
			     transaction.print_size = $('#print_size').val();
			     transaction.print_notes = $('#print_notes').val();

			     transaction.finish_qty = $('#finish_qty').val();
			     transaction.finish_size = $('#finish_size').val();
			     transaction.finish_notes = $('#finish_notes').val();

			     transaction.finishing_btn = '';
			     $('[name="finishing_btn"]:checked').each(function() {
								      transaction.finishing_btn += $(this).val() + ';';   
								  });
			     transaction.finishing_notes = $('#finishing_notes').val();

			     transaction.delivery_btn = '';
			     $('[name="delivery_btn"]:checked').each(function() {
								      transaction.delivery_btn += $(this).val() + ';';   
								  });
			     transaction.delivery_notes = $('#delivery_notes').val();

			     transaction.staff_production = $('#staff_production').val();
			     transaction.staff_production_notes = $('#staff_production_notes').val();

 			     transaction.qa_btn = '';
			     $('[name="qa_btn"]:checked').each(function() {
								      transaction.qa = $(this).val();   
								  });
			     transaction.qa_notes = $('#qa_notes').val();

			     transaction.status_btn = '';
			     $('[name="status_btn"]:checked').each(function() {
								      transaction.status_btn += $(this).val();   
								  });
			     transaction.status_notes = $('#status_notes').val();

			   			   
			     var post_json = new Object();
			     post_json.transaction = transaction;
			     
 
			    var request =  $.ajax({
					type: 'POST',
					url: post_url,
					data: {'json' : JSON.stringify(post_json)},
					success:  function(data) {					 
					    // $('#msgbox').fadeIn();$('#msg').html(data);
					    var json = $.parseJSON(data);	
					    if (json.close)
					    {		 		
						top.window.resizeTo(720,300);
						$('#save').hide();	
						$('#msgbox').fadeIn();
						$('#msg').html(json.msg);
						
						$('#inner-wrapper')
						    .fadeOut(111, 
							     function () {
								 setTimeout(
								 'window.close()', 500);
							     });
						window.opener.location.reload(true); 
					    }
					    else
					    { 
						$('#msgbox').fadeIn();
						$('#msg').html(json.msg);
					    }
					}
				    });
			     
			     request.fail(function(jqXHR, textStatus) {
					      console.log(jqXHR.responseText);
					  });
			 });

	      
	      
	       


	  });//end doc ready


    });// end getjson