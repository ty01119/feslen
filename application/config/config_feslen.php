<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['cf_feslen']['name'] = 'Feslen BMS';
$config['cf_feslen']['co_name'] = '<i>3A Holdings</i>';
$config['cf_feslen']['build'] = 'build 20140121';
$config['cf_feslen']['office_ips'] =  array('203.173.163.245', 
	'203.173.163.246', 
	'203.173.163.247', 
	'203.173.163.248', 
	'203.118.130.85',
	'203.118.134.156');


$config['site']['email'] = 'ke@3a.co.nz';
$config['site']['name'] = 'Feslen BMS';
$config['site']['sender'] = '3A web solution | Feslen BMS';