<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

/**
 * Form Declaration
 *
 * Creates the opening portion of the form.
 *
 * @access	public
 * @param	string	the URI segments of the form destination
 * @param	array	a key/value pair of attributes
 * @param	array	a key/value pair hidden data
 * @return	string
 */	
if ( ! function_exists('is_logged_in'))
{
	function is_logged_in()
	{
		$CI =& get_instance();

		$is_logged_in = $CI->session->userdata('is_logged_in');

		if(!isset($is_logged_in) || !$is_logged_in )
		{
			redirect('login');	
		}
		
		$cf_feslen = $CI->config->item('cf_feslen');
    		$office_ips = $cf_feslen['office_ips'];
    		$user_ip = $CI->input->ip_address();
    		
		if(!in_array($user_ip, $office_ips) && $CI->session->userdata('remote_login') != 'yes')
		{			 
	    		$CI->session->unset_userdata('is_logged_in');
		        $msg = 'You do not have the authority to use this App outside of 3A offices. o_O';
			$CI->session->set_flashdata('msg', $msg);			
			redirect('login');	
		}			 
		
		// check if already checked in		 
		if (!$CI->session->userdata('check_in') && $CI->uri->segment(1, 0) != 'user')
		{
		        $msg = 'Please check in to use this App. ◕‿◕';
			$CI->session->set_flashdata('msg', $msg);
			if ($CI->uri->segment(1, 0) != 'panel' && $CI->session->userdata('login_id') != '000')	
				redirect('panel');			
		}
		
				
	}

}


if ( ! function_exists('logged_in'))
{
	function logged_in()
	{
		$CI =& get_instance();

		$is_logged_in = $CI->session->userdata('is_logged_in');

		if(!isset($is_logged_in) || !$is_logged_in )
		  {
		    return FALSE;
		  }	
		else 
		  {
		    return TRUE;
		  }	
	}
}


if ( ! function_exists('keep_logged_in'))
{
	function keep_logged_in()
	{
		$CI =& get_instance();

		$is_logged_in = $CI->session->userdata('is_logged_in');

    		$cf_feslen = $CI->config->item('cf_feslen');
    		$office_ips = $cf_feslen['office_ips'];
    		$user_ip = $CI->input->ip_address();
    		

		if((!isset($is_logged_in) || !$is_logged_in) 
			&& get_cookie('uid') 
			&& get_cookie('passwd') 
			&& in_array($user_ip, $office_ips))
		{
			$user = $CI->user_model->login_cookie(get_cookie('uid'), get_cookie('passwd'));
			$data = array(
			      'is_logged_in' => true,
			      'check_in' => $CI->user_model->check_attn($user->working_id),
			      'login_id'  => $user->working_id,
			      'authority' => $user->authority,
			      'branch'    => $user->branch,
			      'user_name' => $user->name,
			      'user_position' => $user->position,		      
			      'working_time' => $user->working_time,
			      'cart' => array(),   
			      'cart1' => array()	        
			      );	
			$CI->session->set_userdata($data);

		        $msg = 're-Logged in successfully.';
			$CI->session->set_flashdata('msg', $msg);
			
		}			 	
	}

}

if ( ! function_exists('check_auth'))
{

  function check_auth($auth, $redirect = TRUE){
    $CI =& get_instance();
    
    $pos = strpos($CI->session->userdata('authority'), $auth);
    
    if($pos === false) {

      if ($redirect) {
      	$msg = 'Authorisation rejected.';
      	$CI->session->set_flashdata('msg', $msg);
      	redirect('panel');
      }
      else
      {
      	echo $msg = 'Authorisation rejected.'; die;
      }
    }
    
  }
}

if ( ! function_exists('pass_auth'))
{

  function pass_auth($auth){
    $CI =& get_instance();
    
    $pos = strpos($CI->session->userdata('authority'), $auth);
    
    if($pos === false) {
      return FALSE;
    }
    else {
	return TRUE;
    }
    
  }
}