<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

/**
 * Form Declaration
 *
 * Creates the opening portion of the form.
 *
 * @access	public
 * @param	string	the URI segments of the form destination
 * @param	array	a key/value pair of attributes
 * @param	array	a key/value pair hidden data
 * @return	string
 */	
if ( ! function_exists('get_turnover_rate'))
  {
    function get_turnover_rate()
    {
      $CI =& get_instance();
      if ( $CI->session->userdata('branch') )
	{	      
	  $date['fr'] = strtotime('last sunday');
	  $date['to'] = strtotime('next sunday');

	  $CI->load->model('stat_model');
	  $CI->load->model('branch_model');
	  $branch = $CI->branch_model->load_by_name($CI->session->userdata('branch'));
		
	  $sum = $CI->stat_model->turnover_by_branch($branch->branch, $date);

	  return number_format($sum->total / $branch->turnover * 100, 2, '.', '');
	}
      else 
	{
	  return 0;
	} 
    }
  }
  
if ( ! function_exists('refine_branch_data'))
  {
    function refine_branch_data($branch, $date)
    {      
	$company_change_date = "2012-06-01"; 

	if (strtotime($date) > strtotime($company_change_date)) 
	  { 
	     $branch->bank_acc_num = $branch->bank_acc_num2;
	     $branch->gst_num = $branch->gst_num2;

	  } 	      
	return $branch;
    }
  }

  
if ( ! function_exists('get_reminders'))
  {
    function get_reminders()
    {      
	$CI =& get_instance();      
	$CI->load->model('leads_model');	
	$reminder_leads = $CI->leads_model->get_reminders($CI->session->userdata('user_name'));
	
	$reminder = '';
	if ($reminder_leads) $reminder .= 'Leads to follow ('.$reminder_leads.')';
	
	return $reminder;
    }
  }