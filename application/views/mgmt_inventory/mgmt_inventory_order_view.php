  

<script type="text/javascript" src="<?php echo base_url();?>lib/jquery.form.js"></script> 
<script>
  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#name" )
	.autocomplete({
			minLength: 1,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/product_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
			


		      });




      
    });
</script>
<script type="text/javascript">

      var span1 = '<span style="width:400px;display:inline-block;text-align:left">';
      var span2 = '</span><span style="width:60px;display:inline-block;text-align:right">';
      var span3 = '</span><span style="width:60px;display:inline-block;text-align:right">' ;
      var span4 = '</span><br />';
      var nbsp8 = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';



$(function() {
    $.getJSON( "<?php echo base_url(); ?>mgmt_inventory/cart", function(data) {display_cart(data);}); 
    function display_cart(data){
      var cart_data = data;
      $('#cart').html('');
      var subtotal = 0;
      for (i in cart_data){
	var html =  span1
	  + cart_data[i].product + span2
	  + cart_data[i].quantity + span3
	  + cart_data[i].unit_price + span4;
	
	subtotal += cart_data[i].quantity * cart_data[i].unit_price;
	
	$('#cart').append(html).fadeIn('slow');
		 }
      var html_total =  'Subtotal: ' + (subtotal).toFixed(2) 
	+ nbsp8 + 'GST: ' + (subtotal * 0.15).toFixed(2)  
	+ nbsp8 + 'Total: ' + (subtotal * 1.15).toFixed(2);      
      $('#cart').fadeIn('slow');
      $('#cart-total').html(html_total); 
    }
    

    $('.aForm').ajaxForm({
        beforeSubmit: function(a,f,o) {
        },
        success: function(data) {   
	  var data = $.parseJSON(data);
	  display_cart(data);        
        }
    });


    $('#reset').click(function(){
	$.post("<?php echo base_url(); ?>mgmt_inventory/reset", 
	       function(){  
		 $.getJSON( "<?php echo base_url(); ?>mgmt_inventory/cart", function(data) {display_cart(data);});
	       });	 
      });
    
    $('#hide-cart').click(function(){
	if ($('#cart').is(":visible")){
	  $('#cart').stop(true,true).hide(111, function(){$('#hide-cart').html('[ Show Cart ]');}); 	 
	}
	else {
	  $('#cart').stop(true,true).show(111 , function(){$('#hide-cart').html('[ Hide Cart ]');});  
	   }
      });
    
    $('#cart-total').click(function(){
	$('#hide-cart').click();
      }).dblclick(function(){
          $('#reset').click();
        });
    
    $('.quote-noty')
      .mouseenter(function() {
	  $(this).stop(true,true).fadeTo('normal', 1);
	})
      .mouseleave(function() {
	  $(this).stop(true,true).fadeTo('normal', 0.7);
	});
	

});
</script> 

<style>
.quote-noty {
opacity: 0.7;
display:block;
left:10px; 
   position:fixed; 
   text-align:left; 
   bottom:5px; 
         background:#000000;
         color:#ffffff; 
   padding:15px;
    border-radius: 6px;
     box-shadow: 0 4px 10px #000000;
z-index:999;

}
</style>

<div class="quote-noty">
<div class="font11px" style="width:540px;" id="cart"></div>
<div class="font11px" style="width:540px;margin-top:5px;font-weight:700;">
<a class="white font10px" id="reset" href="javascript:void(0)">[ Empty Cart ]</a>
<a class="white font10px" id="hide-cart"  style="margin-left:50px;" href="javascript:void(0)">[ Hide Cart ]</a>
<span id="cart-total"  style="margin-left:50px;"></span>
</div>
</div>

<?php echo form_open('mgmt_inventory/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >



<tr><td align="left" class="ui_button font10px" colspan="4">
<?php $this->load->view('mgmt_inventory/menu_bar.php'); ?>
</td></tr>
<!--
<tr><td align="left" class="ui_button font10px" style="width:200px">
<?php 

echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);

?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php

echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);

?>
</td><td align="left" class="ui_button font10px">

</td><td align="left" class="ui_button font10px"></td></tr>
-->

<tr><td align="left" class="font10px">
<?php
echo form_input(
		array(
		      'name'        => 'name',
		      'id'        => 'name',
		      'value'	  => $name,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		);

?>
</td><td align="left" class="font10px">

</td><td align="left" class="ui_button font10px">
<?php 
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => $search,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		 );
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td></tr>
</table>
<?php echo form_close(); ?>

<div class="ui_button font10px" id="cates">
 <?php 
$category = isset($category) ? $category : '';

$cates = $cate_tree;
foreach ($cates as $cate) 
  {
    if (is_array($cate)) 
      {
	$parent = array_shift($cate);
	echo '<span class="link">';
	echo ($parent->url_title == $category) ? '<button disabled="disabled">'.$parent->name.'</button>' : '<a href="'.site_url('mgmt_inventory/cate/'.$parent->url_title).'" >'.$parent->name.'</a>';  
	echo '</span>';
	foreach ($cate as $sub)
	  {	  
	echo '<span class="link">';
	    echo ($sub->url_title == $category) ? '<button disabled="disabled">'.$sub->name.'</button>' : '<a href="'.site_url('mgmt_inventory/cate/'.$sub->url_title).'" >'.$sub->name.'</a>';	  
	echo '</span>';
	  }
      }
    else
      {		echo '<span class="link">';
	echo ($cate->url_title == $category) ? '<button disabled="disabled">'.$cate->name.'</button>' : '<a href="'.site_url('mgmt_inventory/cate/'.$cate->url_title).'" >'.$cate->name.'</a>';
	echo '</span>';
      }
  }
?>
</div>

 
		
<table border="0" cellpadding="5" cellspacing="0" style="margin:30px 0; min-width:900px; float: left; width:100%;" >

<tr>
<th colspan="8" align="left"><?php echo $table_title;?></th>
</tr>

 

<tr class="bgc">
<td colspan="8" align="right" class="ui_button font9px">
<?php 
echo $this->pagination->create_links(); 
$attr = ($per_page == 25) ? array() : array('class' => 'white');
$attr['style'] = 'margin:0 10px;';
echo anchor("mgmt_inventory/filter/per_page/$total", 'Display All', $attr);
 
?></td>
</tr>

<tr class="bgc">
<th align="left" valign="middle" width="60">
<?php echo anchor("mgmt_inventory/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="60">
<?php 
/*
echo anchor("mgmt_inventory/orderby/date", 'Date', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'date'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
*/
?>
</th>
<th align="left" valign="middle" >
<?php echo anchor("mgmt_inventory/orderby/name", 'Product Name', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'name'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle"  width="70">
<?php echo anchor("mgmt_inventory/orderby/qty", 'Quantity', array('class' => 'black frt'));
if (isset($orderby) && $orderby['order'] == 'qty'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s frt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n frt"></span>';
endif;
?>
</th>
<th align="right" valign="middle" width="70">
<?php echo anchor("mgmt_inventory/orderby/price", 'U/P', array('class' => 'black frt'));
if (isset($orderby) && $orderby['order'] == 'price'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s frt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n frt"></span>';
endif;
?>
</th>
<th align="right" valign="middle" >

</th>

<th align="right" valign="middle" width="20"></th>

<th align="center" valign="middle" width="140">Operations</th>
</tr>
<tr><td colspan="8" align="left">
<?php 
 
foreach($query->result() as $row):
 
?> 
<div id="<?php echo $row->id;?>" style="width:240px; height:100px;padding:10px;float:left;margin:10px;border:1px solid #909090;" class="bgc item" >
 
  <div style="width:200px; height:100px;float:left;">
<b><?php  echo $row->name;?></b><br />
<?php 
 echo $row->id.br(); 
echo  'Price: $'.$row->price.br();
echo  'Stock: '.$row->qty.br();
echo  'Detail: '.$row->detail.br();
?>
 </div>
  <div style="width:40px; height:100px;float:right;">
  <?php echo form_open_multipart('mgmt_inventory/cart', 
  array('class' => 'aForm', 'id' => 'uf'.$row->id), 
  array('id' => $row->id, 'name' => $row->name, 'price' => $row->price)
  );
?>
  <?php 
  echo form_input(
		  array(
			'name'=>'qty', 
			'id'=>'qty', 
			'value'=> '1',
			'class'       => 'ui-corner-all',
			'style'       => 'width:30px;margin:10px;'
			)
		  );
?>
<span  class="ui_button font9px">
<?php 

echo ($row->qty < 1) ? 'Out of Stock' : form_submit(array('value'=>'Add'));

?>
</span>
<?php echo form_close(); ?>
 </div>
<br style="clear:both;"/>
 </div>

<?php endforeach;?>

</td></tr>
<tr><td colspan="8" align="left"></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"><?php echo 'Total Data Returned: ';echo $total;?></th>
<th align="left" valign="middle" colspan="2"> </th>
<th align="right" valign="middle"> </th>
<th align="right" valign="middle"> </th>
<th align="right" valign="middle"></th>
<th align="center" valign="middle" class="ui_button font9px"><?php echo '';?></th>
</tr>
</table>

  