  <script>
  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#name" )
	.autocomplete({
			minLength: 1,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/product_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
			


		      });

      
    });
</script>
<style>
.bg-red { background-color: #eeeeee;   }
</style>
<?php echo form_open('mgmt_inventory/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="ui_button font10px" colspan="4">


<?php $this->load->view('mgmt_inventory/menu_bar.php'); ?>
</td></tr>
<!--
<tr><td align="left" class="ui_button font10px" style="width:200px">
<?php 

echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);

?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php

echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);

?>
</td><td align="left" class="ui_button font10px">

</td><td align="left" class="ui_button font10px"></td></tr>
-->

<tr><td align="left" class="font10px">
<?php
echo form_input(
		array(
		      'name'        => 'name',
		      'id'        => 'name',
		      'value'	  => $name,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		);

?>
</td><td align="left" class="font10px">

</td><td align="left" class="ui_button font10px">
<?php 
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => $search,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		 );
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td></tr>
</table>
<?php echo form_close(); ?>


<div class="ui_button font10px" id="cates">
 <?php 
$category = isset($category) ? $category : '';

$cates = $cate_tree;
foreach ($cates as $cate) 
  {
    if (is_array($cate)) 
      {
	$parent = array_shift($cate);
	echo '<span class="link">';
	echo ($parent->url_title == $category) ? '<button disabled="disabled">'.$parent->name.'</button>' : '<a href="'.site_url('mgmt_inventory/cate/'.$parent->url_title).'" >'.$parent->name.'</a>';  
	echo '</span>';
	foreach ($cate as $sub)
	  {	  
	echo '<span class="link">';
	    echo ($sub->url_title == $category) ? '<button disabled="disabled">'.$sub->name.'</button>' : '<a href="'.site_url('mgmt_inventory/cate/'.$sub->url_title).'" >'.$sub->name.'</a>';	  
	echo '</span>';
	  }
      }
    else
      {		echo '<span class="link">';
	echo ($cate->url_title == $category) ? '<button disabled="disabled">'.$cate->name.'</button>' : '<a href="'.site_url('mgmt_inventory/cate/'.$cate->url_title).'" >'.$cate->name.'</a>';
	echo '</span>';
      }
  }
?>
</div>


 

		
<table border="0" cellpadding="5" cellspacing="0" style="margin:30px 0; min-width:900px; float: left;  >

<tr>
<th colspan="9" align="left"><?php echo $table_title;?></th>
</tr>

 

<tr class="bgc">
<td colspan="9" align="right" class="ui_button font9px">
<?php 
echo $this->pagination->create_links(); 
$attr = ($per_page == 25) ? array() : array('class' => 'white');
$attr['style'] = 'margin:0 10px;';
echo anchor("mgmt_inventory/filter/per_page/$total", 'Display All', $attr);
 
?></td>
</tr>

<tr class="bgc">
<th align="left" valign="middle" width="60">
<?php echo anchor("mgmt_inventory/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="60">
<?php 
/*
echo anchor("mgmt_inventory/orderby/date", 'Date', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'date'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
*/
?>
</th>
<th align="left" valign="middle" >
<?php echo anchor("mgmt_inventory/orderby/name", 'Product Name', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'name'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle"  width="120">
<?php echo anchor("mgmt_inventory/orderby/category", 'Category', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'category'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle"  width="70">
<?php echo anchor("mgmt_inventory/orderby/qty", 'Quantity', array('class' => 'black frt'));
if (isset($orderby) && $orderby['order'] == 'qty'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s frt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n frt"></span>';
endif;
?>
</th>
<th align="right" valign="middle" width="70">
<?php echo anchor("mgmt_inventory/orderby/price", 'U/P', array('class' => 'black frt'));
if (isset($orderby) && $orderby['order'] == 'price'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s frt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n frt"></span>';
endif;
?>
</th>
<th align="right" valign="middle" >

</th>

<th align="right" valign="middle" width="20"></th>

<th align="center" valign="middle" width="140">Operations</th>
</tr>

<?php 
 
echo $low_stock;

foreach($query->result() as $row):
if ($low_stock && ($row->qty > $row->low_stock)) continue;

?> 
<tr class="bgc <?php echo ($row->qty < $row->low_stock) ? 'bg-red' : '';?>" title="<?php echo $row->detail;?>">
<td align="left" valign="middle"><?php echo $row->id;?></td>
<td align="left" valign="middle"><?php /* echo date('d/m/Y H:i:s', strtotime($row->date)); */?></td>
<td align="left" valign="middle"><?php echo $row->name;?></td>
<td align="left" valign="middle"><?php echo $row->category;?></td>
<td align="right" valign="middle"><?php echo $row->qty;?></td>
<td align="right" valign="middle"><?php echo $row->price?></td>
<td align="right" valign="middle"></td>
<td align="right" valign="middle"></td>
<td align="center" valign="middle" class="ui_button font9px">
<?php 
$data = array('class' => 'invo-lib-btn','content' => 'Edit', 'onclick'=>"openView('".site_url("mgmt_inventory/edit/$row->id")."','edit-".$row->id."');");
echo form_button($data);
$data = array('class' => 'invo-lib-btn','content' => 'View', 'onclick'=>"openView('".site_url("mgmt_inventory/view/$row->id")."','view-".$row->id."');");
echo form_button($data);
$data = array('class' => 'invo-lib-btn','content' => 'Del', 'onclick'=>"window.location='".site_url("mgmt_inventory/del_msg/$row->id")."'");
echo form_button($data);
?>
</td></tr>
<tr><td colspan="9" align="left">
<div id="progressbar<?php echo $row->id;?>"></div>

 
<script>
	$(function() {
		$( "#progressbar<?php echo $row->id;?>" ).progressbar({
			value: <?php echo $row->qty / ($row->low_stock * 5) * 100;?>
		});
	});
</script>
 
</td></tr>

<?php endforeach;?>

<tr><td colspan="9" align="left"></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"><?php echo 'Total Data Returned: ';echo $total;?></th>
<th align="left" valign="middle" colspan="2"> </th>
<th align="right" valign="middle"> </th>
<th align="right" valign="middle"> </th>
<th align="right" valign="middle"></th>
<th align="center" valign="middle" class="ui_button font9px"><?php echo '';?></th>
</tr>
</table>

 
  