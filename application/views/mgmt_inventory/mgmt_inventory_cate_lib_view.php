<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="ui_button font10px" colspan="4">
<?php $this->load->view('mgmt_inventory/menu_bar.php'); ?>
</td></tr>
</table>
 

 
		
<table border="0" cellpadding="5" cellspacing="0" style="margin:30px 0; min-width:900px; float: left; width:100%;">

<tr>
<th colspan="5"  align="left"><?php echo $table_title;?></th>
</tr>

<tr class="bgc">
<td colspan="5"  align="left" class="ui_button font9px">

<?php 
$attr = (isset($filter['parent'])) ? array('class' => 'white') : array();
echo anchor("mgmt_inventory_cate/filter_main_cates", 'Main Categories',$attr);
?>

<?php 
echo form_open('mgmt_inventory_cate/search', array('id' => 'myform', 'style' => 'display:inline;'));
?>
<span class="font11px">
<?php
  echo form_input( 
		  array(
			'name'        => 'search',
			'value'	      => 'Search..',
			'class'       => 'blur  ui-corner-all',
			'style'       => 'width:200px;margin:10px;'
			) 
		   );
?>
</span>
<?php
  echo form_submit(array(
			 'name'        => 'submit',
			 'value'       => 'Go'
			 )
		   );
?>

<?php echo form_close(); ?>
<span class="font12px">
<?php 
if ($search)
{
echo '&nbsp;&nbsp;&nbsp; Listing search results for: '.$search;
}
echo '&nbsp;&nbsp;&nbsp; Total: '.$total;
?>
</span>
</td>
</tr>

<tr class="bgc">
<td colspan="5"  align="right" class="ui_button font9px"><?php echo $this->pagination->create_links(); ?></td>
</tr>

<tr>
<th align="left" valign="middle" width="40">
<?php echo anchor("mgmt_inventory_cate/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="100">
<?php echo anchor("mgmt_inventory_cate/orderby/parent", 'Parent', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'parent'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="left" valign="middle" >
<?php echo anchor("mgmt_inventory_cate/orderby/name", 'Name', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'name'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="center" valign="middle" width="100"></th>
<th align="center" valign="middle" width="200">Operations</th>
</tr>

<?php foreach($query->result() as $row):?>
<tr class="bgc">
<td align="left" valign="middle"><?php echo $row->id;?></td>
<td align="left" valign="middle"><?php echo $row->parent;?></td>
<td align="left" valign="middle"><?php echo $row->name;?></td>
<td align="center" valign="middle">
<?php
?>
</td>
<td align="center" valign="middle" class="ui_button font9px"><?php echo anchor("mgmt_inventory_cate/edit/$row->id", 'Edit').nbs(10).anchor("mgmt_inventory_cate/del/$row->id", 'Del');?></td>
</tr>
<?php endforeach;?>

</table>
 