
<script>

var json_url = '<?php echo $json_url;?>';
var post_url = '<?php echo $post_url;?>';
var read_only = <?php echo $read_only;?>;
$(function() { 
		$( "#branch" ).autocomplete({
		//	source:  branch_shortnames
		});      
    });
</script>

<script>
$(function() {
if (!read_only) {
    $( "#category" )
      .autocomplete({
	minLength: 0,
	    source: function(request, response){
	    $.ajax({
	      url: '<?php echo site_url('ajax/cate_names');?>',
		  dataType: 'json',
		  type: 'POST',
		  data: request,
		  success: function(data){
		  response(data);
		}
	      });
	  }
	})
      .click(function() {
	  $(this).autocomplete( "search" , '' );
	});
} // end if
      
  });
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/mgmt_inventory_edit_view.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>js/phpjs.js"></script>
<div style="padding:10px 0;" id="inner-wrapper">
<div style="width:680px;background: #e1e1e1;color:#000000;" id="inner-text">


<table border="0" cellpadding="5" cellspacing="0" style="width:680px;padding:10px;">
<tr class="bgc">
<td class="ui_button" >
<?php
echo form_label('Product:', 'name');?>
</td>
<td colspan="5">
<?php
$attr = array('name' => 'name', 'id' => 'name', 'value' => '', 'class' => 'ui-corner-all cst', 'style' => 'width:300px;') ;
echo form_input($attr);
?>   
</td>
</tr>
<tr class="bgc">
<td class="ui_button" >
<?php
echo form_label('Detail:', 'name');?>
</td>
<td colspan="5">
<?php
$attr = array('name' => 'detail', 'id' => 'detail', 'value' => '', 'class' => 'ui-corner-all cst', 'style' => 'width:400px;') ;
echo form_input($attr);
?>   
</td>
</tr>
<tr class="bgc">
<td class="ui_button" >
<?php
echo form_label('Branch:', 'branch');?>
</td>
<td>
<?php
$attr = array('name' => 'branch', 'id' => 'branch', 'value' => '', 'class' => 'ui-corner-all cst', 'style' => 'width:100px;') ;
echo form_input($attr);
?>   
</td>

<td class="ui_button" >
<?php
echo form_label('Category:', 'category');?>
</td>
<td>
<?php
$attr = array('name' => 'category', 'id' => 'category', 'value' => '', 'class' => 'ui-corner-all cst', 'style' => 'width:100px;') ;
echo form_input($attr);
?>   
</td>
<td></td><td></td>

</tr>

</tr>
<tr class="bgc hide">
<td>
<?php
echo form_label('Purchase ID:', 'pur_id');?>
</td><td colspan="5">
<?php
$attr = array('name' => 'pur_id', 'id' => 'pur_id', 'class' => 'ui-corner-all cst', 'style' => 'width:100px;'); 
echo form_input($attr);

?>
</td>
<td></td><td></td>

</tr>
<tr>
<td>
<?php
echo form_label('Stock ID:', 'id');?>
</td><td>
<?php
$attr = array('name' => 'id', 'id' => 'id', 'class' => 'ui-corner-all cst', 'readonly' => 'readonly', 'style' => 'width:100px;') ;
echo form_input($attr);

?>  
</td>
<td>

</td><td>

</td>
<td></td><td></td>
</tr>


<tr>
<td>
<?php
echo form_label('Subtotal:', 'subtotal');?>
</td><td>
<?php
$attr = array('name' => 'subtotal', 'id' => 'subtotal', 'class' => 'ui-corner-all invo cst', 'readonly' => 'readonly'); 
echo form_input($attr);
?>  
</td>
<td>
<?php
echo form_label('GST:', 'gst');?>
</td><td>
<?php
$attr = array('name' => 'gst', 'id' => 'gst', 'class' => 'ui-corner-all invo cst', 'readonly' => 'readonly'); 
echo form_input($attr);
?>
</td>
<td>
 
</td><td>
 
</td>
</tr>

<tr class="bgc">

<td>
<?php
echo form_label('Total:', 'total');?>
</td><td colspan="5">
<?php
$attr = array('name' => 'total', 'id' => 'total', 'class' => 'ui-corner-all invo cst'); 
echo form_input($attr);
?>
</td>
</tr>


<tr class="bgc">
<td>
<?php
echo form_label('QTY:', 'qty');?>
</td><td>
<?php
$attr = array('name' => 'qty', 'id' => 'qty', 'class' => 'ui-corner-all invo cst'); 
echo form_input($attr);
?>  
</td>
<td>
<?php
 echo form_label('U/P:', 'price');
?>
</td><td>
<?php
$attr = array('name' => 'price', 'id' => 'price', 'class' => 'ui-corner-all invo cst'); 
echo form_input($attr);
?>
</td>
<td>
<?php
  //echo form_label('Debit:', 'debit');
?>
</td><td>
<?php
$attr = array('name' => 'debit', 'id' => 'debit', 'class' => 'hide ui-corner-all invo', 'readonly' => 'readonly'); 
echo form_input($attr);
?>
</td>
</tr>


<tr class="bgc">
<td>
<?php
echo form_label('Low Stock:', 'low_stock');?>
</td><td>
<?php
$attr = array('name' => 'low_stock', 'id' => 'low_stock', 'class' => 'ui-corner-all invo cst'); 
echo form_input($attr);
?>  
</td>
<td>
 
</td><td>
 
</td>
<td>
 
</td><td>
 
</td>
</tr>
</table>

   
</div>
<br style="clear:both">
<div style="float: left; padding:0 5px;" class="ui_button">
<?php 
echo form_input(array(
			 'type'  => 'button',
			 'name'  => 'save',
			 'id'    => 'save',
			 'value' => 'Save'
			 )
		   );
?>
</div>
<div style="float: right; padding:0 5px;" class="ui_button">
<?php 
  echo form_input(array(
			 'type' => 'button',
                         'name' => 'close',
			 'id'   => 'close',
			 'value' => 'Close'
			 )
		   );
?>
</div>
<br style="clear:both"><br />

 

</div>