  <script>
  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#customer" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/cust_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
			


		      });

      
    });
</script>
<style type="text/css">
.fp {
	color: black;
}
.fup {
	background-color: LightSteelBlue; color: black;
}
.uf {
	background-color: #C68E17; color: black;
}
.sample {
display: inline-block;
width: 160px;
height:20px;
}
.sam-box {
display: inline-block;
width: 10px;
padding: 0 4px;
}
</style>
<?php echo form_open('trash/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0"  style="width:1000px">
<tr><td align="left" class="ui_button font10px" style="width:200px">
<?php 
echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_input(
		array(
		      'name'        => 'customer',
		      'id'        => 'customer',
		      'value'	  => $customer,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		);

?>
</td>
<td align="left" class="ui_button font10px" style="width:50px">
</td>
<td align="right" class="ui_button font10px">
<span class="ui_buttonset">
<?php 
$attr =  ($this->uri->segment(1) == 'trash') ? array('class' => 'white') : array();
echo anchor(site_url('trash'), 'Trash',$attr);
?>
</span>
</td>


<tr><td align="left" class="font10px"> 
</td><td align="left" class="font10px"> 
</td><td align="left" class="ui_button font10px">
<?php 
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => $search,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		 );
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td></tr>
<td align="right" class="ui_button font10px">

</td>
</table>
<?php echo form_close(); ?>



<div style="float: left; padding:20px 22px;">
  <div style="float: left; width:960px;background: #e1e1e1;color:#000000;">

		
<table border="0" cellpadding="5" cellspacing="0" width="900px" style="margin:30px auto;">

<tr>
<th colspan="7" align="left"><?php echo $table_title;?></th>
</tr>

 

<tr class="bgc">
<td colspan="8" align="right" class="ui_button font9px">
<?php 
echo $this->pagination->create_links(); 
$attr = ($per_page == 25) ? array() : array('class' => 'white');
$attr['style'] = 'margin:0 10px;';
echo anchor("trash/filter/per_page/$total", 'Display All', $attr);
 
?></td>
</tr>

<tr class="bgc">
<th align="left" valign="middle" width="60">
<?php echo anchor("trash/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="120">
<?php echo anchor("trash/orderby/date", 'Date', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'date'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" >
<?php echo anchor("trash/orderby/cust_name", 'Customer', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'cust_name'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="right" valign="middle" width="70">
<?php echo anchor("trash/orderby/subtotal", 'Subtotal', array('class' => 'black frt'));
if (isset($orderby) && $orderby['order'] == 'subtotal'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s frt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n frt"></span>';
endif;
?>
</th>
<th align="right" valign="middle" width="70">
<?php echo anchor("trash/orderby/total", 'Total', array('class' => 'black frt'));
if (isset($orderby) && $orderby['order'] == 'total'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s frt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n frt"></span>';
endif;
?>
</th>
<th align="right" valign="middle" width="70">
<?php echo anchor("trash/orderby/total-sp---sp-paid", 'Debit', array('class' => 'black frt'));
if (isset($orderby) && $orderby['order'] == 'total-sp---sp-paid'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s frt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n frt"></span>';
endif;
?>
</th>
<th align="center" valign="middle" class="font9px"></th>
<th align="center" valign="middle" width="190">Operations</th>
</tr>

<?php 
$accu = new stdClass;
$accu->debit = $accu->subtotal = $accu->total = 0;


foreach($query->result() as $row):

$row->debit = $row->total - $row->paid;
$accu->subtotal += $row->subtotal;
$accu->total += $row->total;
$accu->debit += $row->debit;

$delivered = $row->delivered ? '' : '&bull';
$outsrc = $row->outsourced ? '&Omega;' : '';

$job_status = $row->debit < 0.05 ? 'fp' : ($row->finished ? 'fup' : 'uf');
?> 
<tr class="bgc">
<td align="left" valign="middle"><?php echo $row->id;?></td>
<td align="left" valign="middle"><?php echo date('d/m/Y H:i:s', strtotime($row->date));?></td>
<td align="left" valign="middle"><?php echo $row->cust_name;?></td>
<td align="right" valign="middle"><?php echo number_format($row->subtotal,2,'.',',');?></td>
<td align="right" valign="middle"><?php echo number_format($row->total,2,'.',',');?></td>
<td align="right" valign="middle"><?php echo number_format($row->debit,2,'.',',');?></td>
<td align="center" valign="middle" class="font11px"></td>
<td align="center" valign="middle" class="ui_button font9px">
<?php 
 
$data = array('class' => 'invo-lib-btn','content' => 'View', 'onclick'=>"openView('".site_url("invoice/view/$row->id")."','view-".$row->id."');");
echo form_button($data);
 
$data = array('class' => 'invo-lib-btn','content' => 'RCV', 'onclick'=>"window.location='".site_url("trash/rec_msg/$row->id")."'");
echo form_button($data);
/*
echo anchor("javascript:openView(invoice/edit/$row->id);", 'Edit').' '
  .anchor("invoice/edit/$row->id", 'View').' '
  .anchor("invoice/del/$row->id", 'Del');
*/
?>
</td></tr>
<?php endforeach;?>

<tr><td colspan="8" align="left"></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"><?php echo 'Total Data Returned: ';echo $total;?></th>
<th align="left" valign="middle">Accumulated Total of the Page............</th>
<th align="right" valign="middle"><?php echo number_format($accu->subtotal,2,'.',',');?></th>
<th align="right" valign="middle"><?php echo number_format($accu->total,2,'.',',');?></th>
<th align="right" valign="middle"><?php echo number_format($accu->debit,2,'.',',');?></th>
<td ></td>
<th align="center" valign="middle" class="ui_button font9px"><?php echo '';?></th>
</tr>

<tr><td colspan="8" align="left"><br /><br /></td></tr>

<tr><td colspan="8" align="left">


</td></tr>
</table>
 



  </div>
  </div>
