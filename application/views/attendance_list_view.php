  <script>
  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#name" )
	.autocomplete({
			minLength: 1,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/staff_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
			


		      });

      
    });
</script>

<?php echo form_open('attendance/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="ui_button font10px" style="width:200px">
<?php 
echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_input(
		array(
		      'name'        => 'name',
		      'id'        => 'name',
		      'value'	  => $name,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		);

?>
</td><td align="left" class="ui_button font10px"></td></tr>

<tr><td align="left" class="font10px">

</td><td align="left" class="font10px">

</td><td align="left" class="ui_button font10px">
<?php 
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => $search,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		 );
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td></tr>
</table>
<?php echo form_close(); ?>

 	
<table border="0" cellpadding="5" cellspacing="0" style="margin:0 0 30px 0; min-width:900px; float: left; width:100%; ">

<tr>
<th colspan="7" align="left"><?php echo $table_title;?></th>
</tr>

 

<tr class="bgc">
<td colspan="7" align="right" class="ui_button font9px">
<?php 
echo $this->pagination->create_links(); 
$attr = ($per_page == 25) ? array() : array('class' => 'white');
$attr['style'] = 'margin:0 10px;';
echo anchor("attendance/filter/per_page/$total", 'Display All', $attr);
//$data = array('content' => 'View Report', 'onclick'=>"openView('".site_url("attendance/report")."','report');");
//echo form_button($data);
?></td>
</tr>

<tr class="bgc">
<th align="left" valign="middle" width="160">
<?php echo anchor("attendance/orderby/date", 'Date', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'date'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="80">
<?php echo anchor("attendance/orderby/working_id", 'Working ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'working_id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="100">
<?php echo anchor("attendance/orderby/name", 'Name', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'name'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="100">
<?php echo anchor("attendance/orderby/check_in_time", 'Check In Time', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'check_in_time'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="120">
<?php echo anchor("attendance/orderby/check_in_ip", 'Check In IP', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'check_in_ip'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="100">
<?php echo anchor("attendance/orderby/check_out_time", 'Check Out Time', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'check_out_time'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>

<th align="left" valign="middle" width="120">

<?php echo anchor("attendance/orderby/check_out_ip", 'Check Out IP', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'check_out_ip'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>

</th>


</tr>

<?php 
 


foreach($query->result() as $row):

$ips = $cf_feslen['office_ips'];
$in = ' class="red" ';
$out =  ' class="red" '; 
$late = ' class="red" ';
if (in_array($row->check_in_ip, $ips)) {$in = '';}
if (in_array($row->check_out_ip, $ips)) {$out = '';}
if (
strtotime(date('H:i:s', strtotime($row->check_in_time))) < strtotime('+15 minutes', strtotime($row->working_time))
||
date('N', strtotime($row->check_in_time)) > 5
) 
{$late = '';}
?> 
<tr class="bgc">
<td align="left" valign="middle"><?php echo date('l M j, Y', strtotime($row->date));?></td>
<td align="left" valign="middle"><?php echo $row->working_id;?></td>
<td align="left" valign="middle"><?php echo $row->name;?></td>
<td align="left" valign="middle" <?php echo $late;?> ><?php echo ($row->check_in_time == '00:00:00') ? '' : $row->check_in_time;?></td>
<td align="left" valign="middle" <?php echo $in;?> ><?php echo $row->check_in_ip;?></td>
<td align="left" valign="middle"><?php echo ($row->check_out_time == '00:00:00') ? '' : $row->check_out_time;?></td>
<td align="left" valign="middle" <?php echo $out;?> ><?php echo $row->check_out_ip;?></td>
</tr>
<?php endforeach;?>

<tr><td colspan="7" align="left"></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"><?php echo 'Total Data Returned: ';echo $total;?></th>
<th align="left" valign="middle">............</th>
<th align="right" valign="middle"> </th>
<th align="right" valign="middle"> </th>
<th align="right" valign="middle"></th>
<th align="center" valign="middle" class="ui_button font9px"><?php echo '';?></th>
</tr>
</table>

  