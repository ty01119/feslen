<span class="ui_buttonset">
<?php 
/***
$data = array('content' => 'Turnover', 'onclick'=>"window.location.href='".site_url('stat_turnover')."'");
if ($this->router->class == 'stat_turnover') $data['disabled'] = 'disabled';
echo form_button($data);
 
$data = array('content' => 'Yearly Turnover Charts', 'onclick'=>"window.location.href='".site_url('stat_turnover_charts')."'");
if ($this->router->class == 'stat_turnover_charts') $data['disabled'] = 'disabled';
echo form_button($data);
 
$data = array('content' => 'Turnover by Division', 'onclick'=>"window.location.href='".site_url('stat_turnover_division')."'");
if ($this->router->class == 'stat_turnover_division') $data['disabled'] = 'disabled';
echo form_button($data);
  
$data = array('content' => 'Turnover by Clients', 'onclick'=>"window.location.href='".site_url('stat_turnover_clients')."'");
if ($this->router->class == 'stat_turnover_clients') $data['disabled'] = 'disabled';
echo form_button($data);
***/

$attr =  ($this->uri->segment(1) == 'stat_turnover') ? array('class' => 'white') : array();
echo anchor(site_url('stat_turnover'), 'Turnover',$attr);

$attr =  ($this->uri->segment(1) == 'stat_turnover_charts') ? array('class' => 'white') : array();
echo anchor(site_url('stat_turnover_charts'), 'Yearly Turnover Charts',$attr);

$attr =  ($this->uri->segment(1) == 'stat_turnover_month') ? array('class' => 'white') : array();
echo anchor(site_url('stat_turnover_month'), 'Monthly Bar Chart by Year',$attr);

$attr =  ($this->uri->segment(1) == 'stat_turnover_division') ? array('class' => 'white') : array();
echo anchor(site_url('stat_turnover_division'), 'Turnover by Division',$attr);

$attr =  ($this->uri->segment(1) == 'stat_turnover_clients') ? array('class' => 'white') : array();
echo anchor(site_url('stat_turnover_clients'), 'Turnover by Clients',$attr);
?>
</span>

<script type="text/javascript"> 
$(".ui_buttonset").buttonset(); 
</script>