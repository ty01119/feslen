 <script>  
  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#customer" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/cust_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
			


		      });

      
    });
</script>
   <?php echo form_open('stat_intertrans/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="font10px" colspan="7"> 

<?php $this->load->view('statistics/menu_view'); ?>
</td>
</tr>


<tr>
<td align="left" class="ui_button font10px" style="width:200px">
     <?php 
echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px">

<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td><td align="left" class="ui_button font10px"></td></tr>

<tr><td align="left" class="font10px">


</td><td align="left" class="font10px">


</td><td align="left" class="ui_button font10px">

</td><td align="left" class="ui_button font10px">
</td></tr>
</table>
<?php echo form_close();?>


<div style="float: left; padding:20px 22px;">
  <div style="float: left; width:960px;background: #e1e1e1;color:#000000;">

		
<table border="0" cellpadding="5" cellspacing="0" width="930px" style="margin:15px auto;">

<tr>
<th colspan="7" align="left"><?php echo $table_title;?></th>
</tr>
<tr><td colspan="7" align="left"></td></tr>
 

<tr><td colspan="7" align="left">
<?php 
    $tmpl = array (
		   'table_open'          => '<table border="1" cellpadding="2" cellspacing="1">',		   
                    'heading_row_start'   => '<tr>',
                    'heading_row_end'     => '</tr>',
                    'heading_cell_start'  => '<th>',
                    'heading_cell_end'    => '</th>',
                    'row_start'           => '<tr>',
                    'row_end'             => '</tr>',
                    'cell_start'          => '<td align="right">',
                    'cell_end'            => '</td>',
                    'row_alt_start'       => '<tr>',
                    'row_alt_end'         => '</tr>',
                    'cell_alt_start'      => '<td align="right">',
                    'cell_alt_end'        => '</td>',
                    'table_close'         => '</table>'
              );

    $this->table->set_template($tmpl); 
    $table1 = array(
		  array('Branch', 'for Manukau', 'for Production', 'for Penrose', 'for Epsom', 'for City', 'for Albany', 'for Energysigns', 'for Websolution', 'TOTAL'),		  
		  array('Production', $table['production']['manukau'], $table['production']['production'], $table['production']['penrose'],  $table['production']['epsom'], $table['production']['city'], $table['production']['albany'], $table['production']['energysigns'], $table['production']['websolution'], array_sum($table['production'])),
		  array('Penrose', $table['penrose']['manukau'], $table['penrose']['production'], $table['penrose']['penrose'],  $table['penrose']['epsom'], $table['penrose']['city'], $table['penrose']['albany'], $table['penrose']['energysigns'], $table['penrose']['websolution'], array_sum($table['penrose'])),
		  array('Epsom', $table['epsom']['manukau'], $table['epsom']['production'], $table['epsom']['penrose'],  $table['epsom']['epsom'], $table['epsom']['city'], $table['epsom']['albany'], $table['epsom']['energysigns'], $table['epsom']['websolution'], array_sum($table['epsom'])),
		  array('City', $table['city']['manukau'], $table['city']['production'], $table['city']['penrose'], $table['city']['epsom'], $table['city']['city'], $table['city']['albany'], $table['city']['energysigns'], $table['city']['websolution'], array_sum($table['city'])),  
		  array('Albany', $table['albany']['manukau'], $table['albany']['production'], $table['albany']['penrose'], $table['albany']['epsom'], $table['albany']['city'], $table['albany']['albany'], $table['albany']['energysigns'], $table['albany']['websolution'] , array_sum($table['albany'])),		  
		  array('Manukau', $table['manukau']['manukau'], $table['manukau']['production'], $table['manukau']['penrose'], $table['manukau']['epsom'], $table['manukau']['city'], $table['manukau']['albany'], $table['manukau']['energysigns'], $table['manukau']['websolution'] , array_sum($table['manukau'])),
		  array('Energysigns', $table['energysigns']['manukau'], $table['energysigns']['production'], $table['energysigns']['penrose'], $table['energysigns']['epsom'], $table['energysigns']['city'], $table['energysigns']['albany'], $table['energysigns']['energysigns'], $table['energysigns']['websolution'], array_sum($table['energysigns'])),
		  array('Websolution', $table['websolution']['manukau'], $table['websolution']['production'], $table['websolution']['penrose'], $table['websolution']['epsom'], $table['websolution']['city'], $table['websolution']['albany'], $table['websolution']['energysigns'], $table['websolution']['websolution'], array_sum($table['websolution'])),
		  array('TOTAL',  $table['total']['manukau'], $table['total']['production'], $table['total']['penrose'], $table['total']['epsom'], $table['total']['city'], $table['total']['albany'], $table['total']['energysigns'], $table['total']['websolution'], array_sum($table['total']))
  );
  
    echo $this->table->generate($table1); 

echo br(2);
   $table2 = array(
   		array('Branch', ''), 
     		array('Production Balance', array_sum($table['production']) - $table['total']['production']),  
     		array('Penrose Balance', array_sum($table['penrose']) - $table['total']['penrose']), 
     		array('Epsom Balance', array_sum($table['epsom']) - $table['total']['epsom']),      		
     		array('City Balance', array_sum($table['city']) - $table['total']['city']),      		
     		array('Albany Balance', array_sum($table['albany']) - $table['total']['albany']),       		
     		array('Manukau Balance', array_sum($table['manukau']) - $table['total']['manukau']),     		
     		array('Energysigns Balance', array_sum($table['energysigns']) - $table['total']['energysigns']),      		
     		array('Websolution Balance', array_sum($table['websolution']) - $table['total']['websolution']), 
   );
    echo $this->table->generate($table2); 
?>

</td></tr>
 
 
<tr><td colspan="7" align="left"></td></tr>
</table>
 



  </div>
  </div>