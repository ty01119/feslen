  <script>  
  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#customer" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/cust_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
			


		      });

      
    });
</script>
   <?php echo form_open('stat_turnover/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="font10px" colspan="7"> 

<?php $this->load->view('statistics/menu_view'); ?>
</td>
</tr>


<tr>
<td align="left" class="ui_button font10px" style="width:200px">
     <?php 
echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_input(
		array(
		      'name'        => 'customer',
		      'id'        => 'customer',
		      'value'	  => $customer,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		);

?>
</td><td align="left" class="ui_button font10px"></td></tr>

<tr><td align="left" class="font10px">
<span class="ui_buttonset">
<?php 
$attr = (isset($filter['1']['finished']) 
	 && !$filter['1']['finished']) ? array('class' => 'white') : array();
echo anchor("stat_turnover/filter/1/finished/0", 'X',$attr);
$attr = (isset($filter['1']['finished']) 
	 && $filter['1']['finished']) ? array('class' => 'white') : array();
echo anchor("stat_turnover/filter/1/finished/1", 'Finished',$attr);
?>
</span>
<span class="ui_buttonset">
<?php 
$attr = (isset($filter['2']['delivered']) 
	 && !$filter['2']['delivered']) ? array('class' => 'white') : array();
echo anchor("stat_turnover/filter/2/delivered/0", 'X',$attr);
$attr = (isset($filter['2']['delivered']) 
	 && $filter['2']['delivered']) ? array('class' => 'white') : array();
echo anchor("stat_turnover/filter/2/delivered/1", 'Delivered',$attr);
?>
</span>
</td><td align="left" class="font10px">
<span class="ui_buttonset">
<?php 
$attr = (isset($filter['3']['sql']) && ($filter['3']['sql'] == 'paid-ne-total')) ? array('class' => 'white') : array();
echo anchor("stat_turnover/filter/3/sql/paid-ne-total", 'X',$attr);
$attr = (isset($filter['3']['sql']) && ($filter['3']['sql'] == 'paid-eq-total')) ? array('class' => 'white') : array();
echo anchor("stat_turnover/filter/3/sql/paid-eq-total", 'Paid',$attr);
?>
</span>
<span class="ui_buttonset">
<?php 
$attr = (isset($filter['4']['outsourced']) 
	 && !$filter['4']['outsourced']) ? array('class' => 'white') : array();
echo anchor("stat_turnover/filter/4/outsourced/0", 'X',$attr);
$attr = (isset($filter['4']['outsourced']) 
	 && $filter['4']['outsourced']) ? array('class' => 'white') : array();
echo anchor("stat_turnover/filter/4/outsourced/1", 'Outsourced',$attr);
?>
</span>
</td><td align="left" class="ui_button font10px">
<?php 
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => $search,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		 );
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td></tr>
</table>
<?php echo form_close();?>

		
<table border="0" cellpadding="5" cellspacing="0" style="margin:30px 0; min-width:900px; float: left; ">

<tr>
<th colspan="7" align="left"><?php echo $table_title;?></th>
</tr>
<tr><td colspan="7" align="left"></td></tr>
 

<?php 
$accu = new stdClass;
$accu->debit = $accu->subtotal = $accu->total = 0;
$accu_all->debit = $accu_all->subtotal = $accu_all->total = 0;
foreach($turnover_copy as $row):
$row->turnover->debit = $row->turnover->total - $row->turnover->paid;
$accu->subtotal += $row->turnover->subtotal;
$accu->total += $row->turnover->total;
$accu->debit += $row->turnover->debit;
?> 
<tr>
<th align="left" valign="middle" width="120" colspan="2"></th>
<th align="left" valign="middle"><?php echo $row->detail->biz_name.' - '.ucfirst($row->name);?></th>
<th align="right" valign="middle" width="70">Subtotal</th>
<th align="right" valign="middle" width="70">Total</th>
<th align="right" valign="middle" width="70">Debit</th>
<th align="center" valign="middle" class="font9px"></th>
</tr>

<tr class="bgc inv-item">
<td align="left" valign="middle" colspan="2"></td>
<td align="left" valign="middle">Invoice Turnover Statistics</td>
<td align="right" valign="middle"><?php echo number_format($row->turnover->subtotal,2,'.',',');?></td>
<td align="right" valign="middle"><?php echo number_format($row->turnover->total,2,'.',',');?></td>
<td align="right" valign="middle"><?php echo number_format($row->turnover->debit,2,'.',',');?></td>
<td align="center" valign="middle" class="font11px"></td>
</tr>

<tr><td colspan="7" align="left">
<div id="progressbar<?php echo $row->name;?>" style="height:24px;line-height:24px;">
	<span class="pblabel"><?php echo number_format($row->turnover->total/$row->detail->turnover*100, 2, '.', '');?>%
	of 
	<?php echo $row->detail->turnover;?>
	</span>
</div>
<script type="text/javascript"> 
$( "#progressbar<?php echo $row->name;?>" ).progressbar({ value: <?php echo $row->turnover->total/$row->detail->turnover*100;?> });
var label = $( "#progressbar<?php echo $row->name;?>" ).find('.pblabel').clone().width($('#progressbar<?php echo $row->name;?>').width());
$( "#progressbar<?php echo $row->name;?>" ).find('.ui-progressbar-value').append(label);	 
</script>
</td></tr>

<tr><td colspan="7" align="left"><br /></td></tr>
<?php endforeach;?>

<tr><td colspan="7" align="left"></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"> </th>
<th align="left" valign="middle">Accumulated Total ............</th>
<th align="right" valign="middle"><?php echo number_format($accu->subtotal,2,'.',',');?></th>
<th align="right" valign="middle"><?php echo number_format($accu->total,2,'.',',');?></th>
<th align="right" valign="middle"><?php echo number_format($accu->debit,2,'.',',');?></th>
<th align="center" valign="middle" class="ui_button font9px"><?php echo '';?></th>
</tr>
<tr><td colspan="7" align="left"><br /><br /></td></tr>
<!-- end copy -->

<?php
$accu_all->debit += $accu->debit; 
$accu_all->subtotal += $accu->subtotal;
$accu_all->total += $accu->total;
?>

<!-- start web -->
<?php 
$accu = new stdClass;
$accu->debit = $accu->subtotal = $accu->total = 0;
foreach($turnover_web as $row):
$row->turnover->debit = $row->turnover->total - $row->turnover->paid;
$accu->subtotal += $row->turnover->subtotal;
$accu->total += $row->turnover->total;
$accu->debit += $row->turnover->debit;
?> 
<tr>
<th align="left" valign="middle" width="120" colspan="2"></th>
<th align="left" valign="middle"><?php echo $row->detail->biz_name;?></th>
<th align="right" valign="middle" width="70">Subtotal</th>
<th align="right" valign="middle" width="70">Total</th>
<th align="right" valign="middle" width="70">Debit</th>
<th align="center" valign="middle" class="font9px"></th>
</tr>

<tr class="bgc inv-item">
<td align="left" valign="middle" colspan="2"></td>
<td align="left" valign="middle">Invoice Turnover Statistics</td>
<td align="right" valign="middle"><?php echo number_format($row->turnover->subtotal,2,'.',',');?></td>
<td align="right" valign="middle"><?php echo number_format($row->turnover->total,2,'.',',');?></td>
<td align="right" valign="middle"><?php echo number_format($row->turnover->debit,2,'.',',');?></td>
<td align="center" valign="middle" class="font11px"></td>
</tr>

<tr><td colspan="7" align="left">
<div id="progressbar<?php echo $row->name;?>" style="height:24px;line-height:24px;">
	<span class="pblabel"><?php echo number_format($row->turnover->total/$row->detail->turnover*100, 2, '.', '');?>%
	of 
	<?php echo $row->detail->turnover;?>
	</span>
</div>
<script type="text/javascript"> 
$( "#progressbar<?php echo $row->name;?>" ).progressbar({ value: <?php echo $row->turnover->total/$row->detail->turnover*100;?> });
var label = $( "#progressbar<?php echo $row->name;?>" ).find('.pblabel').clone().width($('#progressbar<?php echo $row->name;?>').width());
$( "#progressbar<?php echo $row->name;?>" ).find('.ui-progressbar-value').append(label);	 
</script>
</td></tr>

<tr><td colspan="7" align="left"><br /></td></tr>
<?php endforeach;?>

<tr><td colspan="7" align="left"></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"> </th>
<th align="left" valign="middle">Accumulated Total ............</th>
<th align="right" valign="middle"><?php echo number_format($accu->subtotal,2,'.',',');?></th>
<th align="right" valign="middle"><?php echo number_format($accu->total,2,'.',',');?></th>
<th align="right" valign="middle"><?php echo number_format($accu->debit,2,'.',',');?></th>
<th align="center" valign="middle" class="ui_button font9px"><?php echo '';?></th>
</tr>
<tr><td colspan="7" align="left"><br /><br /></td></tr>
<!-- end web -->


<?php
$accu_all->debit += $accu->debit; 
$accu_all->subtotal += $accu->subtotal;
$accu_all->total += $accu->total;
?>

<!-- start signs -->


<?php 
$accu = new stdClass;
$accu->debit = $accu->subtotal = $accu->total = 0;
foreach($turnover_signs as $row):
$row->turnover->debit = $row->turnover->total - $row->turnover->paid;
$accu->subtotal += $row->turnover->subtotal;
$accu->total += $row->turnover->total;
$accu->debit += $row->turnover->debit;
?> 
<tr>
<th align="left" valign="middle" width="120" colspan="2"></th>
<th align="left" valign="middle"><?php echo $row->detail->biz_name;?></th>
<th align="right" valign="middle" width="70">Subtotal</th>
<th align="right" valign="middle" width="70">Total</th>
<th align="right" valign="middle" width="70">Debit</th>
<th align="center" valign="middle" class="font9px"></th>
</tr>

<tr class="bgc inv-item">
<td align="left" valign="middle" colspan="2"></td>
<td align="left" valign="middle">Invoice Turnover Statistics</td>
<td align="right" valign="middle"><?php echo number_format($row->turnover->subtotal,2,'.',',');?></td>
<td align="right" valign="middle"><?php echo number_format($row->turnover->total,2,'.',',');?></td>
<td align="right" valign="middle"><?php echo number_format($row->turnover->debit,2,'.',',');?></td>
<td align="center" valign="middle" class="font11px"></td>
</tr>

<tr><td colspan="7" align="left">
<div id="progressbar<?php echo $row->name;?>" style="height:24px;line-height:24px;"> 
	<span class="pblabel"><?php echo number_format($row->turnover->total/$row->detail->turnover*100, 2, '.', '');?>%
	of 
	<?php echo $row->detail->turnover;?>
	</span>
</div>
<script type="text/javascript"> 
$( "#progressbar<?php echo $row->name;?>" ).progressbar({ value: <?php echo $row->turnover->total/$row->detail->turnover*100;?> });
var label = $( "#progressbar<?php echo $row->name;?>" ).find('.pblabel').clone().width($('#progressbar<?php echo $row->name;?>').width());
$( "#progressbar<?php echo $row->name;?>" ).find('.ui-progressbar-value').append(label);	 
</script>
</td></tr>

<tr><td colspan="7" align="left"><br /></td></tr>
<?php endforeach;?>

<tr><td colspan="7" align="left"></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"> </th>
<th align="left" valign="middle">Accumulated Total ............</th>
<th align="right" valign="middle"><?php echo number_format($accu->subtotal,2,'.',',');?></th>
<th align="right" valign="middle"><?php echo number_format($accu->total,2,'.',',');?></th>
<th align="right" valign="middle"><?php echo number_format($accu->debit,2,'.',',');?></th>
<th align="center" valign="middle" class="ui_button font9px"><?php echo '';?></th>
</tr>
<tr><td colspan="7" align="left"><br /><br /></td></tr>
<!-- end signs -->


<?php
$accu_all->debit += $accu->debit; 
$accu_all->subtotal += $accu->subtotal;
$accu_all->total += $accu->total;
?>
<!-- start all -->
<tr><td colspan="7" align="left">
Over all total:
<hr /></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"> </th>
<th align="left" valign="middle" >Accumulated Total ............</th>
<th align="right" valign="middle"><?php echo number_format($accu_all->subtotal,2,'.',',');?></th>
<th align="right" valign="middle"><?php echo number_format($accu_all->total,2,'.',',');?></th>
<th align="right" valign="middle"><?php echo number_format($accu_all->debit,2,'.',',');?></th>
<th align="center" valign="middle" class="ui_button font9px"><?php echo '';?></th>
</tr>
<!-- end all -->
<tr><td colspan="7" align="left"></td></tr>
</table>