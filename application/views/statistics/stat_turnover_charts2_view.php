<script type="text/javascript">
  $(function() {
      $( "#branch" )
	.autocomplete({
			minLength: 0,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/branch_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
		      }).click(function() {
	  $(this).autocomplete( "search" , '' );
	});
    });
</script>      
		      
		      <!-- START  -->
<link href="<?php echo base_url(); ?>lib/jq-ui-selectmenu/jquery.ui.selectmenu.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>lib/jq-ui-selectmenu/jquery.ui.selectmenu.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>lib/highcharts/js/highcharts.js"></script>
 

		
<!-- 1b) Optional: the exporting module -->
<script type="text/javascript" src="<?php echo base_url(); ?>lib/highcharts/js/modules/exporting.js"></script>

		<!-- 2. Add the JavaScript to initialize the chart on document ready -->
		<script type="text/javascript">
 
		 
			var chart;
			$(document).ready(function() {
			
			
				
				
			});
				
		</script>
		
<!-- END  -->



   <?php echo form_open('stat_turnover_charts/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="font10px" colspan="7"> 
<?php $this->load->view('statistics/menu_view'); ?>
</td>
</tr>
 
<tr>
<td align="left" class="ui_button font10px" style="width:200px">
     <?php 
			  /*
echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);  */
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php /*
echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);

			  */
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_input(
		array(
		      'name'        => 'branch',
		      'id'        => 'branch',
		      'value'	  => $branch,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		);

?>
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td><td align="left" class="ui_button font10px"></td></tr>

<tr>
<td align="left" class="font10px" colspan="7">
 <span class="ui_buttonset">
<?php 
$attr =  ($chart == 'monthly') ? array('class' => 'white') : array();
echo anchor(site_url('stat_turnover_charts/monthly'), 'Monthly Bar Chart',$attr);
$attr =  ($chart == 'weekly') ? array('class' => 'white') : array();
echo anchor(site_url('stat_turnover_charts/weekly'), 'Weekly Line Chart',$attr);
?>
</span>
 

</td></tr>
 
</table>
<?php echo form_close();?>

<script type="text/javascript"> 
$(".ui_buttonset").buttonset(); 

</script>

 

 <?php echo $table_title;?> 
 

 
<?php if ($chart == 'monthly' ) { ?>
	<div id="container" style="min-width: 900px;width: 100%;height: 450px; margin: 0 auto"></div>
			<script type="text/javascript">
	chart = new Highcharts.Chart({
					chart: {
						renderTo: 'container',
						defaultSeriesType: 'column'
					},    
					credits: {
        					enabled: false	
    					},
					title: {
						text: 'Monthly Turnover'
					},
					subtitle: {
						text: 'Source: Feslen BMS 3A Edition'
					},
					xAxis: {
						categories: [
							'Jan 11', 
							'Feb 11', 
							'Mar 11', 
							'Apr 11', 
							'May 11', 
							'Jun 11', 
							'Jul 11', 
							'Aug 11', 
							'Sep 11', 
							'Oct 11', 
							'Nov 11', 
							'Dec 11',
							'Jan 12', 
							'Feb 12', 
							'Mar 12', 
							'Apr 12', 
							'May 12', 
							'Jun 12', 
							'Jul 12', 
							'Aug 12', 
							'Sep 12', 
							'Oct 12', 
							'Nov 12', 
							'Dec 12',
							'Jan 13', 
							'Feb 13', 
							'Mar 13', 
							'Apr 13', 
							'May 13', 
							'Jun 13', 
							'Jul 13', 
							'Aug 13', 
							'Sep 13', 
							'Oct 13', 
							'Nov 13', 
							'Dec 13',
							'Jan 14', 
							'Feb 14', 
							'Mar 14', 
							'Apr 14', 
							'May 14', 
							'Jun 14', 
							'Jul 14', 
							'Aug 14', 
							'Sep 14', 
							'Oct 14', 
							'Nov 14', 
							'Dec 14'
						]
					},
					yAxis: {
						min: 0,
						title: {
							text: 'Turnover (NZD)'
						}
					},
					legend: {
						layout: 'vertical',
						backgroundColor: '#FFFFFF',
						align: 'left',
						verticalAlign: 'top',
						x: 100,
						y: 70,
						floating: true,
						shadow: true
					},
					tooltip: {
						formatter: function() {
							return ''+
								this.x +': '+ this.y +' NZD';
						}
					},
					plotOptions: {
						column: {
							pointPadding: 0.2,
							borderWidth: 0
						}
					},
				        series: [<?php echo $series1;?>]
				});
</script>	
<?php } ?>		
			
			
<br style="clear:both;"/><br />
<?php if ($chart == 'weekly' ) { ?>
	<div id="container2" style="min-width: 900px;width: 100%; height: 450px; margin: 0 auto"></div>

		<script type="text/javascript">
	
			// chart2 
					chart = new Highcharts.Chart({
					chart: {
						renderTo: 'container2',
						defaultSeriesType: 'line'
					},    
					credits: {
        					enabled: false	
    					},
					title: {
						text: 'Weekly Turnover',
					},
					subtitle: {
						text: 'Source:  Feslen BMS 3A Edition',
					},
					xAxis: {
						categories: [1,2,3,4,5,6,7,8,9,
						10,11,12,13,14,15,16,17,18,19,
						20,21,22,23,24,25,26,27,28,29,
						30,31,23,33,34,35,36,37,38,39,
						40,41,24,34,44,45,46,47,48,49,
						50,51,52,1,2,3,4,5,6,7,8,9,
						10,11,12,13,14,15,16,17,18,19,
						20,21,22,23,24,25,26,27,28,29,
						30,31,23,33,34,35,36,37,38,39,
						40,41,24,34,44,45,46,47,48,49,
						50,51,52,1,2,3,4,5,6,7,8,9,
						10,11,12,13,14,15,16,17,18,19,
						20,21,22,23,24,25,26,27,28,29,
						30,31,23,33,34,35,36,37,38,39,
						40,41,24,34,44,45,46,47,48,49,
						50,51,52,1,2,3,4,5,6,7,8,9,
						10,11,12,13,14,15,16,17,18,19,
						20,21,22,23,24,25,26,27,28,29,
						30,31,23,33,34,35,36,37,38,39,
						40,41,24,34,44,45,46,47,48,49,
						50,51,52
						]
					},
					yAxis: {
						title: {
							text: 'Turnover (NZD)'
						},
						plotLines: [{
							value: 0,
							width: 1,
							color: '#808080'
						}]
					},
					tooltip: {
						formatter: function() {
				                return '<b>'+ this.series.name +'</b><br/>'+
								this.x +': '+ this.y +' NZD';
						}
					},
					legend: {
						layout: 'vertical',
						backgroundColor: '#FFFFFF',
						align: 'left',
						verticalAlign: 'top',
						x: 100,
						y: 70,
						floating: true,
						shadow: true
					},
					series: [<?php echo $series3;?>]
				});
</script>
<?php }?>
 