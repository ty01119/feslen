  <script>  
  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#customer" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/cust_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
			


		      });

      
    });
</script>
<script type="text/javascript">
  $(function() {
      $( "#branch" )
	.autocomplete({
			minLength: 0,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/branch_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
		      }).click(function() {
	  $(this).autocomplete( "search" , '' );
	});
    });
</script>  
      <!-- START  -->

<script type="text/javascript" src="<?php echo base_url(); ?>lib/highcharts/js/highcharts.js"></script>
 		
<!-- 1b) Optional: the exporting module -->
<script type="text/javascript" src="<?php echo base_url(); ?>lib/highcharts/js/modules/exporting.js"></script>	
<!-- END  -->


   <?php echo form_open('stat_turnover_clients/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="font10px" colspan="7"> 

<?php $this->load->view('statistics/menu_view'); ?>
</td>
</tr>


<tr>
<td align="left" class="ui_button font10px" style="width:200px">
     <?php 
echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_input(
		array(
		      'name'        => 'branch',
		      'id'        => 'branch',
		      'value'	  => $branch,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		);

?>
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
		 
/***
echo form_input(
		array(
		      'name'        => 'customer',
		      'id'        => 'customer',
		      'value'	  => $customer,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		);
***/
?>
</td><td align="left" class="ui_button font10px"></td></tr>

<tr><td align="left" class="font10px">
<span class="ui_buttonset">
<?php 
/***
$attr = (isset($filter['1']['finished']) 
	 && !$filter['1']['finished']) ? array('class' => 'white') : array();
echo anchor("stat_turnover_clients/filter/1/finished/0", 'X',$attr);
$attr = (isset($filter['1']['finished']) 
	 && $filter['1']['finished']) ? array('class' => 'white') : array();
echo anchor("stat_turnover_clients/filter/1/finished/1", 'Finished',$attr);
***/
?>
</span>
<span class="ui_buttonset">
<?php 
/***
$attr = (isset($filter['2']['delivered']) 
	 && !$filter['2']['delivered']) ? array('class' => 'white') : array();
echo anchor("stat_turnover_clients/filter/2/delivered/0", 'X',$attr);
$attr = (isset($filter['2']['delivered']) 
	 && $filter['2']['delivered']) ? array('class' => 'white') : array();
echo anchor("stat_turnover_clients/filter/2/delivered/1", 'Delivered',$attr);
***/
?>
</span>
</td><td align="left" class="font10px">
<span class="ui_buttonset">
<?php 
/***
$attr = (isset($filter['3']['sql']) && ($filter['3']['sql'] == 'paid-ne-total')) ? array('class' => 'white') : array();
echo anchor("stat_turnover_clients/filter/3/sql/paid-ne-total", 'X',$attr);
$attr = (isset($filter['3']['sql']) && ($filter['3']['sql'] == 'paid-eq-total')) ? array('class' => 'white') : array();
echo anchor("stat_turnover_clients/filter/3/sql/paid-eq-total", 'Paid',$attr);
***/
?>
</span>
<span class="ui_buttonset">
<?php 
/***
$attr = (isset($filter['4']['outsourced']) 
	 && !$filter['4']['outsourced']) ? array('class' => 'white') : array();
echo anchor("stat_turnover_clients/filter/4/outsourced/0", 'X',$attr);
$attr = (isset($filter['4']['outsourced']) 
	 && $filter['4']['outsourced']) ? array('class' => 'white') : array();
echo anchor("stat_turnover_clients/filter/4/outsourced/1", 'Outsourced',$attr);
***/
?>
</span>
</td><td align="left" class="ui_button font10px">
<?php 
/***
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => $search,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		 );
***/
?>
</td><td align="left" class="ui_button font10px">
<?php

?>
</td></tr>
</table>
<?php echo form_close();?>


 

		
<table border="0" cellpadding="5" cellspacing="0" style="margin:30px 0; min-width:900px; float: left; width:100%;">

<tr>
<th colspan="7" align="left"><?php echo $table_title;?></th>
</tr>
<tr><td colspan="7" align="left">


<?php 
//foreach ($turnover_clients as $branch => $data) { 
?>

		<div id="container-<?php echo $branch;?>" style="width: 1000px; height: 450px; margin: 0 auto"></div>
		<script type="text/javascript">
		
			var chart;
 
				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'container-<?php echo $branch;?>',
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false
					},    
					credits: {
        					enabled: false	
    					},
					title: {
						text: 'Turnover by Clients (<?php echo ucwords($branch);?>)'
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
						}
					},
					plotOptions: {
						pie: {
							allowPointSelect: true, 
							cursor: 'pointer', 
							dataLabels: {
								enabled: true,
								color: '#000000',
								connectorColor: '#000000',
								formatter: function() {
									return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
								}
							}
						}				
					},
				    series: [{
						type: 'pie',
						name: 'Turnover',       
						data: [
<?php
foreach ($stat_data as $item){ echo "['".$item['name'].' ['.$item['times'].'] '.$item['total']."', ".$item['total']."],"; }
?>
						]
					}]
				});
	 
				
		</script>
		
<br class="clr"/><br />
<?php

//}
 
?>

</td></tr>
 

 
 
<tr><td colspan="7" align="left">

</td></tr>

<tr>
<th align="left" valign="middle" colspan="2"> </th>
<th align="left" valign="middle">Accumulated Total ............</th>
<th align="right" valign="middle"><?php ?></th>
<th align="right" valign="middle"><?php ?></th>
<th align="right" valign="middle"><?php ?></th>
<th align="center" valign="middle" class="ui_button font9px"><?php echo '';?></th>
</tr>
<tr><td colspan="7" align="left"><br /><br /></td></tr>
 
 
<!-- start all -->
<tr><td colspan="7" align="left">
Over all total:
<hr /></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"> </th>
<th align="left" valign="middle" >Accumulated Total ............</th>
<th align="right" valign="middle"><?php  ?></th>
<th align="right" valign="middle"><?php ?></th>
<th align="right" valign="middle"><?php ?></th>
<th align="center" valign="middle" class="ui_button font9px"><?php echo '';?></th>
</tr>
<!-- end all -->
<tr><td colspan="7" align="left"></td></tr>
</table>
  