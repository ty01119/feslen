<script>
var json_url = '<?php echo $json_url;?>';
var post_url = '<?php echo $post_url;?>';
var read_only = <?php echo $read_only;?>;
</script>
<script type="text/javascript" src="<?php echo base_url();?>js/payment_edit_view.js"></script>

<div style="padding:10px 0;" id="inner-wrapper">
<div style="width:680px;background: #e1e1e1;color:#000000;" id="inner-text">


<table border="0" cellpadding="5" cellspacing="0" style="width:680px;padding:10px;">



<tr class="bgc">
<td>
<?php
echo form_label('Payment ID:', 'id');?>
</td><td>
<?php
$attr = array('name' => 'id', 'id' => 'id', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;', 'readonly' => 'readonly') ;
echo form_input($attr);

?>  
</td>
<td>
<?php
echo form_label('Date:', 'date');?>
</td><td>
<?php
$attr = array('name' => 'date', 'id' => 'date', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;', 'readonly' => 'readonly'); 
echo form_input($attr);
?>
</td>
</tr>

<tr><td colspan="4"></td></tr><tr><td colspan="4"></td></tr>

<tr class="bgc">
<td>
<?php
echo form_label('Invoice ID:', 'invoice_id');?>
</td><td>
<?php
$attr = array('name' => 'invoice_id', 'id' => 'invoice_id', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td>
<td>
<?php
echo form_label('Detail:', 'detail');?>
</td><td>
<?php
$attr = array('name' => 'detail', 'id' => 'detail', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;'); 
echo form_input($attr);
?>
</td></tr>

<tr><td colspan="4"></td></tr><tr><td colspan="4"></td></tr>

<tr class="bgc">
<td>
<?php
echo form_label('Paid:', 'paid');?>
</td><td>
<?php
$attr = array('name' => 'paid', 'id' => 'paid', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td>
<td>
<?php
  //echo form_label('URL:', 'url');
?>
</td><td>
<?php
  //$attr = array('name' => 'url', 'id' => 'url', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;'); 
  //echo form_input($attr);
?>
</td></tr>

<tr><td colspan="4"></td></tr><tr><td colspan="4"></td></tr>

<tr class="bgc">
<td>Payment Method: </td>
<td colspan="5">
<div class="ui_buttonset font10px">
<?php
echo form_checkbox(array('name'=>'payment', 'id'=>'pmt_cash', 'value'=>'Cash', 'style'=>'margin:10px'));
echo form_label('Cash', 'pmt_cash');
echo form_checkbox(array('name'=>'payment', 'id'=>'pmt_eftpos', 'value'=>'Eftpos', 'style'=>'margin:10px'));
echo form_label('Eftpos', 'pmt_eftpos');
echo form_checkbox(array('name'=>'payment', 'id'=>'pmt_cheque', 'value'=>'Cheque', 'style'=>'margin:10px'));
echo form_label('Cheque', 'pmt_cheque');
echo form_checkbox(array('name'=>'payment', 'id'=>'pmt_banks', 'value'=>'Banks', 'style'=>'margin:10px'));
echo form_label('Banks', 'pmt_banks');
echo form_checkbox(array('name'=>'payment', 'id'=>'pmt_other', 'value'=>'Other', 'style'=>'margin:10px'));
echo form_label('Other', 'pmt_other');
?>
</div>
</td>
</tr>

<tr><td colspan="4"></td></tr><tr><td colspan="4"></td></tr>
<!--
<tr class="bgc">
<td>
<?php
echo form_label('Discount:', 'discount');?>
</td><td>
<?php
$attr = array('name' => 'discount', 'id' => 'discount', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td>
<td>
<?php
echo form_label('Pricing:', 'pricing');?>
</td><td>
<?php
$attr = array('name' => 'pricing', 'id' => 'pricing', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;'); 
echo form_input($attr);
?>
</td></tr>

<tr><td colspan="4"></td></tr><tr><td colspan="4"></td></tr>

<tr class="bgc">
<td>
<?php
echo form_label('Address:', 'address');?>
</td><td>
<?php
$attr = array('name' => 'address', 'id' => 'address', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td>
<td>
<?php
echo form_label('City:', 'city');?>
</td><td>
<?php
$attr = array('name' => 'city', 'id' => 'city', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;'); 
echo form_input($attr);
?>
</td></tr>

<tr><td colspan="4"></td></tr><tr><td colspan="4"></td></tr>

<tr class="bgc">
<td>
<?php
echo form_label('Ship Name:', 'ship_name');?>
</td><td colspan="5">
<?php
$attr = array('name' => 'ship_name', 'id' => 'ship_name', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td></tr>

<tr><td colspan="4"></td></tr>

<tr class="bgc">
<td>
<?php
echo form_label('Ship Address:', 'ship_address');?>
</td><td>
<?php
$attr = array('name' => 'ship_address', 'id' => 'ship_address', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td>
<td>
<?php
echo form_label('Ship City:', 'ship_city');?>
</td><td>
<?php
$attr = array('name' => 'ship_city', 'id' => 'ship_city', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;'); 
echo form_input($attr);
?>
</td>

</tr>

<tr class="bgc">
<td valign="top">
<?php
echo form_label('Notes:', 'notes');?>
</td>
<td colspan="3">
<textarea name="notes" id="notes" rows="2" style="width:500px" class="cst ui-corner-all"></textarea>
</td>
</tr>
-->

</table>

   
</div>
<br style="clear:both">
<div style="float: left; padding:0 5px;" class="ui_button">
<?php 
echo form_input(array(
			 'type'  => 'button',
			 'name'  => 'save',
			 'id'    => 'save',
			 'value' => 'Save'
			 )
		   );
?>
</div>
<div style="float: right; padding:0 5px;" class="ui_button">
<?php 
  echo form_input(array(
			 'type' => 'button',
                         'name' => 'close',
			 'id'   => 'close',
			 'value' => 'Close'
			 )
		   );
?>
</div>


</div>

