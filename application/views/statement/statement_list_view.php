  <script>
  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#customer" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/cust_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
			


		      });

 
	$('.cust_report').click(function(){
	    var name = $('#customer').val();
	    openView('statement/view','statement_view');
	  });
	  
 
	$('.edit_customer')
			.click(function () {  
				cust_name = this.id;  
				openView(base_url + 'customer/edit/' + escape(cust_name), 'edit-client');
			});
 
    });
</script>

<?php echo form_open('statement/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="ui_button font10px" style="width:200px">
<?php 
echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px">
<?php 
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => '',
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		 );
?>
</td><td align="left" class="ui_button font10px">


<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td></tr>

<tr><td align="left" class="font10px">

</td><td align="left" class="font10px">

</td><td align="left" class="ui_button font10px">
</td><td align="left" class="ui_button font10px">
</td></tr>
</table>
<?php echo form_close(); ?>


 

		
<table border="0" cellpadding="5" cellspacing="0"  style="margin:0 0 30px 0; min-width:900px; float: left; width:100%;">

<tr>
<th colspan="7" align="left"></th>
</tr>

 

<tr class="bgc">
<td colspan="7" align="right" class="ui_button font9px">
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle" width="60">
 
</th>
<th align="left" valign="middle" >
Customer
</th>
<th align="left" valign="middle" width="120">

</th>
<th align="right" valign="middle" width="70">
 
</th>
<th align="right" valign="middle" width="70">
Total
</th>

<th align="right" valign="middle" width="20"></th>

<th align="center" valign="middle" width="190">Operations</th>
</tr>

<?php 
$accu = new stdClass;
$accu->subtotal = $accu->total = 0;


foreach($customers as $name => $debit):
 

?> 
<tr class="bgc" id="cust-<?php echo url_title($name);?>">
<td align="left" valign="middle">

<div class="frt pt edit_customer" style="margin:2px 2px 2px 20px;" title="Edit Customer" id="<?php echo $name;?>" >
<span class="ui-icon ui-icon-gear flt"></span> 
</div>
</td>
<td align="left" valign="middle">
<?php echo $name;?>
</td>
<td align="left" valign="middle">
</td>
<td align="right" valign="middle"></td>
<td align="right" valign="middle"><?php echo number_format($debit,2,'.',',');?></td>
<td align="right" valign="middle"></td>
<td align="center" valign="middle" class="ui_button font9px">
<?php
$attributes = array('id' => 'myform', 'target' => 'statement_view', 'style' => 'float:left;');
echo form_open('statement/view', $attributes);
echo form_input(
		array(
		      'type'	      => 'hidden',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to'])
		      )
		);
echo form_input(
		array(
		      'type'	      => 'hidden',
		      'name'        => 'customer',
		      'id'        => 'customer',
		      'value'	  => $name
		      ) 
		);
echo form_button($data = array('class' => 'cust_report',   'content' => 'View', 'type' => 'submit'));
 
echo form_close();
?>
<?php
$attributes = array('id' => 'myform', 'target' => 'statement_pdf', 'style' => 'float:left;');
echo form_open('statement/pdf', $attributes);
echo form_input(
		array(
		      'type'	      => 'hidden',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to'])
		      )
		);
echo form_input(
		array(
		      'type'	      => 'hidden',
		      'name'        => 'customer',
		      'id'        => 'customer',
		      'value'	  => $name
		      ) 
		);
echo form_button($data = array('class' => 'statement-pdf',   'content' => 'PDF', 'type' => 'submit'));
 
echo form_close();
?>
<?php
$attributes = array('id' => 'myform', 'target' => '_self', 'style' => 'float:left;');
echo form_open('statement/email_msg', $attributes);
echo form_input(
		array(
		      'type'	      => 'hidden',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to'])
		      )
		);
echo form_input(
		array(
		      'type'	      => 'hidden',
		      'name'        => 'customer',
		      'id'        => 'customer',
		      'value'	  => $name
		      ) 
		);
echo form_button($data = array('class' => 'statement-email',   'content' => 'Email', 'type' => 'submit'));
 
echo form_close();
?>
<?php 
 
//$data = array('class' => 'invo-lib-btn','content' => 'PDF', 'onclick'=>"window.location='".site_url("statement/pdf/")."'");
//echo form_button($data);  
 
?>
</td></tr>
<?php endforeach;?>

<tr><td colspan="7" align="left"></td></tr>
 
</table>

  