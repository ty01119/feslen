<style>
      body {
      background-color:#ffffff;
      }
      table {
      margin: 5%;
      width:90%;
      border-collapse: collapse; clear:both;
      }
      table td,
      table th {
      padding: 0; 
      } 
      #title{
      width:40%;
      height:36px;
      color:#ffffff;
      background-color:#00064e;
      text-align: center;
      }
      #logo{
      width:10%;
      }
      #name{
      width:40%; text-align: center;
      }
      .title2{
      width:20%;
      height:24px;
      text-align:center;
      background-color:#004080;
      }
      .title3{
      width:20%;
      height:24px;
      text-align:center;
      }
      .info{
      width:20%;
      height:24px;
      text-align:left;
      }
      .l2title{
      width:15%;
      }
      .l2cnt{
      width:25%;
      }
      .hbsp15{
      width:10%;
      }
      .vline{
      display:block;height:14px;
      border-top:1px solid #e1e1e1;
      }
      .vbsp15{
      height:18px; 
      }
      
      
   
      .cb1{
      height: 20px; 
      background-color:#004080;
      }
      
      
      .white20b{
      color: #ffffff;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 24px;
      }
      .black20b{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 24px;
      }
      .white12b{
      color: #ffffff;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 14px;
      }
      .white8b{
      color: #ffffff;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 12px;
      }
      .black12b{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 14px;
      }
      .black8b{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 11px;
      }
      .black8{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-size: 12px;
      }
      
            
      .ltvline{ 
      border-left:1px solid #e1e1e1;
      }
      .rtvline{ 
      border-right:1px solid #e1e1e1;
      }
      
      .hline{  
      border-top:1px solid #e1e1e1;
      } 
      
      .l4lt{
      width:45%;
      }
      .l4mi{
      width:15%;
      }
      .l4sp1{
      width:2%;
      }
      .l4vline{
      width:2%;
      }
      .l4item{
      width:10%;
      text-align:left;
      }
     .l4nbsp{
      width:5%;
      }
      .l4value{
      width:11%;
      text-align:right;
      }
 /* end */
.total-hr{
	background-color:#004080;
	height:40px;
	}
.total-cell {
	width:100px;
	height:40px;      
	text-align:center;
	}
	
.itemst-cell1{
      width:70px; 
      text-align:left;
      }
      .itemst-cell2{
      width:278px; 
      text-align:left;
      }
      .itemst-cell3{
      width:70px;
      text-align:right;
      }
      .itemst-cell3-1{
      width:70px;
      text-align:right;
      }
      .itemst-cell4{
      width:70px;
      text-align:right;
      }

      .itemst-cell5{
      width:70px;
      text-align:right;
      }
      .itemst-cell6{
      width:70px;
      text-align:right;
      }
      .cb1{
      height:14px;
      background-color:#004080;
      }
.white20b{
      color: #ffffff;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 24px;
      }
      .black20b{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 24px;
      }
      .white12b{
      color: #ffffff;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 14px;
      }

      .white8b{
      color: #ffffff;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 12px;
      }
      .black14b{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 14px;
      }
      .black12b{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 12px;
      }
      .black8b{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 11px;
      }
      .black8{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-size: 12px;
      }    
            .ltvline{
      
      border-left:1px solid #e1e1e1;
      } 
 </style> 
    <table class="collapse" border="0" align="center"> 
      <tbody>
	<tr>
	  <td colspan="2" id="title"><span class="white20b">STATEMENT</td>
	  <td rowspan="3" id="logo">&nbsp;</td>
	  <td rowspan="3" id="name"><span class="black20b"><?php echo $branch->biz_name;?></span>

	    <br /><br />
	    <span class="black14b"><?php echo $branch->address;?></span></td>
	</tr>
	<tr>
	  <td class="title2"><span class="white12b">CUSTOMER NO.</span></td>
	  <td class="title2"><span class="white12b">DATE</span></td>
	</tr>

	<tr>
	 <td class="title3">
	 <span class="black14b"><?php echo $customer->id;?></span>
	 </td>
	  <td class="title3">
	 <span class="black14b"><?php echo date("d/m/y");?></span>
	 </td>
	</tr>

	<tr>
	  <td colspan="2"><span class="hline"></span></td>
	  <td ></td>
	  <td ></td>
	</tr>


	<tr>
	  <td colspan="2" class="info"><span class="black8b">
<?php echo 'Bank:'.nbs(2).$branch->bank_acc_num.nbs(4).'GST NO.:'.nbs(2).$branch->gst_num;?>
</span></td> 
	  <td></td>
	  <td class="info"><span class="black8b">
<?php echo 'Tel.:'.nbs(2).$branch->telephone.nbs(4).'Fax:'.nbs(2).$branch->fax;?>
</span></td>
	</tr>
      </tbody>

    </table>
    <table border="0" align="center"> 
      <tbody>
	<tr>
	  <td class="l2title"><span class="black14b">Customer</span></td>
	  <td class="l2cnt"><span class="black8"><?php echo $customer->name;?></span></td>
	  <td class="hbsp15"></td>
	  <td class="l2titel"><span class="black8b">Invoices Till</span></td>

	  <td class="l2cnt"><span class="black8"><?php echo date('d/m/Y', $date['to']);?></span></td>
	</tr>
	<tr><td colspan="2"><span class="hline"></span></td>
	  <td class="hbsp15"></td>
	  <td colspan="2"><span class="hline"></span></td>
	</tr>
	<tr><td colspan="5" class="vbsp15"></td></tr>
	<tr>
	  <td class="l2title"> 
	    <span class="black8b"><?php echo $customer->id;?></span>

	  </td>
	  <td class="l2cnt">
	    <span class="black8">
	      <?php echo $customer->address;?>
	      <br />
	      <?php echo $customer->city;?>
	    </span>
	  </td>
	  <td class="hbsp15"></td>
	  <td class="l2titel"><span class="black8b">Ship Info</span></td>

	  <td class="l2cnt"><span class="black8"><?php echo $customer->ship_name;?></span></td>
	</tr>
	<tr>
	  <td class="l2title"><span class="black8b">&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
	  <td class="l2cnt"><span class="black8">
 	      <i>Tel&nbsp;&nbsp;<?php echo $customer->phone;?></i><br />
	      <i>Fax&nbsp;&nbsp;<?php echo $customer->fax;?></i><br />
	      <i>Mol&nbsp;&nbsp;<?php echo $customer->cellphone;?></i><br />
	  </span></td>
	  <td class="hbsp15"></td>
	  <td class="l2titel"><span class="black8b">&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
	  <td class="l2cnt">
	    <span class="black8">
	      <?php echo $customer->ship_address;?>
	      <br />
	      <?php echo $customer->ship_city;?>
	    </span>
	  </td>
	</tr>
      </tbody>
    </table>
    <table border="0" align="center"> 
      <tbody> 
	<tr> 
	  <td class="itemst-cell1 cb1"><span class="white8b">&nbsp;&nbsp;Invoice ID</span></td> 
	  <td class="itemst-cell2 cb1"><span class="white8b">&nbsp;&nbsp;Invoice Info</span></td> 
	  <td class="itemst-cell3 cb1"><span class="white8b">Subtotal&nbsp;&nbsp;</span></td> 
	  <td class="itemst-cell3-1 cb1"><span class="white8b">Tax&nbsp;&nbsp;</span></td> 
	  <td class="itemst-cell4 cb1"><span class="white8b">Total&nbsp;&nbsp;</span></td> 
	  <td class="itemst-cell5 cb1"><span class="white8b">Paid&nbsp;&nbsp;</span></td> 
	  <td class="itemst-cell6 cb1"><span class="white8b">Debit&nbsp;&nbsp;</span></td> 
	</tr>




<?php 
 

$mktime_m = date("m", $date['to']);
$mktime_d = date("d", $date['to']);
$mktime_y = date("Y", $date['to']);


$accu = new stdClass;
$accu->debit = $accu->subtotal = $accu->total = 0;

$aucc60plus = new stdClass;
$accu60plus->debit = $accu60plus->subtotal = $accu60plus->total = 0;

$aucc30_60 = new stdClass;
$accu30_60->debit = $accu30_60->subtotal = $accu30_60->total = 0;

$aucc30 = new stdClass;
$accu30->debit = $accu30->subtotal = $accu30->total = 0;

$aucc0 = new stdClass;
$accu0->debit = $accu0->subtotal = $accu0->total = 0;

foreach($query->result() as $row):

$row->debit = $row->total - $row->paid;
$accu->subtotal += $row->subtotal;
$accu->total += $row->total;
$accu->debit += $row->debit;
 

if (strtotime($row->date) < mktime(0, 0, 0, $mktime_m-2, 1, $mktime_y))
 $accu60plus->debit += $row->debit;
else if (strtotime($row->date) < mktime(0, 0, 0, $mktime_m-1, 1, $mktime_y))
 $accu30_60->debit += $row->debit; 
else if (strtotime($row->date) < mktime(0, 0, 0, $mktime_m, 1, $mktime_y))
 $accu30->debit += $row->debit; 
else
 $accu0->debit += $row->debit;
?> 
<tr>
	  
	  <td class="itemst-cell1 ltvline">
	    <span class="black8">&nbsp;&nbsp;<?php echo $row->id;?> </span>
	  </td>

 
	  <td class="itemst-cell2 ltvline">
	    <span class="black8">&nbsp;&nbsp;Date:&nbsp;&nbsp;<?php echo date('d/m/Y', strtotime($row->date));?>
		<?php echo ($row->info) ? '&nbsp;&nbsp;Info:&nbsp;&nbsp;'.$row->info : '';?>
	    </span>
	  </td>
	  
	  <td class="itemst-cell3 ltvline">
	    <span class="black8"><?php echo number_format($row->subtotal,2,'.',',');?>&nbsp;&nbsp;</span>
	  </td>
	  
	  <td class="itemst-cell3-1 ltvline">
	    <span class="black8"><?php echo number_format($row->tax,2,'.',',');?>&nbsp;&nbsp;</span>
	  </td>
	  
	  <td class="itemst-cell4 ltvline">
	    <span class="black8"><?php echo number_format($row->total,2,'.',',');?>&nbsp;&nbsp;</span>
	  </td>
	  
	  <td class="itemst-cell5 ltvline">
	    <span class="black8"><?php echo number_format($row->paid,2,'.',',');?>&nbsp;&nbsp;</span>
	  </td>
	  
	  <td class="itemst-cell6 ltvline rtvline">
	    <span class="black8"><?php echo number_format($row->debit,2,'.',',');?>&nbsp; &nbsp;</span>
	  </td>
	  
	</tr>
<?php endforeach;?>



	<tr>
	  
	  <td class="itemst-cell1 ltvline">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  
	  <td class="itemst-cell2 ltvline">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  
	  <td class="itemst-cell3 ltvline">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  
	  <td class="itemst-cell3-1 ltvline">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  
	  <td class="itemst-cell4 ltvline">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  
	  <td class="itemst-cell5 ltvline">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  
	  <td class="itemst-cell6 ltvline rtvline">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  
	</tr> 

	<tr>
	  <td colspan="14" class="cb1"></td>
	</tr>
      </tbody>
    </table>

    <table border="2" bordercolor="#000000" align="center" > 
      <tbody>
 	<tr class="total-hr">
 
	  <td class="total-cell"><span class="white12b"> &gt; 2 Months</span></td>
	 
	  <td class="total-cell"><span class="white12b"> 1 - 2 Months </span></td>
	 
	  <td class="total-cell"><span class="white12b"> 0 -1 Month </span></td>
	  
	  <td class="total-cell"><span class="white12b">Current Due</span></td>
 
	  <td class="total-cell"><span class="white12b">Total NZD</span></td>
 
	  <td class="total-cell"><span class="white12b">Amount Due</span></td>
 
	</tr>
	<tr>
	 
	  <td class="total-cell">
	    <span class="black8"><?php echo number_format($accu60plus->debit, 2, '.', ',');?></span>
	  </td>

 	  <td class="total-cell">
	    <span class="black8"><?php echo number_format($accu30_60->debit, 2, '.', ',');?></span>
	  </td>
 
	  <td class="total-cell">
	    <span class="black8"><?php echo number_format($accu30->debit, 2, '.', ',');?></span>
	  </td>
 
	  <td class="total-cell">
	    <span class="black8"><?php echo number_format($accu0->debit, 2, '.', ',');?></span>
	  </td>
 
	  <td class="total-cell">
	    <span class="black8"><?php echo number_format($accu->debit, 2, '.', ',');?></span>
	  </td>
 
	  <td class="total-cell">
	    <span class="black8"><?php echo number_format($accu->debit, 2, '.', ',');?></span>
	  </td>
 
	</tr>
      </tbody>
    </table>