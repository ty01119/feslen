<style>
      body {
      background-color:#ffffff;
      }
      table {
      margin: 5%;
      width:90%;
      border-collapse: collapse; clear:both;
      }
      table td,
      table th {
      padding: 0; 
      } 
      #title{
      width:40%;
      height:36px;
      color:#ffffff;
      background-color:#00064e;
      text-align: center;
      }
      #logo{
      width:10%;
      }
      #name{
      width:40%; text-align: center;
      }
      .title2{
      width:20%;
      height:24px;
      text-align:center;
      background-color:#004080;
      }
      .title3{
      width:20%;
      height:24px;
      text-align:center;
      }
      .info{
      width:20%;
      height:24px;
      text-align:left;
      }
      .l2title{
      width:15%;
      }
      .l2cnt{
      width:25%;
      }
      .hbsp15{
      width:10%;
      }
      .vline{
      display:block;height:14px;
      border-top:1px solid #e1e1e1;
      }
      .vbsp15{
      height:18px; 
      }
      
      
      .item-cell1{
      width: 50% 
      text-align:left;
      }
      .item-cell2{
      width:10%;  
      text-align:right;
      }
      .item-cell3{
      width:10%;  
      text-align:right;
      }
      .item-cell4{
      width:10%;   
      text-align:right;
      }
      .item-cell5{
      width:10%;  
      text-align:right;
      }
      .cb1{
      height: 20px; 
      background-color:#004080;
      }
      
      
      .white20b{
      color: #ffffff;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 24px;
      }
      .black20b{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 24px;
      }
      .white12b{
      color: #ffffff;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 14px;
      }
      .white8b{
      color: #ffffff;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 12px;
      }
      .black12b{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 14px;
      }
      .black8b{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 11px;
      }
      .black8{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-size: 12px;
      }
      
            
      .ltvline{ 
      border-left:1px solid #e1e1e1;
      }
      .rtvline{ 
      border-right:1px solid #e1e1e1;
      }
      
      .hline{  
      border-top:1px solid #e1e1e1;
      } 
      
      .l4lt{
      width:45%;
      }
      .l4mi{
      width:15%;
      }
      .l4sp1{
      width:2%;
      }
      .l4vline{
      width:2%;
      }
      .l4item{
      width:10%;
      text-align:left;
      }
     .l4nbsp{
      width:5%;
      }
      .l4value{
      width:11%;
      text-align:right;
      }
 
    </style>

  
 

    <table class="collapse" border="0" align="center"> 
      <tbody>
	<tr>
	  <td colspan="2" id="title"><span class="white20b">TAX INVOICE</td>
	  <td rowspan="3" id="logo">
	  <!--
	  <img src="<?php echo 
	  ($branch->branch == 'websolution') ? 
	  base_url().'images/inv-logo-3aweb.png' :
	  base_url().'images/inv-logo-3a.jpg';
	  ?>" style="width:78px;height:78px;"/>
	  -->
	  </td>
	  <td rowspan="3" id="name"><span class="black20b"><?php echo $branch->biz_name;?></span>
	    <br /><br /> 
	    <span class="black12b"><?php echo $branch->address;?></span>
	    <br /> 
	    <span class="black8b"  >www.3a.co.nz
<?php echo nbs(4).'Email:'.nbs(2).$branch->email;?>
</span>
</td>
	</tr>
	<tr>
	  <td class="title2"><span class="white12b">NUMBER</span></td>
	  <td class="title2"><span class="white12b">DATE</span></td>
	</tr>
	<tr>
	 <td class="title3">
	 <span class="black12b"><?php echo $invoice->id;?></span>
	 </td>
	  <td class="title3">
	 <span class="black12b"><?php echo $invoice->date;?></span>
	 </td>
	</tr>

	<tr>
	  <td colspan="2"><span class="hline"></span></td>
	  <td ></td>
	  <td ></td>
	</tr>


	<tr>
	  <td colspan="2" class="info"><span class="black8b">
<?php echo 'Bank:'.nbs(2).$branch->bank_acc_num.nbs(4).'GST NO.:'.nbs(2).$branch->gst_num;?>
</span></td> 
	  <td></td>
	  <td class="info"><span class="black8b">
<?php echo 'Tel.:'.nbs(2).$branch->telephone.nbs(4).'Fax:'.nbs(2).$branch->fax;?>
</span></td>
	</tr>
      </tbody>
    </table>
    <table border="0" align="center"> 
      <tbody>
	<tr>
	  <td class="l2title"><span class="black12b">Customer</span></td>
	  <td class="l2cnt"><span class="black8"><?php echo $invoice->cust_name;?></span></td>
	  <td class="hbsp15"></td>
	  <td class="l2title"><span class="black8b">Info</span></td>
	  <td class="l2cnt"><span class="black8"><?php echo $invoice->info;?></span></td>
	</tr>
	<tr><td colspan="2"><span class="hline"></span></td>
	  <td class="hbsp15"></td>
	  <td colspan="2"><span class="hline"></span></td>
	</tr>
	<tr><td colspan="5" class="vbsp15"></td></tr>
	<tr>
	  <td class="l2title"> 
	    <span class="black8b"><?php echo $customer->id;?></span>
	  </td>
	  <td class="l2cnt">
	    <span class="black8">
	      <?php echo $customer->address;?>
	      <br />
	      <?php echo $customer->city;?>
	    </span>
	  </td>
	  <td class="hbsp15"></td>
	  <td class="l2titel"><span class="black8b">Ship to</span></td>
	  <td class="l2cnt"><span class="black8"><?php echo $customer->ship_name;?></span></td>
	</tr>
	<tr>
	  <td class="l2title"><span class="black8b">&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
	  <td class="l2cnt"><span class="black8">
	      <i>Tel&nbsp;&nbsp;<?php echo $customer->phone;?></i><br />
	      <i>Fax&nbsp;&nbsp;<?php echo $customer->fax;?></i><br />
	      <i>Mol&nbsp;&nbsp;<?php echo $customer->cellphone;?></i><br />
	  </span></td>
	  <td class="hbsp15"></td>
	  <td class="l2titel"><span class="black8b">&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
	  <td class="l2cnt">
	    <span class="black8">
	      <?php echo $customer->ship_address;?>
	      <br />
	      <?php echo $customer->ship_city;?>
	    </span>
	  </td>
	</tr>
      </tbody>
    </table>
    <table border="0" align="center"> 
      <tbody>
	<tr>

	  <td class="item-cell1 cb1"><span class="white8b">&nbsp;&nbsp;Description</span></td>

	  <td class="item-cell2 cb1"><span class="white8b">QTY&nbsp;&nbsp;</span></td>

	  <td class="item-cell3 cb1"><span class="white8b">U/P&nbsp;&nbsp;</span></td>

	  <td class="item-cell4 cb1"><span class="white8b">DISC&nbsp;&nbsp;</span></td>

	  <td class="item-cell5 cb1"><span class="white8b">Amount&nbsp;&nbsp;</span></td>

	</tr>
<?php foreach ($contents as $item):?>
	<tr>
 
	  <td class="item-cell1 ltvline">
	    <div class="black8" style="padding: 0 1%;"><?php echo $item->product;?></div>
	  </td>
	  
	  <td class="item-cell2 ltvline">
	    <span class="black8"><?php echo ($item->unit_price != 0)? $item->quantity : '';?></span>
	  </td>
 
	  <td class="item-cell3 ltvline">
	    <span class="black8"><?php echo ($item->unit_price != 0) ? $item->unit_price : '';?></span>
	  </td>
 
	  <td class="item-cell4 ltvline">
	    <span class="black8"><?php echo ($item->unit_price != 0) ? $item->discount.'%' : '';?></span>
	  </td>
	 
	  <td class="item-cell5 ltvline rtvline">
	    <span class="black8">
	      <?php echo ($item->unit_price != 0) ? number_format($item->unit_price * $item->quantity * (1 - $item->discount / 100), 2, '.', ',') : '';?>
	    </span>
	  </td>
	 
	</tr>
<?php endforeach;?>

<?php if ($invoice->discount != 0) { ?>
	<tr>
 
	  <td class="item-cell1 ltvline"><span class="black8">PROMOTION: ( Discount: <?php echo $invoice->discount;?>% off )
	  
	  </span></td>
	 
	  <td class="item-cell2 ltvline"><span class="black8"></span></td>
	 
	  <td class="item-cell3 ltvline"><span class="black8"></span></td>
 
	  <td class="item-cell4 ltvline"><span class="black8"></span></td>
	 
	  <td class="item-cell5 ltvline rtvline"><span class="black8"> -<?php echo number_format($invoice->subtotal / (1 - $invoice->discount / 100) - $invoice->subtotal, 2, '.', ',') ?></span></td>
	 
	</tr>
<?php } ?>
	<tr>
	  <td class="item-cell1 ltvline"><span class="black8"></span></td> 
	  <td class="item-cell2 ltvline"><span class="black8"></span></td>
	  <td class="item-cell3 ltvline"><span class="black8"></span></td> 
	  <td class="item-cell4 ltvline"><span class="black8"></span></td> 
	  <td class="item-cell5 ltvline rtvline"><span class="black8">&nbsp;</span></td> 
	</tr>
	 
	<tr>
	  <td colspan="5" class="cb1"> </td>
	</tr>
      </tbody>
    </table>

    <table border="0" align="center"> 
      <tbody>
	<tr>
	  <td class="l4lt"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td class="l4mi"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td class="l4sp1"></td>
	  <td class="l4vline ltvline"></td>
	  <td class="l4item"><span class="black8">&nbsp;&nbsp;Subtotal</span></td>
	  <td class="l4nbsp"></td>
	  <td class="l4value"><span class="black8"><?php echo $invoice->subtotal;?>&nbsp;&nbsp;</span></td>
	</tr>
	<tr>
	  <td class="l4lt"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td class="l4mi"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td class="l4sp1"></td>
	  <td class="l4vline ltvline"></td>
	  <td class="l4item"><span class="black8">&nbsp;&nbsp;GST</span></td>
	  <td class="l4nbsp"></td>
	  <td class="l4value"><span class="black8"><?php echo $invoice->tax;?>&nbsp;&nbsp;</span></td>
	</tr>
	<tr>
	  <td class="l4lt"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td class="l4mi"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td class="l4sp1"></td>
	  <td class="l4vline ltvline"></td>
	  <td class="l4item"><span class="black8">&nbsp;&nbsp;Total</span></td>
	  <td class="l4nbsp"></td>
	  <td class="l4value"><span class="black8"><?php echo $invoice->total;?>&nbsp;&nbsp;</span></td>
	</tr>
	<tr>
	  <td class="l4lt"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td class="l4mi"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td class="l4sp1 hline"></td>
	  <td class="l4vline hline ltvline"></td>
	  <td class="l4item"><span class="black8">&nbsp;&nbsp;Paid</span></td>
	  <td class="l4nbsp"></td>
	  <td class="l4value"><span class="black8"><?php echo $invoice->paid;?>&nbsp;&nbsp;</span></td>
	</tr>
	<tr>
	  <td class="l4lt"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td class="l4mi"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td class="l4sp1"></td>
	  <td class="l4vline ltvline"></td>
	  <td class="l4item"><span class="black8">&nbsp;&nbsp;Balance</span></td>
	  <td class="l4nbsp"></td>
	  <td class="l4value">
	    <span class="black8"><?php echo number_format($invoice->total - $invoice->paid, 2, '.', ',');?>&nbsp;&nbsp;</span>
	  </td>
	</tr>
      </tbody>
    </table>
  