<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
 
<link rel="shortcut icon" href="<?php echo base_url(); ?>images/favicon.ico">

<?php $this->load->view('includes/jquery_ui'); ?>
<?php



 
?>

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.functions.js"></script>
<link href="<?php echo base_url(); ?>css/jquery.css" rel="stylesheet" type="text/css" media="screen" />
   <style>

      table {
      margin-top: 10px;
      width:678px;
      border-collapse: collapse; clear:both;
      }
      table td,
      table th {
      padding: 0; 
      } 
      #title{
      width:300px;
      height:36px;
      color:#ffffff;
      background-color:#00064e;
      text-align: center;
      }
      #logo{
      width:78px;
      }
      #name{
      width:300px; text-align: center;
      }
      .title2{
      width:150px;
      height:24px;
      text-align:center;
      background-color:#004080;
      }
      .title3{
      width:150px;
      height:24px;
      text-align:center;
      }
      .info{
      width:150px;
      height:24px;
      text-align:left;
      }
      .l2title{
      width:90px;
      }
      .l2cnt{
      width:240px;
      }
      .hbsp15{
      width:18px;
      }
      .vline{
      display:block;height:14.4px;
      border-top:1px solid #e1e1e1;
      }
      .vbsp15{
      height:18px; 
      }
      .total-hr{
	background-color:#004080;
	height:40px;
	}
      .total-cell {
	width:100px;
	height:40px;      text-align:center;
	}
      .item-cell1{
      width:70px; 
      text-align:left;
      }
      .item-cell2{
      width:278px; 
      text-align:left;
      }
      .item-cell3{
      width:70px;
      text-align:right;
      }
      .item-cell3-1{
      width:70px;
      text-align:right;
      }
      .item-cell4{
      width:70px;
      text-align:right;
      }

      .item-cell5{
      width:70px;
      text-align:right;
      }
      .item-cell6{
      width:70px;
      text-align:right;
      }
      .cb1{
      height:14.4px;
      background-color:#004080;
      }
      .white20b{
      color: #ffffff;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 24px;
      }
      .black20b{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 24px;
      }
      .white12b{
      color: #ffffff;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 14.4px;
      }

      .white8b{
      color: #ffffff;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 12px;
      }
      .black14b{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 14.4px;
      }
      .black12b{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 12px;
      }
      .black8b{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 11px;
      }
      .black8{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-size: 12px;
      }
      .ltvline{
      display:inline-block;height:16px;
      border-left:1px solid #e1e1e1;
      }
      #l4hline1{
      display:inline-block;width:14.4px;height:14.4px;
      border-top:1px solid #e1e1e1;
      }
      #l4hline2{
      display:inline-block;width:14.4px;height:14.4px;
      border-top:1px solid #e1e1e1;border-left:1px solid #e1e1e1;
      }
      #l4lt{
      width:414;
      }
      #l4mi{
      width:120px;
      }
      .l4sp1{
      width:14.4px;
      }
      .l4vline{
      width:2px;
      }
      .l4item{
      width:48px;
      text-align:left;
      }
     .l4nbsp{
      width:31.2px;
      }
      .l4value{
      width:48px;
      text-align:right;
      }


#func-menu{
margin:10px auto 10px auto;
      width:678px; 
padding:5px;
}
button {
margin:0 5px 0 5px;
}
    </style>


<script type="text/javascript">
$(document)
    .ready(function() {
	$('#print').click(function(){
		$('#func-menu').hide();
		window.print();
		$('body').fadeOut('fast', function() {
			window.close();
  		});
	});
	$('#close').click(function(){
		$('body').fadeOut('fast', function() {
			window.close();
  		});
	});

	 
}) 
</script>

  </head>
  <body>

<div id="func-menu" class="ui-widget-content ui-corner-all ui_button">
<?php
$data = array('id' => 'print', 'class' => 'font10px flt', 'content' => 'Print');
echo form_button($data);
?>

<?php
$data = array('id' => 'close', 'class' => 'font10px frt', 'content' => 'Close');
echo form_button($data);
?>
<br style="clear:both;"/>
</div>

    <table class="collapse" border="0" align="center"> 
      <tbody>
	<tr>
	  <td colspan="2" id="title"><span class="white20b">STATEMENT</td>
	  <td rowspan="3" id="logo">&nbsp;</td>
	  <td rowspan="3" id="name"><span class="black20b"><?php echo $branch->biz_name;?></span>

	    <br /><br />
	    <span class="black14b"><?php echo $branch->address;?></span></td>
	</tr>
	<tr>
	  <td class="title2"><span class="white12b">CUSTOMER NO.</span></td>
	  <td class="title2"><span class="white12b">DATE</span></td>
	</tr>

	<tr>
	 <td class="title3">
	 <span class="black14b"><?php echo $customer->id;?></span>
	 </td>
	  <td class="title3">
	 <span class="black14b"><?php echo date("d/m/y");?></span>
	 </td>
	</tr>

	<tr>
	  <td colspan="2"><span class="vline"></span></td>
	  <td ></td>
	  <td ></td>
	</tr>


	<tr>
	  <td colspan="2" class="info"><span class="black8b">
<?php echo 'Bank:'.nbs(2).$branch->bank_acc_num.nbs(4).'GST NO.:'.nbs(2).$branch->gst_num;?>
</span></td> 
	  <td></td>
	  <td class="info"><span class="black8b">
<?php echo 'Tel.:'.nbs(2).$branch->telephone.nbs(4).'Fax:'.nbs(2).$branch->fax;?>
</span></td>
	</tr>
      </tbody>

    </table>
    <table border="0" align="center"> 
      <tbody>
	<tr>
	  <td class="l2title"><span class="black14b">Customer</span></td>
	  <td class="l2cnt"><span class="black8"><?php echo $customer->name;?></span></td>
	  <td class="hbsp15"></td>
	  <td class="l2titel"><span class="black8b">Invoices Till</span></td>

	  <td class="l2cnt"><span class="black8"><?php echo date('d/m/Y', $date['to']);?></span></td>
	</tr>
	<tr><td colspan="2"><span class="vline"></span></td>
	  <td class="hbsp15"></td>
	  <td colspan="2"><span class="vline"></span></td>
	</tr>
	<tr><td colspan="5" class="vbsp15"></td></tr>
	<tr>
	  <td class="l2title"> 
	    <span class="black8b"><?php echo $customer->id;?></span>

	  </td>
	  <td class="l2cnt">
	    <span class="black8">
	      <?php echo $customer->address;?>
	      <br />
	      <?php echo $customer->city;?>
	    </span>
	  </td>
	  <td class="hbsp15"></td>
	  <td class="l2titel"><span class="black8b">Ship Info</span></td>

	  <td class="l2cnt"><span class="black8"><?php echo $customer->ship_name;?></span></td>
	</tr>
	<tr>
	  <td class="l2title"><span class="black8b">&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
	  <td class="l2cnt"><span class="black8">
 	      <i>Tel&nbsp;&nbsp;<?php echo $customer->phone;?></i><br />
	      <i>Fax&nbsp;&nbsp;<?php echo $customer->fax;?></i><br />
	      <i>Mol&nbsp;&nbsp;<?php echo $customer->cellphone;?></i><br />
	  </span></td>
	  <td class="hbsp15"></td>
	  <td class="l2titel"><span class="black8b">&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
	  <td class="l2cnt">
	    <span class="black8">
	      <?php echo $customer->ship_address;?>
	      <br />
	      <?php echo $customer->ship_city;?>
	    </span>
	  </td>
	</tr>
      </tbody>
    </table>
    <table border="0" align="center"> 
      <tbody> 
	<tr>
	  <td class="cb1"></td>
	  <td class="item-cell1 cb1"><span class="white8b">&nbsp;&nbsp;Invoice ID</span></td>
	  <td class="cb1"></td>
	  <td class="item-cell2 cb1"><span class="white8b">&nbsp;&nbsp;Invoice Info</span></td>
	  <td class="cb1"></td>
	  <td class="item-cell3 cb1"><span class="white8b">Subtotal&nbsp;&nbsp;</span></td>
	  <td class="cb1"></td>
	  <td class="item-cell3-1 cb1"><span class="white8b">Tax&nbsp;&nbsp;</span></td>
	  <td class="cb1"></td>
	  <td class="item-cell4 cb1"><span class="white8b">Total&nbsp;&nbsp;</span></td>
	  <td class="cb1"></td>
	  <td class="item-cell5 cb1"><span class="white8b">Paid&nbsp;&nbsp;</span></td>
	  <td class="cb1"></td>
	  <td class="item-cell6 cb1"><span class="white8b">Debit&nbsp;&nbsp;</span></td>
	  <td class="cb1"></td>
	</tr>




<?php 
 

$mktime_m = date("m", $date['to']);
$mktime_d = date("d", $date['to']);
$mktime_y = date("Y", $date['to']);


$accu = new stdClass;
$accu->debit = $accu->subtotal = $accu->total = 0;

$aucc60plus = new stdClass;
$accu60plus->debit = $accu60plus->subtotal = $accu60plus->total = 0;

$aucc30_60 = new stdClass;
$accu30_60->debit = $accu30_60->subtotal = $accu30_60->total = 0;

$aucc30 = new stdClass;
$accu30->debit = $accu30->subtotal = $accu30->total = 0;

$aucc0 = new stdClass;
$accu0->debit = $accu0->subtotal = $accu0->total = 0;

foreach($query->result() as $row):

$row->debit = $row->total - $row->paid;
$accu->subtotal += $row->subtotal;
$accu->total += $row->total;
$accu->debit += $row->debit;
 

if (strtotime($row->date) < mktime(0, 0, 0, $mktime_m-2, 1, $mktime_y))
 $accu60plus->debit += $row->debit;
else if (strtotime($row->date) < mktime(0, 0, 0, $mktime_m-1, 1, $mktime_y))
 $accu30_60->debit += $row->debit; 
else if (strtotime($row->date) < mktime(0, 0, 0, $mktime_m, 1, $mktime_y))
 $accu30->debit += $row->debit; 
else
 $accu0->debit += $row->debit;
?> 
<tr>
	  <td ><span class="ltvline"></span></td>
	  <td class="item-cell1">
	    <span class="black8">&nbsp;&nbsp;<?php echo $row->id;?> </span>
	  </td>

	  <td><span class="ltvline"></span></td>
	  <td class="item-cell2">
	    <span class="black8">&nbsp;&nbsp;Date:&nbsp;&nbsp;<?php echo date('d/m/Y', strtotime($row->date));?>
		<?php echo ($row->info) ? '&nbsp;&nbsp;Info:&nbsp;&nbsp;'.$row->info : '';?>
	    </span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3">
	    <span class="black8"><?php echo number_format($row->subtotal,2,'.',',');?>&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3-1">
	    <span class="black8"><?php echo number_format($row->tax,2,'.',',');?>&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell4">
	    <span class="black8"><?php echo number_format($row->total,2,'.',',');?>&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell5">
	    <span class="black8"><?php echo number_format($row->paid,2,'.',',');?>&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell6">
	    <span class="black8"><?php echo number_format($row->debit,2,'.',',');?>&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	</tr>
<?php endforeach;?>



	<tr>
	  <td ><span class="ltvline"></span></td>
	  <td class="item-cell1">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell2">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3-1">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell4">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell5">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell6">
	    <span class="black8">&nbsp; &nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	</tr>
	<tr>
	  <td ><span class="ltvline"></span></td>
	  <td class="item-cell1">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell2">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3-1">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell4">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell5">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell6">
	    <span class="black8">&nbsp; &nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	</tr>
	<tr>
	  <td ><span class="ltvline"></span></td>
	  <td class="item-cell1">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell2">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3-1">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell4">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell5">
	    <span class="black8">&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell6">
	    <span class="black8">&nbsp; &nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	</tr>


	<tr>
	  <td colspan="14" class="cb1"></td>
	</tr>
      </tbody>
    </table>

    <table border="2" bordercolor="#000000" align="center" > 
      <tbody>
 	<tr class="total-hr">
 
	  <td class="total-cell"><span class="white12b"> &gt; 2 Months</span></td>
	 
	  <td class="total-cell"><span class="white12b"> 1 - 2 Months </span></td>
	 
	  <td class="total-cell"><span class="white12b"> 0 -1 Month </span></td>
	  
	  <td class="total-cell"><span class="white12b">Current Due</span></td>
 
	  <td class="total-cell"><span class="white12b">Total NZD</span></td>
 
	  <td class="total-cell"><span class="white12b">Amount Due</span></td>
 
	</tr>
	<tr>
	 
	  <td class="total-cell">
	    <span class="black8"><?php echo number_format($accu60plus->debit, 2, '.', ',');?></span>
	  </td>

 	  <td class="total-cell">
	    <span class="black8"><?php echo number_format($accu30_60->debit, 2, '.', ',');?></span>
	  </td>
 
	  <td class="total-cell">
	    <span class="black8"><?php echo number_format($accu30->debit, 2, '.', ',');?></span>
	  </td>
 
	  <td class="total-cell">
	    <span class="black8"><?php echo number_format($accu0->debit, 2, '.', ',');?></span>
	  </td>
 
	  <td class="total-cell">
	    <span class="black8"><?php echo number_format($accu->debit, 2, '.', ',');?></span>
	  </td>
 
	  <td class="total-cell">
	    <span class="black8"><?php echo number_format($accu->debit, 2, '.', ',');?></span>
	  </td>
 
	</tr>
	
 

      </tbody>
    </table>
    
  </body> 
</html>