  <script>
  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#obj_branch" )
	.autocomplete({
			minLength: 0,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo base_url();?>ajax/branch_names',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
		      }).click(function() {
	  $(this).autocomplete( "search" , '' );
	});

      
    });
</script>
<style type="text/css">
.fp {
	color: black;
}
.fup {
	background-color: LightSteelBlue; color: black;
}
.uf {
	background-color: #C68E17; color: black;
}
.sample {
display: inline-block;
width: 160px;
height:20px;
}
.sam-box {
display: inline-block;
width: 10px;
padding: 0 4px;
}
</style>
<?php echo form_open('mgmt_cost/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >

<tr><td align="left" class="font10px">
<span class="ui_buttonset">
<?php
$data = array('content' => 'Cost Management', 'style' => 'margin:5px 0;', 'onclick'=>"window.location.href='".site_url('mgmt_cost')."'");
echo form_button($data);
$data = array('class' => '','content' => '+', 'onclick'=>"openView('".site_url("mgmt_cost/add")."','Add Domain');");
echo form_button($data);
?>
</span>
<span class="ui_buttonset"></span>
</td>
<td align="left" class="font10px">

<span class="ui_buttonset">
<?php 
$attr = (isset($filter['1']['deleted']) 
	 && !$filter['1']['deleted']) ? array('class' => 'white') : array();
echo anchor("mgmt_cost/filter/1/deleted/0", 'X',$attr);
$attr = (isset($filter['1']['deleted']) 
	 && $filter['1']['deleted']) ? array('class' => 'white') : array();
echo anchor("mgmt_cost/filter/1/deleted/1", 'Deleted', $attr);
?>
</span>
<span class="ui_buttonset"></span>
</td><td align="left" class="ui_button font10px">
</td><td align="left" class="ui_button font10px">
</td></tr>

<tr><td align="left" class="ui_button font10px" style="width:200px">
<?php 
echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
 
</td>
<td align="left" class="ui_button font10px" style="width:50px">
</td>
<td align="right" class="ui_button font10px">
 
</td>



</tr>

<tr><td align="left" class="ui_button font10px" style="width:200px">
<?php  
echo form_input(
		array(
		      'name'        => 'obj_branch',
		      'id'        => 'obj_branch',
		      'value'	  => $obj_branch,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		);

?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php 
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => $search,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		 );
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td><td align="left" class="ui_button font10px"></td></tr>




</table>
<?php echo form_close(); ?>



<div style="float: left; padding:20px 22px;">
  <div style="float: left; width:960px;background: #e1e1e1;color:#000000;">

<table border="0" cellpadding="5" cellspacing="0" width="900px" style="margin:30px auto;">

<tr>
<th colspan="7" align="left"><?php echo $table_title;?></th>
</tr>

 

<tr class="bgc">
<td colspan="8" align="right" class="ui_button font9px">
<?php 
echo $this->pagination->create_links(); 
$attr = ($per_page == 25) ? array() : array('class' => 'white');
$attr['style'] = 'margin:0 10px;';
echo anchor("mgmt_cost/filter/per_page/$total", 'Display All', $attr);
$data = array('content' => 'View Report', 'onclick'=>"openView('".site_url("mgmt_cost/report")."','report');");
echo form_button($data);

?></td>
</tr>

<tr class="bgc">
<th align="left" valign="middle" width="40">
<?php echo anchor("mgmt_cost/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>

<th align="left" valign="middle" width="80">
<?php echo anchor("mgmt_cost/orderby/branch", 'Branch', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'branch'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>

<th align="left" valign="middle" width="70">
<?php echo anchor("mgmt_cost/orderby/date", 'Date', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'date'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>



<th align="left" valign="middle" width="180">
<?php echo anchor("mgmt_cost/orderby/category", 'Cost Type', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'category'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>

<th align="left" valign="middle">
<?php echo anchor("mgmt_cost/orderby/notes", 'Notes', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'notes'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>


<th align="left" valign="middle" width="80">
<?php echo anchor("mgmt_cost/orderby/total", 'Total', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'total'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>


<th align="center" valign="middle" width="180">Operations</th>
</tr>

<?php 
$accu = new stdClass;


foreach($query->result() as $row):

$accu->total += $row->total; 

?>
 
<tr class="bgc">
<td align="left" valign="middle"><?php echo $row->id;?></td>
<td align="left" valign="middle"><?php echo $row->branch;?></td>
<td align="left" valign="middle"><?php echo date('d/m/Y', strtotime($row->date));?></td>

<td align="left" valign="middle"><?php echo $row->category;?></td>
<td align="left" valign="middle"><?php echo $row->notes;?></td>
<td align="left" valign="middle"><?php echo $row->total;?></td>


<td align="center" valign="middle" class="ui_button font9px">
<?php 
$data = array('class' => 'invo-lib-btn','content' => 'Edit', 'onclick'=>"openView('".site_url("mgmt_cost/edit/$row->id")."','edit-".$row->id."');");
echo form_button($data);
$data = array('class' => 'invo-lib-btn','content' => 'View', 'onclick'=>"openView('".site_url("mgmt_cost/view/$row->id")."','view-".$row->id."');");
echo form_button($data);
$data = array('class' => 'invo-lib-btn','content' => 'Dup', 'onclick'=>"openView('".site_url("mgmt_cost/dup/$row->id")."','dup-".$row->id."');");
echo form_button($data);
$data = array('class' => 'invo-lib-btn','content' => 'Del', 'onclick'=>"window.location = '".site_url("mgmt_cost/del_msg/$row->id")."';");
echo form_button($data);
?>
</td>

</tr>
<?php endforeach;?>

<tr><td colspan="7" align="left"></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"><?php echo 'Total Returned: ';echo $total;?></th>
<th align="left" valign="middle" colspan="3">Accumulated Total of the Page............</th>
 
<th align="left" valign="middle"><?php echo number_format($accu->total,2,'.',',');?></th>

<th align="right" valign="middle"></th>

 
</tr>
</table>
 



  </div>
  </div>