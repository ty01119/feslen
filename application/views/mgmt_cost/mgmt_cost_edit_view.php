<script>

var json_url = '<?php echo $json_url;?>';
var post_url = '<?php echo $post_url;?>';
var read_only = <?php echo $read_only;?>;
$(function() {

      $( "#branch" )
	.autocomplete({
			minLength: 0,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo base_url();?>ajax/branch_names',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
		      }).click(function() {
	  $(this).autocomplete( "search" , '' );
	});

    $( "#category" )
      .autocomplete({
	minLength: 0,
	    source: function(request, response){
	    $.ajax({
	      url: '<?php echo site_url('ajax/cost_cate_names');?>',
		  dataType: 'json',
		  type: 'POST',
		  data: request,
		  success: function(data){
		  response(data);
		}
	      });
	  }
	})
      .click(function() {
	  $(this).autocomplete( "search" , '' );
	});
 
    $( "#date" ).datepicker({
      dateFormat: 'dd/mm/yy'
	  });
	  
     $( "#invo-items" ).sortable(); 
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>js/mgmt_cost_edit_view.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>js/phpjs.js"></script>
<div style="padding:10px 0;" id="inner-wrapper">
<div style="width:680px;background: #e1e1e1;color:#000000;" id="inner-text">


<table border="0" cellpadding="5" cellspacing="0" style="width:680px;padding:10px;">
<tr class="bgc">
<td class="ui_button" >
<?php
echo form_label('Branch', 'branch');?>
</td>
<td colspan="5">
<?php
$attr = array('name' => 'branch', 'id' => 'branch', 'value' => '', 'class' => 'ui-corner-all cst', 'style' => 'width:300px;') ;
echo form_input($attr);
?>   
</td>
</tr>
<tr class="bgc">
<td class="ui_button" >
<?php
echo form_label('Cost Type:', 'category');?>
</td>
<td colspan="5">
<?php
$attr = array('name' => 'category', 'id' => 'category', 'value' => '', 'class' => 'ui-corner-all cst', 'style' => 'width:265px;') ;
echo form_input($attr);
?>   
</td>
</tr>
<tr>
<td>
<?php
echo form_label('Date:', 'date');?>
</td><td>
<?php
$attr = array('name' => 'date', 'id' => 'date', 'class' => 'ui-corner-all cst', 'style' => 'width:100px;') ;
echo form_input($attr);

?>  
</td>
<td></td><td></td>
<td></td><td></td>
</tr>

<tr>
<td colspan="6">

<div >
<span class="ui-icon ui-icon-grip-solid-horizontal flt" style="margin:2px 5px 0 0;"></span>
<div style="width:420px;margin:4px;" class="item-title">
<strong>PRODUCT</strong></div>
<div style="width:40px;text-align:right;margin:4px;" class="item-title">
<strong>QTY</strong></div>
<div style="width:60px;text-align:right;margin:4px;" class="item-title">
<strong>U/P</strong></div>
<div style="width:30px;text-align:right;margin:4px;" class="item-title"></div>
</div>
 

<ul id="invo-items" style="width:650px;clear:both;">
 
</ul>
 

<div style="width:650px;height:20px;" class="bgc">
<div class="flt pt" style="margin:2px;" title="add more" id="item_add">
<span class="ui-icon ui-icon-plusthick"></span>
</div>
</div>

</td>
</tr>


<tr class="bgc">
<td><?php
echo form_label('Total: ', 'total');?></td>
<td colspan="5">
<?php
$attr = array('name' => 'total', 'id' => 'total', 'class' => 'ui-corner-all cst', 'style' => 'width:200px;') ;
echo form_input($attr);

?> 
</td>
</tr>  
</tr>
<tr class="bgc">
<td valign="top">
<?php
echo form_label('Management Notes:', 'notes');
?>
</td>
<td colspan="5">
<textarea name="notes" id="notes" rows="4" style="width:500px" class="ui-corner-all cst"></textarea>
</td>
</tr>
</table>

   
</div>
<br style="clear:both">
<div style="float: left; padding:0 5px;" class="ui_button">
<?php 
echo form_input(array(
			 'type'  => 'button',
			 'name'  => 'save',
			 'id'    => 'save',
			 'value' => 'Save'
			 )
		   );
?>
</div>
<div style="float: right; padding:0 5px;" class="ui_button">
<?php 
  echo form_input(array(
			 'type' => 'button',
                         'name' => 'close',
			 'id'   => 'close',
			 'value' => 'Close'
			 )
		   );
?>
</div>


</div>