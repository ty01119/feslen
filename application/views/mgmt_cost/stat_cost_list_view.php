  <script>
  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#obj_branch" )
	.autocomplete({
			minLength: 0,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo base_url();?>ajax/branch_names',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
		      }).click(function() {
	  $(this).autocomplete( "search" , '' );
	});

      
    });
</script>
<style type="text/css">
.fp {
	color: black;
}
.fup {
	background-color: LightSteelBlue; color: black;
}
.uf {
	background-color: #C68E17; color: black;
}
.sample {
display: inline-block;
width: 160px;
height:20px;
}
.sam-box {
display: inline-block;
width: 10px;
padding: 0 4px;
}
</style>
<?php echo form_open('stat_cost/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >

<tr><td align="left" class="font10px">
<span class="ui_buttonset">
<?php
$data = array('content' => 'Stat Cost Management', 'style' => 'margin:5px 0;', 'onclick'=>"window.location.href='".site_url('stat_cost')."'");
echo form_button($data);
$data = array('class' => '','content' => '+', 'onclick'=>"openView('".site_url("mgmt_cost/add")."','Add Domain');");
echo form_button($data);
?>
</span>
<span class="ui_buttonset"></span>
</td>
<td align="left" class="font10px">

<span class="ui_buttonset">
<?php 
$attr = (isset($filter['1']['deleted']) 
	 && !$filter['1']['deleted']) ? array('class' => 'white') : array();
echo anchor("mgmt_cost/filter/1/deleted/0", 'X',$attr);
$attr = (isset($filter['1']['deleted']) 
	 && $filter['1']['deleted']) ? array('class' => 'white') : array();
echo anchor("mgmt_cost/filter/1/deleted/1", 'Deleted', $attr);
?>
</span>
<span class="ui_buttonset"></span>
</td><td align="left" class="ui_button font10px">
</td><td align="left" class="ui_button font10px">
</td></tr>

<tr><td align="left" class="ui_button font10px" style="width:200px">
<?php 
echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">

<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td>
<td align="left" class="ui_button font10px" style="width:50px">
</td>
<td align="right" class="ui_button font10px">
 
</td>



</tr>

 




</table>
<?php echo form_close(); ?>



<div style="float: left; padding:20px 22px;">
  <div style="float: left; width:960px;background: #e1e1e1;color:#000000;">

<table border="0" cellpadding="5" cellspacing="0" width="900px" style="margin:30px auto;">

<tr>
<th colspan="7" align="left"><?php echo $table_title;?></th>
</tr>


<tr><td colspan="7" align="left"></td></tr>
 

<tr><td colspan="7" align="left">

<?php 
    $tmpl = array (
		   'table_open'          => '<table border="1" cellpadding="2" cellspacing="1">',		   
                    'heading_row_start'   => '<tr>',
                    'heading_row_end'     => '</tr>',
                    'heading_cell_start'  => '<th>',
                    'heading_cell_end'    => '</th>',
                    'row_start'           => '<tr>',
                    'row_end'             => '</tr>',
                    'cell_start'          => '<td align="right">',
                    'cell_end'            => '</td>',
                    'row_alt_start'       => '<tr>',
                    'row_alt_end'         => '</tr>',
                    'cell_alt_start'      => '<td align="right">',
                    'cell_alt_end'        => '</td>',
                    'table_close'         => '</table>'
              );

    $this->table->set_template($tmpl); 
     
?>
    <table>
    <tr>
    <td>
    <?php    echo $this->table->generate($table['epsom']); ?>
    </td>
    <td>
 <?php 
    echo $this->table->generate($table['onehunga']);
     ?>
    </td>
 
    <td>
 <?php 
 
    echo $this->table->generate($table['city']);
     ?>
    </td>
    

    <td>
 <?php 
 
    echo $this->table->generate($table['newmarket']);
     ?>
    </td>
    <td>
 <?php 
    echo $this->table->generate($table['albany']); 
     ?>
    </td>
    </tr>
    <tr>
    <td>
 <?php 
    echo $this->table->generate($table['energysigns']); 
     ?>
    </td>

    <td>
 <?php 
    echo $this->table->generate($table['websolution']); 
     ?>
    </td>

    </tr>
 </table>
 


</td></tr>
 
 
<tr><td colspan="7" align="left"></td></tr>
</table>



  </div>
  </div>