<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
<?php $this->load->view('includes/html_head'); ?>

<style>
   body, #wrapper {
 background: #ffffff;
 color:#000000;
 }
   </style>
   </head>
<body>
<div id="wrapper" style="width:700px;">
<h1><?php echo $title;?></h1>
<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="ui_button font11px" style="width:200px">

<?php 
echo 'From:'.nbs(4);
echo date('d/m/Y', $date['fr']);
?>
</td><td align="left" class="ui_button font11px" style="width:200px">
<?php
  echo 'To:'.nbs(4);
echo date('d/m/Y', $date['to']);
?>
</td><td align="left" class="ui_button font11px">
<?php 
echo ($obj_branch== 'branch..') ? '' : 'Branch:'.nbs(4).$obj_branch;

?>
</td><td align="left" class="ui_button font10px"></td></tr>

<tr><td colspan="2" align="left" class="font11px">

 

  
</td><td align="left" class="ui_button font11px">
<?php 
  echo ($search == 'Search..') ? '' : 'Search result:'.nbs(4).$search;
?>
</td><td align="left" class="ui_button font10px">
</td></tr>
</table>

  <div style="float: left; width:700px;">

		
<table border="0" cellpadding="5" cellspacing="0" width="700px" style="margin:30px auto;">

<tr>
<th colspan="7" align="left"><?php echo $table_title;?></th>
</tr>

 

<tr>
<td colspan="7" align="right" class="ui_button font9px">

</td>
</tr>

<tr>
<th align="left" valign="middle" width="60">
  <?php echo '<span class="flt">ID</span>';
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="120">
<?php  echo '<span class="flt">Date</span>';
if (isset($orderby) && $orderby['order'] == 'date'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" >
<?php echo '<span class="flt">Branch</span>';
if (isset($orderby) && $orderby['order'] == 'branch'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="right" valign="middle" width="70">
 
</th>
<th align="right" valign="middle" width="70">
<?php echo '<span class="frt">Total</span>';
if (isset($orderby) && $orderby['order'] == 'total'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s frt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n frt"></span>';
endif;
?>
</th>
<th align="right" valign="middle" width="70">
 
</th>

</tr>

<?php 
$accu = new stdClass;
$accu->debit = $accu->subtotal = $accu->total = 0;


foreach($query->result() as $row):
 
$accu->total += $row->total; 
?> 
<tr class="bgc">
<td align="left" valign="middle"><?php echo $row->id;?></td>
<td align="left" valign="middle"><?php echo date('d/m/Y H:i:s', strtotime($row->date));?></td>
<td align="left" valign="middle"><?php echo $row->branch;?></td>
<td align="right" valign="middle"><?php echo '';?></td>
<td align="right" valign="middle"><?php echo number_format($row->total,2,'.',',');?></td>
<td align="right" valign="middle"><?php echo '';?></td>
</tr>
<?php endforeach;?>

<tr><td colspan="7" align="left"></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"><?php echo 'Total Data Returned: ';echo $total;?></th>
<th align="left" valign="middle">Accumulated Total ............</th>
<th align="right" valign="middle"><?php  ?></th>
<th align="right" valign="middle"><?php echo number_format($accu->total,2,'.',',');?></th>
<th align="right" valign="middle"><?php ?></th>
<th align="center" valign="middle" class="ui_button font9px"><?php echo '';?></th>
</tr>
</table>

 
 

  </div>



</div>
</body>
</html>
