  <script>
  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#name" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/doc_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
			


		      });

      
    });
</script>

<?php echo form_open('mgmt_doc/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="ui_button font10px">
<span class="ui_buttonset">
<?php 
$data = array('content' => 'Doc Management', 'style' => 'margin:5px 0;', 'onclick'=>"window.location.href='".site_url('mgmt_doc')."'");
echo form_button($data);
$data = array('content' => '+', 'style' => 'margin:5px 0;', 'onclick'=>"window.location.href='".site_url('mgmt_doc/add')."'");
echo form_button($data);
?>
</span>
</td><td align="left" class="ui_button font10px" colspan="3">
 

</td></tr>

<tr><td align="left" class="font10px">
<span class="ui_button">
<?php
echo form_input(
		array(
		      'name'        => 'name',
		      'id'        => 'name',
		      'value'	  => $name,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:5px;'
		      ) 
		);
?>
</span>
</td><td align="left" class="font10px">

</td><td align="left" class="ui_button font10px">
<?php 
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => $search,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		 );
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td></tr>
</table>
<?php echo form_close(); ?>



<div style="float: left; padding:20px 22px;">
  <div style="float: left; width:960px;background: #e1e1e1;color:#000000;">

		
<table border="0" cellpadding="5" cellspacing="0" width="900px" style="margin:30px auto;">

<tr>
<th colspan="7" align="left"><?php echo $table_title;?></th>
</tr>

 

<tr class="bgc">
<td colspan="7" align="right" class="ui_button font9px">
<?php 
echo $this->pagination->create_links(); 
$attr = ($per_page == 25) ? array() : array('class' => 'white');
$attr['style'] = 'margin:0 10px;';
echo anchor("mgmt_doc/filter/per_page/$total", 'Display All', $attr);
?></td>
</tr>

<tr class="bgc">
<th align="left" valign="middle" width="60">
<?php echo anchor("mgmt_doc/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="120">
<?php echo anchor("mgmt_doc/orderby/dept", 'Department', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'dept'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="60">
<?php echo anchor("mgmt_doc/orderby/order", 'Order', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'order'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle">
<?php echo anchor("mgmt_doc/orderby/name", 'Name', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'name'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="60">
<?php echo anchor("mgmt_doc/orderby/cnfd", 'CNFD', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'cnfd'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>

<th align="center" valign="middle" width="140">Operations</th>
</tr>

<?php 



foreach($query->result() as $row):


?> 
<tr class="bgc">
<td align="left" valign="middle"><?php echo $row->id;?></td>
<td align="left" valign="middle"><?php echo $row->dept;?></td>
<td align="left" valign="middle"><?php echo $row->order;?></td>
<td align="left" valign="middle"><?php echo $row->name;?></td>
<td align="left" valign="middle"><?php echo $row->cnfd;?></td>

<td align="center" valign="middle" class="ui_button font9px">
<?php 
$data = array('class' => 'invo-lib-btn','content' => 'Edit', 'onclick'=>"window.location.href='".site_url("mgmt_doc/edit/$row->id")."'");
echo form_button($data);
//$data = array('class' => 'invo-lib-btn','content' => 'View', 'onclick'=>"openView('".site_url("mgmt_doc/view/$row->id")."','view-".$row->id."');");
//echo form_button($data);
$data = array('class' => 'invo-lib-btn','content' => 'Del', 'onclick'=>"window.location.href='".site_url("mgmt_doc/del_msg/$row->id")."'");
echo form_button($data);
?>
</td></tr>
<?php endforeach;?>

<tr><td colspan="7" align="left"></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"><?php echo 'Total Data Returned: ';echo $total;?></th>
<th align="left" valign="middle"></th>
<th align="right" valign="middle"></th>
<th align="right" valign="middle"></th>
<th align="center" valign="middle" class="ui_button font9px"></th>
</tr>
</table>

 
 

  </div>
  </div>
