  <script>
  
    $(function() {
      $('.inv-item').dblclick(function(){	  
	  openView('<?php echo base_url();?>mgmt_task/edit/'+ this.id,'view' + this.id);
	});
    });

  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#customer" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/cust_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			}
		      });

    $('.blur[name="staff_production"]')
	   .blur(function() {
		     if ($.trim(this.value) == ''){
			 this.value = 'Production Staff...';
		     }
		 });
     $('.blur').focus(function() {
		     if (this.value == 'Production Staff...' 
		     || this.value == 'Pre-press Staff...'
		     || this.value == 'Branch...'){ 
			  this.value = '';
		      } else {
			  this.select();
		      }
		});

    $('.blur[name="staff_pre_press"]')
	   .blur(function() {
		     if ($.trim(this.value) == ''){
			 this.value = 'Pre-press Staff...';
		     }
		 }); 

    $('.blur[name="branch"]')
	   .blur(function() {
		     if ($.trim(this.value) == ''){
			 this.value = 'Branch...';
		     }
		 }); 
			  
    $( "#staff_pre_press, #staff_production" )
      .autocomplete({
	minLength: 0,
	    source: function(request, response){
	    $.ajax({
	      url: '<?php echo site_url('ajax/staff_names');?>',
		  dataType: 'json',
		  type: 'POST',
		  data: request,
		  success: function(data){
		  response(data);
		}
	      });
	  }
	}).click(function() {
    		$(this).autocomplete("search", "");
	});
    
      $( "#task_branch" )
      .autocomplete({ minLength: 0, source:  printing_branches,})
      .click(function() {
    		$(this).autocomplete('search', '');
	});
	
      //end
    });
</script>
<style type="text/css">
.fp {
	color: black;
}
.fup {
	background-color: LightSteelBlue; color: black;
}
.uf {
	background-color: #C68E17; color: black;
}
.sample {
display: inline-block;
width: 160px;
height:20px;
}
.sam-box {
display: inline-block;
width: 10px;
padding: 0 4px;
}
</style>
<?php echo form_open('mgmt_task/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="ui_button font10px"  colspan="2">
<span class="ui_buttonset">
<?php 
$data = array('content' => 'My Tasks', 'style' => 'margin:5px 0;', 'onclick'=>"window.location.href='".site_url('mgmt_task/my')."'");
echo form_button($data);
//$data = array('content' => 'My Branch', 'style' => 'margin:5px 0;', 'onclick'=>"window.location.href='".site_url('mgmt_task/branch')."'");
//echo form_button($data);
$data = array('content' => 'Tasks', 'style' => 'margin:5px 0;', 'onclick'=>"window.location.href='".site_url('mgmt_task')."'");
echo form_button($data);
$data = array('content' => '+', 'style' => 'margin:5px 0;', 'onclick'=>"openView('".site_url("mgmt_task/add")."','Add task');");
echo form_button($data);
?>
</span>
</td><td align="left" class="ui_button font10px" colspan="2">

</td>
</tr>

<tr><td align="left" class="ui_button font10px" style="width:200px">
<?php 
echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_input(
		array(
		      'name'        => 'staff_production',
		      'id'        => 'staff_production',
		      'value'	  => $staff_production,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		);

?>
</td>
<td align="left" class="ui_button font10px">
<?php
echo form_input(
		array(
		      'name'        => 'staff_pre_press',
		      'id'        => 'staff_pre_press',
		      'value'	  => $staff_pre_press,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		);

?>
</td>
</tr>

<tr><td align="left" class="font10px" colspan="2">
<span class="ui_buttonset">
<?php 
$attr = (isset($filter['1']['urgent']) 
	 && !$filter['1']['urgent']) ? array('class' => 'white') : array();
echo anchor("mgmt_task/filter/1/urgent/0", 'X',$attr);
$attr = (isset($filter['1']['urgent']) 
	 && $filter['1']['urgent']) ? array('class' => 'white') : array();
echo anchor("mgmt_task/filter/1/urgent/1", 'Urgent',$attr);
?>
</span>
<span class="ui_buttonset">
<?php 
$attr = (isset($filter['2']['delivered']) 
	 && !$filter['2']['delivered']) ? array('class' => 'white') : array();
//echo anchor("mgmt_task/filter/2/delivered/0", 'X',$attr);
$attr = (isset($filter['2']['delivered']) 
	 && $filter['2']['delivered']) ? array('class' => 'white') : array();
//echo anchor("mgmt_task/filter/2/delivered/1", 'Delivered',$attr);
?>
</span>


<span class="ui_buttonset">
<?php 
$attr = (isset($filter['2']['status']) 
	 && $filter['2']['status'] == 'finished')  ? array('class' => 'white') : array();
echo anchor("mgmt_task/filter/2/status/finished", 'Finished',$attr);
$attr = (isset($filter['2']['status']) 
	 && $filter['2']['status'] == 'queued') ? array('class' => 'white') : array();
echo anchor("mgmt_task/filter/2/status/queued", 'Queued',$attr);
?>
</span>

<span class="ui_buttonset">
<?php 
$attr = (isset($filter['3']['paid']) && !$filter['1']['paid']) ? array('class' => 'white') : array();
//echo anchor("mgmt_task/filter/3/paid/0", 'X',$attr);
$attr = (isset($filter['3']['paid']) && !$filter['1']['paid']) ? array('class' => 'white') : array();
//echo anchor("mgmt_task/filter/3/paid/1", 'Paid',$attr);
?>
</span>
<span class="ui_buttonset">

</span>
</td><td align="left" class="ui_button font10px">
<?php 
echo form_input( 
		array(
		      'name'        => 'task_branch',
		      'id'	    => 'task_branch',
		      'value'	      => $task_branch,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		 );
?>
</td><td align="left" class="ui_button font10px">
<?php 
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => $search,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		 );
?>
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td></tr>
</table>
<?php echo form_close(); ?>



 

		
<table border="0" cellpadding="5" cellspacing="0"  style="margin:30px 0; min-width:900px; float: left; width:100%;">

<tr>
<th colspan="8" align="left"><?php echo $table_title;?>
</th>
</tr>


<tr class="bgc">
<td colspan="8" align="right" class="ui_button font9px">
<?php 
echo $this->pagination->create_links(); 
$attr = ($per_page == 25) ? array() : array('class' => 'white');
$attr['style'] = 'margin:0 10px;';
echo anchor("mgmt_task/filter/per_page/$total", 'Display All', $attr);

?></td>
</tr>

<script type="text/javascript"> 
$("button, input:submit, input:button, a", ".ui_button").button();
$(".ui_buttonset").buttonset(); 
</script>

<tr class="bgc">
<th align="left" valign="middle" width="60">
<?php echo anchor("mgmt_task/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="80">
<?php echo anchor("mgmt_task/orderby/date_due", 'Due Date', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'date_due'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="80" >
<?php echo anchor("mgmt_task/orderby/staff_pre_press", 'Pre_press', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'staff_pre_press'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="80" >
<?php echo anchor("mgmt_task/orderby/staff_production", 'Production', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'staff_production'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" >
<?php echo anchor("mgmt_task/orderby/cust_name", 'Customer', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'cust_name'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="80" >
<?php echo anchor("mgmt_task/orderby/status", 'Status', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'status'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="center" valign="middle" width="20" ></th>
<th align="center" valign="middle" width="120">Operations</th>
</tr>

<?php 
$accu = new stdClass;
$accu->debit = $accu->subtotal = $accu->total = 0;


foreach($query->result() as $row):

$delivered = '';


$job_status = $row->urgent ? 'uf' : '';
 
$job_status = $row->status == 'finished' ? 'fup' : $job_status;


?> 
<tr class="bgc <?php echo $job_status;?> inv-item" id="<?php echo $row->id;?>">
<td align="left" valign="middle"><?php echo $row->id.br().$row->invo_id;?></td>
<td align="left" valign="middle"><?php echo $row->date_due;?></td>
<td align="left" valign="middle"><?php echo $row->staff_pre_press;?></td>
<td align="left" valign="middle"><?php echo $row->staff_production;?></td>
<td align="left" valign="middle"><?php echo $row->cust_name;?></td>
<td align="left" valign="middle"><?php echo $row->status;?></td>
<td align="center" valign="middle" class="font11px"><?php echo $delivered;?></td>
<td align="center" valign="middle" class="font9px">
<?php 
$class = 'invo-lib-btn ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only';
$data = array('class' => $class, 'content' => '<span class="ui-button-text">Edit</span>', 'onclick'=>"openView('".site_url("mgmt_task/edit/$row->id")."','edit-".$row->id."');");
echo form_button($data);
$data = array('class' => $class, 'content' => '<span class="ui-button-text">View</span>', 'onclick'=>"openView('".site_url("mgmt_task/view/$row->id")."','view-".$row->id."');");
echo form_button($data);
//$data = array('class' => $class,'content' => '<span class="ui-button-text">Del</span>', 'onclick'=>"window.location='".site_url("mgmt_task/del_msg/$row->id")."'");
//echo form_button($data);
?>
</td></tr>
<?php endforeach;?>

<tr><td colspan="8" align="left"></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"><?php echo 'Total Data Returned: ';echo $total;?></th>
<th align="left" valign="middle" colspan="2"> </th>
<th align="right" valign="middle"> </th>
<th align="right" valign="middle"> </th>
<th align="right" valign="middle"> </th>
<td ></td>
<th align="center" valign="middle" class="ui_button font9px"><?php echo '';?></th>
</tr>

<tr><td colspan="8" align="left"><br /><br /></td></tr>

<tr><td colspan="8" align="left">

<span class="sample"><span class="fup sam-box">&nbsp;&nbsp;</span> --  </span>
<span class="sample"><span class="uf sam-box">&nbsp;&nbsp;</span> -- Urgent </span><br />
<span class="sample"><span class="sam-box">&nbsp;&bull;</span> --  </span>
<span class="sample"><span class="sam-box">&Omega;</span> -- </span>

</td></tr>
</table>
 
 