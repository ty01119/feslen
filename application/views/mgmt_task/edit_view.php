<script type="text/javascript" src="<?php echo base_url();?>lib/jquery-ui-timepicker-addon.js"></script> 

<script>
var json_url = '<?php echo $json_url;?>';
var post_url = '<?php echo $post_url;?>';
$(function() {
    $( "#cust_name" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/cust_names');?>',
		  		   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},	
		      });
    
    $( "#staff_production" )
      .autocomplete({
	minLength: 0,
	    source: function(request, response){
	    $.ajax({
	      url: '<?php echo site_url('ajax/staff_names');?>',
		  dataType: 'json',
		  type: 'POST',
		  data: request,
		  success: function(data){
		  response(data);
		}
	      });
	  },    
	    }).click(function() {
    		$(this).autocomplete("search", "");
	});
	
	
    $( "#print_size" )
      .autocomplete({ minLength: 0, source: print_size,})
      .click(function() {
    		$(this).autocomplete('search', '');
	});
	
	
    $( "#finish_size" )
      .autocomplete({ minLength: 0, source: finish_size,})
      .click(function() {
    		$(this).autocomplete('search', '');
	});
	
	
    $( "#printer" )
      .autocomplete({ minLength: 0, source: printers,})
      .click(function() {
    		$(this).autocomplete('search', '');
	});
	
	
        $('#date_due').datetimepicker({ dateFormat: 'yy-mm-dd', timeFormat: 'hh:mm:ss', hourGrid: 3,minuteGrid: 15});
  });
  

</script>
<script type="text/javascript" src="<?php echo base_url();?>js/mgmt_task_edit_view.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>js/phpjs.js"></script>
<div style="padding:0;" id="inner-wrapper">

<br style="clear:both">

<div style="width:680px;background: #e1e1e1;color:#000000;" id="inner-text">


<table border="0" cellpadding="5" cellspacing="0" style="width:680px;padding:10px;">


<!-- -->
<tr>
<td>
<label for="invo_id">Invoice ID:</label>
</td>

<td>
<input type="text" name="invo_id" value="" id="invo_id" class="ui-corner-all" style="width:100px;"  />  
</td>

<td>
<label for="task_id">Task ID:</label>
</td>
<td>
<input type="text" name="task_id" value="" id="task_id" readonly="readonly" class="ui-corner-all" style="width:100px;"  /> 
</td>

<td>
<label for="date">Date:</label></td>
<td>
<input type="text" name="date" value="" id="date" readonly="readonly" class="ui-corner-all" style="width:120px;"  />  
</td>
</tr>
<!-- -->

<!-- -->
<tr class="bgc">
<td  >
<label for="date_due">Due Date:</label> 
</td>
<td colspan="3">
<input type="text" name="date_due" value="" id="date_due" class="ui-corner-all" style="width:300px;"  />   
</td>
<td colspan="2">
<div class="ui_buttonset font10px">
<input type="checkbox" name="urgent" value="1" id="urgent" style="margin:10px"  /><label for="urgent">Urgent</label>
</div>
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td class="ui_button" >
<label for="cust_name">Customer:</label></td>
<td colspan="5">
<input type="text" name="cust_name" value="" id="cust_name" class="ui-corner-all" style="width:300px;"  />   
</td>
</tr>
<!-- -->

<!-- -->
<tr class="bgc">
<td class="ui_button" >
<label for="staff_pre_press">Pre-press Staff:</label></td>
<td colspan="5">
<input type="text" name="staff_pre_press" value="" id="staff_pre_press" class="ui-corner-all" style="width:300px;"  />   
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td class="ui_button" >
<label for="file_name">File Name:</label></td>
<td colspan="5">
<textarea name="file_name" id="file_name" rows="1" style="width:500px" class="ui-corner-all"></textarea>
</td>
</tr>
<!-- -->

<!-- -->
<tr class="bgc">
<td>Bleed: </td>
<td colspan="2">
<div class="ui_buttonset font10px">
<input type="radio" name="bleed_btn" value="yes" id="bleed01" style="margin:10px"  /><label for="bleed01">Yes</label>
<input type="radio" name="bleed_btn" value="no" id="bleed02" style="margin:10px"  /><label for="bleed02">No</label>
</div>
</td>
<td colspan="3">
<textarea name="bleed_notes" id="bleed_notes" rows="1" style="width:300px" class="ui-corner-all"></textarea>
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td>Sample Supplied: </td>
<td colspan="2">
<div class="ui_buttonset font10px">
<input type="checkbox" name="sample_btn" value="yes" id="sample01" style="margin:10px"  /><label for="sample01">Yes</label>
<input type="checkbox" name="sample_btn" value="no" id="sample02" style="margin:10px"  /><label for="sample02">No</label>
</div>
</td>
<td colspan="3">
<textarea name="sample_notes" id="sample_notes" rows="1" style="width:300px" class="ui-corner-all"></textarea>
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td>Image Quality: </td>
<td colspan="2">
<div class="ui_buttonset font10px">
<input type="checkbox" name="image_btn" value="pass" id="img00" style="margin:10px"/><label for="img00">Pass</label>
<input type="checkbox" name="image_btn" value="high" id="img01" style="margin:10px"/><label for="img01">High</label>
<input type="checkbox" name="image_btn" value="mid" id="img02" style="margin:10px"/><label for="img02">Mid</label>
<input type="checkbox" name="image_btn" value="low" id="img03" style="margin:10px"/><label for="img03">Low</label>
</div>
</td>
<td colspan="3">
<textarea name="image_notes" id="image_notes" rows="1" style="width:300px" class="ui-corner-all"></textarea>
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td>Text Quality: </td>
<td colspan="5">
<div class="ui_buttonset font10px">
<input type="checkbox" name="text_btn" value="pass" id="txt00" style="margin:10px"  /><label for="txt00">Pass</label>
<input type="checkbox" name="text_btn" value="high" id="txt01" style="margin:10px"  /><label for="txt01">High</label>
<input type="checkbox" name="text_btn" value="mid" id="txt02" style="margin:10px"  /><label for="txt02">Mid</label>
<input type="checkbox" name="text_btn" value="low" id="txt03" style="margin:10px"  /><label for="txt03">Low</label>
<input type="checkbox" name="text_btn" value="all align" id="txt04" style="margin:10px"  /><label for="txt04">All align</label>
<input type="checkbox" name="text_btn" value="not align" id="txt05" style="margin:10px"  /><label for="txt05">Not align</label>
<input type="checkbox" name="text_btn" value="font" id="txt06" style="margin:10px"  /><label for="txt06">Font</label>
</div>
</td>
</tr>
<tr class="bgc">
<td></td>
<td colspan="5">
<textarea name="text_notes" id="text_notes" rows="1" style="width:500px" class="ui-corner-all"></textarea>
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td>Printing: </td>
<td colspan="5">
<div class="ui_buttonset font10px">
<input type="checkbox" name="printing_btn" value="Color Slg Side" id="prt01" style="margin:10px"  /><label for="prt01">Color Slg Side</label>
<input type="checkbox" name="printing_btn" value="Color Dbl Side" id="prt02" style="margin:10px"  /><label for="prt02">Color Dbl Side</label>
<input type="checkbox" name="printing_btn" value="BW Slg Side" id="prt03" style="margin:10px"  /><label for="prt03">BW Slg Side</label>
<input type="checkbox" name="printing_btn" value="BW Dbl Side" id="prt04" style="margin:10px"  /><label for="prt04">BW Dbl Side</label>
<input type="checkbox" name="printing_btn" value="Color / BW Dbl Side" id="prt05" style="margin:10px"  /><label for="prt05">Color / BW Dbl Side</label>
</div>
</td>
</tr>
<tr class="bgc">
<td></td>
<td colspan="5">
<textarea name="printing_notes" id="printing_notes" rows="1" style="width:500px" class="ui-corner-all"></textarea>
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td class="ui_button" >
<label for="printer">Printer:</label></td>
<td colspan="2">
<input type="text" name="printer" value="" id="printer" class="ui-corner-all" style="width:150px;"  />  
</td>
<td colspan="3">
<textarea name="printer_notes" id="printer_notes" rows="1" style="width:250px" class="ui-corner-all"></textarea>
</td>
</tr>
<!-- -->



<!-- -->
<tr class="bgc">
<td>Paper Stock: </td>
<td colspan="5">
<div class="ui_buttonset font10px">
<input type="checkbox" name="paper_btn" value="80g" id="paper01" style="margin:10px"  /><label for="paper01">80g</label>
<input type="checkbox" name="paper_btn" value="100g" id="paper02" style="margin:10px"  /><label for="paper02">100g</label>
<input type="checkbox" name="paper_btn" value="128g" id="paper03" style="margin:10px"  /><label for="paper03">128g</label>
<input type="checkbox" name="paper_btn" value="150g" id="paper04" style="margin:10px"  /><label for="paper04">150g</label>
<input type="checkbox" name="paper_btn" value="170g" id="paper05" style="margin:10px"  /><label for="paper05">170g</label>
<input type="checkbox" name="paper_btn" value="210g" id="paper06" style="margin:10px"  /><label for="paper06">210g</label>
<input type="checkbox" name="paper_btn" value="256g" id="paper07" style="margin:10px"  /><label for="paper07">256g</label>
<input type="checkbox" name="paper_btn" value="300g" id="paper08" style="margin:10px"  /><label for="paper08">300g</label>
<input type="checkbox" name="paper_btn" value="350g" id="paper09" style="margin:10px"  /><label for="paper09">350g</label>
<input type="checkbox" name="paper_btn" value="400g" id="paper10" style="margin:10px"  /><label for="paper10">400g</label>
<input type="checkbox" name="paper_btn" value="label" id="paper11" style="margin:10px"  /><label for="paper11">label</label>
</div>
</td>
</tr>
<tr class="bgc">
<td></td>
<td colspan="5">
<div class="ui_buttonset font10px">
<input type="checkbox" name="paper_btn" value="uncoated" id="paper21" style="margin:10px"  /><label for="paper21">Uncoated</label>
<input type="checkbox" name="paper_btn" value="matt" id="paper22" style="margin:10px"  /><label for="paper22">Matt</label>
<input type="checkbox" name="paper_btn" value="gloss" id="paper23" style="margin:10px"  /><label for="paper23">Gloss</label>
<input type="checkbox" name="paper_btn" value="metalic" id="paper24" style="margin:10px"  /><label for="paper24">Metalic</label>
<input type="checkbox" name="paper_btn" value="texture" id="paper25" style="margin:10px"  /><label for="paper25">Texture</label>
<input type="checkbox" name="paper_btn" value="carbonless tri" id="paper26" style="margin:10px"  /><label for="paper26">Carbonless Tri</label>
<input type="checkbox" name="paper_btn" value="carbonless dup" id="paper27" style="margin:10px"  /><label for="paper27">Carbonless Dup</label>
</div>
</td>
</tr>
<tr class="bgc">
<td></td>
<td colspan="5">
<textarea name="paper_notes" id="paper_notes" rows="1" style="width:500px" class="ui-corner-all"></textarea>
</td>
</tr>
<!-- -->

<!-- -->
<tr>
<td>
Print Quantity:
</td>
<td>
<input type="text" name="print_qty" value="" id="print_qty" class="ui-corner-all" style="width:100px;"  />  
</td>
<td>
Size:
</td>
<td>
<input type="text" name="print_size" value="" id="print_size" class="ui-corner-all" style="width:100px;"  /> 
</td>
<td colspan="2">
<textarea name="print_notes" id="print_notes" rows="1" style="width:200px" class="ui-corner-all"></textarea>
</td>
</tr>
<!-- -->

<!-- -->
<tr>
<td>
Finish Quantity:
</td>
<td>
<input type="text" name="finish_qty" value="" id="finish_qty" class="ui-corner-all" style="width:100px;"  />  
</td>
<td>
Size:
</td>
<td>
<input type="text" name="finish_size" value="" id="finish_size" class="ui-corner-all" style="width:100px;"  /> 
</td>
<td colspan="2">
<textarea name="finish_notes" id="finish_notes" rows="1" style="width:200px" class="ui-corner-all"></textarea>
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td>Finishing: </td>
<td colspan="5">
<div class="ui_buttonset font10px">
<input type="checkbox" name="finishing_btn" value="Wire Binding" id="finishing01" style="margin:10px"  /><label for="finishing01">Wire Binding</label>
<input type="checkbox" name="finishing_btn" value="Plastic Binding" id="finishing02" style="margin:10px"  /><label for="finishing02">Plastic Binding</label>
<input type="checkbox" name="finishing_btn" value="Perfect Binding" id="finishing03" style="margin:10px"  /><label for="finishing03">Perfect Binding</label>
<input type="checkbox" name="finishing_btn" value="Glue Binding" id="finishing04" style="margin:10px"  /><label for="finishing04">Glue Binding </label>
<input type="checkbox" name="finishing_btn" value="Saddle Stitches" id="finishing05" style="margin:10px"  /><label for="finishing05">Saddle Stitches</label>
<input type="checkbox" name="finishing_btn" value="Staple" id="finishing06" style="margin:10px"  /><label for="finishing06">Staple </label>
</div>
</td>
</tr>
<tr class="bgc">
<td></td>
<td colspan="5">
<div class="ui_buttonset font10px">
<input type="checkbox" name="finishing_btn" value="Creasing" id="finishing07" style="margin:10px"  /><label for="finishing07">Creasing</label>
<input type="checkbox" name="finishing_btn" value="Folding" id="finishing08" style="margin:10px"  /><label for="finishing08">Folding </label>
<input type="checkbox" name="finishing_btn" value="Perforation" id="finishing09" style="margin:10px"  /><label for="finishing09">Perforation </label>
<input type="checkbox" name="finishing_btn" value="Collating" id="finishing10" style="margin:10px"  /><label for="finishing10">Collating</label>
<input type="checkbox" name="finishing_btn" value="Hole Punching" id="finishing11" style="margin:10px"  /><label for="finishing11">Hole Punching</label>
</div>
</td>
</tr>
<tr class="bgc">
<td>&rarr; Laminating</td>
<td colspan="5">
<div class="ui_buttonset font10px">
<input type="checkbox" name="finishing_btn" value="Pouch Matt" id="finishing12" style="margin:10px"  /><label for="finishing12">Pouch Matt</label>
<input type="checkbox" name="finishing_btn" value="Pouch Gloss" id="finishing13" style="margin:10px"  /><label for="finishing13">Pouch Gloss</label>
<input type="checkbox" name="finishing_btn" value="Roll Matt" id="finishing14" style="margin:10px"  /><label for="finishing14">Roll Matt</label>
<input type="checkbox" name="finishing_btn" value="Roll Gloss" id="finishing15" style="margin:10px"  /><label for="finishing15">Roll Gloss</label>
<input type="checkbox" name="finishing_btn" value="Cold Roll Matt" id="finishing16" style="margin:10px"  /><label for="finishing16">Cold Roll Matt</label>
<input type="checkbox" name="finishing_btn" value="Cold Roll Gloss" id="finishing17" style="margin:10px"  /><label for="finishing17">Cold Roll Gloss</label>
</div>
</td>
</tr>
<tr class="bgc">
<td></td>
<td colspan="5">
<textarea name="finishing_notes" id="finishing_notes" rows="1" style="width:500px" class="ui-corner-all"></textarea>
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td>Delivery: </td>
<td colspan="5">
<div class="ui_buttonset font10px">
<input type="checkbox" name="delivery_btn" value="Pickup Epsom" id="ship0" style="margin:10px"  /><label for="ship0">Pickup Epsom</label>
<input type="checkbox" name="delivery_btn" value="Pickup City" id="ship1" style="margin:10px"  /><label for="ship1">Pickup City</label>
<input type="checkbox" name="delivery_btn" value="Pickup Albany" id="ship2" style="margin:10px"  /><label for="ship2">Pickup Albany</label>
<input type="checkbox" name="delivery_btn" value="Pickup Onehunga" id="ship3" style="margin:10px"  /><label for="ship3">Pickup Onehunga</label>
<input type="checkbox" name="delivery_btn" value="Pickup Penrose" id="ship4" style="margin:10px"  /><label for="ship4">Pickup Penrose</label>
</div>
</td>
</tr>
<tr class="bgc">
<td></td>
<td colspan="5">
<div class="ui_buttonset font10px">
<input type="checkbox" name="delivery_btn" value="Shipping by Carrier" id="ship5" style="margin:10px"  /><label for="ship5">Shipping by Carrier</label>
<input type="checkbox" name="delivery_btn" value="Deliver by Staff" id="ship6" style="margin:10px"  /><label for="ship6">Deliver by Staff</label>
</div>
</td>
</tr>
<tr class="bgc">
<td></td>
<td colspan="5">
<textarea name="delivery_notes" id="delivery_notes" rows="1" style="width:500px" class="ui-corner-all"></textarea>
</td>
</tr>
<!-- -->

<!-- -->
<tr class="bgc">
<td class="ui_button" >
<label for="staff_production">Production Staff:</label></td>
<td colspan="5">
<input type="text" name="staff_production" value="" id="staff_production" class="ui-corner-all" style="width:300px;"  />   
</td>
</tr>
<tr class="bgc">
<td>
</td>
<td colspan="5">
<textarea name="staff_production_notes" id="staff_production_notes" rows="2" style="width:500px" class="ui-corner-all"></textarea>
</td>
</tr>
<!-- -->
 


<!-- -->
<tr class="bgc">
<td class="ui_button" >
	     Quality Assurance:
</td>
<td colspan="2">
<div class="ui_buttonset font10px">
<input type="checkbox" name="qa_btn" value="Process Approved by <?php echo $this->session->userdata('user_name');?>" id="qa_btn" style="margin:10px"  /><label for="qa_btn">Process Approved by <?php echo $this->session->userdata('user_name');?></label>
</td>
<td colspan="3">
<input type="text" name="qa" value="" id="qa" class="ui-corner-all" readonly="readonly" style="width:200px;"  />   
</td>
</tr>
<tr class="bgc">
<td>
</td>
<td colspan="5">
<textarea name="qa_notes" id="qa_notes" rows="2" style="width:500px" class="ui-corner-all"></textarea>
</td>
</tr>
<!-- -->
 

<!-- -->	
<tr class="bgc">
<td>Job Status: </td>
<td colspan="5">
<div class="ui_buttonset font10px">
<input type="radio" name="status_btn" value="queued" id="queued" style="margin:10px"  /><label for="queued">Queued</label>
<input type="radio" name="status_btn" value="processing" id="processing" style="margin:10px"  /><label for="processing">Processing</label>
<input type="radio" name="status_btn" value="on hold" id="on_hold" style="margin:10px"  /><label for="on_hold">On Hold</label>
<input type="radio" name="status_btn" value="terminated" id="terminated" style="margin:10px"  /><label for="terminated">Terminated</label>
<input type="radio" name="status_btn" value="finished" id="finished" style="margin:10px"  /><label for="finished">Finished</label></div>
</td>
</tr>
<tr class="bgc">
<td></td>
<td colspan="5">
<textarea name="status_notes" id="status_notes" rows="1" style="width:500px" class="ui-corner-all"></textarea>
</td>
</tr>
<!-- -->
	

</table>

   
</div>
<br style="clear:both">
<div style="float: left; padding:0 5px;" class="ui_button">
<?php 
echo form_input(array(
			 'type'  => 'button',
			 'name'  => 'save',
			 'id'    => 'save',
			 'value' => 'Save'
			 )
		   );
?>
</div>
<div style="float: right; padding:0 5px;" class="ui_button">
<?php 
  echo form_input(array(
			 'type' => 'button',
                         'name' => 'close',
			 'id'   => 'close',
			 'value' => 'Close'
			 )
		   );
?>
</div>
<br style="clear:both"><br />
</div>