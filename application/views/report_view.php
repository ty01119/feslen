<script type="text/javascript"> 
  
</script> 

<script type="text/javascript"> 
  $(function() {
     
	 $( "#customer, #customer2" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/cust_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
			});

      var dates = $( "#from, #to, #to1" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


	$('#cust_report').click(function(){
	    var name = $('#customer').val();

	    openView('report/cust_report','cust_report');
	   
 	  
	  });
	
	$('#invoice_report').click(function(){
 

	    openView('report/invoice_report','invoice_report');
	   
 	  
	  });
 });

</script> 
<script type="text/javascript" src="<?php echo base_url(); ?>js/quoting_view.js"></script>
 
 
<div class="ui_button font11px" style="margin-top:15px;width:440px;display:none;"  id="reset">
<?php
echo form_button($data = array(
    'name' => 'button',
    'id' => 'button',
     'style' => 'float:right;',  
    'content' => 'Reset Cart'
));
?>
</div>
<br style="clear:both;"/> 
<div style="margin:0 0 30px 0; min-width:900px; float: left; width:100%; ">

    <!--
    *******************************************************************************
     ***
     *******************************************************************************/
    -->
<div style="margin:30px 30px;width:900px;" class="flt" id="dp_price_pt">
<strong>Customer Statement</strong><br /><br />
 
<div class="font11px flt bgc">
<span class="quote_item ui_button font10px"> 

<?php
$attributes = array('id' => 'myform', 'target' => 'cust_report');

echo form_open('report/cust_report', $attributes);

echo form_label('<button type="button">To</button>', 'to1');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to1',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', strtotime("-1 day")),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
echo form_input(
		array(
		      'name'        => 'customer',
		      'id'        => 'customer',
		      'value'	  => 'Customer..',
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:240px;margin:10px;'
		      ) 
		);

?>

 
<?php
echo form_button($data = array('id' => 'cust_report', 'style' => 'width:70px;', 'content' => 'Generate', 'type' => 'submit'));
 
echo form_close();
?>
</span>
 

</div></div>
    <!--	
  /*******************************************************************************
   ***
   *******************************************************************************/
    -->

    <!--
    *******************************************************************************
     ***
     *******************************************************************************/
    -->
<div style="margin:30px 30px;width:900px;" class="flt" id="dp_price_pt">
<strong>Invoice Statement</strong><br /><br />
 
<div class="font11px flt bgc">
 

<?php
$attributes = array('id' => 'myform2', 'target' => 'invoice_report');

echo form_open('report/invoice_report', $attributes);
 ?>
<table border="0" cellpadding="0" cellspacing="0" style="margin:10px;" >
<tr><td align="left" class="ui_button font10px" style="width:200px">
<?php 
echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', strtotime("-1 week")),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', strtotime("-1 day")),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px">
<?php
 

echo form_input(
		array(
		      'name'        => 'customer',
		      'id'        => 'customer2',
		      'value'	  => 'Customer..',
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:240px;margin:10px;'
		      ) 
		);
?>
</td><td align="left" class="ui_button font10px"></td></tr>

<tr><td align="left" class="font10px">
<span class="ui_buttonset">
<?php
echo form_label('X', 'ra1-1');
echo form_checkbox(array('type' => 'radio', 'checked' => FALSE, 'name' => 'finished', 'id' => 'ra1-1', 'value' => 'no'));
echo form_label('Finished', 'ra1-2');
echo form_checkbox(array('type' => 'radio', 'checked' => FALSE, 'name' => 'finished', 'id' => 'ra1-2', 'value' => 'yes'));
?>
</span>
<span class="ui_buttonset">
<?php
echo form_label('X', 'ra2-1');
echo form_checkbox(array('type' => 'radio', 'checked' => FALSE, 'name' => 'delivered', 'id' => 'ra2-1', 'value' => 'no'));
echo form_label('Delivered', 'ra2-2');
echo form_checkbox(array('type' => 'radio', 'checked' => FALSE, 'name' => 'delivered', 'id' => 'ra2-2', 'value' => 'yes'));
?>
</span>
</td><td align="left" class="font10px">
<span class="ui_buttonset">
<?php 
echo form_label('X', 'ra3-1');
echo form_checkbox(array('type' => 'radio', 'checked' => FALSE, 'name' => 'paid', 'id' => 'ra3-1', 'value' => 'no'));
echo form_label('Paid', 'ra3-2');
echo form_checkbox(array('type' => 'radio', 'checked' => FALSE, 'name' => 'paid', 'id' => 'ra3-2', 'value' => 'yes'));
?>
</span>
 
</td><td align="left" class="ui_button font10px">

</td><td align="left" class="ui_button font10px">
<?php
echo form_button($data = array('id' => 'invoice_report', 'style' => 'width:70px;', 'content' => 'Generate', 'type' => 'submit'));
?>
</td></tr> 
</table>
 


 
<?php

 
echo form_close();
?>
</div></div>
    <!--	
  /*******************************************************************************
   ***
   *******************************************************************************/
    -->
<br style="clear:both;"/><br />

 
  </div>