<script type="text/javascript" src="<?php echo base_url();?>lib/jquery-ui-timepicker-addon.js"></script> 

<script>
var json_url = '<?php echo $json_url;?>';
var post_url = '<?php echo $post_url;?>';
$(function() {
    $( "#cust_name" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/cust_names');?>',
		  		   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},	
		      });
    
    $( "#owner, #follow" )
      .autocomplete({
	minLength: 0,
	    source: function(request, response){
	    $.ajax({
	      url: '<?php echo site_url('ajax/staff_names');?>',
		  dataType: 'json',
		  type: 'POST',
		  data: request,
		  success: function(data){
		  response(data);
		}
	      });
	  },    
	    }).click(function() {
    		$(this).autocomplete("search", "");
	});
	
	
    $( "#print_size" )
      .autocomplete({ minLength: 0, source: print_size,})
      .click(function() {
    		$(this).autocomplete('search', '');
	});
	
	
    $( "#finish_size" )
      .autocomplete({ minLength: 0, source: finish_size,})
      .click(function() {
    		$(this).autocomplete('search', '');
	});
	
	
    $( "#printer" )
      .autocomplete({ minLength: 0, source: printers,})
      .click(function() {
    		$(this).autocomplete('search', '');
	}); 
	
    $('#reminder_date').datepicker({ dateFormat: 'yy-mm-dd'});
    
  });
</script>
<script type="text/javascript" src="<?php echo base_url();?>js/leads_edit_view.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>js/phpjs.js"></script>
<div style="padding:0;" id="inner-wrapper">

<br style="clear:both">

<div style="width:680px;background: #e1e1e1;color:#000000;" id="inner-text">


<table border="0" cellpadding="5" cellspacing="0" style="width:680px;padding:10px;">


<!-- -->
<tr>
<td>
<label for="lead_id">Lead ID:</label>
</td>

<td>
<input type="text" name="lead_id" value="" id="lead_id" class="ui-corner-all" style="width:100px;"  />  
</td>

<td>
<label for="date">Date:</label>
</td>
<td>
<input type="text" name="date" value="" id="date" readonly="readonly" class="ui-corner-all" style="width:120px;"  />  
</td>


<td> 
</td>
<td> 
</td>


</tr>
<!-- -->

<!-- -->
<tr class="bgc">
<td  >
<label for="date_due">Reminder Date:</label> 
</td>
<td >
<input type="text" name="reminder_date" value="" id="reminder_date" class="ui-corner-all" style="width:100px;"  />   
</td>
<td>
Reminder Notes:
</td>
<td colspan="3">
<div class="ui_buttonset font10px">
<textarea name="reminder_notes" id="reminder_notes" rows="1" style="width:300px" class="ui-corner-all"></textarea>
</div>
</td>
</tr>
<!-- -->
 

<!-- -->
<tr class="bgc">
<td class="ui_button" >
<label for="cust_name">Owner:</label></td>
<td >
<input type="text" name="owner" value="" id="owner" class="ui-corner-all" style="width:100px;"  />   
</td>
<td class="ui_button" >
<label for="cust_name">Follow:</label></td>
<td >
<input type="text" name="follow" value="" id="follow" class="ui-corner-all" style="width:100px;"  />   
</td>
<td class="ui_button" > <!--
<label for="cust_name">Owner:</label></td> -->
<td > <!--
<input type="text" name="cust_name" value="" id="cust_name" class="ui-corner-all" style="width:100px;"  /> -->   
</td>
</tr>
<!-- -->


<!-- -->	
<tr class="bgc">
<td>Job Status: </td>
<td colspan="5">
<div class="ui_buttonset font10px">
<input type="radio" name="status_btn" value="new" id="new" style="margin:10px"  /><label for="new">New</label>
<input type="radio" name="status_btn" value="engaged" id="engaged" style="margin:10px"  /><label for="engaged">Engaged</label>
<input type="radio" name="status_btn" value="disqualified" id="disqualified" style="margin:10px"  /><label for="disqualified">Disqualified</label>
<input type="radio" name="status_btn" value="dead" id="dead" style="margin:10px"  /><label for="dead">Dead</label>
<input type="radio" name="status_btn" value="converted" id="converted" style="margin:10px"  /><label for="converted">Converted</label></div>
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td class="ui_button" >
 </td>
<td colspan="5">
client information  
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td class="ui_button" >
<label for="file_name">Company</label></td>
<td colspan="5">
<input type="text" name="client_company" value="" id="client_company" class="ui-corner-all" style="width:300px;"  />   
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td class="ui_button" >
<label for="cust_name">Contact:</label></td>
<td >
<input type="text" name="client_contact" value="" id="client_contact" class="ui-corner-all" style="width:120px;"  />   
</td>
<td class="ui_button" >
<label for="cust_name">Phone:</label></td>
<td >
<input type="text" name="client_phone" value="" id="client_phone" class="ui-corner-all" style="width:120px;"  />   
</td>
<td class="ui_button" >  
<label for="cust_name">Email:</label></td> 
<td > 
<input type="text" name="client_email" value="" id="client_email" class="ui-corner-all" style="width:120px;"  />    
</td>
</tr>

<tr class="bgc">
<td class="ui_button" >
<label for="file_name">Address</label></td>
<td colspan="5">
<input type="text" name="client_address" value="" id="client_address" class="ui-corner-all" style="width:300px;"  />   
</td>
</tr>
<tr class="bgc">
<td class="ui_button" >
<label for="cust_name">Notes:</label></td>
<td colspan="5">
<textarea name="client_notes" id="client_notes" rows="1" style="width:500px" class="ui-corner-all"></textarea>
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td class="ui_button" >
 </td>
<td colspan="5">
process information  
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td class="ui_button" >
<label for="cust_name">Quote#:</label></td>
<td >
<input type="text" name="quote_id" value="" id="quote_id" class="ui-corner-all" style="width:120px;"  />   
</td>
<td class="ui_button" >
<label for="cust_name">Invoice#:</label></td>
<td >
<input type="text" name="invoice_id" value="" id="invoice_id" class="ui-corner-all" style="width:120px;"  />   
</td>
<td class="ui_button" >  
<label for="cust_name">Customer#:</label></td> 
<td > 
<input type="text" name="customer_id" value="" id="customer_id" class="ui-corner-all" style="width:120px;"  />    
</td>
</tr>
 
<tr class="bgc">
<td class="ui_button" >
<label for="cust_name">Add Log:</label></td>
<td colspan="5">
<textarea name="log" id="log" rows="2" style="width:500px" class="ui-corner-all"></textarea>
</td>
</tr>
 
<tr class="bgc">
<td class="ui_button" >
<label for="logs">Logs:</label></td>
<td colspan="5">
<textarea name="logs" id="logs" rows="2" style="width:500px;height:160px;background:#f1f1f1;" class="ui-corner-all" readonly="readonly"></textarea>
</td>
</tr>
<!-- -->
<tr class="bgc">
<td  >
	      
</td>
<td colspan="5">
<div class="ui_buttonset font10px">
<input type="checkbox" name="deleted" value="1" id="deleted" style="margin:10px" /><label for="deleted">Deleted</label>
</td> 
</tr>

</table>

   
</div>
<br style="clear:both">
<div style="float: left; padding:0 5px;" class="ui_button">
<?php 
echo form_input(array(
			 'type'  => 'button',
			 'name'  => 'save',
			 'id'    => 'save',
			 'value' => 'Save'
			 )
		   );
?>
</div>
<div style="float: right; padding:0 5px;" class="ui_button">
<?php 
  echo form_input(array(
			 'type' => 'button',
                         'name' => 'close',
			 'id'   => 'close',
			 'value' => 'Close'
			 )
		   );
?>
</div>
<br style="clear:both"><br />
</div>