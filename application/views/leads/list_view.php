  <script>
  
    $(function() {
      $('.inv-item').dblclick(function(){	  
	  openView('<?php echo base_url();?>mgmt_task/edit/'+ this.id,'view' + this.id);
	});
    });

  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#customer" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/cust_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			}
		      });

    $('.blur[name="lead_follow"]')
	   .blur(function() {
		     if ($.trim(this.value) == ''){
			 this.value = 'Production Staff...';
		     }
		 });
     $('.blur').focus(function() {
		     if (this.value == 'Production Staff...' 
		     || this.value == 'Pre-press Staff...'
		     || this.value == 'Branch...'){ 
			  this.value = '';
		      } else {
			  this.select();
		      }
		});

    $('.blur[name="lead_owner"]')
	   .blur(function() {
		     if ($.trim(this.value) == ''){
			 this.value = 'Pre-press Staff...';
		     }
		 }); 

    $('.blur[name="branch"]')
	   .blur(function() {
		     if ($.trim(this.value) == ''){
			 this.value = 'Branch...';
		     }
		 }); 
			  
    $( "#lead_owner, #lead_follow" )
      .autocomplete({
	minLength: 0,
	    source: function(request, response){
	    $.ajax({
	      url: '<?php echo site_url('ajax/staff_names');?>',
		  dataType: 'json',
		  type: 'POST',
		  data: request,
		  success: function(data){
		  response(data);
		}
	      });
	  }
	}).click(function() {
    		$(this).autocomplete("search", "");
	});
    
      $( "#task_branch" )
      .autocomplete({ minLength: 0, source:  printing_branches,})
      .click(function() {
    		$(this).autocomplete('search', '');
	});
	
      //end
    });
</script>
<style type="text/css">
.fp {
	color: black;
}
.blue {
	background-color: LightSteelBlue; color: black;
}
.lead-new { outline: #888888 dashed 3px; }
.lead-engaged { outline: #888888 dotted 3px; }
.lead-disqualified,
.lead-dead { outline: black solid 3px; }
.lead-converted { background-color: #eeeeee; }
.reminderon { background-color: yellow; } 

.sample {
display: inline-block;
width: 160px;
height:20px;
}
.sam-box {
display: inline-block;
width: 10px;
padding: 0 4px;
}
</style>
<?php echo form_open('leads/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="ui_button font10px"  colspan="2">
<span class="ui_buttonset">
<?php 

//$data = array('content' => 'My Branch', 'style' => 'margin:5px 0;', 'onclick'=>"window.location.href='".site_url('mgmt_task/branch')."'");
//echo form_button($data);
$data = array('content' => 'All Open Leads', 'style' => 'margin:5px 0;', 'onclick'=>"window.location.href='".site_url('leads')."'");
echo form_button($data);
$data = array('content' => '+', 'style' => 'margin:5px 0;', 'onclick'=>"openView('".site_url("leads/add")."','Add task');");
echo form_button($data);
?>
</span>
</td><td align="left" class="ui_button font10px" colspan="2">

</td>
</tr>

<tr><td align="left" class="ui_button font10px" style="width:200px">
<?php 
echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_input(
		array(
		      'name'        => 'lead_follow',
		      'id'        => 'lead_follow',
		      'value'	  => $lead_follow,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		);

?>
</td>
<td align="left" class="ui_button font10px">
<?php
echo form_input(
		array(
		      'name'        => 'lead_owner',
		      'id'        => 'lead_owner',
		      'value'	  => $lead_owner,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		);

?>
</td>
</tr>

<tr><td align="left" class="font10px" colspan="3">  


<span class="ui_buttonset">
<?php 
$attr = (isset($filter['2']['status']) 
	 && $filter['2']['status'] == 'new') ? array('class' => 'white') : array();
echo anchor("leads/filter/2/status/new", 'New',$attr);

$attr = (isset($filter['2']['status']) 
	 && $filter['2']['status'] == 'engaged') ? array('class' => 'white') : array();
echo anchor("leads/filter/2/status/engaged", 'Engaged',$attr);

$attr = (isset($filter['2']['status']) 
	 && $filter['2']['status'] == 'disqualified') ? array('class' => 'white') : array();
echo anchor("leads/filter/2/status/disqualified", 'Disqualified',$attr);

$attr = (isset($filter['2']['status']) 
	 && $filter['2']['status'] == 'dead')  ? array('class' => 'white') : array();
echo anchor("leads/filter/2/status/dead", 'Dead',$attr);

$attr = (isset($filter['2']['status']) 
	 && $filter['2']['status'] == 'converted') ? array('class' => 'white') : array();
echo anchor("leads/filter/2/status/converted", 'Converted',$attr);
?>
</span>

<span class="ui_buttonset">
<?php 
$attr = (isset($filter['3']['paid']) && !$filter['1']['paid']) ? array('class' => 'white') : array();
//echo anchor("mgmt_task/filter/3/paid/0", 'X',$attr);
$attr = (isset($filter['3']['paid']) && !$filter['1']['paid']) ? array('class' => 'white') : array();
//echo anchor("mgmt_task/filter/3/paid/1", 'Paid',$attr);
?>
</span>
<span class="ui_buttonset">

<?php 
$attr = (isset($filter['1']['deleted']) 
	 && !$filter['1']['deleted']) ? array('class' => 'white') : array();
echo anchor("leads/filter/1/deleted/0", 'X',$attr);
$attr = (isset($filter['1']['deleted']) 
	 && $filter['1']['deleted']) ? array('class' => 'white') : array();
echo anchor("leads/filter/1/deleted/1", 'Deleted', $attr);
?>
</span>
</td> <td align="left" class="ui_button font10px">
<?php 
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => $search,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		 );
?>
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td></tr>
</table>
<?php echo form_close(); ?>


 

		
<table border="0" cellpadding="5" cellspacing="0"  style="margin:0 0 30px 0; min-width:900px; float: left; width:100%;">

<tr>
<th colspan="8" align="left"><?php echo $table_title;?>
</th>
</tr>


<tr class="bgc">
<td colspan="8" align="right" class="ui_button font9px">
<?php 
echo $this->pagination->create_links(); 
$attr = ($per_page == 25) ? array() : array('class' => 'white');
$attr['style'] = 'margin:0 10px;';
echo anchor("mgmt_task/filter/per_page/$total", 'Display All', $attr);

?></td>
</tr>

<script type="text/javascript"> 
$("button, input:submit, input:button, a", ".ui_button").button();
$(".ui_buttonset").buttonset(); 
</script>

<tr class="bgc">
<th align="left" valign="middle" width="60">
<?php echo anchor("leads/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="80">
<?php echo anchor("leads/orderby/date", 'Date', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'date'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="80" >
<?php echo anchor("leads/orderby/owner", 'Owner', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'owner'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="80" >
<?php echo anchor("leads/orderby/follow", 'Follow', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'follow'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" >
<?php echo anchor("mgmt_task/orderby/cust_name", 'Customer', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'cust_name'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="80" >
<?php echo anchor("leads/orderby/status", 'Status', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'status'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="center" valign="middle" width="20" ></th>
<th align="center" valign="middle" width="120"></th>
</tr>

<tr><td colspan="8" align="left">
 
<style>
.item {
transition: all .5s ease;
width:240px; height:100px;padding:10px;float:left;margin:10px;border:1px solid #909090;overflow:hidden;
}
.logs {
max-width: 460px;
transition: all .5s ease; 
opacity: 0;
position:absolute;
z-index: -1;
margin: 0 10px ;
padding: 10px;
box-shadow: 0 0 8px 4px #B7B7B7;
}
.item-wrapper {float:left;transition: all .5s ease;}
.item-wrapper:focus { outline: none; }
.item-wrapper:focus > .logs{ 
       transition: all .5s ease;   opacity: 1;
position:absolute;
background: #f1f1f1;
z-index: 2;
     
}
</style>
<?php 
$accu = new stdClass;
$accu->debit = $accu->subtotal = $accu->total = 0;

foreach($query->result() as $row):

$delivered = '';


//$job_status = $row->urgent ? 'uf' : '';
$job_status =  '';
$job_status = 'lead-'.$row->status;
$reminder = date("Y-m-d 00:00:00") == $row->reminder_date ? 'reminderon' : '';
$data = json_decode($row->json);
?> 
<div class="item-wrapper" tabindex="0" >
<div id="id<?php echo $row->id;?>" class="shadow item <?php echo $job_status;?>" >
 
<div style="height: 70px;">
<b><?php  echo '['.$row->id.'] '.substr($row->company.' '.$data->client_contact, 0, 32);?></b> <br />
<div class="reminder <?php echo $reminder;?>" title="<?php echo  $row->reminder_notes; ?>" style="padding: 5px;  ">
<?php echo  substr($row->reminder_date, 0, 10)."&nbsp;&nbsp;"; ?>
<?php echo  substr($row->reminder_notes, 0, 26); ?>

</div>
<?php 
echo  'Owner: '.$row->owner.br();
echo  'Follow: '.$row->follow.br();

?>
</div>

<div class="font9px" style="height: 22px;padding-top:8px;width:240px;">
<?php
$class = 'invo-lib-btn ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only';
$btn_data = array('class' => $class, 'content' => '<span class="ui-button-text">Edit</span>', 'onclick'=>"openView('".site_url("leads/edit/$row->id")."','edit-".$row->id."');");
echo form_button($btn_data);
 
?>
</div>
 
<br style="clear:both;"/>
 </div><br style="clear:both;"/>
<div class="logs shadow" style=" white-space: pre-wrap;" ><?php echo $data->logs;?></div>
</div>
<?php endforeach;?>

</td></tr>

<tr><td colspan="8" align="left"></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"><?php echo 'Total Data Returned: ';echo $total;?></th>
<th align="left" valign="middle" colspan="2"> </th>
<th align="right" valign="middle"> </th>
<th align="right" valign="middle"> </th>
<th align="right" valign="middle"> </th>
<td ></td>
<th align="center" valign="middle" class="ui_button font9px"><?php echo '';?></th>
</tr>

<tr><td colspan="8" align="left"><br /><br /></td></tr>

<tr><td colspan="8" align="left">

<span class="sample"><span class="fup sam-box"></span> --  </span>
<span class="sample"><span class="uf sam-box"></span> --  </span><br />
<span class="sample"><span class="sam-box"></span> --  </span>
<span class="sample"><span class="sam-box"></span> -- </span>

</td></tr>
</table>
 
 