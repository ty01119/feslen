Dear <?php echo $customer->billing_contact ? $customer->billing_contact : 'Customer';?>,

<?php echo $email_msg;?>

Please find the attached statement of your account.

We accept credit card, bank cheque or direct debit. Please make sure you have include your invoice number when submitting the payment.

Please contact us for confirmation once payment has been made.
  
 
Thanks,

<?php echo $branch->biz_name;?> 

P <?php echo $branch->telephone;?> 

E <?php echo $branch->email;?> 

W <?php echo $branch->website;?> 

A <?php echo $branch->address;?>