Dear <?php echo $member->contact;?>,

<?php echo $email_msg;?>

<?php
if (strtotime($invoice->due_date) > time()) { // not over due 
?>
Please find your invoice attached.
<?php
} else { // over due
?>
This letter is a friendly reminder that payment on your account （<?php echo $member->name;?>） in the amount of $<?php echo number_format($invoice->total - $invoice->paid, 2, '.', ',');?> was due on <?php echo date('d/m/Y', strtotime($invoice->due_date));?>. 
<?php 
}
?>  

If you have already sent us your payment, kindly disregard this letter.   

If not, you can pay it online to <?php echo $branch->bank_acc_num;?>.

Be sure to include your invoice number <?php echo $invoice->id;?> as reference in order to facilitate proper processing.

Your prompt payment will be appreciated. 
 
Thanks,

<?php echo $branch->biz_name;?> 

P <?php echo $branch->telephone;?> 

E <?php echo $branch->email;?> 

W <?php echo $branch->website;?> 

A <?php echo $branch->address;?>