  <script>
  
    $(function() {
      $('.inv-item').dblclick(function(){	  
	  openView('<?php echo base_url();?>web_app_task/edit/'+ this.id,'view' + this.id);
	});
    });

  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#customer" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/cust_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			}
		      });

    $( "#developer, #designer" )
      .autocomplete({
	minLength: 1,
	    source: function(request, response){
	    $.ajax({
	      url: '<?php echo site_url('ajax/staff_names');?>',
		  dataType: 'json',
		  type: 'POST',
		  data: request,
		  success: function(data){
		  response(data);
		}
	      });
	  }
	    });
    
      
    });
</script>
<style type="text/css">
.fp {
	color: black;
}
.fup {
	background-color: LightSteelBlue; color: black;
}
.uf {
	background-color: #C68E17; color: black;
}
.sample {
display: inline-block;
width: 160px;
height:20px;
}
.sam-box {
display: inline-block;
width: 10px;
padding: 0 4px;
}
</style>
<?php echo form_open('web_app_task/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="ui_button font10px">
<span class="ui_buttonset">
<?php 
$data = array('content' => 'Task Management', 'style' => 'margin:5px 0;', 'onclick'=>"window.location.href='".site_url('web_app_task')."'");
echo form_button($data);
$data = array('content' => '+', 'style' => 'margin:5px 0;', 'onclick'=>"openView('".site_url("web_app_task/add")."','Add task');");
echo form_button($data);
?>
</span>
</td><td align="left" class="ui_button font10px" colspan="3">

</td>
</tr>

<tr><td align="left" class="ui_button font10px" style="width:200px">
<?php 
echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_input(
		array(
		      'name'        => 'designer',
		      'id'        => 'designer',
		      'value'	  => $designer,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		);

?>
</td>
<td align="left" class="ui_button font10px">
<?php
echo form_input(
		array(
		      'name'        => 'developer',
		      'id'        => 'developer',
		      'value'	  => $developer,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		);

?>
</td>
</tr>

<tr><td align="left" class="font10px">
<span class="ui_buttonset">
<?php 
$attr = (isset($filter['1']['finished']) 
	 && !$filter['1']['finished']) ? array('class' => 'white') : array();
echo anchor("web_app_task/filter/1/finished/0", 'X',$attr);
$attr = (isset($filter['1']['finished']) 
	 && $filter['1']['finished']) ? array('class' => 'white') : array();
echo anchor("web_app_task/filter/1/finished/1", 'Finished',$attr);
?>
</span>
<span class="ui_buttonset">
<?php 
$attr = (isset($filter['2']['delivered']) 
	 && !$filter['2']['delivered']) ? array('class' => 'white') : array();
echo anchor("web_app_task/filter/2/delivered/0", 'X',$attr);
$attr = (isset($filter['2']['delivered']) 
	 && $filter['2']['delivered']) ? array('class' => 'white') : array();
echo anchor("web_app_task/filter/2/delivered/1", 'Delivered',$attr);
?>
</span>
</td><td align="left" class="font10px">
<span class="ui_buttonset">
<?php 
$attr = (isset($filter['3']['paid']) && !$filter['1']['paid']) ? array('class' => 'white') : array();
echo anchor("web_app_task/filter/3/paid/0", 'X',$attr);
$attr = (isset($filter['3']['paid']) && !$filter['1']['paid']) ? array('class' => 'white') : array();
echo anchor("web_app_task/filter/3/paid/1", 'Paid',$attr);
?>
</span>
<span class="ui_buttonset">

</span>
</td><td align="left" class="ui_button font10px">
<?php 
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => $search,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		 );
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td></tr>
</table>
<?php echo form_close(); ?>



 

		
<table border="0" cellpadding="5" cellspacing="0"  style="margin:30px 0; min-width:900px; float: left; width:100%;">

<tr>
<th colspan="8" align="left"><?php echo $table_title;?>
</th>
</tr>


<tr class="bgc">
<td colspan="8" align="right" class="ui_button font9px">
<?php 
echo $this->pagination->create_links(); 
$attr = ($per_page == 25) ? array() : array('class' => 'white');
$attr['style'] = 'margin:0 10px;';
echo anchor("web_app_task/filter/per_page/$total", 'Display All', $attr);

?></td>
</tr>

<script type="text/javascript"> 
$("button, input:submit, input:button, a", ".ui_button").button();
$(".ui_buttonset").buttonset(); 
</script>

<tr class="bgc">
<th align="left" valign="middle" width="60">
<?php echo anchor("web_app_task/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="80">
<?php echo anchor("web_app_task/orderby/date", 'Date', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'date'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" >
<?php echo anchor("web_app_task/orderby/cust_name", 'Customer', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'cust_name'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="80" >
<?php echo anchor("web_app_task/orderby/developer", 'Developer', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'developer'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="80" >
<?php echo anchor("web_app_task/orderby/designer", 'Designer', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'designer'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="80" >
<?php echo anchor("web_app_task/orderby/qa", 'QA', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'qa'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="center" valign="middle" width="20" ></th>
<th align="center" valign="middle" width="120">Operations</th>
</tr>

<?php 
$accu = new stdClass;
$accu->debit = $accu->subtotal = $accu->total = 0;


foreach($query->result() as $row):

$delivered = $row->delivered ? '' : '&bull;';
$job_status = $row->paid ? 'fp' : ($row->finished ? 'fup' : 'uf');
?> 
<tr class="bgc <?php echo $job_status;?> inv-item" id="<?php echo $row->id;?>">
<td align="left" valign="middle"><?php echo $row->id;?></td>
<td align="left" valign="middle"><?php echo date('d/m/Y', strtotime($row->date));?></td>
<td align="left" valign="middle"><?php echo $row->cust_name;?></td>
<td align="left" valign="middle"><?php echo $row->developer;?></td>
<td align="left" valign="middle"><?php echo $row->designer;?></td>
<td align="left" valign="middle"><?php echo $row->qa;?></td>
<td align="center" valign="middle" class="font11px"><?php echo $delivered;?></td>
<td align="center" valign="middle" class="font9px">
<?php 
$class = 'invo-lib-btn ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only';
$data = array('class' => $class, 'content' => '<span class="ui-button-text">Edit</span>', 'onclick'=>"openView('".site_url("web_app_task/edit/$row->id")."','edit-".$row->id."');");
echo form_button($data);
//$data = array('class' => $class,'content' => '<span class="ui-button-text">Del</span>', 'onclick'=>"window.location='".site_url("web_app_task/del_msg/$row->id")."'");
//echo form_button($data);
?>
</td></tr>
<?php endforeach;?>

<tr><td colspan="8" align="left"></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"><?php echo 'Total Data Returned: ';echo $total;?></th>
<th align="left" valign="middle" colspan="2"> </th>
<th align="right" valign="middle"> </th>
<th align="right" valign="middle"> </th>
<th align="right" valign="middle"> </th>
<td ></td>
<th align="center" valign="middle" class="ui_button font9px"><?php echo '';?></th>
</tr>

<tr><td colspan="8" align="left"><br /><br /></td></tr>

<tr><td colspan="8" align="left">

<span class="sample"><span class="fup sam-box">&nbsp;&nbsp;</span> -- Finished &amp; Unpaid</span>
<span class="sample"><span class="uf sam-box">&nbsp;&nbsp;</span> -- Unfinished </span><br />
<span class="sample"><span class="sam-box">&nbsp;&bull;</span> -- Undelivered </span>
<span class="sample"><span class="sam-box">&Omega;</span> -- Outsourced(Inc. Offset)</span>

</td></tr>
</table>
 
 