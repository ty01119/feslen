<script>

var json_url = '<?php echo $json_url;?>';
var post_url = '<?php echo $post_url;?>';
var read_only = <?php echo $read_only;?>;
$(function() {
    $( "#cust_name" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/cust_names');?>',
		  		   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},			
		      });    

     $( "#registrar" ).autocomplete({
		 source: ['1st Domains', 'Go Daddy', 'Aplus.net', 'Crazy Domain Names', "client"]
	 });

     $( "#host" ).autocomplete({
		 source: ['HostGator', 'Hosting Zoom', 'OpenHost', 'not with 3A', 'Just Host']
	 });
    $( "#due_date" ).datepicker({
      dateFormat: 'dd/mm/yy'
	  });
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>js/web_app_domain_edit_view.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>js/phpjs.js"></script>
<div style="padding:10px 0;" id="inner-wrapper">
<div style="width:680px;background: #e1e1e1;color:#000000;" id="inner-text">


<table border="0" cellpadding="5" cellspacing="0" style="width:680px;padding:10px;">
<tr class="bgc">
<td class="ui_button" >
<?php
echo form_label('Customer:', 'cust_name');?>
</td>
<td colspan="5">
<?php
$attr = array('name' => 'cust_name', 'id' => 'cust_name', 'value' => '', 'class' => 'ui-corner-all cst', 'style' => 'width:300px;') ;
echo form_input($attr);
?>   
</td>
</tr>
<tr class="bgc">
<td class="ui_button" >
<?php
echo form_label('URL:', 'url');?>
</td>
<td colspan="5">
http://
<?php
$attr = array('name' => 'url', 'id' => 'url', 'value' => '', 'class' => 'ui-corner-all cst', 'style' => 'width:265px;') ;
echo form_input($attr);
?>   
</td>
</tr>
<tr>
<td>
<?php
echo form_label('Due Date:', 'due_date');?>
</td><td>
<?php
$attr = array('name' => 'due_date', 'id' => 'due_date', 'class' => 'ui-corner-all cst', 'style' => 'width:100px;') ;
echo form_input($attr);

?>  
</td>
<td></td><td></td>
<td></td><td></td>
</tr>



<tr class="bgc">
<td><?php
echo form_label('Registrar: ', 'registrar');?></td>
<td colspan="5">
<?php
$attr = array('name' => 'registrar', 'id' => 'registrar', 'class' => 'ui-corner-all cst', 'style' => 'width:200px;') ;
echo form_input($attr);

?> 
</td>
</tr>
<tr class="bgc">
<td><?php
echo form_label('Host: ', 'host');?></td>
<td colspan="5">
<?php
$attr = array('name' => 'host', 'id' => 'host', 'class' => 'ui-corner-all cst', 'style' => 'width:200px;') ;
echo form_input($attr);

?> 
</td>
</tr>

<tr class="bgc">
<td valign="top">
<?php
echo form_label('Passwords:', 'passwords');
?>
</td>
<td colspan="5">
<textarea name="passwords" id="passwords" rows="8" style="width:500px" class="ui-corner-all cst"></textarea>
</td>
</tr>
<tr class="bgc">
<td valign="top">
<?php
echo form_label('Management Notes:', 'mgmt_notes');
?>
</td>
<td colspan="5">
<textarea name="mgmt_notes" id="mgmt_notes" rows="4" style="width:500px" class="ui-corner-all cst"></textarea>
</td>
</tr>
</table>

   
</div>
<br style="clear:both">
<div style="float: left; padding:0 5px;" class="ui_button">
<?php 
echo form_input(array(
			 'type'  => 'button',
			 'name'  => 'save',
			 'id'    => 'save',
			 'value' => 'Save'
			 )
		   );
?>
</div>
<div style="float: right; padding:0 5px;" class="ui_button">
<?php 
  echo form_input(array(
			 'type' => 'button',
                         'name' => 'close',
			 'id'   => 'close',
			 'value' => 'Close'
			 )
		   );
?>
</div>


</div>