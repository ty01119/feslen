<script type="text/javascript" src="<?php echo base_url();?>lib/qtip/jquery.qtip-1.0.0-rc3.min.js"></script>  
 
  <script>
  
    $(function() {
      $('.inv-item').dblclick(function(){	  
	  openView('<?php echo base_url();?>invoice/view/'+ this.id.substring(3),'view' + this.id);
	});
    });

  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#customer" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/cust_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
			


		      });

      
    });
</script>
<style type="text/css">
.fp {
	color: black;
}
.fup {
	background-color: LightSteelBlue; color: black;
}
.uf {
	background-color: #C68E17; color: black;
}
.bd {
	background-color: #888888;
}
.sample {
display: inline-block;
width: 160px;
height:20px;
}
.sam-box {
display: inline-block;
width: 10px;
padding: 0 4px;
}
</style>
<?php echo form_open('invoice/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" style="min-width:900px;  width:100%;">
<tr><td align="left" class="ui_button font10px" style="width:200px">
<?php 
echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:300px">
<?php
echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_input(
		array(
		      'name'        => 'customer',
		      'id'        => 'customer',
		      'value'	  => $customer,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		);

?>
</td>
<td align="left" class="ui_button font10px" style="width:50px">
</td>
<td align="right" class="ui_button font10px">
<span class="ui_buttonset">
<?php 
$attr =  ($this->uri->segment(1) == 'trash') ? array('class' => 'white') : array();
echo anchor(site_url('trash'), 'Trash',$attr);
?>
</span>
</td>



</tr>

<tr><td align="left" class="font10px">
<span class="ui_buttonset">
<?php 
$attr = (isset($filter['1']['finished']) 
	 && !$filter['1']['finished']) ? array('class' => 'white') : array();
echo anchor("invoice/filter/1/finished/0", 'X',$attr);
$attr = (isset($filter['1']['finished']) 
	 && $filter['1']['finished']) ? array('class' => 'white') : array();
echo anchor("invoice/filter/1/finished/1", 'Finished',$attr);
?>
</span>
<span class="ui_buttonset">
<?php 
$attr = (isset($filter['2']['delivered']) 
	 && !$filter['2']['delivered']) ? array('class' => 'white') : array();
echo anchor("invoice/filter/2/delivered/0", 'X',$attr);
$attr = (isset($filter['2']['delivered']) 
	 && $filter['2']['delivered']) ? array('class' => 'white') : array();
echo anchor("invoice/filter/2/delivered/1", 'Delivered',$attr);
?>
</span>
</td><td align="left" class="font10px">
<span class="ui_buttonset">
<?php 
$attr = (isset($filter['3']['sql']) && ($filter['3']['sql'] == 'paid-ne-total')) ? array('class' => 'white') : array();
echo anchor("invoice/filter/3/sql/paid-ne-total", 'X',$attr);
$attr = (isset($filter['3']['sql']) && ($filter['3']['sql'] == 'paid-eq-total')) ? array('class' => 'white') : array();
echo anchor("invoice/filter/3/sql/paid-eq-total", 'Paid',$attr);
?>
</span>
<span class="ui_buttonset">
<?php 
$attr = (isset($filter['4']['outsourced']) 
	 && !$filter['4']['outsourced']) ? array('class' => 'white') : array();
echo anchor("invoice/filter/4/outsourced/0", 'X',$attr);
$attr = (isset($filter['4']['outsourced']) 
	 && $filter['4']['outsourced']) ? array('class' => 'white') : array();
echo anchor("invoice/filter/4/outsourced/1", 'Outsourced',$attr);
?>
</span>

<span class="ui_buttonset">
<?php 
$attr = (isset($filter['5']['bad_debt']) 
	 && !$filter['5']['bad_debt']) ? array('class' => 'white') : array();
echo anchor("invoice/filter/5/bad_debt/0", 'X',$attr);
$attr = (isset($filter['5']['bad_debt']) 
	 && $filter['5']['bad_debt']) ? array('class' => 'white') : array();
echo anchor("invoice/filter/5/bad_debt/1", 'Bad Debt',$attr);
?>
</span>


</td><td align="left" class="ui_button font10px">
<?php 
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => $search,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		 );
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td>
<td align="right" class="ui_button font10px">

</td>
</tr>
</table>

<?php echo form_close(); ?>



 

		
<table border="0" cellpadding="5" cellspacing="0"   style="margin:0 0 30px 0; min-width:900px; float: left; width:100%; " class="inv-table">

<tr>
<th colspan="8" align="left"><?php echo $table_title;?></th>
</tr>

<tr class="bgc">
<td colspan="8" align="right" class="ui_button font9px">
<?php 
echo $this->pagination->create_links(); 
$attr = ($per_page == 25) ? array() : array('class' => 'white');
$attr['style'] = 'margin:0 10px;';
echo anchor("invoice/filter/per_page/$total", 'Display All', $attr);

$data = array('content' => 'View Report', 'onclick'=>"openView('".site_url("invoice/report")."','report');");
echo form_button($data);
?></td>
</tr>

<script type="text/javascript"> 
$("button, input:submit, input:button, a", ".ui_button").button();
$(".ui_buttonset").buttonset(); 
</script>

<tr class="bgc">
<th align="left" valign="middle" width="60" style="width:5%;">
<?php echo anchor("invoice/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle"  style="width:10%;">
<?php echo anchor("invoice/orderby/date", 'Date', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'date'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" style="width:40%;">
<?php echo anchor("invoice/orderby/cust_name", 'Customer', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'cust_name'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="right" valign="middle" style="width:8%;">
<?php echo anchor("invoice/orderby/subtotal", 'Subtotal', array('class' => 'black frt'));
if (isset($orderby) && $orderby['order'] == 'subtotal'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s frt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n frt"></span>';
endif;
?>
</th>
<th align="right" valign="middle" style="width:8%;">
<?php echo anchor("invoice/orderby/total", 'Total', array('class' => 'black frt'));
if (isset($orderby) && $orderby['order'] == 'total'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s frt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n frt"></span>';
endif;
?>
</th>
<th align="right" valign="middle" style="width:8%;">
<?php echo anchor("invoice/orderby/total-sp---sp-paid", 'Debit', array('class' => 'black frt'));
if (isset($orderby) && $orderby['order'] == 'total-sp---sp-paid'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s frt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n frt"></span>';
endif;
?>
</th>
<th align="center" valign="middle" class="font9px" style="width:1%;"></th>
<th align="center" valign="middle"  style="width:20%;">Operations</th>
</tr>

<?php 
$accu = new stdClass;
$accu->debit = $accu->subtotal = $accu->total = 0;


foreach($query->result() as $row):

$row->debit = $row->total - $row->paid;
$accu->subtotal += $row->subtotal;
$accu->total += $row->total;
$accu->debit += $row->debit;

$delivered = $row->delivered ? '' : '&bull;';
$outsrc = $row->outsourced ? '&Omega;' : '';

$job_status = $row->debit < 0.05 ? 'fp' : ($row->finished ? 'fup' : 'uf');
$job_status = $row->bad_debt ? 'bd' : $job_status;
?> 
<tr class="bgc <?php echo $job_status;?> inv-item" id="inv<?php echo $row->id;?>" tabindex="0">
<td align="left" valign="middle"><?php echo $row->id;?></td>
<td align="left" valign="middle" title="<?php echo strip_tags($row->logs);?>"><?php echo date('d/m/Y H:i:s', strtotime($row->date));?></td>
<td align="left" valign="middle"><?php echo $row->cust_name;?></td>
<td align="right" valign="middle"><?php echo number_format($row->subtotal,2,'.',',');?></td>
<td align="right" valign="middle"><?php echo number_format($row->total,2,'.',',');?></td>
<td align="right" valign="middle"><?php echo number_format($row->debit,2,'.',',');?></td>
<td align="center" valign="middle" class="font11px"><?php echo $delivered.' '.$outsrc;?></td>
<td align="center" valign="middle" class="font9px">
<?php 
$class = 'invo-lib-btn ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only';
$data = array('class' => $class, 'content' => '<span class="ui-button-text">Edit</span>', 'onclick'=>"openView('".site_url("invoice/edit/$row->id")."','edit-".$row->id."');");
echo form_button($data);
$data = array('class' => $class,'content' => '<span class="ui-button-text">View</span>', 'onclick'=>"openView('".site_url("invoice/view/$row->id")."','view-".$row->id."');");
echo form_button($data);
$data = array('class' => $class,'content' => '<span class="ui-button-text">PRT</span>', 'onclick'=>"openView('".site_url("invoice/prt/$row->id")."','prt-".$row->id."');");
echo form_button($data);
//$data = array('class' => $class,'content' => '<span class="ui-button-text">PDF</span>', 'onclick'=>"window.location='".site_url("invoice/pdf/$row->id")."'");
//echo form_button($data);

$data = array('class' => $class,'content' => '<span class="ui-button-text">FUN</span>', 'id' => 'fun'.$row->id);
echo form_button($data);


//$data = array('class' => $class,'content' => '<span class="ui-button-text">Del</span>', 'onclick'=>"window.location='".site_url("invoice/del_msg/$row->id")."'");
//echo form_button($data);
?>

<script>
      $('#fun<?php echo $row->id;?>').qtip(
      {
         content: '<?php 
$html = ' type="button" class="fun-btm invo-lib-btn ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"';

$fun = "window.location=\'".site_url("invoice/email_msg/$row->id")."\'";
echo '<button '.'onclick="'.$fun.'"'.$html.'><span class="ui-button-text font9px">Mail</span></button>';
$fun = "openView(\'".site_url("invoice/dup/$row->id")."\')";
echo '<button '.'onclick="'.$fun.'"'.$html.'><span class="ui-button-text font9px">DUP</span></button>';

$fun = "window.location=\'".site_url("invoice/pdf/$row->id")."\'";
echo '<button '.'onclick="'.$fun.'"'.$html.'><span class="ui-button-text font9px">PDF</span></button>';

$fun = "window.location=\'".site_url("invoice/del_msg/$row->id")."\'";
echo nbs(2).'<button '.'onclick="'.$fun.'"'.$html.'><span class="ui-button-text font9px">Del</span></button>';
         ?>', 
         position: { corner: { target: 'topLeft', tooltip: 'bottomMiddle'} },
         hide: { fixed: true, effect: { length: 200, type: 'fade' } },
         style: { padding: '2px 2px', name: 'light' },
	 show: { effect: { length: 200, type: 'fade' } }
      });
</script>


</td> 
</tr>


<tr class="bgc " id="info<?php echo $row->id;?>" >
<td align="left" valign="middle"> </td>
<td align="left" valign="middle"> </td>
<td align="left" valign="middle"><?php echo $row->logs; ?></td>
<td align="right" valign="middle"></td>
<td align="right" valign="middle"></td>
<td align="right" valign="middle"></td>
<td align="center" valign="middle" class="font11px"></td>
<td align="center" valign="middle" class="font9px">
<?php 
 
?> 
</td>
</tr>



<style>
 
#info<?php echo $row->id;?> {
transition: all .5s ease; 
opacity: 0;
position:absolute;
z-index: -1;
 box-shadow: 0 0 8px 4px #B7B7B7;
}
 
#inv<?php echo $row->id;?>:focus ~ #info<?php echo $row->id;?>  {
padding-top: -10px;
     transition: all .5s ease;   opacity: 1;
position:absolute;
background: #f1f1f1;
z-index: 2;
}

 
</style>
<?php endforeach;?>

<tr><td colspan="8" align="left"></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"><?php echo 'Total Data Returned: ';echo $total;?></th>
<th align="left" valign="middle">Accumulated Total of the Page............</th>
<th align="right" valign="middle"><?php echo number_format($accu->subtotal,2,'.',',');?></th>
<th align="right" valign="middle"><?php echo number_format($accu->total,2,'.',',');?></th>
<th align="right" valign="middle"><?php echo number_format($accu->debit,2,'.',',');?></th>
<td ></td>
<th align="center" valign="middle" class="ui_button font9px"><?php echo '';?></th>
</tr>

<tr><td colspan="8" align="left"><br /><br /></td></tr>

<tr><td colspan="8" align="left">

<span class="sample"><span class="fup sam-box">&nbsp;&nbsp;</span> -- Finished &amp; Unpaid</span>
<span class="sample"><span class="uf sam-box">&nbsp;&nbsp;</span> -- Unfinished </span><br />
<span class="sample"><span class="sam-box">&nbsp;&bull;</span> -- Undelivered </span>
<span class="sample"><span class="sam-box">&Omega;</span> -- Outsourced(Inc. Offset)</span>

</td></tr>
</table>
 
 