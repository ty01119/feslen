<?php if (isset($menu_main1)): ?>
<div class="menu-main-left buttonset">
<?php 
foreach ($menu_main1 as $item){
$array = array('name'=>'menu-main1', 'id'=>$item['id'], 'onclick'=>$item['onclick']);
if ($item['checked']) $array['checked'] = "checked";
echo form_radio($array);
echo form_label($item['label'], $item['id']);
}

$array = array('name'=>'menu-main1', 'id'=>'menu-main1_0', 'onclick'=>"window.location.href='".site_url('leads/my')."'");
if ($this->uri->segment(1) == 'leads' && $this->uri->segment(2) == 'my') $array['checked'] = "checked";
echo form_radio($array);
echo form_label('My Profile', 'menu-main1_0');

echo form_radio(array('name'=>'menu-main1', 'id'=>'menu-main1_1', 'onclick'=>"openView('".site_url('user/edit_profile')."','profile');"));
echo form_label('Staffs', 'menu-main1_1'); 
 
echo form_radio(array('name'=>'menu-main1', 'id'=>'menu-main1_3', 'onclick'=>"window.location.href='".site_url('customer')."'"));
echo form_label('SOP', 'menu-main1_3');

echo form_radio(array('name'=>'menu-main1', 'id'=>'menu-main1_4', 'onclick'=>"window.location.href='".site_url('staff')."'"));
echo form_label('Files', 'menu-main1_4');

echo form_radio(array('name'=>'menu-main1', 'id'=>'menu-main1_5', 'onclick'=>"window.location.href='".site_url('files_sop')."'"));
echo form_label('Check In', 'menu-main1_5');
?>
</div> <!-- END menu ml -->

<div class="menu-main-left buttonset">
<?php 
echo form_radio(array('name'=>'menu-main2', 'id'=>'menu-main2_0', 'onclick'=>"window.location.href='".site_url('leads/my')."'"));
echo form_label('Projects', 'menu-main2_0');

echo form_radio(array('name'=>'menu-main2', 'id'=>'menu-main2_1', 'onclick'=>"openView('".site_url('user/edit_profile')."','profile');"));
echo form_label('Tasks', 'menu-main2_1'); 

echo form_radio(array('name'=>'menu-main2', 'id'=>'menu-main2_3', 'onclick'=>"window.location.href='".site_url('customer')."'"));
echo form_label('Signates', 'menu-main2_3');
?>
</div> <!-- END menu ml -->

<div class="menu-main-left buttonset" style="float:right;">
<?php 
echo form_radio(array('name'=>'menu-main3', 'id'=>'menu-main3_0', 'onclick'=>"window.location.href='".site_url('leads/my')."'"));
echo form_label('Projects', 'menu-main3_0');

echo form_radio(array('name'=>'menu-main3', 'id'=>'menu-main3_1', 'onclick'=>"openView('".site_url('user/edit_profile')."','profile');"));
echo form_label('Tasks', 'menu-main3_1'); 

echo form_radio(array('name'=>'menu-main3', 'id'=>'menu-main3_3', 'onclick'=>"window.location.href='".site_url('customer')."'"));
echo form_label('Signates', 'menu-main3_3');
?>
</div> <!-- END menu ml -->

<div class="menu-main-left">

<label for="from" class="button">From</label>
<input type="text" name="from" value="01/01/1970" id="from" class="ui-corner-all">

<label for="to" class="button">To</label>
<input type="text" name="to" value="01/01/1970" id="to" class="ui-corner-all">

</div> <!-- END menu ml -->

<div class="menu-main-left">
<input type="text" name="customer" value="Customer.." id="customer" class="blur ui-corner-all" style="width:200px;"> 
</div> <!-- END menu ml -->

<div class="menu-main-left"> 
<input type="text" name="search" value="Search.." class="blur ui-corner-all" style="width:200px;">
<input type="submit" name="submit" value="Go" class="button">
</div> <!-- END menu ml -->

<br style="clear:both;"/> 
<?php endif; ?>