<div id="navigation">
<div id="radio_menu0" class="ui_radio flt">
<?php 

echo form_radio(array('name'=>'radio_menu0', 'id'=>'radio_menu0_0', 'onclick'=>"window.location.href='".site_url('leads/my')."'"));
echo form_label('My Leads', 'radio_menu0_0');

echo form_radio(array('name'=>'radio_menu0', 'id'=>'radio_menu0_1', 'onclick'=>"openView('".site_url('user/edit_profile')."','profile');"));
echo form_label('My Profile', 'radio_menu0_1');

echo form_radio(array('name'=>'radio_menu0', 'id'=>'radio_menu0_2', 'onclick'=>"window.location.href='".site_url('quoting')."'"));
echo form_label('Quoting', 'radio_menu0_2');
 
echo form_radio(array('name'=>'radio_menu0', 'id'=>'radio_menu0_3', 'onclick'=>"window.location.href='".site_url('customer')."'"));
echo form_label('Customers', 'radio_menu0_3');

echo form_radio(array('name'=>'radio_menu0', 'id'=>'radio_menu0_4', 'onclick'=>"window.location.href='".site_url('staff')."'"));
echo form_label('Contacts', 'radio_menu0_4');

echo form_radio(array('name'=>'radio_menu0', 'id'=>'radio_menu0_5', 'onclick'=>"window.location.href='".site_url('files_sop')."'"));
echo form_label('SOP', 'radio_menu0_5');

echo form_radio(array('name'=>'radio_menu0', 'id'=>'radio_menu0_6', 'onclick'=>"window.location.href='".site_url('files')."'"));
echo form_label('File Folder', 'radio_menu0_6');

echo form_radio(array('name'=>'radio_menu0', 'id'=>'radio_menu0_7', 'onclick'=>"openView('http://upload.3a.co.nz/itn/','sendfile')"));
echo form_label('Upload File', 'radio_menu0_7');

echo form_radio(array('name'=>'radio_menu0', 'id'=>'radio_menu0_8', 'onclick'=>"window.open('http://mail.google.com/a/3a.co.nz', '_blank')"));
echo form_label('Mail', 'radio_menu0_8');

echo form_radio(array('name'=>'radio_menu0', 'id'=>'radio_menu0_8a', 'onclick'=>"openView('http://www.google.com/calendar/embed?src=ke%403a.co.nz&ctz=Pacific/Auckland', '_blank')"));
echo form_label('Calendar', 'radio_menu0_8a');

echo form_radio(array('name'=>'radio_menu0', 'id'=>'radio_menu0_9', 'onclick'=>"window.open('http://www.3a.co.nz', '_blank')"));
echo form_label('3A Website', 'radio_menu0_9');
?>
</div>



<div id="radio_menu2" class="ui_radio frt">
<?php
echo form_radio(array('name'=>'radio_menu2', 'id'=>'radio_menu2_1', 'onclick'=>"window.location.href='".site_url('attendance')."'"));
echo form_label('Attendance', 'radio_menu2_1');

if ($this->session->userdata('check_in')) 
  {
    echo form_radio(array('name'=>'radio_menu2', 'id'=>'radio_menu2_2', 'onclick'=>"window.location.href='".site_url('user/check_out')."'"));
    echo form_label('Check Out', 'radio_menu2_2');
  }
else 
  {
    echo form_radio(array('name'=>'radio_menu2', 'id'=>'radio_menu2_2', 'onclick'=>"window.location.href='".site_url('user/check_in')."'"));
    echo form_label('Check In', 'radio_menu2_2');
  }
?>
</div>

<br style="clear:both;"/><br />
<div id="radio_menu1" class="ui_radio flt">
<?php

echo form_radio(array('name'=>'radio_menu1', 'id'=>'radio_menu1_1', 'onclick'=>"window.location.href='".site_url('invoice')."'"));
echo form_label('3A '.strtoupper($this->session->userdata('branch')).' | Invoices', 'radio_menu1_1');

echo form_radio(array('name'=>'radio_menu1', 'id'=>'radio_menu1_2', 'onclick'=>"openView('".site_url('invoice/add')."','add');"));
echo form_label('+', 'radio_menu1_2');

echo form_radio(array('name'=>'radio_menu1', 'id'=>'radio_menu1_5', 'onclick'=>"window.location.href='".site_url('quotation')."'"));
echo form_label('Quotes', 'radio_menu1_5');

echo form_radio(array('name'=>'radio_menu1', 'id'=>'radio_menu1_6',  'onclick'=>"openView('".site_url('quotation/add')."','add');"));
echo form_label('+', 'radio_menu1_6');

echo form_radio(array('name'=>'radio_menu1', 'id'=>'radio_menu1_3', 'onclick'=>"window.location.href='".site_url('intertrans')."'"));
echo form_label('Internals', 'radio_menu1_3');

echo form_radio(array('name'=>'radio_menu1', 'id'=>'radio_menu1_4', 'onclick'=>"openView('".site_url('intertrans/add')."','add');"));
echo form_label('+', 'radio_menu1_4');

echo form_radio(array('name'=>'radio_menu1', 'id'=>'radio_menu1_8',  'onclick'=>"window.location.href='".site_url('payment')."'"));
echo form_label('Payments', 'radio_menu1_8');

echo form_radio(array('name'=>'radio_menu1', 'id'=>'radio_menu1_8a',  'onclick'=>"openView('".site_url('payment/add')."','add');"));
echo form_label('+', 'radio_menu1_8a');

echo form_radio(array('name'=>'radio_menu1', 'id'=>'radio_menu1_7',  'onclick'=>"window.location.href='".site_url('report')."'"));
echo form_label('Reports', 'radio_menu1_7');

echo form_radio(array('name'=>'radio_menu1', 'id'=>'radio_menu1_9',  'onclick'=>"window.location.href='".site_url('statement')."'"));
echo form_label('Statement', 'radio_menu1_9');
?>
</div>

<div id="radio_menu2" class="ui_radio flt">
<?php 
echo form_radio(array('name'=>'radio_menu2', 'id'=>'radio_menu2_9', 'onclick'=>"window.location.href='".site_url('web_app_task')."'"));
echo form_label('Web', 'radio_menu2_9'); 
?>
<?php 
echo form_radio(array('name'=>'radio_menu2', 'id'=>'radio_menu2_8', 'onclick'=>"window.location.href='".site_url('mgmt_task')."'"));
echo form_label('Tasks', 'radio_menu2_8'); 
?>
<?php 
echo form_radio(array('name'=>'radio_menu2', 'id'=>'radio_menu2_8a', 'onclick'=>"window.location.href='".site_url('mgmt_task_signs')."'"));
echo form_label('Signs', 'radio_menu2_8a'); 
?>
</div>


<script type="text/javascript"> 
$(".ui_radio").buttonset(); 
</script>


<div class="frt">
<div id="progressbar_turnover" style="width:150px;height:18px;line-height:18px;"> 
	<span class="pblabel"><?php echo get_turnover_rate();?>%</span>
</div>
<script type="text/javascript"> 
$( "#progressbar_turnover" ).progressbar({ value: <?php echo get_turnover_rate();?> });
var label = $( "#progressbar_turnover" ).find('.pblabel').clone().width($('#progressbar_turnover').width());
$( "#progressbar_turnover" ).find('.ui-progressbar-value').append(label);
$( "#progressbar_turnover" ).dblclick(function(){window.location.href='<?php echo site_url('stat_turnover');?>';});
</script>
</div>

<script type="text/javascript"> 
	       
var page = '<?php echo $this->uri->segment(1);?>';
var func = '<?php echo $this->uri->segment(2);?>';
var seg3 = '<?php echo $this->uri->segment(3);?>';

switch(page)
  {
  case 'leads':   
    $('#radio_menu0_0').attr('checked',true);
    break;
  case 'files':   
    $('#radio_menu0_6').attr('checked',true);
    break;
  case 'files_sop':   
    $('#radio_menu0_5').attr('checked',true);
    break;
  case 'staff':   
    $('#radio_menu0_4').attr('checked',true);
    break;
  case 'quoting':   
    $('#radio_menu0_2').attr('checked',true);
    break;
  case 'customer':   
    $('#radio_menu0_3').attr('checked',true);
    break;
 case 'invoice':   
    $('#radio_menu1_1').attr('checked',true);
    break;
 case 'quotation':   
    $('#radio_menu1_5').attr('checked',true);
    break;
 case 'intertrans':   
    $('#radio_menu1_3').attr('checked',true);
    break;
 case 'report':   
    $('#radio_menu1_7').attr('checked',true);
    break;
 case 'payment':   
    $('#radio_menu1_8').attr('checked',true);
    break;
 case 'statement':   
    $('#radio_menu1_9').attr('checked',true);
    break;
 case 'web_app_task':   
    $('#radio_menu2_9').attr('checked',true);
    break;
  case 'attendance':   
    switch(func)
      {
      case 'lib':
	$('#radio_menu2_1').attr('checked',true);
	break;
      case 'add':
	$('#radio_menu2_2').attr('checked',true); 
	break;
      case 'edit':
	$('#radio_menu2_3').attr('checked',true); 
	break;
      default:
      }
    break;

  case 2:
    break;
  default:
  }
</script>
</div>