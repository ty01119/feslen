<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?php $this->load->view('includes/html_head'); ?>

<body>
<div id="frame-wrapper" class="ui-widget" >

<!-- START HEADER -->
<div id="header" class="ui-widget-header ui-corner-top"  >
<span style="font-size:18px"><?php echo $frame_name;?></span>
</div>
<!-- END HEADER -->


<!-- START CONTENT -->
<div id="content-wrapper" class="ui-widget-content"  > 
<!-- START CONTENT INNER -->
<div id="content-inner" style="clear:both;">
<div class="ui-state-highlight ui-corner-all" style="float:left; padding:5px; margin:5px; clear:both;display:none;" id="msgbox">
<div class="ui-icon ui-icon-info" style="float: left; margin-right:10px;"></div>
<div style="float: left;" id="msg">
</div>
</div><br style="clear:both;">
<?php $this->load->view($main_content); ?>

<br style="clear:both;"><br />
</div>
<!-- END CONTENT INNER -->
<br style="clear:both;"/><br />
</div>
<!-- END CONTENT -->

<!-- START FOOTER -->
<div id="footer" class="ui-widget-header ui-corner-bottom" >
<span style="font-size:10px"><?php echo $cf_feslen['name'];?>&nbsp;&nbsp;&nbsp;&nbsp;&copy; 2010 &nbsp;3A Web Solution</span>
</div>
<!-- END FOOTER -->

</div>
</body>
</html>