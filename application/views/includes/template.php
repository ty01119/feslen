<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?php $this->load->view('includes/html_head'); ?>

<body>
<div id="wrapper" class="ui-widget">

<!-- START HEADER -->
<?php $this->load->view('includes/header'); ?>
<!-- END HEADER -->


<div id="main" class="ui-widget-content"> 
<?php $this->load->view('includes/session_msg'); ?>
<?php //$this->load->view('includes/filter_menu'); 
?>
<?php $this->load->view($main_content); ?>

<br style="clear:both;"/> 
</div> <!-- END main -->
 
 
<?php $this->load->view('includes/footer'); ?>

</div>
<!-- END Wrapper -->




<!-- start btm -->
<a id="back-top" href="#top" title="Back Top"></a>

<script type="text/javascript">
$(function () { 
$('#back-top').click(function(){
$('html, body').animate({scrollTop:0}, 'normal');
});

	var scroll_timer;
	var displayed = false;
	var $message = $('#back-top');
	var $window = $(window);
	var top = $(document.body).children(0).position().top;
 
	/* react to scroll event */
	$window.scroll(function () {
		window.clearTimeout(scroll_timer);
		scroll_timer = window.setTimeout(function () {  
			if($window.scrollTop() <= top)  
			{
				displayed = false;
				$message.fadeOut(500);
			}
			else if(displayed == false)  
			{
				displayed = true;
				$message.stop(true, true).show().click(function () { $message.fadeOut(500); });
			}
		}, 100);
	});
});
</script>
<!-- END btm -->
</body>
</html>