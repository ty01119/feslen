<div id="header" class="ui-widget-header"> 
<div id="logo">
<a style="font-size:24px;" href="<?php echo site_url();?>"><?php echo $cf_feslen['name'];?></a>
<span style="font-size:10px;">&nbsp;&nbsp;<?php echo $cf_feslen['co_name'];?></span>
</div> <!-- END logo -->

<div id="header-info">
<?php if (logged_in()) { ?> 

<div style="float:left; width:200px;"> 
<div id="progressbar_turnover" style="height:20px;line-height:20px;"><span class="pblabel">3A <?php echo $this->session->userdata('branch');?>  <?php echo get_turnover_rate();?>% (<?php echo $this->session->userdata('user_name');?>)</span>
</div>
<script type="text/javascript"> 
$( "#progressbar_turnover" ).progressbar({ value: <?php echo get_turnover_rate();?>  });
var label = $( "#progressbar_turnover" ).find('.pblabel').clone().width($('#progressbar_turnover').width());
$( "#progressbar_turnover" ).find('.ui-progressbar-value').append(label);
$( "#progressbar_turnover" ).dblclick(function(){window.location.href="<?php echo site_url('stat_turnover');?>";});
</script>
</div>

<span class="extra-info">
<a class="white" href="<?php echo base_url();?>user/change_password">Change Password</a>
</span>

<?php } ?> 
</div> <!-- END header info -->


<?php if (logged_in()) { ?>
<?php $this->load->view('includes/newsfeed'); ?> 
<?php } ?> 

<div class="menu-top-right buttonset">
<?php 
$array = array('name'=>'menu-topr', 'id'=>'menu-topr_0', 'onclick'=>"openView('".site_url('user/edit_profile')."','profile');");
if ($this->uri->segment(1) == 'leads' && $this->uri->segment(2) == 'my') $array['checked'] = "checked";
echo form_radio($array);
echo form_label('My Profile', 'menu-topr_0');

$array = array('name'=>'menu-topr', 'id'=>'menu-topr_1', 'onclick'=>"window.location.href='".site_url('staff')."'");
if ($this->uri->segment(1) == 'staff') $array['checked'] = "checked";
echo form_radio($array);
echo form_label('Staffs', 'menu-topr_1'); 
 
$array = array('name'=>'menu-topr', 'id'=>'menu-topr_3', 'onclick'=>"window.location.href='".site_url('files_sop')."'");
if ($this->uri->segment(1) == 'files_sop') $array['checked'] = "checked";
echo form_radio($array);
echo form_label('SOP', 'menu-topr_3');

$array = array('name'=>'menu-topr', 'id'=>'menu-topr_4', 'onclick'=>"window.location.href='".site_url('files')."'");
if ($this->uri->segment(1) == 'files') $array['checked'] = "checked";
echo form_radio($array);
echo form_label('Files', 'menu-topr_4');

$array = array('name'=>'menu-topr', 'id'=>'menu-topr_5a', 'onclick'=>"window.location.href='".site_url('attendance')."'");
if ($this->uri->segment(1) == 'attendance') $array['checked'] = "checked";
echo form_radio($array);
echo form_label('Attendance', 'menu-topr_5a');
 
if ($this->session->userdata('check_in')) 
  {
    echo form_radio(array('name'=>'menu-topr', 'id'=>'menu-topr_5', 'onclick'=>"window.location.href='".site_url('login/logout')."'"));
    echo form_label('Log Out', 'menu-topr_5');
  }
else 
  {
    echo form_radio(array('name'=>'menu-topr', 'id'=>'menu-topr_5', 'onclick'=>"window.location.href='".site_url('user/check_in')."'"));
    echo form_label('Check In', 'menu-topr_5');
  }

?>
</div> <!-- END header tr -->
 

<div class="menu-top-right buttonset">
<?php 

$array = array('name'=>'menu-topr2', 'id'=>'menu-topr2_0', 'onclick'=>"window.location.href='".site_url('web_app_task')."'");
if ($this->uri->segment(1) == 'web_app_task') $array['checked'] = "checked";
echo form_radio($array);
echo form_label('Websites', 'menu-topr2_0');

$array = array('name'=>'menu-topr2', 'id'=>'menu-topr2_1', 'onclick'=>"window.location.href='".site_url('mgmt_task')."'");
if ($this->uri->segment(1) == 'mgmt_task') $array['checked'] = "checked";
echo form_radio($array);
echo form_label('Prints', 'menu-topr2_1'); 

$array = array('name'=>'menu-topr2', 'id'=>'menu-topr2_3', 'onclick'=>"window.location.href='".site_url('mgmt_task_signs')."'");
if ($this->uri->segment(1) == 'mgmt_task_signs') $array['checked'] = "checked";
echo form_radio($array);
echo form_label('Signs', 'menu-topr2_3');
?>
</div> <!-- END menu tr -->





<div class="menu-top-left buttonset" style="clear:left;">
<?php 
$array = array('name'=>'menu-topl', 'id'=>'menu-topl_0', 'onclick'=>"window.location.href='".site_url('leads/my')."'");
if ($this->uri->segment(1) == 'leads') $array['checked'] = "checked";
echo form_radio($array);
echo form_label('Leads', 'menu-topl_0');

$array = array('name'=>'menu-topl', 'id'=>'menu-topl_1', 'onclick'=>"window.location.href='".site_url('quoting')."'");
if ($this->uri->segment(1) == 'quoting') $array['checked'] = "checked";
echo form_radio($array);
echo form_label('Quoting', 'menu-topl_1');

$array = array('name'=>'menu-topl', 'id'=>'menu-topl_2', 'onclick'=>"window.location.href='".site_url('customer')."'");
if ($this->uri->segment(1) == 'customer') $array['checked'] = "checked";
echo form_radio($array);
echo form_label('Customers', 'menu-topl_2');
 
$array = array('name'=>'menu-topl', 'id'=>'menu-topl_3', 'onclick'=>"window.location.href='".site_url('invoice')."'");
if ($this->uri->segment(1) == 'invoice') $array['checked'] = "checked";
echo form_radio($array);
echo form_label('Invoices', 'menu-topl_3');
echo form_radio(array('name'=>'menu-topl', 'id'=>'menu-topl_3a', 'onclick'=>"openView('".site_url('invoice/add')."','add');"));
echo form_label('+', 'menu-topl_3a');

$array = array('name'=>'menu-topl', 'id'=>'menu-topl_4', 'onclick'=>"window.location.href='".site_url('quotation')."'");
if ($this->uri->segment(1) == 'quotation') $array['checked'] = "checked";
echo form_radio($array);
echo form_label('Quotes', 'menu-topl_4');
echo form_radio(array('name'=>'menu-topl', 'id'=>'menu-topl_4a', 'onclick'=>"openView('".site_url('quotation/add')."','add');"));
echo form_label('+', 'menu-topl_4a');

$array = array('name'=>'menu-topl', 'id'=>'menu-topl_5', 'onclick'=>"window.location.href='".site_url('intertrans')."'");
if ($this->uri->segment(1) == 'intertrans') $array['checked'] = "checked";
echo form_radio($array);
echo form_label('Internals', 'menu-topl_5'); 
echo form_radio(array('name'=>'menu-topl', 'id'=>'menu-topl_5a', 'onclick'=>"openView('".site_url('intertrans/add')."','add');"));
echo form_label('+', 'menu-topl_5a');

$array = array('name'=>'menu-topl', 'id'=>'menu-topl_6', 'onclick'=>"window.location.href='".site_url('payment')."'");
if ($this->uri->segment(1) == 'payment') $array['checked'] = "checked";
echo form_radio($array);
echo form_label('Payments', 'menu-topl_6');
echo form_radio(array('name'=>'menu-topl', 'id'=>'menu-topl_6a', 'onclick'=>"openView('".site_url('payment/add')."','add');"));
echo form_label('+', 'menu-topl_6a');

$array = array('name'=>'menu-topl', 'id'=>'menu-topl_7', 'onclick'=>"window.location.href='".site_url('statement')."'");
if ($this->uri->segment(1) == 'statement') $array['checked'] = "checked";
echo form_radio($array);
echo form_label('Statement', 'menu-topl_7');

$array = array('name'=>'menu-topl', 'id'=>'menu-topl_8', 'onclick'=>"window.location.href='".site_url('report')."'");
if ($this->uri->segment(1) == 'report') $array['checked'] = "checked";
echo form_radio($array);
echo form_label('Reports', 'menu-topl_8');
?>
</div> <!-- END menu tl -->



</div> <!-- END header -->