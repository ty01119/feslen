<script type="text/javascript">
$(document).ready(function(){

  if ( !$.browser.webkit && !$.browser.mozilla) {
    alert( "Please use Mozilla Firefox or Google Chrome for this application." );
  }

});
</script>
<div style="float:left; padding:0 5px; clear:both;">
<h2 >Web System Login</h2>
</div>
<br style="clear:both;" /><br /><br /><br />
<div id="login_form" class="ui-widget-content ui-corner-all" style="float: none; clear:both;margin:0px auto; width:300px; align:left;padding:0 15px 5px 15px;"> 
<?php 
   echo form_open('login/validate_login', array('id' => 'myform'));
?>
<table border="0" cellpadding="0" cellspacing="0">
  <tr><td colspan="2">
  <h4>System Login</h4></td></tr>
  <tr><td>Login ID: </td><td>
<?php 
   echo form_input( 
		   array(
			 'name'        => 'login_id',
			 'style'       => 'width:200px;margin:10px;',
			 'class' => 'ui-corner-all'
			 ) 
		    );
?>
</td></tr><tr><td>Password:</td><td>
<?php 
echo form_password(	
		   array(
			 'name'        => 'password',
			 'style'       => 'width:200px;margin:10px;',
			 'class' => 'ui-corner-all'
			 )
			); 
?>
</td>
</tr>

<tr>
<td colspan="2" align="left" class="font9px">
<?php
echo br();
$data = array(
              'name'        => 'keepsignin',
	      'id'          => 'keepsignin',
              'value'       => 'keepsignin',
              'class'       => 'check',
	      'checked'     => FALSE
	      );
echo form_checkbox($data);
echo form_label('Keep me logged in (Office Only)', 'keepsignin');
echo br(2);
?>
</td></tr>

<tr>
<td colspan="2" align="right"  class="ui_button">
<?php 
   echo form_submit(array(
			  'name'        => 'submit',
			  'value'       => 'Login',
			  'style'       => 'margin:10px;'
			  )
		    );
?>

</td></tr>
</table>

<?php echo form_close(); ?>





 
</div>
<br /><br />