<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<link rel="shortcut icon" href="<?php echo base_url(); ?>images/favicon.ico">

<?php $this->load->view('includes/jquery_ui'); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.functions.js"></script>
<link href="<?php echo base_url(); ?>css/jquery.css" rel="stylesheet" type="text/css" media="screen" />


    <style>
      body {
      background-color:#ffffff;
      }
      table {
      margin-top: 10px;
      width:678px;
      border-collapse: collapse; clear:both;
      }
      table td,
      table th {
      padding: 0; 
      } 
      #title{
      width:300px;
      height:36px;
      color:#ffffff;
      background-color:#00064e;
      text-align: center;
      }
      #logo{
      width:78px;
      }
      #name{
      width:300px; text-align: center;
      }
      .title2{
      width:150px;
      height:24px;
      text-align:center;
      background-color:#004080;
      }
      .title3{
      width:150px;
      height:24px;
      text-align:center;
      }
      .info{
      width:150px;
      height:24px;
      text-align:left;
      }
      .l2title{
      width:90px;
      }
      .l2cnt{
      width:240px;
      }
      .hbsp15{
      width:18px;
      }
      .vline{
      display:block;height:14.4px;
      border-top:1px solid #e1e1e1;
      }
      .vbsp15{
      height:18px; 
      }
      .item-cell1{
      width:438px; 
      text-align:left;
      }
      .item-cell2{
      width:60px; 
      text-align:right;
      }
      .item-cell3{
      width:80px;
      text-align:right;
      }
      .item-cell4{
      width:40px;
      text-align:right;
      }

      .item-cell5{
      width:60px;
      text-align:right;
      }
      .cb1{
      height:14.4px;
      background-color:#004080;
      }
      .white20b{
      color: #ffffff;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 24px;
      }
      .black20b{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 24px;
      }
      .white12b{
      color: #ffffff;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 14.4px;
      }

      .white8b{
      color: #ffffff;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 12px;
      }
      .black12b{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 14.4px;
      }
      .black8b{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 11px;
      }
      .black8{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-size: 12px;
      }
      .ltvline{
      display:inline-block;height:16px;
      border-left:1px solid #e1e1e1;
      }
      #l4hline1{
      display:inline-block;width:14.4px;height:14.4px;
      border-top:1px solid #e1e1e1;
      }
      #l4hline2{
      display:inline-block;width:14.4px;height:14.4px;
      border-top:1px solid #e1e1e1;border-left:1px solid #e1e1e1;
      }
      #l4lt{
      width:414;
      }
      #l4mi{
      width:120px;
      }
      .l4sp1{
      width:14.4px;
      }
      .l4vline{
      width:2px;
      }
      .l4item{
      width:48px;
      text-align:left;
      }
     .l4nbsp{
      width:31.2px;
      }
      .l4value{
      width:48px;
      text-align:right;
      }

table#terms {
 page-break-before: always;
      margin-top: 10px;
      width:678px;
      border-collapse: collapse; clear:both;      
	  font-family: Arial, Helvetica, sans-serif;
 
      font-size: 11px;
      }
 table#terms h1,  table#terms h2{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 11px; display: inline;
      }
 
table#terms, table#terms table td {
vertical-align:top;
 }
 

table#pickup { 
      margin-top: 10px;
      width:678px;
      border-collapse: collapse; clear:both;      
	  font-family: Arial, Helvetica, sans-serif;
 
      font-size: 13px;
      }
table#pickup td{padding: 2px 0;
      }

#func-menu{
margin:10px auto 10px auto;
      width:678px; 
padding:5px;
}
button {
margin:0 5px 0 5px;
}
    </style>


<script type="text/javascript">
$(document)
    .ready(function() {
    	$('table').dblclick(function(){
		$('#func-menu').hide();$('#terms').hide();
		window.print();
		window.close();
	});
	$('#print').click(function(){
		$('#func-menu').hide();$('#terms').hide();$('#pickup').hide();
		window.print();
	//	$('body').fadeOut('fast', function() {
			window.close();
  	//	});
	});
	
	$('#print-pk').click(function(){
		$('#func-menu').hide();$('#terms').hide();
		window.print();
	//	$('body').fadeOut('fast', function() {
			window.close();
  	//	});
	});
	$('#print-all').click(function(){
		$('#func-menu').hide();
		window.print();
	//	$('body').fadeOut('fast', function() {
			window.close();
  	//	});
	});
	$('#close').click(function(){
	//	$('body').fadeOut('fast', function() {
			window.close();
  	//	});
	});
 
	$('#add-task').click(function(){
		//$('body').fadeOut('fast');
	//	window.location.href = '<?php echo site_url("mgmt_task/add/$invoice->id");?>';
		openView('<?php echo site_url("mgmt_task/add/$invoice->id");?>','new task');
	});
 
	$('#add-task-signs').click(function(){
		//$('body').fadeOut('fast');
	//	window.location.href = '<?php echo site_url("mgmt_task/add/$invoice->id");?>';
		openView('<?php echo site_url("mgmt_task_signs/add/$invoice->id");?>','new task');
	});
	 
}) 
</script>

  </head>
  <body>

<div id="func-menu" class="ui-widget-content ui-corner-all ui_button">
<?php
$data = array('id' => 'print', 'class' => 'font10px flt', 'content' => 'Print');
echo form_button($data);
?>
<?php
$data = array('id' => 'print-all', 'class' => 'font10px flt', 'content' => 'Print All');
echo form_button($data);
?>
<?php
$data = array('id' => 'print-pk', 'class' => 'font10px flt', 'content' => 'Print Pickup');
echo form_button($data);
?>
<?php
$data = array('id' => 'add-task', 'class' => 'font10px flt', 'content' => 'Add Task');
echo form_button($data);
?>
<?php
$data = array('id' => 'add-task-signs', 'class' => 'font10px flt', 'content' => 'Add Task for Sign');
echo form_button($data);
?>
<?php
//$data = array('id' => 'duplicate', 'class' => 'font10px flt', 'content' => 'Duplicate');
//echo form_button($data);
?>
 
<?php
$data = array('id' => 'close', 'class' => 'font10px frt', 'content' => 'Close');
echo form_button($data);
?>
<br style="clear:both;"/>
</div>

    <table class="collapse" border="0" align="center"> 
      <tbody>
	<tr>
	  <td colspan="2" id="title"><span class="white20b">TAX INVOICE</td>
	  <td rowspan="3" id="logo">
	  <!--
	  <img src="<?php echo 
	  ($branch->branch == 'websolution') ? 
	  base_url().'images/inv-logo-3aweb.png' :
	  base_url().'images/inv-logo-3a.jpg';
	  ?>" style="width:78px;height:78px;"/>
	  -->
	  </td>
	  <td rowspan="3" id="name"><span class="black20b"><?php echo $branch->biz_name;?></span>
	    <br /><br /> 
	    <span class="black12b"><?php echo $branch->address;?></span>
	    <br /> 
	    <span class="black8b"  >www.3a.co.nz
<?php echo nbs(4).'Email:'.nbs(2).$branch->email;?>
</span>
</td>
	</tr>
	<tr>
	  <td class="title2"><span class="white12b">NUMBER</span></td>
	  <td class="title2"><span class="white12b">DATE</span></td>
	</tr>
	<tr>
	 <td class="title3">
	 <span class="black12b"><?php echo $invoice->id;?></span>
	 </td>
	  <td class="title3">
	 <span class="black12b"><?php echo $invoice->date;?></span>
	 </td>
	</tr>

	<tr>
	  <td colspan="2"><span class="vline"></span></td>
	  <td ></td>
	  <td ></td>
	</tr>


	<tr>
	  <td colspan="2" class="info"><span class="black8b">
<?php echo 'Bank:'.nbs(2).$branch->bank_acc_num.nbs(4).'GST NO.:'.nbs(2).$branch->gst_num;?>
</span></td> 
	  <td></td>
	  <td class="info"><span class="black8b">
<?php echo 'Tel.:'.nbs(2).$branch->telephone.nbs(4).'Fax:'.nbs(2).$branch->fax;?>
</span></td>
	</tr>
      </tbody>
    </table>
    <table border="0" align="center"> 
      <tbody>
	<tr>
	  <td class="l2title"><span class="black12b">Customer</span></td>
	  <td class="l2cnt"><span class="black8"><?php echo $invoice->cust_name;?></span></td>
	  <td class="hbsp15"></td>
	  <td class="l2titel"><span class="black8b">Info</span></td>
	  <td class="l2cnt"><span class="black8"><?php echo $invoice->info;?></span></td>
	</tr>
	<tr><td colspan="2"><span class="vline"></span></td>
	  <td class="hbsp15"></td>
	  <td colspan="2"><span class="vline"></span></td>
	</tr>
	<tr><td colspan="5" class="vbsp15"></td></tr>
	<tr>
	  <td class="l2title"> 
	    <span class="black8b"><?php echo $customer->id;?></span>
	  </td>
	  <td class="l2cnt">
	    <span class="black8">
	      <?php echo $customer->address;?>
	      <br />
	      <?php echo $customer->city;?>
	    </span>
	  </td>
	  <td class="hbsp15"></td>
	  <td class="l2titel"><span class="black8b">Ship to</span></td>
	  <td class="l2cnt"><span class="black8"><?php echo $customer->ship_name;?></span></td>
	</tr>
	<tr>
	  <td class="l2title"><span class="black8b">&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
	  <td class="l2cnt"><span class="black8">
	      <i>Tel&nbsp;&nbsp;<?php echo $customer->phone;?></i><br />
	      <i>Fax&nbsp;&nbsp;<?php echo $customer->fax;?></i><br />
	      <i>Mol&nbsp;&nbsp;<?php echo $customer->cellphone;?></i><br />
	  </span></td>
	  <td class="hbsp15"></td>
	  <td class="l2titel"><span class="black8b">&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
	  <td class="l2cnt">
	    <span class="black8">
	      <?php echo $customer->ship_address;?>
	      <br />
	      <?php echo $customer->ship_city;?>
	    </span>
	  </td>
	</tr>
      </tbody>
    </table>
    <table border="0" align="center"> 
      <tbody>
	<tr>
	  <td class="cb1"></td>
	  <td class="item-cell1 cb1"><span class="white8b">&nbsp;&nbsp;Description</span></td>
	  <td class="cb1"></td>
	  <td class="item-cell2 cb1"><span class="white8b">QTY&nbsp;&nbsp;</span></td>
	  <td class="cb1"></td>
	  <td class="item-cell3 cb1"><span class="white8b">U/P&nbsp;&nbsp;</span></td>
	  <td class="cb1"></td>
	  <td class="item-cell4 cb1"><span class="white8b">DISC&nbsp;&nbsp;</span></td>
	  <td class="cb1"></td>
	  <td class="item-cell5 cb1"><span class="white8b">Amount&nbsp;&nbsp;</span></td>
	  <td class="cb1"></td>
	</tr>
<?php foreach ($contents as $item):?>
	<tr>
	  <td ><span class="ltvline"></span></td>
	  <td class="item-cell1">
	    <span class="black8">&nbsp;&nbsp;<?php echo $item->product;?></span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell2">
	    <span class="black8"><?php echo ($item->unit_price != 0)? $item->quantity : '';?>&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3">
	    <span class="black8"><?php echo ($item->unit_price != 0) ? $item->unit_price : '';?>&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell4">
	    <span class="black8"><?php echo ($item->unit_price != 0) ? $item->discount.'%' : '';?>&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell5">
	    <span class="black8">
	      <?php echo ($item->unit_price != 0) ? number_format($item->unit_price * $item->quantity * (1 - $item->discount / 100), 2, '.', ',') : '';?>&nbsp; &nbsp;
	    </span>
	  </td>
	  <td><span class="ltvline"></span></td>
	</tr>
<?php endforeach;?>

<?php if ($invoice->discount != 0) { ?>
	<tr>
	  <td ><span class="ltvline"></span></td>
	  <td class="item-cell1"><span class="black8">&nbsp;&nbsp;&nbsp;&nbsp;PROMOTION: ( Discount: <?php echo $invoice->discount;?>% off )
	  
	  </span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell2"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell4"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell5"><span class="black8"> -<?php echo number_format($invoice->subtotal / (1 - $invoice->discount / 100) - $invoice->subtotal, 2, '.', ',') ?>&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	</tr>
<?php } ?>
	<tr>
	  <td ><span class="ltvline"></span></td>
	  <td class="item-cell1"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell2"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell4"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell5"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	</tr>
	<tr>
	  <td ><span class="ltvline"></span></td>
	  <td class="item-cell1"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell2"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell4"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell5"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	</tr>
	<tr>
	  <td ><span class="ltvline"></span></td>
	  <td class="item-cell1"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell2"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell4"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell5"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	</tr>
	<tr>
	  <td ><span class="ltvline"></span></td>
	  <td class="item-cell1"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell2"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell4"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell5"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	</tr>
	<tr>
	  <td ><span class="ltvline"></span></td>
	  <td class="item-cell1"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell2"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell4"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell5"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	</tr>
	<tr>
	  <td colspan="11" class="cb1"></td>
	</tr>
      </tbody>
    </table>

    <table border="0" align="center"> 
      <tbody>
	<tr>
	  <td id="l4lt"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td id="l4mi"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td class="l4sp1"></td>
	  <td class="l4vline"><span class="ltvline"></span></td>
	  <td class="l4item"><span class="black8">&nbsp;&nbsp;Subtotal</span></td>
	  <td class="l4nbsp"></td>
	  <td class="l4value"><span class="black8"><?php echo $invoice->subtotal;?>&nbsp;&nbsp;</span></td>
	</tr>
	<tr>
	  <td id="l4lt"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td id="l4mi"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td class="l4sp1"></td>
	  <td class="l4vline"><span class="ltvline"></span></td>
	  <td class="l4item"><span class="black8">&nbsp;&nbsp;GST</span></td>
	  <td class="l4nbsp"></td>
	  <td class="l4value"><span class="black8"><?php echo $invoice->tax;?>&nbsp;&nbsp;</span></td>
	</tr>
	<tr>
	  <td id="l4lt"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td id="l4mi"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td class="l4sp1"></td>
	  <td class="l4vline"><span class="ltvline"></span></td>
	  <td class="l4item"><span class="black8">&nbsp;&nbsp;Total</span></td>
	  <td class="l4nbsp"></td>
	  <td class="l4value"><span class="black8"><?php echo $invoice->total;?>&nbsp;&nbsp;</span></td>
	</tr>
	<tr>
	  <td id="l4lt"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td id="l4mi"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td class="l4sp1"><span id="l4hline1"></span></td>
	  <td class="l4vline"><span id="l4hline2"></span></td>
	  <td class="l4item"><span class="black8">&nbsp;&nbsp;Paid</span></td>
	  <td class="l4nbsp"></td>
	  <td class="l4value"><span class="black8"><?php echo $invoice->paid;?>&nbsp;&nbsp;</span></td>
	</tr>
	<tr>
	  <td id="l4lt"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td id="l4mi"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td class="l4sp1"></td>
	  <td class="l4vline"><span class="ltvline"></span></td>
	  <td class="l4item"><span class="black8">&nbsp;&nbsp;Balance</span></td>
	  <td class="l4nbsp"></td>
	  <td class="l4value">
	    <span class="black8"><?php echo number_format($invoice->total - $invoice->paid, 2, '.', ',');?>&nbsp;&nbsp;</span>
	  </td>
	</tr>
      </tbody>
    </table>
 
 <table id="pickup" class="collapse" border="0" align="center" style="width: 678px;"> 
      <tbody>
	<tr>
	<td colspan="2" style="text-align: left;font-weight: bold;font-size:16px;">
	PICK UP INFORMATION
	</td>
	</tr>
	<tr>
	<td>Pick up Date:</td><td></td>
	</tr>	
	<tr>
	<td>Name:</td><td></td>
	</tr>	
	<tr>
	<td>Position:</td><td></td>
	</tr>	
	<tr>
	<td>contact no:</td><td></td>
	</tr>	
	<tr>
	<td colspan="2" style="text-align: left;">
* Payment will be expected within 7 days after pick up.
	</td>
	</tr>
      </tbody>
    </table>
        
    <table id="terms" class="collapse" border="0" align="center" style="width: 678px;"> 
      <tbody>
	<tr>
	<td colspan="2" style="text-align: center;font-weight: bold;font-size:16px;">
	TERMS & CONDITIONS
	</td>
	</tr>
	<tr>
	<td style="width: 300px;padding: 15px; ">
	<table style="width: 300px;"> 
      <tbody>
	<tr>
	<td style="width: 17px;"><h1>1. </h1></td>
	<td><h1>Definitions</h1> </td>
	</tr>
	<tr>
	<td><h1> </h1></td>
	<td>In these conditions unless the context otherwise
requires:</td>
	</tr>
	<tr>
	<td><h2>•</h2></td>
	<td>Company means <?php echo $branch->company;?></td>
	</tr>
	<tr>
	<td><h2>•</h2></td>
	<td>Buyer means the person, or company buying the goods
and services from the Company.</td>
	</tr>
	<tr>
	<td><h2>•</h2></td>
	<td>Products and/or services mean the products and/
or services being purchased by the Buyer from the
Company.</td>
	</tr>
	<tr>
	<td><h2>•</h2></td>
	<td>Contract means the contract between the Company and
the Buyer for the purchase of the goods</td>
	</tr>
	<tr>
	<td><h2>•</h2></td>
	<td>Contract price means the price of goods as agreed
between the Buyer and the Company.</td>
	</tr>
	<tr>
	<td><h2>•</h2></td>
	<td>Person includes a corporation, association, firm,
company, partnership or individual. Quotation shall
mean price on offer for a fixed term. Manager is the
companies appointed decision maker.</td>
	</tr>
	<tr>
	<td style="width: 15px;"><h1>2. </h1></td>
	<td><h1>Quotation</h1> </td>
	</tr>
	<tr>
	<td><h1> </h1></td>
	<td>The Buyer may request a Quotation from the Company
setting out the price and quantity of the Goods to be
supplied. If the Quotation is acceptable to the Buyer,
the Buyer may place an order within an acceptable
timeframe.</td>
	</tr>
	<tr>
	<td style="width: 15px;"><h1>3. </h1></td>
	<td><h1>Acceptance</h1> </td>
	</tr>
	<tr>
	<td><h1> </h1></td>
	<td>If any instruction is received by the Company from the
Buyer for the supply of products and/or services, it
shall constitute acceptance of the terms and conditions
contained herein. Upon acceptance of these terms and
conditions by the Buyer, the terms and conditions are
definitive and binding.</td>
	</tr>
	<tr>
	<td style="width: 15px;"><h1>4. </h1></td>
	<td><h1>Terms and Conditions</h1> </td>
	</tr>
	<tr>
	<td><h1> </h1></td>
	<td>These terms and conditions and any subsequent terms
and conditions issued by the Company shall apply to
all orders for the goods and the services made by the
Buyer after the date and time at which these conditions
are first delivered or sent by email to, or otherwise
brought to the notice of, any employee, staff member
or representative of the Buyer. It shall be the Buyer’s
responsibility to ensure that these conditions are
promptly brought to the attention of the appropriate
staff of the Buyer, and accordingly any order made by
the Buyer after the date and time described above in
this clause shall be deemed to be an acceptance of
these conditions.</td>
	</tr>
	<tr>
	<td><h1>5. </h1></td>
	<td><h1>Price</h1> </td>
	</tr>
	<tr>
	<td><h2>5.1</h2></td>
	<td>The Price shall be as indicated on invoices provided by
the Company to the Buyer in respect of products and/
or services supplied; or</td>
	</tr>
	<tr>
	<td><h2>5.2</h2></td>
	<td>The Price shall be the Price of the Company’s current
Price at the date of delivery of any goods.</td>
	</tr>
	<tr>
	<td><h2>5.3</h2></td>
	<td>Time for payment for the products and/or services shall
be of the essence and will be stated on the invoice,
quotation, tender documents, work authorisation form
or any other work commencement forms. If no time is
stated then payment shall be due on delivery of any
goods.</td>
	</tr>
	<tr>
	<td><h2>5.4</h2></td>
	<td>The Buyer agrees that the cost Price shall be
determined by the Company, and shall take into
consideration “one-off” costs such as design and
production.</td>
	</tr>
	<tr>
	<td><h2>5.5</h2></td>
	<td>The Company reserves the right to implement a
surcharge for alterations to specifications of products
after the order has been placed.</td>
	</tr>
	
	 </tbody>
	</table>
	
 
 


</td>
	<td style="width: 300px;padding: 15px; ">
		<table style="width: 300px;"> 
      <tbody>
	<tr>
	<td style="width: 17px;"><h1>6. </h1></td>
	<td><h1>Payment, Late Payment, Default of Payment and
Consequences of Default of Payment</h1></td>
	</tr>
	<tr>
	<td><h2>6.1</h2></td>
	<td>The method of payment will be made by cash, or by
cheque, or by bank cheque, or by direct credit, or by
any other method as agreed to between the Buyer and
the Company.</td>
	</tr>
    <tr>
	<td><h2>6.2</h2></td>
	<td>At the company’s sole discretion<br />
(a) Payment shall be due on the delivery of the Goods<br />
or<br />
(b) Payment for approved Buyers shall be due Thirty
(30) days.</td>
	</tr>
    <tr>
	<td><h2>6.3</h2></td>
	<td>Late payment shall incur interest at the rate of 15% per
annum calculated on a daily basis. This shall be payable
on any monies outstanding under the Contract from
the date payment was due until the date payment is
received by the Company, but without prejudice to the
Company’s other rights or remedies in respect of the
Buyer’s default in failing to make payment on the due
date.</td>
	</tr>
    <tr>
	<td><h2>6.4</h2></td>
	<td>Without prejudice to any other remedies the Company
may have, if at any time the Buyer is in breach of any
obligation (including those relating to payment), the
Company may suspend or terminate the supply of
Goods to the Buyer and any of its other obligations
under the terms and conditions. The Company will not
be liable to the Buyer for any loss or damage the Buyer
suffers because the Company exercised its rights under
this clause.</td>
	</tr>
    <tr>
	<td><h2>7</h2></td>
	<td><h1>Governing Laws</h1></td>
	</tr>
    <tr>
	<td><h2></h2></td>
	<td>These Terms of Trade will be interpreted in accordance
with applicable government legislation, which will have
exclusive legal jurisdiction over any dispute in relation
to the products and/or services or these Terms of Trade.</td>
	</tr>
    <tr>
	<td><h2>8</h2></td>
	<td><h1>Dispute Resolution</h1></td>
	</tr>
    <tr>
	<td><h2></h2></td>
	<td>
The Company will endeavour to resolve any dispute
between the Buyer and itself without the need for
Court proceedings. Any such attempt is without legal
prejudice.</td>
	</tr>
    <tr>
	<td><h2>9</h2></td>
	<td><h1>Reservation of Title</h1></td>
	</tr>
    <tr>
	<td><h2></h2></td>
	<td>Ownership and title of the goods remains with The
Company until the purchased price and all other monies
owing by the Buyer, under the contract or any other
contract to The Company, have been paid in full.</td>
	</tr>
    <tr>
	<td><h2>10</h2></td>
	<td><h1>Warranty</h1></td>
	</tr>
    <tr>
	<td><h2></h2></td>
	<td>
The Company warrants that it will repair or make good
any defects in the goods, if written notice of the claim
is received by the Company within seven (7) days from
the date the goods were delivered. If the Company
elects to repair or replace any defective goods,
such work shall be undertaken at such place as the
Company may reasonably specify and the Buyer shall
be responsible at its cost and risk for shipment of the
defective goods to the place specified.</td>
	</tr>
    <tr>
	<td><h2>11</h2></td>
	<td><h1>Liability</h1></td>
	</tr>
    <tr>
	<td><h2></h2></td>
	<td>
The Company shall not be liable for any loss of any
kind whatsoever suffered by the Buyer as a result of
any breach of any of the Company’s obligations under
the contract, including any cancellation of the contract
or any negligence on the part of the Company, its
servants, agents or contractors, nor shall the Company
be liable for any loss, damage or injury caused to the
Buyer’s servants, agents, contractors, buyers, visitors,
tenants, trespassers or other persons. The Buyer shall
indemnify the Company against any claim by any such
person.</td>
	</tr>
	
	 </tbody>
	</table>
	
	
	
	</td>
	</tr>
      </tbody>
    </table>


  </body> 
</html>