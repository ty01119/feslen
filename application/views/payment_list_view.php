  <script>
  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });



      
    });
</script>

<?php echo form_open('payment/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="ui_button font10px" style="width:200px">
<?php 
echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px">
<?php

?>
</td><td align="left" class="ui_button font10px"></td></tr>

<tr><td align="left" class="font10px">

</td><td align="left" class="font10px">

</td><td align="left" class="ui_button font10px">
<?php 
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => $search,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		 );
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td></tr>
</table>
<?php echo form_close(); ?>

 

		
<table border="0" cellpadding="5" cellspacing="0" width="900px"  style="margin:0 0 30px 0; min-width:900px; float: left; width:100%;">

<tr>
<th colspan="7" align="left"><?php echo $table_title;?></th>
</tr>

 

<tr class="bgc">
<td colspan="7" align="right" class="ui_button font9px">
<?php 
echo $this->pagination->create_links(); 
$attr = ($per_page == 25) ? array() : array('class' => 'white');
$attr['style'] = 'margin:0 10px;';
echo anchor("payment/filter/per_page/$total", 'Display All', $attr);

//$data = array('content' => 'View Report', 'onclick'=>"openView('".site_url("payment/report")."','report');");
//echo form_button($data);
?></td>
</tr>

<tr class="bgc">
<th align="left" valign="middle" width="60">
<?php echo anchor("payment/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="120">
<?php echo anchor("payment/orderby/date", 'Date', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'date'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle"  width="60">
<?php echo anchor("payment/orderby/invoice_id", 'Invoice ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'invoice_id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" >Detail</th>


<th align="left" valign="middle" width="120">Payment Method</th>

<th align="right" valign="middle" width="70">
<?php echo anchor("payment/orderby/paid", 'Paid', array('class' => 'black frt'));
if (isset($orderby) && $orderby['order'] == 'paid'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s frt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n frt"></span>';
endif;
?>
</th>
<th align="center" valign="middle" >Operations</th>
</tr>

<?php 
$accu = new stdClass;
$accu->total = $accu->eftpos = $accu->cheque = $accu->cash = $accu->banks = $accu->other = 0;

foreach($query->result() as $row):

$accu->total += $row->paid;

switch ($tmp_str = str_replace(';', '', $row->payment_meth)) {
    case 'Eftpos':
      $accu->eftpos += $row->paid;
        break;
    case 'Cheque':
      $accu->cheque += $row->paid;
        break;
    case 'Cash':
      $accu->cash += $row->paid;
        break;
    case 'Banks':
      $accu->banks += $row->paid;
        break;	
    default:
      $accu->other += $row->paid;
}

?> 
<tr class="bgc">
<td align="left" valign="middle"><?php echo $row->id;?></td>
<td align="left" valign="middle"><?php echo date('D M j, Y', strtotime($row->date));?></td>
<td align="left" valign="middle"><?php echo $row->invoice_id;?></td>
<td align="left" valign="middle"><?php echo $row->detail;?></td>
<td align="left" valign="middle"><?php echo $row->payment_meth;?></td>
<td align="right" valign="middle"><?php echo number_format($row->paid,2,'.',',');?></td>
<td align="center" valign="middle" class="ui_button font9px">
<?php 
$data = array('class' => 'invo-lib-btn','content' => 'Edit', 'onclick'=>"openView('".site_url("payment/edit/$row->id")."','edit-".$row->id."');");
echo form_button($data);
 
/*
echo anchor("javascript:openView(payment/edit/$row->id);", 'Edit').' '
  .anchor("payment/edit/$row->id", 'View').' '
  .anchor("payment/del/$row->id", 'Del');*/
?>
</td></tr>
<?php endforeach;?>

<tr><td colspan="7" align="left"></td></tr>

</table>
	
<table border="0" cellpadding="5" cellspacing="0" width="900px" style="margin:30px auto;">
<tr>
<th align="right" valign="middle"></th>
<th align="right" valign="middle" width="80">Cash</th>
<th align="right" valign="middle" width="80">Eftpos</th>
<th align="right" valign="middle" width="80">Cheque</th>
<th align="right" valign="middle" width="80">Banks</th>
<th align="right" valign="middle" width="80">Other</th>
<th align="right" valign="middle" width="80">Total</th>
</tr>
<tr>
<th align="right" valign="middle"></th>
<th align="right" valign="middle"><?php echo number_format($accu->cash,2,'.',',');?></th>
<th align="right" valign="middle"><?php echo number_format($accu->eftpos,2,'.',',');?></th>
<th align="right" valign="middle"><?php echo number_format($accu->cheque,2,'.',',');?></th>
<th align="right" valign="middle"><?php echo number_format($accu->banks,2,'.',',');?></th>
<th align="right" valign="middle"><?php echo number_format($accu->other,2,'.',',');?></th>
<th align="right" valign="middle"><?php echo number_format($accu->total,2,'.',',');?></th>
</tr>
</table>
 