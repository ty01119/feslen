<script>

var json_url = '<?php echo $json_url;?>';
var post_url = '<?php echo $post_url;?>';
var cart_url = '<?php echo $cart_url;?>';
$(function() {
    $( "#customer" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/cust_names');?>',
		  		   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
			
			
			
		      });
    
    $( "#developer, #designer" )
      .autocomplete({
	minLength: 1,
	    source: function(request, response){
	    $.ajax({
	      url: '<?php echo site_url('ajax/staff_names');?>',
		  dataType: 'json',
		  type: 'POST',
		  data: request,
		  success: function(data){
		  response(data);
		}
	      });
	  },    
	    });
    
  });
</script>
<script type="text/javascript" src="<?php echo base_url();?>js/web_app_task_edit_view.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>js/phpjs.js"></script>
<div style="padding:0;" id="inner-wrapper">

<br style="clear:both">

<div style="width:680px;background: #e1e1e1;color:#000000;" id="inner-text">


<table border="0" cellpadding="5" cellspacing="0" style="width:680px;padding:10px;">


<tr>
<td>
<?php
echo form_label('Task ID:', 'id');?>
</td><td>
<?php
$attr = array('name' => 'id', 'id' => 'id', 'class' => 'ui-corner-all', 'readonly' => 'readonly', 'style' => 'width:100px;') ;
echo form_input($attr);

?>  
</td>
<td>

<?php
echo form_label('Invoice ID:', 'invo_id');?>
</td>
<td>

<?php
$attr = array('name' => 'invo_id', 'id' => 'invo_id', 'value' => '', 'class' => 'ui-corner-all', 'style' => 'width:100px;'); 
echo form_input($attr);
?>  
</td>

<td>
<?php
echo form_label('Date:', 'date');?>
</td><td>
<?php
$attr = array('name' => 'date', 'id' => 'date', 'value' => '', 'class' => 'ui-corner-all', 'style' => 'width:100px;'); 
echo form_input($attr);
?>  
</td>

</tr>

<tr class="bgc">
<td class="ui_button" >
<?php
echo form_label('Customer:', 'customer');?>
</td>
<td colspan="5">
<?php
$attr = array('name' => 'customer', 'id' => 'customer', 'value' => '', 'class' => 'ui-corner-all', 'style' => 'width:300px;') ;
echo form_input($attr);
?>   
</td>
</tr>

<tr class="bgc">
<td class="ui_button" >
<?php
echo form_label('Developer:', 'developer');?>
</td>
<td colspan="5">
<?php
$attr = array('name' => 'developer', 'id' => 'developer', 'value' => '', 'class' => 'ui-corner-all', 'style' => 'width:300px;') ;
echo form_input($attr);
?>   
</td>
</tr>

<tr class="bgc">
<td class="ui_button" >
<?php
echo form_label('Designer:', 'designer');?>
</td>
<td colspan="5">
<?php
$attr = array('name' => 'designer', 'id' => 'designer', 'value' => '', 'class' => 'ui-corner-all', 'style' => 'width:300px;') ;
echo form_input($attr);
?>   
</td>
</tr>

<tr class="bgc">
<td class="ui_button" >
<?php
echo form_label('QA Engineer :', 'qa');?>
</td>
<td colspan="5">
<?php
$attr = array('name' => 'qa', 'id' => 'qa', 'value' => '', 'class' => 'ui-corner-all', 'style' => 'width:300px;') ;
echo form_input($attr);
?>   
</td>
</tr>


<tr class="bgc">
<td>Job Status: </td>
<td colspan="5">
<div class="ui_buttonset font10px">
<?php
echo form_checkbox(array('name'=>'finished', 'id'=>'finished', 'value'=>'Finished', 'style'=>'margin:10px'));
echo form_label('Finished', 'finished');
echo form_checkbox(array('name'=>'delivered', 'id'=>'delivered', 'value'=>'Delivered', 'style'=>'margin:10px'));
echo form_label('Delivered', 'delivered');
echo form_checkbox(array('name'=>'job_status', 'id'=>'paid', 'value'=>'Paid', 'style'=>'margin:10px'));
echo form_label('Totally Paid', 'paid');
?>
</div>
</td>


<tr class="bgc">
<td valign="top">
<?php
echo form_label('Notes:', 'notes');?>
</td>
<td colspan="5">
<textarea name="notes" id="notes" rows="12" style="width:500px" class="ui-corner-all"></textarea>
</td>
</tr>


<tr class="bgc">
<td valign="top">
<?php
echo form_label('Add Log:', 'comments');?>
</td>
<td colspan="5">
<textarea name="comments" id="comments" rows="2" style="width:500px" class="ui-corner-all"></textarea>
</td>
</tr>


</table>

   
</div>
<br style="clear:both">
<div style="float: left; padding:0 5px;" class="ui_button">
<?php 
echo form_input(array(
			 'type'  => 'button',
			 'name'  => 'save',
			 'id'    => 'save',
			 'value' => 'Save'
			 )
		   );
?>
</div>
<div style="float: right; padding:0 5px;" class="ui_button">
<?php 
  echo form_input(array(
			 'type' => 'button',
                         'name' => 'close',
			 'id'   => 'close',
			 'value' => 'Close'
			 )
		   );
?>
</div>
<br style="clear:both"><br />


<table border="0" cellpadding="5" cellspacing="0" style="width:680px;padding:10px;">


<tr>
<td valign="top">Logs</td>
<td colspan="5">
<div id="logs" style="width:500px">
</div>
</td>
</tr>
</table>
</div>