 
<script type="text/javascript" src="<?php echo base_url();?>lib/jquery.form.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>lib/jquery-ui-timepicker-addon.js"></script> 
 <script>
  $(function() {
      var dates = $( "#from, #to" )
	.datetimepicker({
		      dateFormat: 'dd/mm/yy',	timeFormat: ''
		    });


      $( "#name" )
	.autocomplete({
			minLength: 1,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/staff_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
			


		      });

      
    });
</script>

<script type="text/javascript">
$(function() {
    $('.attnForm').ajaxForm({
        beforeSubmit: function(a,f,o) {
            $('#ajaxOutput').html('Submitting...');
        },
        success: function(data) {
            var $out = $('#ajaxOutput');
	    
            $out.html(data);
	    //var item = $.parseJSON(data);
            
        }
    });
});
$(function() {
    $('.datetime').timepicker({timeFormat: 'hh:mm:ss',	hourGrid: 3,minuteGrid: 15});
});
</script> 


<?php echo form_open('mgmt_attn/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="ui_button font10px">
<span class="ui_buttonset">
<?php 
$data = array('content' => 'Attendance Management', 'style' => 'margin:5px 0;', 'onclick'=>"window.location.href='".site_url('mgmt_attn')."'");
echo form_button($data);
?>
</span>

</td><td align="left" class="ui_button font10px">
<span class="ui_buttonset">
<?php 
$data = array('content' => 'Add All for Today', 'style' => 'margin:5px 0;', 'onclick'=>"openView('".site_url('cron_jobs/attn_list')."','cron list');");
echo form_button($data);
?>
</span>
</td><td align="left" class="ui_button font10px">

</td><td align="left" class="ui_button font10px">

</td></tr>

<tr><td align="left" class="ui_button font10px" style="width:200px">
<?php 
echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_input(
		array(
		      'name'        => 'name',
		      'id'        => 'name',
		      'value'	  => $name,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		);

?>
</td><td align="left" class="ui_button font10px"></td></tr>

<tr><td align="left" class="font10px">

</td><td align="left" class="font10px">

</td><td align="left" class="ui_button font10px">
<?php 
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => $search,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		 );
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td></tr>
</table>
<?php echo form_close(); ?>



 

		
<table border="0" cellpadding="5" cellspacing="0" style="margin:0 0 30px 0; min-width:900px; float: left; width:100%; ">

<tr>
<th colspan="7" align="left"><?php echo $table_title;?> 
 <span class="font12px frt" id="ajaxOutput"></span>
</th>
</tr>

 

<tr class="bgc">
<td colspan="7" align="right" class="ui_button font9px">
<?php 
echo $this->pagination->create_links(); 
$attr = ($per_page == 25) ? array() : array('class' => 'white');
$attr['style'] = 'margin:0 10px;';
echo anchor("attendance/filter/per_page/$total", 'Display All', $attr);
//$data = array('content' => 'View Report', 'onclick'=>"openView('".site_url("attendance/report")."','report');");
//echo form_button($data);
?></td>
</tr>

<tr class="bgc">
<th align="left" valign="middle" width="100">
<?php echo anchor("attendance/orderby/date", 'Date', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'date'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>

<th align="left" valign="middle" width="80">
<?php echo anchor("attendance/orderby/working_id", 'Working ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'working_id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="100">
<?php echo anchor("attendance/orderby/name", 'Name', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'name'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="100">
<?php echo anchor("attendance/orderby/check_in_time", 'Check In Time', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'check_in_time'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="160">
<?php echo anchor("attendance/orderby/check_in_ip", 'Check In IP', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'check_in_ip'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="100">
<?php echo anchor("attendance/orderby/check_out_time", 'Check Out Time', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'check_out_time'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>

<th align="left" valign="middle" width="160">

<?php echo anchor("attendance/orderby/check_out_ip", 'Check Out IP', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'check_out_ip'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>

</th>

<th align="center" valign="middle" ></th>
</tr>

<?php 
 


foreach($query->result() as $row):

echo form_open('mgmt_attn/attn_submit', array('class' => 'attnForm'), array('id' => $row->id));

$ips = array('60.234.178.21', '60.234.178.22', '60.234.178.11', '60.234.178.15', '60.234.178.16');
$in = ' class="red" ';
$out =  ' class="red" ';
if (in_array($row->check_in_ip, $ips)) {$in = '';}
if (in_array($row->check_out_ip, $ips)) {$out = '';}

?> 
<tr class="bgc">

<td align="left" valign="middle">
<?php 

echo $row->date;
/*
  echo form_input(
		  array(
			'name'=>'date', 
			'id'=>'date', 
			'value'=>$row->date,
			'class'       => 'ui-corner-all',
			'style'       => 'width:130px;'
			)
		  );
		  */
?>
</td>

<td align="left" valign="middle">
<?php 
  echo form_input(
		  array(
			'name'=>'working_id', 
			'id'=>'working_id', 
			'value'=>$row->working_id,
			'class'       => 'ui-corner-all',
			'style'       => 'width:80px;'
			)
		  );
?>
</td>
<td align="left" valign="middle">
<?php 
  echo form_input(
		  array(
			'name'=>'name', 
			'id'=>'name', 
			'value'=>$row->name,
			'class'       => 'ui-corner-all',
			'style'       => 'width:80px;'
			)
		  );
?></td>
<td align="left" valign="middle">
<?php 
  echo form_input(
		  array(
			'name'=>'check_in_time', 
			'id'=>'check_in_time', 
			'value'=>$row->check_in_time,
			'class'       => 'ui-corner-all datetime',
			'style'       => 'width:90px;'
			)
		  );
?>
</td>
<td align="left" valign="middle" <?php echo $in;?> >
<?php 
  echo form_input(
		  array(
			'name'=>'check_in_ip', 
			'id'=>'check_in_ip', 
			'value'=>$row->check_in_ip,
			'class'       => 'ui-corner-all',
			'style'       => 'width:150px;'
			)
		  );
?>
</td>
<td align="left" valign="middle">
<?php 
  echo form_input(
		  array(
			'name'=>'check_out_time', 
			'id'=>'check_out_time', 
			'value'=>$row->check_out_time,
			'class'       => 'ui-corner-all datetime',
			'style'       => 'width:90px;'
			)
		  );
?>
</td>
<td align="left" valign="middle" <?php echo $out;?> >
<?php 
  echo form_input(
		  array(
			'name'=>'check_out_ip', 
			'id'=>'check_out_ip', 
			'value'=>$row->check_out_ip,
			'class'       => 'ui-corner-all',
			'style'       => 'width:150px;'
			)
		  );
?>
</td>
<td align="center" valign="middle" class="ui_button font9px">
<?php echo form_submit(array('value'=>'Save'));?>
</td></tr>
<?php echo form_close(); ?>
<?php endforeach;?>

<tr><td colspan="7" align="left"></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"><?php echo 'Total Data Returned: ';echo $total;?></th>
<th align="left" valign="middle">............</th>
<th align="right" valign="middle"> </th>
<th align="right" valign="middle"> </th>
<th align="right" valign="middle"></th>
<th align="center" valign="middle" class="ui_button font9px"><?php echo '';?></th>
</tr>
</table>
 