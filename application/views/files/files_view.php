<script type="text/javascript">

$(document).ready(function() {
		var files = <?php echo json_encode($files); ?>;
                var base_url = '<?php echo $base_url;?>';
		var file_tree = build_file_tree(files, base_url);
		file_tree.appendTo('#files');
		
		function build_file_tree(files, base_url) {
			
			var tree = $('<ul>');
			
			for (x in files) {
				if (typeof files[x] == "object") {
					var span = $('<span>').html('<span class="ui-icon ui-icon-folder-open flt"></span>&nbsp;&nbsp;' + x).appendTo(
						$('<li>').appendTo(tree)
					);  
					var subtree = build_file_tree(files[x], base_url + '/'+x+'/').hide();
					span.after(subtree);
					span.click(function() { 
						$(this).parent().find('ul:first').toggle('slow'); 
					});
					
				} else {
					$('<li>').html('<a target="_blank" class="black-n" href="'+ base_url + files[x]+'"><span class="ui-icon ui-icon-document flt"></span>&nbsp;&nbsp;'+ files[x] +'</a>').appendTo(tree);
				}
				
			}
			
			return tree;
			
		}
	});
</script>
	
<style type="text/css">
	.treetxt {
		font: 14px/18px Arial;
	}

	ul {
		list-style: none;
		padding-left: 20px;
		cursor: pointer;
	}
	li{
		padding-left: 20px;
		margin: 2px;
	}
</style>


<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="ui_button font10px">
<?php
$data = array('content' => 'Upload File', 'style' => 'margin:5px 0;', 'onclick'=>"openView('http://upload.3a.co.nz/itn','uploads');");
echo form_button($data);
?>
</td><td align="left" class="ui_button font10px">
<?php
$data = array('content' => 'Client Uploads', 'style' => 'margin:5px 0;', 'onclick'=>"openView('http://upload.3a.co.nz/ubr/list.php','uploads');");
echo form_button($data);
?>
</td><td align="left" class="ui_button font10px">
<?php
$data = array('content' => '3A Staff Uploads', 'style' => 'margin:5px 0;', 'onclick'=>"openView('http://upload.3a.co.nz/itn/list.php','uploads');");
echo form_button($data);
?>
</td><td align="left" class="ui_button font10px">

</td><td align="left" class="ui_button font10px">

</td></tr>

<tr><td align="left" class="font10px">
<span class="ui_button">

</span>
</td><td align="left" class="ui_button font10px">

</td><td align="left" class="ui_button font10px">

</td></tr>
</table>




 

		
<table border="0" cellpadding="5" cellspacing="0"  style="margin:30px 0; min-width:900px; float: left; width:100%;">

<tr>
<th colspan="7" align="left"><h2><?php echo $table_title;?></h2></th>
</tr>


 

<tr><td colspan="7" align="left">	<div id="files" class="treetxt"></div> </td></tr>
<tr><td colspan="7" align="left"></td></tr>
 
 

  

</table>

 
  