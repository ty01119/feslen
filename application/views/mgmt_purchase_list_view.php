  <script>
  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#vendor" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/vendor_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
			


		      });

      
    });
</script>

<?php echo form_open('mgmt_purchase/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="ui_button font10px" >
<span class="ui_buttonset">
<?php 
$data = array('content' => 'Purchase Management', 'style' => 'margin:5px 0;', 'onclick'=>"window.location.href='".site_url('mgmt_purchase')."'");
echo form_button($data);

$data = array('content' => '+', 'style' => 'margin:5px 0;', 'onclick'=>"openView('".site_url("mgmt_purchase/add")."','Add Purchase');");
echo form_button($data);
?>
</span>
</td><td align="left" class="ui_button font10px" colspan="3">
<?php 
$data = array('content' => 'Inventory Management', 'style' => 'margin:5px 0;', 'onclick'=>"window.location.href='".site_url('mgmt_inventory')."'");
echo form_button($data);
?>
<?php 
$data = array('content' => 'Bill Of Lading', 'style' => 'margin:5px 0;', 'onclick'=>"window.location.href='".site_url('mgmt_bol')."'");
echo form_button($data);
?>
<?php 
$data = array('content' => 'Vendors', 'style' => 'margin:5px 0;', 'onclick'=>"window.location.href='".site_url('mgmt_vendor')."'");
echo form_button($data);
?>
</td></tr>
<tr><td align="left" class="ui_button font10px" style="width:200px">
<?php 
echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_input(
		array(
		      'name'        => 'vendor',
		      'id'        => 'vendor',
		      'value'	  => $vendor,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		);

?>
</td><td align="left" class="ui_button font10px"></td></tr>

<tr><td align="left" class="font10px">

</td><td align="left" class="font10px">

</td><td align="left" class="ui_button font10px">
<?php 
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => $search,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		 );
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td></tr>
</table>
<?php echo form_close(); ?>


 

		
<table border="0" cellpadding="5" cellspacing="0" style="margin:30px 0; min-width:900px; float: left; width:100%;">

<tr>
<th colspan="8" align="left"><?php echo $table_title;?></th>
</tr>

 

<tr class="bgc">
<td colspan="8" align="right" class="ui_button font9px">
<?php 
echo $this->pagination->create_links(); 
$attr = ($per_page == 25) ? array() : array('class' => 'white');
$attr['style'] = 'margin:0 10px;';
echo anchor("mgmt_purchase/filter/per_page/$total", 'Display All', $attr);
$data = array('content' => 'View Report', 'onclick'=>"openView('".site_url("mgmt_purchase/report")."','report');");
echo form_button($data);
?></td>
</tr>

<tr class="bgc">
<th align="left" valign="middle" width="60">
<?php echo anchor("mgmt_purchase/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="120">
<?php echo anchor("mgmt_purchase/orderby/date", 'Date', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'date'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" >
<?php echo anchor("mgmt_purchase/orderby/cust_name", 'Vendor', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'cust_name'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle"  width="100">
<?php echo anchor("mgmt_purchase/orderby/branch", 'Branch', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'branch'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="right" valign="middle" width="70">
<?php echo anchor("mgmt_purchase/orderby/subtotal", 'Subtotal', array('class' => 'black frt'));
if (isset($orderby) && $orderby['order'] == 'subtotal'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s frt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n frt"></span>';
endif;
?>
</th>
<th align="right" valign="middle" width="70">
<?php echo anchor("mgmt_purchase/orderby/total", 'Total', array('class' => 'black frt'));
if (isset($orderby) && $orderby['order'] == 'total'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s frt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n frt"></span>';
endif;
?>
</th>

<th align="right" valign="middle" width="20"></th>

<th align="center" valign="middle" width="140">Operations</th>
</tr>

<?php 
$accu = new stdClass;
$accu->subtotal = $accu->total = 0;


foreach($query->result() as $row):

$row->debit = $row->total - $row->paid;
$accu->subtotal += $row->subtotal;
$accu->total += $row->total;

?> 
<tr class="bgc">
<td align="left" valign="middle"><?php echo $row->id;?></td>
<td align="left" valign="middle"><?php echo date('d/m/Y H:i:s', strtotime($row->date));?></td>
<td align="left" valign="middle"><?php echo $row->cust_name;?></td>
<td align="left" valign="middle"><?php echo $row->branch;?></td>
<td align="right" valign="middle"><?php echo number_format($row->subtotal,2,'.',',');?></td>
<td align="right" valign="middle"><?php echo number_format($row->total,2,'.',',');?></td>
<td align="right" valign="middle"></td>
<td align="center" valign="middle" class="ui_button font9px">
<?php 
$data = array('class' => 'invo-lib-btn','content' => 'Edit', 'onclick'=>"openView('".site_url("mgmt_purchase/edit/$row->id")."','edit-".$row->id."');");
echo form_button($data);
$data = array('class' => 'invo-lib-btn','content' => 'View', 'onclick'=>"openView('".site_url("mgmt_purchase/view/$row->id")."','view-".$row->id."');");
echo form_button($data);

$data = array('class' => 'invo-lib-btn','content' => 'Del', 'onclick'=>"openView('".site_url("mgmt_purchase/del/$row->id")."','del-".$row->id."');");
echo form_button($data);
/*
echo anchor("javascript:openView(mgmt_purchase/edit/$row->id);", 'Edit').' '
  .anchor("mgmt_purchase/edit/$row->id", 'View').' '
  .anchor("mgmt_purchase/del/$row->id", 'Del');*/
?>
</td></tr>
<?php endforeach;?>

<tr><td colspan="8" align="left"></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"><?php echo 'Total Data Returned: ';echo $total;?></th>
<th align="left" valign="middle" colspan="2">Accumulated Total of the Page............</th>
<th align="right" valign="middle"><?php echo number_format($accu->subtotal,2,'.',',');?></th>
<th align="right" valign="middle"><?php echo number_format($accu->total,2,'.',',');?></th>
<th align="right" valign="middle"></th>
<th align="center" valign="middle" class="ui_button font9px"><?php echo '';?></th>
</tr>
</table>

  