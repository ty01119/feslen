  <script>
  $(function() {
           
    });
</script>


<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="ui_button font10px">

<span class="ui_buttonset">
<?php 
$data = array('content' => 'Staff Management', 'style' => 'margin:5px 0;', 'onclick'=>"window.location.href='".site_url('mgmt_staff')."'");
echo form_button($data);
$data = array('content' => '+', 'style' => 'margin:5px 0;', 'onclick'=>"openView('".site_url("mgmt_staff/add")."','Add Staff');");
echo form_button($data);
?>
</span>
</td><td align="left" class="ui_button font10px">

</td><td align="left" class="ui_button font10px">

</td><td align="left" class="ui_button font10px">

</td></tr>

<tr><td align="left" class="font10px">
<span class="ui_buttonset">
<?php 
$attr = (isset($filter['1']['status']) 
	 && ($filter['1']['status'] == 'inactive')) ? array('class' => 'white') : array();
echo anchor("mgmt_staff/filter/1/status/inactive", 'X',$attr);
$attr = (isset($filter['1']['status']) 
	 && ($filter['1']['status'] == 'active')) ? array('class' => 'white') : array();
echo anchor("mgmt_staff/filter/1/status/active", 'Active',$attr);
?>
</span>
<span class="ui_buttonset">
<?php 
$attr = (isset($filter['2']['full_time']) 
	 && ($filter['2']['full_time'] == 'no')) ? array('class' => 'white') : array();
echo anchor("mgmt_staff/filter/2/full_time/no", 'X',$attr);
$attr = (isset($filter['2']['full_time']) 
	 && ($filter['2']['full_time'] == 'yes')) ? array('class' => 'white') : array();
echo anchor("mgmt_staff/filter/2/full_time/yes", 'Full Time',$attr);
?>
</span>
</td><td align="left" class="font10px">

</td><td align="left" class="ui_button font10px">

</td><td align="left" class="ui_button font10px">

</td></tr>
</table>




 

		
<table border="0" cellpadding="5" cellspacing="0" style="margin:30px 0; min-width:900px; float: left; width:100%;" >

<tr>
<th colspan="7" align="left"><?php echo $table_title;?></th>
</tr>

 

<tr class="bgc">
<td colspan="7" align="right" class="ui_button font9px">
<?php 
echo $this->pagination->create_links(); 
$attr = ($per_page == 25) ? array() : array('class' => 'white');
$attr['style'] = 'margin:0 10px;';
echo anchor("mgmt_staff/filter/per_page/$total", 'Display All', $attr);
?></td>
</tr>

<tr class="bgc">

<th align="left" valign="middle" width="90">
<?php echo anchor("mgmt_staff/orderby/working_id", 'Working ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'working_id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="120">
<?php echo anchor("mgmt_staff/orderby/name", 'Name', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'name'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="70">
<?php echo anchor("mgmt_staff/orderby/branch", 'Branch', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'branch'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="100">
<?php echo anchor("mgmt_staff/orderby/cellphone", 'cellphone', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'cellphone'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle">
<?php echo anchor("mgmt_staff/orderby/position", 'Position', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'position'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>

<th align="left" valign="middle" width="70">
<?php echo anchor("mgmt_staff/orderby/status", 'Status', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'status'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="center" valign="middle" width="140">Operations</th>
</tr>

<?php 



foreach($query->result() as $row):

  if (strlen($row->email) > 20) {
    $row->email = substr($row->email, 0, 17).'...';
  }
if (strlen($row->name) > 40) {
  $row->name = substr($row->name, 0, 37).'...';
}
?> 
<tr class="bgc">

<td align="left" valign="middle"><?php echo $row->working_id;?></td>
<td align="left" valign="middle"><?php echo $row->name;?></td>
<td align="left" valign="middle"><?php echo $row->branch;?></td>
<td align="left" valign="middle"><?php echo $row->cellphone;?></td>
<td align="left" valign="middle"><?php echo $row->position;?></td>
<td align="left" valign="middle"><?php echo $row->status;?></td>
<td align="center" valign="middle" class="ui_button font9px">
<?php 
$data = array('class' => 'invo-lib-btn','content' => 'Edit', 'onclick'=>"openView('".site_url("mgmt_staff/edit/$row->working_id")."','edit-".$row->id."');");
echo form_button($data);
$data = array('class' => 'invo-lib-btn','content' => 'View', 'onclick'=>"openView('".site_url("mgmt_staff/view/$row->working_id")."','view-".$row->id."');");
echo form_button($data);

$data = array('class' => 'invo-lib-btn','content' => 'Del', 'onclick'=>"openView('".site_url("mgmt_staff/del/$row->working_id")."','del-".$row->id."');");
echo form_button($data);
 
?>
</td></tr>
<?php endforeach;?>

<tr><td colspan="7" align="left"></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"><?php echo 'Total Data Returned: ';echo $total;?></th>
<th align="left" valign="middle"></th>
<th align="right" valign="middle"></th>
<th align="right" valign="middle"></th>
<th align="right" valign="middle"></th>
<th align="center" valign="middle" class="ui_button font9px"></th>
</tr>
</table>

  