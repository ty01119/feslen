<script>


var json_url = '<?php echo $json_url;?>';
var post_url = '<?php echo $post_url;?>';
var read_only = <?php echo $read_only;?>;
var mode = '<?php if (isset($mode)) echo $mode;?>';
$(function() {
    $( "#date_of_cmnc, #date_of_birth" ).datepicker({
      dateFormat: 'dd/mm/yy'
	  });

    //if (mode == 'add') {$( "#working_id" ).removeAttr('readonly');}
    $( "#working_id" ).removeAttr('readonly');
  });

</script>
<script type="text/javascript" src="<?php echo base_url();?>js/staff_edit_view.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/phpjs.js"></script>
<div style="padding:10px 0;" id="inner-wrapper">
<div style="width:680px;background: #e1e1e1;color:#000000;" id="inner-text">


<table border="0" cellpadding="5" cellspacing="0" style="width:680px;padding:10px;">
<tr class="bgc">
<td>
<?php
echo form_label('Name:', 'name');?>
</td><td>
<?php
$attr = array('name' => 'name', 'id' => 'name', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td>

<td>
 
</td><td>
 
</td>


</tr>
<tr><td colspan="4"></td></tr>

<tr class="bgc">
<td class="ui_button" >
<?php
echo form_label('Full Name:', 'full_name');
?>
</td>
<td colspan="2">
<?php
$attr = array('name' => 'full_name', 'id' => 'full_name', 'class' => 'cst ui-corner-all',  'style' => 'width:300px;'); 
echo form_input($attr);
?> 
</td>
<td>
Working ID:&nbsp;&nbsp;&nbsp;&nbsp;
<?php
$attr = array('name' => 'working_id', 'id' => 'working_id', 'value' => '', 'class' => 'cst ui-corner-all', 'readonly' => 'readonly', 'style' => 'width:100px;') ;
echo form_input($attr);
?>   
</td>
</tr>


 <tr><td colspan="4"></td></tr>

<tr class="bgc">
<td>
<?php
echo form_label('Branch:', 'branch');?>
</td><td>
<?php
$attr = array('name' => 'branch', 'id' => 'branch', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td>

<td>
<?php
echo form_label('Position:', 'position');?>
</td><td>
<?php
$attr = array('name' => 'position', 'id' => 'position', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td>


</tr>

<tr><td colspan="4"></td></tr>

<tr>
<td>
<?php
echo form_label('Authority:', 'authority');
?>
</td><td colspan="3">
<?php
$attr = array('name' => 'authority', 'id' => 'authority', 'class' => 'cst ui-corner-all',  'style' => 'width:500px;'); 
echo form_input($attr);
?>
</td></tr>

<tr class="bgc">
<td>
<?php
echo form_label('Academic Background:', 'academic_bg');?>
</td><td colspan="3">
<?php
$attr = array('name' => 'academic_bg', 'id' => 'academic_bg', 'class' => 'cst ui-corner-all', 'style' => 'width:500px;'); 
echo form_input($attr);
?>
</td>
</tr>

<tr><td colspan="4"></td></tr>


<tr class="bgc">
<td>
<?php
echo form_label('Cell Phone:', 'cellphone');?>
</td><td>
<?php
$attr = array('name' => 'cellphone', 'id' => 'cellphone', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td>
<td>
<?php
echo form_label('Telephone:', 'telephone');?>
</td><td>
<?php
$attr = array('name' => 'telephone', 'id' => 'telephone', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;'); 
echo form_input($attr);
?>
</td></tr>

<tr><td colspan="4"></td></tr><tr><td colspan="4"></td></tr>

<tr class="bgc">
<td>
<?php
echo form_label('Email:', 'email');?>
</td><td>
<?php
$attr = array('name' => 'email', 'id' => 'email', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td>
<td>
<?php
echo form_label('Address:', 'address');?>
</td><td>
<?php
$attr = array('name' => 'address', 'id' => 'address', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;'); 
echo form_input($attr);
?>
</td></tr>

<tr><td colspan="4"></td></tr><tr><td colspan="4"></td></tr>

<tr class="bgc">
<td>
<?php
echo form_label('Date of Birth:', 'date_of_birth');?>
</td><td>
<?php
$attr = array('name' => 'date_of_birth', 'id' => 'date_of_birth', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td>
<td>
<?php
echo form_label('Date of Commerce:', 'date_of_cmnc');?>
</td><td>
<?php
$attr = array('name' => 'date_of_cmnc', 'id' => 'date_of_cmnc', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;'); 
echo form_input($attr);
?>
</td></tr>

<tr><td colspan="4"></td></tr><tr><td colspan="4"></td></tr>

<tr class="bgc">
<td>
<?php
echo form_label('Ird Number:', 'ird_num');?>
</td><td>
<?php
$attr = array('name' => 'ird_num', 'id' => 'ird_num', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td>
<td>
<?php
echo form_label('Bank Account Number:', 'bank_acc_num');?>
</td><td>
<?php
$attr = array('name' => 'bank_acc_num', 'id' => 'bank_acc_num', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;'); 
echo form_input($attr);
?>
</td></tr>

<tr><td colspan="4"></td></tr><tr><td colspan="4"></td></tr>

<tr class="bgc">
<td>
<?php
echo form_label('Working Hours:', 'working_hrs');?>
</td><td>
<?php
$attr = array('name' => 'working_hrs', 'id' => 'working_hrs', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);
?>  
</td>
<td>
<?php
echo form_label('Working Time:', 'working_time');?>
</td><td>
<?php
$attr = array('name' => 'working_time', 'id' => 'working_time', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);
?> 
</td>
</tr>

<tr><td colspan="4"></td></tr>

<tr class="bgc">
<td>
<?php
echo form_label('Pay Rate:', 'pay_rate');?>
</td><td>
<?php
$attr = array('name' => 'pay_rate', 'id' => 'pay_rate', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>   
</td>
<td>
<?php
echo form_label('Full Time: <br /> (yes / no)', 'full_time');
?>
</td><td>
<?php
$attr = array('name' => 'full_time', 'id' => 'full_time', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);
?>
</td>
</tr>

<tr><td colspan="4"></td></tr>


<tr class="bgc">
<td>
<?php
 echo form_label('Gender <br /> (male / female)', 'gender');
?>
</td><td>
<?php
$attr = array('name' => 'gender', 'id' => 'gender', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td>
<td>
<?php
 echo form_label('Status <br /> (active / inactive)', 'status');
?>
</td><td>
<?php
$attr = array('name' => 'status', 'id' => 'status', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;'); 
echo form_input($attr);
?>
</td>

</tr>

<tr><td colspan="4"></td></tr>

<tr>
<td>
<?php
echo form_label('<strong>Password</strong>:', 'password');
?>
</td><td colspan="3">
<?php
$attr = array('name' => 'password', 'id' => 'password', 'class' => 'cst ui-corner-all',  'style' => 'width:200px;'); 
echo form_input($attr);
?>
</td></tr>

<tr><td colspan="4"></td></tr>

<tr class="bgc">
<td valign="top">
<?php
echo form_label('Management Notes:', 'mgmt_notes');
?>
</td>
<td colspan="5">
<textarea name="mgmt_notes" id="mgmt_notes" rows="4" style="width:500px" class="ui-corner-all cst"></textarea>
</td>
</tr>

<tr class="bgc">
<td>
<?php
  //echo form_label('Pay Rate:', 'pay_rate');
?>
</td><td>
<?php
$attr = array('name' => 'pay_rate', 'id' => 'pay_rate', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
//echo form_input($attr);

?>  
</td>
<td>
<?php
  //echo form_label('Ship City:', 'ship_city');
?>
</td><td>
<?php
$attr = array('name' => 'ship_city', 'id' => 'ship_city', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;'); 
//echo form_input($attr);
?>
</td>

</tr>

</table>

   
</div>
<br style="clear:both">
<div style="float: left; padding:0 5px;" class="ui_button">
<?php 
echo form_input(array(
			 'type'  => 'button',
			 'name'  => 'save',
			 'id'    => 'save',
			 'value' => 'Save'
			 )
		   );
?>
</div>
<div style="float: right; padding:0 5px;" class="ui_button">
<?php 
  echo form_input(array(
			 'type' => 'button',
                         'name' => 'close',
			 'id'   => 'close',
			 'value' => 'Close'
			 )
		   );
?>
</div>


</div>