<script>
var json_url = '<?php echo $json_url;?>';
var post_url = '<?php echo $post_url;?>';
var read_only = <?php echo $read_only;?>;
</script>
<script type="text/javascript" src="<?php echo base_url();?>js/customer_edit_view.js"></script>

<div style="padding:10px 0;" id="inner-wrapper">
<div style="width:680px;background: #e1e1e1;color:#000000;" id="inner-text">


<table border="0" cellpadding="5" cellspacing="0" style="width:680px;padding:10px;">
<tr class="bgc">
<td class="ui_button" >
<?php
echo form_label('Customer:', 'name');?>
</td>
<td colspan="2">
<?php
$attr = array('name' => 'name', 'id' => 'name', 'value' => '', 'class' => 'cst ui-corner-all', 'style' => 'width:300px;') ;
echo form_input($attr);
?> 
</td>
<td>
ID:&nbsp;&nbsp;&nbsp;&nbsp;
<?php
$attr = array('name' => 'id', 'id' => 'id', 'value' => '', 'class' => 'cst ui-corner-all', 'readonly' => 'readonly', 'style' => 'width:100px;') ;
echo form_input($attr);
?>   
</td>
</tr>

<tr><td colspan="4"></td></tr><tr><td colspan="4"></td></tr>

<tr class="bgc">
<td>
<?php
echo form_label('Contact:', 'contact');?>
</td><td>
<?php
$attr = array('name' => 'contact', 'id' => 'contact', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td>
<td>
<?php
echo form_label('Phone:', 'phone');?>
</td><td>
<?php
$attr = array('name' => 'phone', 'id' => 'phone', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;'); 
echo form_input($attr);
?>
</td>
</tr>

<tr><td colspan="4"></td></tr>

<tr class="bgc">
<td>
<?php
echo form_label('Cell Phone:', 'cellphone');?>
</td><td>
<?php
$attr = array('name' => 'cellphone', 'id' => 'cellphone', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td>
<td>
<?php
echo form_label('Fax:', 'fax');?>
</td><td>
<?php
$attr = array('name' => 'fax', 'id' => 'fax', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;'); 
echo form_input($attr);
?>
</td></tr>

<tr><td colspan="4"></td></tr><tr><td colspan="4"></td></tr>

<tr class="bgc">
<td>
<?php
echo form_label('Email:', 'email');?>
</td><td>
<?php
$attr = array('name' => 'email', 'id' => 'email', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td>
<td>
<?php
echo form_label('URL:', 'url');?>
</td><td>
<?php
$attr = array('name' => 'url', 'id' => 'url', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;'); 
echo form_input($attr);
?>
</td></tr>


<tr><td colspan="4"></td></tr><tr><td colspan="4"></td></tr>

<tr class="bgc">
<td>
<?php
echo form_label('Billing Contact:', 'billing_contact');?>
</td><td>
<?php
$attr = array('name' => 'billing_contact', 'id' => 'billing_contact', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td>
<td>
<?php
echo form_label('Billing Email:', 'billing_email');?>
</td><td>
<?php
$attr = array('name' => 'billing_email', 'id' => 'billing_email', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;'); 
echo form_input($attr);
?>
</td>
</tr>


<tr><td colspan="4"></td></tr><tr><td colspan="4"></td></tr>

<tr class="bgc">
<td>
<?php
echo form_label('Discount:', 'discount');?>
</td><td>
<?php
$attr = array('name' => 'discount', 'id' => 'discount', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td>
<td>
<?php
echo form_label('Pricing:', 'pricing');?>
</td><td>
<?php
$attr = array('name' => 'pricing', 'id' => 'pricing', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;'); 
echo form_input($attr);
?>
</td></tr>

<tr><td colspan="4"></td></tr><tr><td colspan="4"></td></tr>

<tr class="bgc">
<td>
<?php
echo form_label('Address:', 'address');?>
</td><td>
<?php
$attr = array('name' => 'address', 'id' => 'address', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td>
<td>
<?php
echo form_label('City:', 'city');?>
</td><td>
<?php
$attr = array('name' => 'city', 'id' => 'city', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;'); 
echo form_input($attr);
?>
</td></tr>

<tr><td colspan="4"></td></tr><tr><td colspan="4"></td></tr>

<tr class="bgc">
<td>
<?php
echo form_label('Ship Name:', 'ship_name');?>
</td><td colspan="5">
<?php
$attr = array('name' => 'ship_name', 'id' => 'ship_name', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td></tr>

<tr><td colspan="4"></td></tr>

<tr class="bgc">
<td>
<?php
echo form_label('Ship Address:', 'ship_address');?>
</td>
<td>
<?php
$attr = array('name' => 'ship_address', 'id' => 'ship_address', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);

?>  
</td>
<td>
<?php
echo form_label('Ship City:', 'ship_city');?>
</td>
<td>
<?php
$attr = array('name' => 'ship_city', 'id' => 'ship_city', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;'); 
echo form_input($attr);
?>
</td>

</tr>

<tr><td colspan="4"></td></tr>
<tr><td colspan="4"></td></tr>

<tr class="bgc">
<td>
<?php
echo form_label('Sales Person:', 'salesperson');?>
</td>
<td  >
<?php
$attr = array('name' => 'salesperson', 'id' => 'salesperson', 'class' => 'cst ui-corner-all', 'style' => 'width:200px;') ;
echo form_input($attr);
?>  
</td>
<td>
<?php
echo form_label('Branch:', 'branch');?>
</td>
<td  >
<?php
$attr = array('name' => 'branch', 'id' => 'branch', 'class' => 'cst ui-corner-all', 'readonly' => 'readonly', 'style' => 'width:200px;') ;
echo form_input($attr);
?>  
</td>


</tr>
<tr><td colspan="4"></td></tr><tr><td colspan="4"></td></tr>

<tr class="bgc">
<td valign="top">
<?php
echo form_label('Notes:', 'notes');?>
</td>
<td colspan="3">
<textarea name="notes" id="notes" rows="2" style="width:500px" class="cst ui-corner-all"></textarea>
</td>
</tr>


</table>

   
</div>
<br style="clear:both">
<div style="float: left; padding:0 5px;" class="ui_button">
<?php 
echo form_input(array(
			 'type'  => 'button',
			 'name'  => 'save',
			 'id'    => 'save',
			 'value' => 'Save'
			 )
		   );
?>
</div>
<div style="float: right; padding:0 5px;" class="ui_button">
<?php 
  echo form_input(array(
			 'type' => 'button',
                         'name' => 'close',
			 'id'   => 'close',
			 'value' => 'Close'
			 )
		   );
?>
</div>


</div>