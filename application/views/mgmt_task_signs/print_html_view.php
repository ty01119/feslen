<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<link rel="shortcut icon" href="<?php echo base_url(); ?>images/favicon.ico">

<?php $this->load->view('includes/jquery_ui'); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.functions.js"></script>
<link href="<?php echo base_url(); ?>css/jquery.css" rel="stylesheet" type="text/css" media="screen" />


    <style>
      body {
      background-color:#ffffff;
      font-family: Arial, Helvetica, sans-serif;
      }
      table {
      margin-top: 10px;
      width:678px;
      border-collapse: collapse; clear:both;
      } 
      table td,
      table th {
      padding: 5px; 
      text-align:left;
      }  
       

table tr  {
border-bottom: #979596 solid 1px;    
}

table td   {
border-right: #979596 solid 1px;
}

table tr td:last-child {
    border-right: none;
   
}
table tr:last-child {
    border-bottom: none;
   
}
 
 
label {
    font-weight: bold;
}
#func-menu{
margin:10px auto 10px auto;
      width:678px; 
padding:5px;
}
button {
margin:0 5px 0 5px;
}
    </style>


<script type="text/javascript">
$(document)
    .ready(function() {
    	$('table').dblclick(function(){
		$('#func-menu').hide();
		window.print();
		window.close();
	});
	$('#print').click(function(){
		$('#func-menu').hide();$('#terms').hide();
		window.print();
	//	$('body').fadeOut('fast', function() {
			window.close();
  	//	});
	});
	
	 
	$('#close').click(function(){
	//	$('body').fadeOut('fast', function() {
			window.close();
  	//	});
	});
 
	 
 	 
}) 
</script>

  </head>
  <body>

<div id="func-menu" class="ui-widget-content ui-corner-all ui_button">
<?php
$data = array('id' => 'print', 'class' => 'font10px flt', 'content' => 'Print');
echo form_button($data);
?>
 
 
<?php
$data = array('id' => 'close', 'class' => 'font10px frt', 'content' => 'Close');
echo form_button($data);
?>
<br style="clear:both;"/>
</div>

    <table class="collapse" border="0" align="center"> 
      <tbody>
<!-- -->
<tr>
<td>
<label for="invo_id">Task Info:</label>
</td>

<td>
<label>Invoice ID: </label>
<?php echo $task->invo_id;?>
</td>

 
<td colspan="2">
<label>Task ID: </label>
<?php echo $task->id;?>
</td>

<td colspan="2">
<label>Date: </label>
<?php echo $task->date;?>
</td>
</tr>
<!-- -->

<!-- -->
<tr class="bgc">
<td  >
<label for="date_due">Due Date:</label> 
</td>
<td colspan="3">
<?php echo $task->date_due;?>
</td>
<td colspan="2">
<?php echo $task->urgent ? 'Urgent' : '';?>
</td>
</tr>
<!-- -->



<!-- -->
<tr class="bgc">
<td class="ui_button" >
<label for="cust_name">Customer:</label></td>
<td colspan="5">
<?php echo $task->cust_name;?> 
</td>
</tr>
<!-- -->

<!-- -->
<tr class="bgc">
<td class="ui_button" >
<label for="staff_pre_press">Pre-press Staff:</label></td>
<td colspan="5">
<?php echo $task->staff_pre_press;?>
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td class="ui_button" >
<label for="file_name">File Name:</label></td>
<td colspan="5">
<?php echo $task->file_name;?>
</td>
</tr>
<!-- -->

 

<!-- -->
<tr class="bgc">
<td><label>Bleed:</label> </td>
<td colspan="2">
<?php echo ucwords(str_replace(';', ' - ', trim($task->bleed_btn, ';')));?>
</td>
<td colspan="3">
<?php echo $task->bleed_notes;?>
</td>
</tr>
<!-- --> 

<!-- -->
<tr class="bgc">
<td><label>Sample Supplied:</label> </td>
<td colspan="2">
<?php echo ucwords(str_replace(';', ' - ', trim($task->sample_btn, ';')));?>
</td>
<td colspan="3">
<?php echo $task->sample_notes;?>
</td>
</tr>
<!-- -->

<!-- -->
<tr class="bgc">
<td><label>Image Quality: </label></td>
<td colspan="2">
<?php echo ucwords(str_replace(';', ' - ', trim($task->image_btn, ';')));?>
</td>
<td colspan="3">
<?php echo $task->image_notes;?>
</td>
</tr>
<!-- -->

<!-- -->
<tr class="bgc">
<td><label>Text Quality: </label></td>
<td colspan="2">
<?php echo ucwords(str_replace(';', ' - ', trim($task->text_btn, ';')));?>
</td>
<td colspan="3">
<?php echo $task->text_notes;?>
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td><label>Printing: </label></td>
<td colspan="2">
<?php echo ucwords(str_replace(';', ' - ', trim($task->printing_btn, ';')));?>
</td>
<td colspan="3">
<?php echo $task->printing_notes;?>
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td><label>Printer: </label></td>
<td colspan="2">
<?php echo $task->printer;?>
</td>
<td colspan="3">
<?php echo $task->printer_notes;?>
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td><label>Paper Stock: </label></td>
<td colspan="2">
<?php echo ucwords(str_replace(';', ' - ', trim($task->paper_btn, ';')));?>
</td>
<td colspan="3">
<?php echo $task->paper_notes;?>
</td>
</tr>
<!-- -->



<!-- -->
<tr>
<td>
<label>Print Quantity:</label>
</td>
<td>
<?php echo $task->print_qty;?>
</td>
<td>
<label>Size:</label>
<?php echo $task->print_size;?>
</td>
<td colspan="3">
<?php echo $task->print_notes;?>
</td>
</tr>
<!-- -->

<!-- -->
<tr>
<td>
<label>Finish Quantity:</label>
</td>
<td>
<?php echo $task->finish_qty;?>
</td>
<td>
<label>Size:</label>
<?php echo $task->finish_size;?>
</td>
<td colspan="3">
<?php echo $task->finish_notes;?>
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td><label>Finishing: </label></td>
<td colspan="2">
<?php echo ucwords(str_replace(';', ' - ', trim($task->finishing_btn, ';')));?>
</td>
<td colspan="3">
<?php echo $task->finishing_notes;?>
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td><label>Delivery: </label></td>
<td colspan="2">
<?php echo ucwords(str_replace(';', ' - ', trim($task->delivery_btn, ';')));?>
</td>
<td colspan="3">
<?php echo $task->delivery_notes;?>
</td>
</tr>
<!-- -->



<!-- -->
<tr class="bgc">
<td><label>Production Staff: </label></td>
<td colspan="2">
<?php echo $task->staff_production;?>
</td>
<td colspan="3">
<?php echo $task->staff_production_notes;?>
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td><label>Quality Assurance: </label></td>
<td colspan="2">
<?php echo $task->qa;?>
</td>
<td colspan="3">
<?php echo $task->qa_notes;?>
</td>
</tr>
<!-- -->


<!-- -->
<tr class="bgc">
<td><label>Job Status: </label></td>
<td colspan="2">
<?php echo ucwords(str_replace(';', ' - ', trim($task->status_btn, ';')));?>
</td>
<td colspan="3">
<?php echo $task->status_notes;?>
</td>
</tr>
<!-- -->




      </tbody> 
    </table>
    
 
        
  


  </body> 
</html>