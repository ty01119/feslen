  <script>
  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#customer" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/cust_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
			


		      });

      
    });
</script>
<style type="text/css">
.fp {
	color: black;
}
.fup {
	background-color: LightSteelBlue; color: black;
}
.uf {
	background-color: #C68E17; color: black;
}
.sample {
display: inline-block;
width: 160px;
height:20px;
}
.sam-box {
display: inline-block;
width: 10px;
padding: 0 4px;
}
</style>
<?php echo form_open('web_app_domain/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >

<tr><td align="left" class="font10px">
<span class="ui_buttonset">
<?php
$data = array('content' => 'Domain Management', 'style' => 'margin:5px 0;', 'onclick'=>"window.location.href='".site_url('web_app_domain')."'");
echo form_button($data);
$data = array('class' => '','content' => '+', 'onclick'=>"openView('".site_url("web_app_domain/add")."','Add Domain');");
echo form_button($data);
?>
</span>
<span class="ui_buttonset"></span>
</td>
<td align="left" class="font10px">

<span class="ui_buttonset">
<?php 
$attr = (isset($filter['1']['deleted']) 
	 && !$filter['1']['deleted']) ? array('class' => 'white') : array();
echo anchor("web_app_domain/filter/1/deleted/0", 'X',$attr);
$attr = (isset($filter['1']['deleted']) 
	 && $filter['1']['deleted']) ? array('class' => 'white') : array();
echo anchor("web_app_domain/filter/1/deleted/1", 'Deleted', $attr);
?>
</span>
<span class="ui_buttonset"></span>
</td><td align="left" class="ui_button font10px">
</td><td align="left" class="ui_button font10px">
</td></tr>

<tr><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_input(
		array(
		      'name'        => 'customer',
		      'id'        => 'customer',
		      'value'	  => $customer,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		);

?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php 
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => $search,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		 );
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td><td align="left" class="ui_button font10px"></td></tr>




</table>
<?php echo form_close(); ?>



 

<table border="0" cellpadding="5" cellspacing="0" style="margin:30px 0; min-width:900px; float: left; width:100%;">

<tr>
<th colspan="7" align="left"><?php echo $table_title;?></th>
</tr>

 

<tr class="bgc">
<td colspan="8" align="right" class="ui_button font9px">
<?php 
echo $this->pagination->create_links(); 
$attr = ($per_page == 25) ? array() : array('class' => 'white');
$attr['style'] = 'margin:0 10px;';
echo anchor("web_app_domain/filter/per_page/$total", 'Display All', $attr);

?></td>
</tr>

<tr class="bgc">
<th align="left" valign="middle" width="40">
<?php echo anchor("web_app_domain/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>

<th align="left" valign="middle" width="180">
<?php echo anchor("web_app_domain/orderby/url", 'URL', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'url'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>

<th align="left" valign="middle" width="70">
<?php echo anchor("web_app_domain/orderby/due_date", 'Due Date', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'due_date'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>



<th align="left" valign="middle" >
<?php echo anchor("web_app_domain/orderby/cust_name", 'Customer', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'cust_name'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="80">
<?php echo anchor("web_app_domain/orderby/registrar", 'Registrar', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'registrar'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="80">
<?php echo anchor("web_app_domain/orderby/host", 'Host', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'host'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>

<th align="center" valign="middle" width="150">Operations</th>
</tr>

<?php 
$accu = new stdClass;


foreach($query->result() as $row):

$expiration_date = strtotime($row->due_date);

$today = strtotime(date("Y-m-d")); 

 


$job_status = ($expiration_date > $today) ? 'fp' : 'fup';
 

?> 
<tr class="bgc <?php echo $job_status;?>">
<td align="left" valign="middle"><?php echo $row->id;?></td>
<td align="left" valign="middle"><?php echo $row->url;?></td>
<td align="left" valign="middle"><?php echo date('d/m/Y', strtotime($row->due_date));?></td>

<td align="left" valign="middle"><?php echo $row->cust_name;?></td>

<td align="left" valign="middle"><?php echo $row->registrar;?></td>
<td align="left" valign="middle"><?php echo $row->host;?></td>

<td align="center" valign="middle" class="ui_button font9px">
<?php 
$data = array('class' => 'invo-lib-btn','content' => 'Edit', 'onclick'=>"openView('".site_url("web_app_domain/edit/$row->id")."','edit-".$row->id."');");
echo form_button($data);
$data = array('class' => 'invo-lib-btn','content' => 'View', 'onclick'=>"openView('".site_url("web_app_domain/view/$row->id")."','view-".$row->id."');");
echo form_button($data);
$data = array('class' => 'invo-lib-btn','content' => 'Del', 'onclick'=>"window.location = '".site_url("web_app_domain/del_msg/$row->id")."';");
echo form_button($data);
?>
</td></tr>
<?php endforeach;?>

<tr><td colspan="8" align="left"></td></tr>

<tr><td colspan="8" align="left"><br /><br /></td></tr>

<tr><td colspan="8" align="left">

<span class="sample"><span class="fup sam-box">&nbsp;&nbsp;</span> -- Over Due</span>


</td></tr>
</table>
 
 