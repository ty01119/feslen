<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 

    <style>
      table {
      margin-top: 10px;
      width:565px;
      border-collapse: collapse;
      }
      table td,
      table th {
      padding: 0; 
      } 
      #title{
      width:250px;
      height:30px;
      color:#ffffff;
      background-color:#00064e;
      text-align: center;
      }
      #logo{
      width:65px;
      }
      #name{
      width:250px; text-align: center;
      }
      .title2{
      width:125px;
      height:20px;
      text-align:center;
      background-color:#004080;
      }
      .title3{
      width:125px;
      height:20px;
      text-align:center;
      }
      .info{
      width:125px;
      height:20px;
      text-align:left;
      }
      .l2title{
      width:75px;
      }
      .l2cnt{
      width:200px;
      }
      .hbsp15{
      width:15px;
      }
      .vline{
      display:block;height:12px;
      border-top:1px solid #e1e1e1;
      }
      .vbsp15{
      height:15px; 
      }
      .item-cell1{
      width:365px; 
      text-align:left;
      }
      .item-cell2{
      width:50px; 
      text-align:right;
      }
      .item-cell3{
      width:50px;
      text-align:right;
      }
      .item-cell4{
      width:50px;
      text-align:right;
      }

      .item-cell5{
      width:50px;
      text-align:right;
      }
      .cb1{
      height:12px;
      background-color:#004080;
      }
      .white20b{
      color: #ffffff;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 20px;
      }
      .black20b{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 20px;
      }
      .white12b{
      color: #ffffff;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 12px;
      }

      .white8b{
      color: #ffffff;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 8px;
      }
      .black12b{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 12px;
      }
      .black8b{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-weight: bold;
      font-size: 8px;
      }
      .black8{
      color: #000000;
      font-family: Arial, Helvetica, sans-serif;
      font-size: 8px;
      }
      .ltvline{
      display:inline-block;height:16px;
      border-left:1px solid #e1e1e1;
      }
      #l4hline1{
      display:inline-block;width:12px;height:12px;
      border-top:1px solid #e1e1e1;
      }
      #l4hline2{
      display:inline-block;width:12px;height:12px;
      border-top:1px solid #e1e1e1;border-left:1px solid #e1e1e1;
      }
      #l4lt{
      width:345px;
      }
      #l4mi{
      width:100px;
      }
      .l4sp1{
      width:12px;
      }
      .l4vline{
      width:2px;
      }
      .l4item{
      width:40px;
      text-align:left;
      }
     .l4nbsp{
      width:26px;
      }
      .l4value{
      width:40px;
      text-align:right;
      }
    </style>
 
  </head>

  <body>

    <table class="collapse" border="0" align="center"> 
      <tbody>
	<tr>
	  <td colspan="2" id="title"><span class="white20b"><?php echo $title?></td>
	  <td rowspan="3" id="logo"><img src="<?php echo 
	  ($branch->branch == 'websolution') ? 
	  base_url().'images/inv-logo-3aweb.png' :
	  base_url().'images/inv-logo-3a.jpg';
	  ?>" style="width:78px;height:78px;"/>
	  </td>
	  <td rowspan="3" id="name"><span class="black20b"><?php echo $branch->biz_name;?></span>
	    <br /><br />
	    <span class="black12b"><?php echo $branch->address;?></span></td>
	</tr>
	<tr>
	  <td class="title2"><span class="white12b">NUMBER</span></td>
	  <td class="title2"><span class="white12b">DATE</span></td>
	</tr>
	<tr>
	 <td class="title3">
	 <span class="black12b"><?php echo $transaction->id;?></span>
	 </td>
	  <td class="title3">
	 <span class="black12b"><?php echo $transaction->date;?></span>
	 </td>
	</tr>

	<tr>
	  <td colspan="2"><span class="vline"></span></td>
	  <td ></td>
	  <td ></td>
	</tr>


	<tr>
	  <td colspan="2" class="info"><span class="black8b">
<?php echo 'Bank:'.nbs(2).$branch->bank_acc_num.nbs(4).'GST NO.:'.nbs(2).$branch->gst_num;?>
</span></td> 
	  <td></td>
	  <td class="info"><span class="black8b">
<?php echo 'Tel.:'.nbs(2).$branch->telephone.nbs(4).'Fax:'.nbs(2).$branch->fax;?>
</span></td>
	</tr>
      </tbody>
    </table>
    <table border="0" align="center"> 
      <tbody>
	<tr>
	  <td class="l2title"><span class="black12b">Customer</span></td>
	  <td class="l2cnt"><span class="black8"><?php echo $transaction->cust_name;?></span></td>
	  <td class="hbsp15"></td>
	  <td class="l2titel"><span class="black8b"> </span></td>
	  <td class="l2cnt"><span class="black8"> </span></td>
	</tr>
	<tr><td colspan="2"><span class="vline"></span></td>
	  <td class="hbsp15"></td>
	  <td colspan="2"><span class="vline"></span></td>
	</tr>
	<tr><td colspan="5" class="vbsp15"></td></tr>
	<tr>
	  <td class="l2title"> 
	    <span class="black8b"><?php echo $customer->id;?></span>
	  </td>
	  <td class="l2cnt">
	    <span class="black8">
	      <?php echo $customer->address;?>
	      <br />
	      <?php echo $customer->city;?>
	    </span>
	  </td>
	  <td class="hbsp15"></td>
	  <td class="l2titel"><span class="black8b">Ship to</span></td>
	  <td class="l2cnt"><span class="black8"><?php echo $customer->ship_name;?></span></td>
	</tr>
	<tr>
	  <td class="l2title"><span class="black8b">&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
	  <td class="l2cnt"><span class="black8">
	      <i>Tel&nbsp;&nbsp;<?php echo $customer->phone;?></i><br />
	      <i>Fax&nbsp;&nbsp;<?php echo $customer->fax;?></i><br />
	      <i>Mol&nbsp;&nbsp;<?php echo $customer->cellphone;?></i><br />
	  </span></td>
	  <td class="hbsp15"></td>
	  <td class="l2titel"><span class="black8b">&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
	  <td class="l2cnt">
	    <span class="black8">
	      <?php echo $customer->ship_address;?>
	      <br />
	      <?php echo $customer->ship_city;?>
	    </span>
	  </td>
	</tr>
      </tbody>
    </table>
    <table border="0" align="center"> 
      <tbody>
	<tr>
	  <td class="cb1"></td>
	  <td class="item-cell1 cb1"><span class="white8b">&nbsp;&nbsp;Description</span></td>
	  <td class="cb1"></td>
	  <td class="item-cell2 cb1"><span class="white8b">QTY&nbsp;&nbsp;</span></td>
	  <td class="cb1"></td>
	  <td class="item-cell3 cb1"><span class="white8b">U/P&nbsp;&nbsp;</span></td>
	  <td class="cb1"></td>
	  <td class="item-cell4 cb1"><span class="white8b">DISC&nbsp;&nbsp;</span></td>
	  <td class="cb1"></td>
	  <td class="item-cell5 cb1"><span class="white8b">Amount&nbsp;&nbsp;</span></td>
	  <td class="cb1"></td>
	</tr>
<?php foreach ($contents as $item):?>
	<tr>
	  <td ><span class="ltvline"></span></td>
	  <td class="item-cell1">
	    <span class="black8">&nbsp;&nbsp;<?php echo $item->product;?></span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell2">
	    <span class="black8"><?php echo $item->unit_price ? $item->quantity : '';?>&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3">
	    <span class="black8"><?php echo $item->unit_price ? $item->unit_price : '';?>&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell4">
	    <span class="black8"><?php echo $item->unit_price ? $item->discount.'%' : '';?>&nbsp;&nbsp;</span>
	  </td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell5">
	    <span class="black8">
	      <?php echo $item->unit_price ? number_format($item->unit_price * $item->quantity * (1 - $item->discount / 100), 2, '.', ',') : '' ;?>&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	</tr>
<?php endforeach;?>
<?php if ($transaction->discount != 0) { ?>
	<tr>
	  <td ><span class="ltvline"></span></td>
	  <td class="item-cell1"><span class="black8">&nbsp;&nbsp;&nbsp;&nbsp;PROMOTION: ( Discount: <?php echo $transaction->discount;?>% off )</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell2"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell4"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell5"><span class="black8">- <?php echo number_format($transaction->subtotal / (1 - $transaction->discount / 100) - $transaction->subtotal, 2, '.', ',');?>&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	</tr>
<?php } ?>
 
	<tr>
	  <td ><span class="ltvline"></span></td>
	  <td class="item-cell1"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell2"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell4"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell5"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	</tr>
	<tr>
	  <td ><span class="ltvline"></span></td>
	  <td class="item-cell1"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell2"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell4"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell5"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	</tr>
	<tr>
	  <td ><span class="ltvline"></span></td>
	  <td class="item-cell1"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell2"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell4"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell5"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	</tr>
	<tr>
	  <td ><span class="ltvline"></span></td>
	  <td class="item-cell1"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell2"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell4"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell5"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	</tr>
	<tr>
	  <td ><span class="ltvline"></span></td>
	  <td class="item-cell1"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell2"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell3"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell4"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	  <td class="item-cell5"><span class="black8">&nbsp; &nbsp;</span></td>
	  <td><span class="ltvline"></span></td>
	</tr>
	<tr>
	  <td colspan="11" class="cb1"></td>
	</tr>
      </tbody>
    </table>

    <table border="0" align="center"> 
      <tbody>
	<tr>
	  <td id="l4lt"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td id="l4mi"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td class="l4sp1"></td>
	  <td class="l4vline"><span class="ltvline"></span></td>
	  <td class="l4item"><span class="black8">&nbsp;&nbsp;Subtotal</span></td>
	  <td class="l4nbsp"></td>
	  <td class="l4value"><span class="black8"><?php echo $transaction->subtotal;?>&nbsp;&nbsp;</span></td>
	</tr>
	<tr>
	  <td id="l4lt"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td id="l4mi"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td class="l4sp1"></td>
	  <td class="l4vline"><span class="ltvline"></span></td>
	  <td class="l4item"><span class="black8">&nbsp;&nbsp;GST</span></td>
	  <td class="l4nbsp"></td>
	  <td class="l4value"><span class="black8"><?php echo $transaction->tax;?>&nbsp;&nbsp;</span></td>
	</tr>
	<tr>
	  <td id="l4lt"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td id="l4mi"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td class="l4sp1"></td>
	  <td class="l4vline"><span class="ltvline"></span></td>
	  <td class="l4item"><span class="black8">&nbsp;&nbsp;Total</span></td>
	  <td class="l4nbsp"></td>
	  <td class="l4value"><span class="black8"><?php echo $transaction->total;?>&nbsp;&nbsp;</span></td>
	</tr>
	<tr>
	  <td id="l4lt"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td id="l4mi"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td class="l4sp1"><span id="l4hline1"></span></td>
	  <td class="l4vline"><span id="l4hline2"></span></td>
	  <td class="l4item"><span class="black8">&nbsp;&nbsp;Paid</span></td>
	  <td class="l4nbsp"></td>
	  <td class="l4value"><span class="black8"><?php echo $transaction->paid;?>&nbsp;&nbsp;</span></td>
	</tr>
	<tr>
	  <td id="l4lt"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td id="l4mi"><span class="black8">&nbsp;&nbsp;</span></td>
	  <td class="l4sp1"></td>
	  <td class="l4vline"><span class="ltvline"></span></td>
	  <td class="l4item"><span class="black8">&nbsp;&nbsp;Balance</span></td>
	  <td class="l4nbsp"></td>
	  <td class="l4value">
	    <span class="black8"><?php echo number_format($transaction->total - $transaction->paid, 2, '.', ',');?>&nbsp;&nbsp;</span>
	  </td>
	</tr>
      </tbody>
    </table>
    <table border="0" align="center"> 
      <tbody>
	<tr>
	  <td class="l4sp1"></td>
	  <td  ><span class="black8">
	  <?php echo isset($terms) ? $terms : '';?>
	  </span></td>
          <td class="l4sp1"></td>
	</tr>
      </tbody>
    </table>

  </body> 
</html>