<script>

var json_url = '<?php echo $json_url;?>';
var post_url = '<?php echo $post_url;?>';
$(function() {
    $( "#customer" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/cust_names');?>',
		  		   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
			
			
			
		      });
      

      
      $( "#invo-items" ).sortable(); 

 
      
   
      
      
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>js/quotation_edit_view.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>js/phpjs.js"></script>
<div style="padding:10px 0;" id="inner-wrapper">
<div style="width:680px;background: #e1e1e1;color:#000000;" id="inner-text">


<table border="0" cellpadding="5" cellspacing="0" style="width:680px;padding:10px;">
<tr class="bgc">
<td class="ui_button" >
<?php
echo form_label('Customer:', 'customer');?>
</td>
<td colspan="5">
<?php
$attr = array('name' => 'customer', 'id' => 'customer', 'value' => '', 'class' => 'ui-corner-all flt', 'style' => 'width:300px;') ;
echo form_input($attr);
?> 
<div class="flt pt" style="margin:2px 2px 2px 20px;" title="Edit Customer" id="edit_customer">
<span class="ui-icon ui-icon-gear flt"></span> 
</div>
</td>
</tr>

<tr>
<td>
<?php
echo form_label('Quotation ID:', 'id');?>
</td><td>
<?php
$attr = array('name' => 'id', 'id' => 'id', 'class' => 'ui-corner-all', 'readonly' => 'readonly', 'style' => 'width:100px;') ;
echo form_input($attr);

?>  
</td>
<td>
<?php
echo form_label('Date:', 'date');?>
</td><td>
<?php
$attr = array('name' => 'date', 'id' => 'date', 'class' => 'ui-corner-all', 'readonly' => 'readonly', 'style' => 'width:100px;'); 
echo form_input($attr);

?>
</td>
<td></td><td></td>
</tr>

<tr>
<td colspan="6">

<div >
<span class="ui-icon ui-icon-grip-solid-horizontal flt" style="margin:2px 5px 0 0;"></span>
<div style="width:420px;margin:4px;" class="item-title">
<strong>PRODUCT</strong></div>
<div style="width:40px;text-align:right;margin:4px;" class="item-title">
<strong>QTY</strong></div>
<div style="width:60px;text-align:right;margin:4px;" class="item-title">
<strong>U/P</strong></div>
<div style="width:30px;text-align:right;margin:4px;" class="item-title">
<strong>DC</strong></div>
</div>
 

<ul id="invo-items" style="width:650px;clear:both;">
 
</ul>
 

<div style="width:650px;height:20px;" class="bgc">
<div class="flt pt" style="margin:2px;" title="add more" id="item_add">
<span class="ui-icon ui-icon-plusthick"></span>
</div>
</div>

</td>
</tr>

<tr class="bgc">
<td>
<?php
echo form_label('Subtotal:', 'subtotal');?>
</td><td>
<?php
$attr = array('name' => 'subtotal', 'id' => 'subtotal', 'class' => 'ui-corner-all invo', 'readonly' => 'readonly'); 
echo form_input($attr);
?>  
</td>
<td>
<?php
echo form_label('GST:', 'gst');?>
</td><td>
<?php
$attr = array('name' => 'gst', 'id' => 'gst', 'class' => 'ui-corner-all invo', 'readonly' => 'readonly'); 
echo form_input($attr);
?>
</td>
<td>
<?php
echo form_label('Total:', 'total');?>
</td><td>
<?php
$attr = array('name' => 'total', 'id' => 'total', 'class' => 'ui-corner-all invo'); 
echo form_input($attr);
?>
</td>
</tr>
<tr class="bgc">
<td>
<?php
echo form_label('Discount:', 'discount');?>
</td><td>
<?php
$attr = array('name' => 'discount', 'id' => 'discount', 'class' => 'ui-corner-all invo'); 
echo form_input($attr);
?>  
</td>
<td>
<?php
  //echo form_label('Paid:', 'paid');
?>
</td><td>
<?php
$attr = array('name' => 'paid', 'id' => 'paid', 'class' => 'hide ui-corner-all invo'); 
echo form_input($attr);
?>
</td>
<td>
<?php
  //echo form_label('Debit:', 'debit');
?>
</td><td>
<?php
$attr = array('name' => 'debit', 'id' => 'debit', 'class' => 'hide ui-corner-all invo', 'readonly' => 'readonly'); 
echo form_input($attr);
?>
</td>
</tr>
<tr class="bgc">
<td >
<?php
  //echo form_label('Extra info:', 'info');
?>
</td>
<td colspan="5">
<?php
$attr = array('name' => 'info', 'id' => 'info', 'class' => 'hide ui-corner-all', 'style' => 'width:500px;'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc hide">
<td>Job Status: </td>
<td colspan="5">
<div class="ui_buttonset font10px">
<?php
echo form_checkbox(array('name'=>'finished', 'id'=>'finished', 'value'=>'Finished', 'style'=>'margin:10px'));
echo form_label('Finished', 'finished');
echo form_checkbox(array('name'=>'delivered', 'id'=>'delivered', 'value'=>'Delivered', 'style'=>'margin:10px'));
echo form_label('Delivered', 'delivered');
echo form_checkbox(array('name'=>'job_status', 'id'=>'tot_paid', 'value'=>'Totally Paid', 'style'=>'margin:10px'));
echo form_label('Totally Paid', 'tot_paid');
echo form_checkbox(array('name'=>'job_status', 'id'=>'outsourced', 'value'=>'Outsourced', 'style'=>'margin:10px'));
echo form_label('Outsourced', 'outsourced');
?>
</div>
</td>
</tr>
<tr class="bgc hide">
<td>Payment Method: </td>
<td colspan="5">
<div class="ui_buttonset font10px">
<?php
echo form_checkbox(array('name'=>'payment', 'id'=>'pmt_cash', 'value'=>'Cash', 'style'=>'margin:10px'));
echo form_label('Cash', 'pmt_cash');
echo form_checkbox(array('name'=>'payment', 'id'=>'pmt_eftpos', 'value'=>'Eftpos', 'style'=>'margin:10px'));
echo form_label('Eftpos', 'pmt_eftpos');
echo form_checkbox(array('name'=>'payment', 'id'=>'pmt_cheque', 'value'=>'Cheque', 'style'=>'margin:10px'));
echo form_label('Cheque', 'pmt_cheque');
echo form_checkbox(array('name'=>'payment', 'id'=>'pmt_banks', 'value'=>'Banks', 'style'=>'margin:10px'));
echo form_label('Banks', 'pmt_banks');
echo form_checkbox(array('name'=>'payment', 'id'=>'pmt_other', 'value'=>'Other', 'style'=>'margin:10px'));
echo form_label('Other', 'pmt_other');
?>
</div>
</td>
</tr>

<tr class="bgc hide">
<td valign="top">
<?php
echo form_label('Comment:', 'comments');
?>
</td>
<td colspan="5">
<textarea name="comments" id="comments" rows="2" style="width:500px" class="ui-corner-all"></textarea>
</td>
</tr>
<tr class="hide">
<td valign="top">Operation Logs</td>
<td colspan="5">
<div id="logs" style="width:500px">
</div>

</td>
</tr>
</table>

   
</div>
<br style="clear:both">
<div style="float: left; padding:0 5px;" class="ui_button">
<?php 
echo form_input(array(
			 'type'  => 'button',
			 'name'  => 'save',
			 'id'    => 'save',
			 'value' => 'Save'
			 )
		   );
?>
</div>
<div style="float: right; padding:0 5px;" class="ui_button">
<?php 
  echo form_input(array(
			 'type' => 'button',
                         'name' => 'close',
			 'id'   => 'close',
			 'value' => 'Close'
			 )
		   );
?>
</div>


</div>
