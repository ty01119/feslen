<?php
$cart_html = '';

if($cart) {
  foreach ($cart as $item) {
    $cart_html .= '<span style="width:400px;display:inline-block;text-align:left">'
      .$item['product'].'</span><span style="width:60px;display:inline-block;text-align:right">'
      .$item['quantity'].'</span><span style="width:60px;display:inline-block;text-align:right">'
      .$item['unit_price'].'</span><br />';
  }
}
?>
<script type="text/javascript"> 
  var cart_html = '<?php echo $cart_html;?>';
</script> 

<script type="text/javascript"> 
  $(function() {
      $('select').selectmenu({wrapperElement: "<div class='font10px' />"});  
      
      if (cart_html) {
	$('#cart').hide().append(cart_html).fadeIn('slow');
	$('#reset').fadeIn();
      }
      
      var span1 = '<span style="width:400px;display:inline-block;text-align:left">';
      var span2 = '</span><span style="width:60px;display:inline-block;text-align:right">';
      var span3 = '</span><span style="width:60px;display:inline-block;text-align:right">' ;
      var span4 = '</span><br />';
	
	function ajax_post(post_data){
	  $.post("<?php echo base_url(); ?>quoting/cart", post_data,
		 function(){
		   var html =  span1
		     + post_data.name + span2
		     + post_data.qty + span3
		     + post_data.price + span4;
		   $('#cart').append(html).fadeIn();
		   $('#reset').fadeIn();
		 });	 	  
	}

	$('#reset').click(function(){
		 $.post("<?php echo base_url(); ?>quoting/reset", 
		 function(){		   
			  $('#cart').fadeOut('slow', 
					     function(){
					       $(this).html('');
					     });
		   $('#reset').fadeOut('slow');
		 });	 
	});

	$('#button_design_cust').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web Design: Custom Web Design';
	    post_data.qty = 1;
	    post_data.price = 300;	   
	    ajax_post(post_data);		  
	  });
	
	$('#button_design_temp').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web Design: Template Web Design';
	    post_data.qty = 1;
	    post_data.price = 150;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_design_hour').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web Design: Design Hours';
	    post_data.qty = 1;
	    post_data.price = 60;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_design_uniq').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web Design: Creative & Unique Style';
	    post_data.qty = 1;
	    post_data.price = 200;	   
	    ajax_post(post_data);		  
	  });	  
	  
	$('#button_dev_cust').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web Development: Custom Web Development (up to 6 Pages)';
	    post_data.qty = 1;
	    post_data.price = 300;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_dev_temp').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web Development: Template Web Development (up to 6 Pages)';
	    post_data.qty = 1;
	    post_data.price = 150;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_dev_page').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web Development: Html Pages';
	    post_data.qty = 1;
	    post_data.price = 50;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_cms_simp').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web CMS: Simple Content Management System';
	    post_data.qty = 1;
	    post_data.price = 198;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_cms_prod').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web CMS: Custom CMS Module - Product Catalogue';
	    post_data.qty = 1;
	    post_data.price = 600;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_cms_user').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web CMS: Custom CMS Module - User Login Mechanism';
	    post_data.qty = 1;
	    post_data.price = 600;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_cms_cart').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web CMS: Custom CMS Module - Shopping Cart Mechanism';
	    post_data.qty = 1;
	    post_data.price = 600;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_cms_lang').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web CMS: Custom CMS Module - Chinese and English Language Switch';
	    post_data.qty = 1;
	    post_data.price = 400;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_cms_sear').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web CMS: Custom CMS Module - Database Search Function';
	    post_data.qty = 1;
	    post_data.price = 200;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_cms_word').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web CMS: WordPress Content Management System';
	    post_data.qty = 1;
	    post_data.price = 100;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_addons_form').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web Add-ons: Email Form (Contact / Appraisal)';
	    post_data.qty = 1;
	    post_data.price = 60;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_addons_slid').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web Add-ons: JavaScript Photo Slides';
	    post_data.qty = 1;
	    post_data.price = 100;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_addons_gall').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web Add-ons: JavaScript Photo Gallery';
	    post_data.qty = 1;
	    post_data.price = 80;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_addons_flas').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web Add-ons: Simple Flash Element';
	    post_data.qty = 1;
	    post_data.price = 50;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_app_cust').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Customised Application: ';
	    post_data.qty = 1;
	    post_data.price = 600;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_app_dev').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Customised Development: ';
	    post_data.qty = 1;
	    post_data.price = 100;	   
	    ajax_post(post_data);		  
	  });
	    
	$('#button_app_hour').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Customised Application: Development Hours';
	    post_data.qty = 1;
	    post_data.price = 100;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_ser_mini').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Maintenance & Services: Website Maintenance';
	    post_data.qty = 1;
	    post_data.price = 30;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_ser_hour').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Maintenance & Services: Maintenance Hours';
	    post_data.qty = 1;
	    post_data.price = 60;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_ser_site').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Maintenance & Services: On Site Service';
	    post_data.qty = 1;
	    post_data.price = 60;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_hosting').click(function(){	
	    var post_data = new Object();
	    post_data.name = 'Web Hosting: 1-Year Web Hosting (From: dd/mm/yyyy To: dd/mm/yyyy)';
	    post_data.qty = 1;
	    post_data.price = 99;	   
	    ajax_post(post_data);	    
	    var post_data = new Object();
	    post_data.name = 'Web Hosting: 1-Year Free Domain name & Email service';
	    post_data.qty = 1;
	    post_data.price = 0;	   
	    ajax_post(post_data);	  
	  });
	  
	$('#button_domain').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web Hosting: Domain Registration Service';
	    post_data.qty = 1;
	    post_data.price = 39;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_enews_desi').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Email Newsletter: Design & html Template';
	    post_data.qty = 1;
	    post_data.price = 120;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_enews_auth').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Email Newsletter: Authoring';
	    post_data.qty = 1;
	    post_data.price = 80;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_enews_send').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Email Newsletter: Sending (500 subscribers)';
	    post_data.qty = 1;
	    post_data.price = 20;	   
	    ajax_post(post_data);		  
	  });

	$('#button_pkg_cust').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web Hosting: 1-Year Web Hosting (From: dd/mm/yyyy To: dd/mm/yyyy)';
	    post_data.qty = 1;
	    post_data.price = 99;	   
	    ajax_post(post_data);	    
	    var post_data = new Object();
	    post_data.name = 'Web Hosting: 1-Year Free Domain name & Email service';
	    post_data.qty = 1;
	    post_data.price = 0;	   
	    ajax_post(post_data);	  
	    var post_data = new Object();
	    post_data.name = 'Web Package: Custom Website Package';
	    post_data.qty = 1;
	    post_data.price = 500;	   
	    ajax_post(post_data);		  
	  });
	
	$('#button_pkg_temp').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web Hosting: 1-Year Web Hosting (From: dd/mm/yyyy To: dd/mm/yyyy)';
	    post_data.qty = 1;
	    post_data.price = 99;	   
	    ajax_post(post_data);	    
	    var post_data = new Object();
	    post_data.name = 'Web Hosting: 1-Year Free Domain name & Email service';
	    post_data.qty = 1;
	    post_data.price = 0;	   
	    ajax_post(post_data);	  
	    var post_data = new Object();
	    post_data.name = 'Web Package: Template Website Package';
	    post_data.qty = 1;
	    post_data.price = 300;	   
	    ajax_post(post_data);		  
	  });
	    
	  
 });

</script> 

<div class="ui_button font10px">
<?php 
echo form_button($data = array(
    'name' => 'button',
    'id' => 'button',  
    'content' => '3A Digital Print',
    'onclick'=>"window.location.href='".site_url('quoting')."'"
));
echo nbs(16);
echo form_button($data = array(
    'name' => 'button',
    'id' => 'button',  
    'content' => '3A Design Express',
    'onclick'=>"window.location.href='".site_url('quoting/designexpress')."'"
));
echo nbs(16);
echo form_button($data = array(
    'name' => 'button',
    'id' => 'button',  
    'content' => '3A web solution',
    'onclick'=>"window.location.href='".site_url('quoting/websolution')."'"
));
echo nbs(16);
echo form_button($data = array(
    'name' => 'button',
    'id' => 'button',  
    'content' => '3A Energy Signs',
    'onclick'=>"window.location.href='".site_url('quoting/energysigns')."'"
));
?>
</div>
<div class="ui_button font11px" style="margin-top:15px;width:540px;" id="cart">


</div>
<div class="ui_button font11px" style="margin-top:15px;width:540px;display:none;"  id="reset">
<?php
echo form_button($data = array(
    'name' => 'button',
    'id' => 'button',
     'style' => 'float:right;',  
    'content' => 'Reset Cart'
));
?>
</div>
<br style="clear:both;"/>
<div style="float: left; padding:20px 22px;">
<div style="float: left; width:960px;background: #e1e1e1;color:#000000;">

  
 <!--
    *******************************************************************************
     ***
     *******************************************************************************/
    -->
<div style="margin:15px 30px;width:900px;" class="flt" id="dp_price_pt">
<strong>Web Packages </strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_pkg_cust', 'style' => 'width: 280px;','content' => 'Custom Website Package - $599 + gst'));
?>
 
<?php
echo form_button($data = array('id' => 'button_pkg_temp', 'style' => 'width: 280px;','content' => 'Template Website Package - $399 + gst'));
?>

<br style="clear:both;"/><br />
</span> 

</div>
</div>
<!--
  /*******************************************************************************
   ***
   *******************************************************************************/		
-->
<div style="margin:15px 30px;width:900px;" class="flt">
<strong>Web Hosting</strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_hosting', 'style' => 'width: 280px;','content' => '1-Year Web Hosting<br />(Free Domain name & Email service) - from $99 + gst'));
?>

<?php
echo form_button($data = array('id' => 'button_domain', 'style' => 'width: 280px;','content' => 'Domain Registration Service<br /> - from $39 + gst'));
?>
</span> 
</div></div>

    <!--
    *******************************************************************************
     ***
     *******************************************************************************/
    -->
<div style="margin:15px 30px;width:900px;" class="flt" id="dp_price_pt">
<strong>Web Design </strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_design_cust', 'style' => 'width: 280px;','content' => 'Custom Web Design - $300 + gst'));
?>
 
<?php
echo form_button($data = array('id' => 'button_design_temp', 'style' => 'width: 280px;','content' => 'Template Web Design - $100 + gst'));
?>

<br style="clear:both;"/><br />

<?php
echo form_button($data = array('id' => 'button_design_hour', 'style' => 'width: 280px;','content' => 'Design Hours - $60 + gst per hour'));
?>

<?php
echo form_button($data = array('id' => 'button_design_uniq', 'style' => 'width: 280px;','content' => 'Creative & Unique Style - $200 + gst'));
?>

</span> 

</div>
</div>
    <!--	
  /*******************************************************************************
   ***
   *******************************************************************************/
    -->
<div style="margin:15px 30px;width:900px;" class="flt" id="dp_price_lf">
<strong>Web Development</strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_dev_cust', 'style' => 'width: 280px;','content' => 'Custom Web Development - $300 + gst'));
?>
 
<?php
echo form_button($data = array('id' => 'button_dev_temp', 'style' => 'width: 280px;','content' => 'Template Web Development - $200 + gst'));
?>

<br style="clear:both;"/><br />

<?php
echo form_button($data = array('id' => 'button_dev_page', 'style' => 'width: 280px;','content' => 'Html Pages - $50 + gst per page'));
?>
</span> 
</div>
</div>
		
 <!--
  /*******************************************************************************
   ***
   *******************************************************************************/		
-->
<div style="margin:15px 30px;width:900px;" class="flt">
<strong>Web CMS</strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_cms_simp', 'style' => 'width: 280px;','content' => 'Simple Content Management System<br />from $198 + gst'));
?>
<br style="clear:both;"/><br />

<?php
echo form_button($data = array('id' => 'button_cms_prod', 'style' => 'width: 280px;','content' => 'Custom CMS Module - Product Catalogue<br />from $600 + gst'));
?>

<?php
echo form_button($data = array('id' => 'button_cms_user', 'style' => 'width: 280px;','content' => 'Custom CMS Module - User Login Mechanism<br />from $600 + gst'));
?>

<?php
echo form_button($data = array('id' => 'button_cms_cart', 'style' => 'width: 280px;','content' => 'Custom CMS Module - Shopping Cart Mechanism<br />from $600 + gst'));
?>

<br style="clear:both;"/><br />

<?php
echo form_button($data = array('id' => 'button_cms_lang', 'style' => 'width: 280px;','content' => 'Chinese and English Language Switch<br />from $400 + gst'));
?>

<?php
echo form_button($data = array('id' => 'button_cms_sear', 'style' => 'width: 280px;','content' => 'Custom Database Search Function<br />from $200 + gst'));
?>

<br style="clear:both;"/><br />
<?php
echo form_button($data = array('id' => 'button_cms_word', 'style' => 'width: 280px;','content' => 'WordPress Content Management System<br />from $100 + gst'));
?>

</span> 
</div></div>

  
<!--
  /*******************************************************************************
   ***
   *******************************************************************************/		
-->
<div style="margin:15px 30px;width:900px;" class="flt">
<strong>Web Add-ons</strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_addons_form', 'style' => 'width: 280px;','content' => 'Email Form (Contact / Appraisal) - from $60 + gst'));
?>
 
<?php
echo form_button($data = array('id' => 'button_addons_slid', 'style' => 'width: 280px;','content' => 'JavaScript Photo Slides - from $100 + gst'));
?>

<?php
echo form_button($data = array('id' => 'button_addons_gall', 'style' => 'width: 280px;','content' => 'JavaScript Photo Gallery - from $80 + gst'));
?>

<br style="clear:both;"/><br />

<?php
echo form_button($data = array('id' => 'button_addons_flas', 'style' => 'width: 280px;','content' => 'Simple Flash Element - from $50 + gst'));
?>

<br style="clear:both;"/><br />
</span> 
</div></div>

  
<!--
  /*******************************************************************************
   ***
   *******************************************************************************/		
-->
<div style="margin:15px 30px;width:900px;" class="flt">
<strong>Customised Application</strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_app_cust', 'style' => 'width: 280px;','content' => 'Customised Web Application - from $600 + gst'));
?>

<?php
echo form_button($data = array('id' => 'button_app_hour', 'style' => 'width: 280px;','content' => 'Development Hours - $100 + gst per hour'));
?>

<?php
echo form_button($data = array('id' => 'button_app_dev', 'style' => 'width: 280px;','content' => 'Customised Web Development - $100 + gst per hour'));
?>
</span> 
</div></div>


  
<!--
  /*******************************************************************************
   ***
   *******************************************************************************/		
-->
<div style="margin:15px 30px;width:900px;" class="flt">
<strong>Maintenance &amp; Services</strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_ser_mini', 'style' => 'width: 280px;','content' => 'Website Maintenance - from $30 + gst'));
?>
 
<?php
echo form_button($data = array('id' => 'button_ser_hour', 'style' => 'width: 280px;','content' => 'Maintenance Hours - $60 + gst per hour'));
?>

<br style="clear:both;"/><br />

<?php
echo form_button($data = array('id' => 'button_ser_site', 'style' => 'width: 280px;','content' => 'On Site Service Hours - from $60 + gst per hour'));
?>


</span> 
</div></div>

<!--
  /*******************************************************************************
   ***
   *******************************************************************************/		
-->
<div style="margin:15px 30px;width:900px;" class="flt">
<strong>e-Newsletter</strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_enews_desi', 'style' => 'width: 280px;','content' => 'Email Newsletter Design & html Template<br />from $120 + gst'));
?>
 
<?php
echo form_button($data = array('id' => 'button_enews_auth', 'style' => 'width: 280px;','content' => 'Email Newsletter Authoring<br />from $80 + gst'));
?>

<?php
echo form_button($data = array('id' => 'button_enews_send', 'style' => 'width: 280px;','content' => 'Email Newsletter Sending (500 subscribers)<br />from $20 + gst'));
?>

</span> 
</div></div>


<br style="clear:both;"/><br />

  </div>
  </div>