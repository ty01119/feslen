<?php $this->load->view('quoting/menu_view'); ?>

<script type="text/javascript"> 
  $(function() { 
      $('select').selectmenu({wrapperElement: "<div class='font10px' />"});  
      
     
	$('#button_design_cust').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web Design: Custom Web Design';
	    post_data.qty = 1;
	    post_data.price = 300;	   
	    ajax_post(post_data);		  
	  });
	
	$('#button_design_temp').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web Design: Template Web Design';
	    post_data.qty = 1;
	    post_data.price = 150;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_design_hour').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web Design: Design Hours';
	    post_data.qty = 1;
	    post_data.price = 60;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_design_uniq').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web Design: Creative & Unique Style';
	    post_data.qty = 1;
	    post_data.price = 200;	   
	    ajax_post(post_data);		  
	  });	  
	  
	$('#button_dev_cust').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web Development: Custom Web Development (up to 6 Pages)';
	    post_data.qty = 1;
	    post_data.price = 300;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_dev_temp').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web Development: Template Web Development (up to 6 Pages)';
	    post_data.qty = 1;
	    post_data.price = 150;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_dev_page').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web Development: Html Pages';
	    post_data.qty = 1;
	    post_data.price = 50;	   
	    ajax_post(post_data);		  
	  });
	  /*
	$('#button_cms_simp').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web CMS: Simple Content Management System';
	    post_data.qty = 1;
	    post_data.price = 198;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_cms_prod').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web CMS: Custom CMS Module - Product Catalogue';
	    post_data.qty = 1;
	    post_data.price = 600;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_cms_user').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web CMS: Custom CMS Module - User Login Mechanism';
	    post_data.qty = 1;
	    post_data.price = 600;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_cms_cart').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web CMS: Custom CMS Module - Shopping Cart Mechanism';
	    post_data.qty = 1;
	    post_data.price = 600;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_cms_lang').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web CMS: Custom CMS Module - Chinese and English Language Switch';
	    post_data.qty = 1;
	    post_data.price = 400;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_cms_sear').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web CMS: Custom CMS Module - Database Search Function';
	    post_data.qty = 1;
	    post_data.price = 200;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_cms_word').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Web CMS: WordPress Content Management System';
	    post_data.qty = 1;
	    post_data.price = 100;	   
	    ajax_post(post_data);		  
	  });
	  */
	  /*
	$('#button_app_cust').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Customised Application: ';
	    post_data.qty = 1;
	    post_data.price = 600;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_app_dev').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Customised Development: ';
	    post_data.qty = 1;
	    post_data.price = 100;	   
	    ajax_post(post_data);		  
	  });
	    
	$('#button_app_hour').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Customised Application: Development Hours';
	    post_data.qty = 1;
	    post_data.price = 100;	   
	    ajax_post(post_data);		  
	  });*/
	  
	$('#button_ser_mini').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Maintenance & Services: Website Maintenance';
	    post_data.qty = 1;
	    post_data.price = 30;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_ser_hour').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Maintenance & Services: Maintenance Hours';
	    post_data.qty = 1;
	    post_data.price = 60;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_ser_site').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Maintenance & Services: On Site Service';
	    post_data.qty = 1;
	    post_data.price = 60;	   
	    ajax_post(post_data);		  
	  });
	  
	  
	$('#button_enews_desi').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Email Newsletter: Design & html Template';
	    post_data.qty = 1;
	    post_data.price = 120;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_enews_auth').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Email Newsletter: Authoring';
	    post_data.qty = 1;
	    post_data.price = 80;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_enews_send').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Email Newsletter: Sending (500 subscribers)';
	    post_data.qty = 1;
	    post_data.price = 20;	   
	    ajax_post(post_data);		  
	  });
	    
	  
 });

</script> 



<div class="ui_button font11px" style="margin-top:15px;width:540px;" id="cart">


</div>
<div class="ui_button font11px" style="margin-top:15px;width:540px;display:none;"  id="reset">
<?php
echo form_button($data = array(
    'name' => 'button',
    'id' => 'button',
     'style' => 'float:right;',  
    'content' => 'Reset Cart'
));
?>
</div>
<br style="clear:both;"/> 
<div  style="margin:0 0 30px 0; min-width:900px; float: left; width:100%;" class="quote-btn-wrap">

<style>
.quote-btn-wrap button {
width: 300px;
margin: 3px;
}
</style>  
 <!--
    *******************************************************************************
     ***
     *******************************************************************************/
    -->
<div style="margin:15px 20px;width:920px;" class="flt" >
<strong>Web Packages </strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_pkg_cust', 'content' => 'Custom Website Package- $699 + gst'));
?> 
 
<?php
echo form_button($data = array('id' => 'button_pkg_mini', 'content' => 'Mini Website Package - $199 + gst'));
?>

<br style="clear:both;"/><br />
</span> 

</div>
</div>

<script type="text/javascript"> 
  $(function() {
	

	$('#button_pkg_cust').click(function(){
 	   
	    var post_data = new Object();
	    var items = new Array();
	    
 	    items.push({'name': 'Web Package: Custom Website Package', 'qty':1, 'price':0});   
	    items.push({'name': 'Web Hosting: 1-Year Web Hosting (Package Web 500)', 'qty':1, 'price':99});  
	    items.push({'name': '+ For Domain: example.com (From: dd/mm/yyyy To: dd/mm/yyyy)', 'qty':1, 'price':0});  
	    items.push({'name': '+ 1-Year Free Domain name & Email service', 'qty':1, 'price':0});  
	    items.push({'name': 'Custom Web Design', 'qty':1, 'price':300});  
	    items.push({'name': '+ 3 conceptual designs', 'qty':1, 'price':0});  
	    items.push({'name': '+ 5 hours of web designer’s time', 'qty':1, 'price':0});  
	    items.push({'name': 'Custom Web Development', 'qty':1, 'price':300});    
	    items.push({'name': '+ up to 6 pages', 'qty':1, 'price':0});   
	     
	    post_data.array = items;
	    ajax_post(post_data);		  
	  });	
	    
	
	$('#button_pkg_mini').click(function(){
	    var post_data = new Object();
	    var items = new Array();
	    items.push({'name': 'Web Package: Mini Website Package', 'qty':1, 'price':0});   
	    items.push({'name': 'Web Hosting: 1-Year Web Hosting (Package Web 500)', 'qty':1, 'price':99});  
	    items.push({'name': '+ For Domain: example.com (From: dd/mm/yyyy To: dd/mm/yyyy)', 'qty':1, 'price':0});  
	    items.push({'name': '+ 1-Year Free Domain name & Email service', 'qty':1, 'price':0});   
	    items.push({'name': '+ 2 hours of web developer’s time', 'qty':1, 'price':160});  
	    items.push({'name': '+ Template based structure', 'qty':1, 'price':0}); 
	    items.push({'name': 'Web Package Discount', 'qty':1, 'price':-60});       
	    
	    post_data.array = items;    	       	   
	    ajax_post(post_data);		  
	  });
 });
</script> 
<!--
  /*******************************************************************************
   ***
   *******************************************************************************/		
-->
<div style="margin:15px 20px; width:920px;" class="flt">
<strong>Web Hosting</strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_hosting1', 'content' => '1-Year Web Hosting (Web 500)<br />(Free Domain name & Email service) - $99 + gst'));
?>
<?php
echo form_button($data = array('id' => 'button_hosting2', 'content' => '1-Year Web Hosting (Web 1000)<br />(Free Domain name & Email service) - $199 + gst'));
?>
<?php
echo form_button($data = array('id' => 'button_domain', 'content' => 'Domain Registration Service<br /> - from $30 + gst'));
?>
<script type="text/javascript"> 
  $(function() {
	$('#button_hosting1').click(function(){	
	    var post_data = new Object();
	    var items = new Array();
 
	    items.push({'name': 'Web Hosting: 1-Year Web Hosting (Package Web 500)', 'qty':1, 'price':99});  
	    items.push({'name': '+ For Domain: example.com (From: dd/mm/yyyy To: dd/mm/yyyy)', 'qty':1, 'price':0});  
	    items.push({'name': '+ 1-Year Free Domain name & Email service', 'qty':1, 'price':0});  
 
	    post_data.array = items;    	       	   
	    ajax_post(post_data);		  
	  });
	$('#button_hosting2').click(function(){		
	    var post_data = new Object();
	    var items = new Array();
 
	    items.push({'name': 'Web Hosting: 1-Year Web Hosting (Package Web 1000)', 'qty':1, 'price':199});  
	    items.push({'name': '+ For Domain: example.com (From: dd/mm/yyyy To: dd/mm/yyyy)', 'qty':1, 'price':0});  
	    items.push({'name': '+ 1-Year Free Domain name & Email service', 'qty':1, 'price':0});  
 
	    post_data.array = items;    	       	   
	    ajax_post(post_data);	  
	  }); 	
	  
	$('#button_domain').click(function(){ 
	    
	    var post_data = new Object();
	    var items = new Array();
 
	    items.push({'name': 'Web Hosting: Domain Registration & management', 'qty':1, 'price':30});    
	    items.push({'name': '+ for .co.nz or .com', 'qty':1, 'price':0});  
 
	    post_data.array = items;  	   
	    ajax_post(post_data);		  
	  });  
 });
</script> 


</span> 
</div></div>

 <!--
    *******************************************************************************
     ***
     *******************************************************************************/
    -->
<div style="margin:15px 20px; width:920px;" class="flt" >
<strong>Joomla CMS Development </strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_joomla1', 'content' => 'Web CMS: Joomla CMS Set up & Tutorial<br />$200 + gst'));
?>
<br style="clear:both;"/><br />
<?php
echo form_button($data = array('id' => 'button_joomla2', 'content' => 'Web CMS: Joomla Based Product Catalogue<br />$500 + gst'));
?>
<?php
echo form_button($data = array('id' => 'button_joomla3', 'content' => 'Web CMS: Joomla Based User Login Mechanism<br />$500 + gst'));
?>
<?php
echo form_button($data = array('id' => 'button_joomla4', 'content' => 'Web CMS: Joomla Based Shopping Cart & Ordering <br />$600 + gst'));
?>
<br style="clear:both;"/><br />
<?php
echo form_button($data = array('id' => 'button_joomla5', 'content' => 'Web CMS: Joomla Based Article Catelogue<br />$300 + gst'));
?>
<?php
echo form_button($data = array('id' => 'button_joomla6', 'content' => 'Web CMS: Joomla Based Multi-lingual Switch<br />$300 + gst'));
?>
</span> 

</div>
</div>

<script type="text/javascript"> 
  $(function() {
	

	$('#button_joomla1').click(function(){
 	   
	    var post_data = new Object();
	    var items = new Array();
	    
 	    items.push({'name': 'Web CMS: Joomla CMS Set up & Tutorial', 'qty':1, 'price':200}); 
	    items.push({'name': 'Discount (html by 3A web solution)', 'qty':1, 'price':-100});   
	     
	    post_data.array = items;
	    ajax_post(post_data);		  
	  });	


	$('#button_joomla2').click(function(){
 	   
	    var post_data = new Object();
	    var items = new Array();
	    
 	    items.push({'name': 'Web CMS: Joomla Based Product Catalogue', 'qty':1, 'price':500}); 
	     
	    post_data.array = items;
	    ajax_post(post_data);		  
	  });	


	$('#button_joomla3').click(function(){
 	   
	    var post_data = new Object();
	    var items = new Array();
	    
 	    items.push({'name': 'Web CMS: Joomla Based User Login Mechanism', 'qty':1, 'price':500}); 
	     
	    post_data.array = items;
	    ajax_post(post_data);		  
	  });	
	   


	$('#button_joomla4').click(function(){
 	   
	    var post_data = new Object();
	    var items = new Array();
	    
 	    items.push({'name': 'Web CMS: Joomla Based Shopping Cart & Ordering Mechanism', 'qty':1, 'price':600});  
	     
	    post_data.array = items;
	    ajax_post(post_data);		  
	  });	
	   


	$('#button_joomla5').click(function(){
 	   
	    var post_data = new Object();
	    var items = new Array();
	    
 	    items.push({'name': 'Web CMS: Joomla Based Article Catelogue', 'qty':1, 'price':300});  
	     
	    post_data.array = items;
	    ajax_post(post_data);		  
	  });	
	   


	$('#button_joomla6').click(function(){
 	   
	    var post_data = new Object();
	    var items = new Array();
	    
 	    items.push({'name': 'Web CMS: Joomla Based Multi-lingual Switch', 'qty':1, 'price':300});       
	    items.push({'name': '+ for custom website package', 'qty':1, 'price':0});  
	     
	    post_data.array = items;
	    ajax_post(post_data);		  
	  });	
	   
	   	
	   
	   
 });
</script> 
<!--
  /*******************************************************************************
   ***
   *******************************************************************************/		
-->
<div style="margin:15px 20px; width:920px;" class="flt">
<strong>Web Add-ons (FOR Joomla CMS)</strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_addons01', 'content' => 'Email Form (Contact / Appraisal) - from $80 + gst'));
 
echo form_button($data = array('id' => 'button_addons02', 'content' => 'JavaScript Photo Slides - from $80 + gst'));
 
echo form_button($data = array('id' => 'button_addons03', 'content' => 'JavaScript Photo Gallery - from $80 + gst'));
 
echo form_button($data = array('id' => 'button_addons04', 'content' => 'Dropdown Menu (2 Levels) - from $80 + gst'));
 
?>

<br style="clear:both;"/><br />
</span> 
</div></div>

<script type="text/javascript"> 
  $(function() {
	

	$('#button_addons01').click(function(){
 	   
	    var post_data = new Object();
	    var items = new Array();
	    
 	    items.push({'name': 'Web Add-ons: Email Form (Contact / Appraisal)', 'qty':1, 'price':80}); 
	     
	    post_data.array = items;
	    ajax_post(post_data);		  
	  });	
	  
	$('#button_addons02').click(function(){
 	   
	    var post_data = new Object();
	    var items = new Array();
	    
 	    items.push({'name': 'Web Add-ons: JavaScript Photo Slideshow', 'qty':1, 'price':80}); 
	     
	    post_data.array = items;
	    ajax_post(post_data);		  
	  });		
	  
	$('#button_addons03').click(function(){
 	   
	    var post_data = new Object();
	    var items = new Array();
	    
 	    items.push({'name': 'Web Add-ons: JavaScript Photo Gallery', 'qty':1, 'price':80}); 
	     
	    post_data.array = items;
	    ajax_post(post_data);		  
	  });   	
	  
	$('#button_addons04').click(function(){
 	   
	    var post_data = new Object();
	    var items = new Array();
	    
 	    items.push({'name': 'Web Add-ons: Dropdown Menu (2 Levels)', 'qty':1, 'price':80}); 
	     
	    post_data.array = items;
	    ajax_post(post_data);		  
	  });   	
	   	   
 });
</script> 
    <!--
    *******************************************************************************
     ***
     *******************************************************************************/
    -->
<div style="margin:15px 20px; width:920px;" class="flt" id="dp_price_pt">
<strong>Web Design & Web Development</strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_design_cust', 'content' => 'Custom Web Design - $300 + gst'));
?> 
<?php
echo form_button($data = array('id' => 'button_design_hour', 'content' => 'Design Hours - $60 + gst per hour'));
?>
 

<br style="clear:both;"/><br />

<?php
echo form_button($data = array('id' => 'button_dev_cust', 'content' => 'Custom Web Development - $300 + gst'));
?>
<?php
echo form_button($data = array('id' => 'button_dev_page', 'content' => 'Html Pages - $50 + gst per page'));
?>

</span> 

</div>
</div>
 
		
 <!--
  /*******************************************************************************
   ***
   *******************************************************************************/		
-->
<div style="margin:15px 20px; width:920px;" class="flt">
<strong>Web CMS (Feslen based) </strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_cms_feslen', 'content' => 'Custom CMS (Feslen based)  <br />from $1000 + gst'));
?>
<br style="clear:both;"/><br />
  
 
</span> 
</div></div>



<script type="text/javascript"> 
  $(function() {
	

	$('#button_cms_feslen').click(function(){
 	   
	    var post_data = new Object();
	    var items = new Array();
	    
 	    items.push({'name': 'Web Add-ons: Custom CMS (Feslen based)', 'qty':1, 'price':1000});     
	    items.push({'name': '+ $600 per database table', 'qty':1, 'price':0});  
	     
	    post_data.array = items;
	    ajax_post(post_data);		  
	  });	
	  
	 
	   	   
 });
</script> 

  
<!--
  /*******************************************************************************
   ***
   *******************************************************************************/		
-->
<div style="margin:15px 20px; width:920px;" class="flt">
<strong>Customised Application & module</strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_cust_comp', 'content' => 'Customised Joomla Component  - from $600 + gst'));
?>
<?php
echo form_button($data = array('id' => 'button_cust_mod', 'content' => 'Customised Joomla Module - from $200 + gst'));
?>
<?php
echo form_button($data = array('id' => 'button_app_hour', 'content' => 'Development Hours - $80 + gst per hour'));
?>
</span> 
</div></div>


<script type="text/javascript"> 
  $(function() {
	

	$('#button_cust_comp').click(function(){
 	   
	    var post_data = new Object();
	    var items = new Array();
	    
 	    items.push({'name': 'Customised Application: Joomla CMS Component', 'qty':1, 'price':600});   
	     
	    post_data.array = items;
	    ajax_post(post_data);		  
	  });	
	

	$('#button_cust_mod').click(function(){
 	   
	    var post_data = new Object();
	    var items = new Array();
	    
 	    items.push({'name': 'Customised Application: Joomla CMS Module', 'qty':1, 'price':200});   
	     
	    post_data.array = items;
	    ajax_post(post_data);		  
	  });	
	

	$('#button_app_hour').click(function(){
 	   
	    var post_data = new Object();
	    var items = new Array();
	    
 	    items.push({'name': 'Customised Application: Development Hours', 'qty':1, 'price':80});   
	     
	    post_data.array = items;
	    ajax_post(post_data);		  
	  });	
	  
	 
	   	   
 });
</script> 

  
<!--
  /*******************************************************************************
   ***
   *******************************************************************************/		
-->
<div style="margin:15px 20px; width:920px;" class="flt">
<strong>Maintenance &amp; Services</strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px">
<?php
echo form_button($data = array('id' => 'button_ser_mini', 'content' => 'Website Maintenance <br /> from $30 + gst'));
?>
<?php
echo form_button($data = array('id' => 'button_ser_hour', 'content' => 'Maintenance Hours <br /> $60 + gst per hour'));
?>
<?php
echo form_button($data = array('id' => 'button_ser_site', 'content' => 'On Site Service <br /> from $60 + gst per hour'));
?>

</span> 
</div></div>

<!--
  /*******************************************************************************
   ***
   *******************************************************************************/		
-->
<div style="margin:15px 20px; width:920px;" class="flt">
<strong>e-Newsletter</strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_enews_desi', 'content' => 'Email Newsletter Design & html Template<br />from $120 + gst'));
?>
<?php
echo form_button($data = array('id' => 'button_enews_auth', 'content' => 'Email Newsletter Authoring<br />from $80 + gst'));
?>
<?php
echo form_button($data = array('id' => 'button_enews_send', 'content' => 'Email Newsletter Sending (500 subscribers)<br />from $20 + gst'));
?>

</span> 
</div></div>


<br style="clear:both;"/><br />

  </div> 