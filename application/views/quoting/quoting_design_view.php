<?php $this->load->view('quoting/menu_view'); ?>

<script type="text/javascript"> 
  $(function() {
      $('select').selectmenu({wrapperElement: "<div class='font10px' />"});  
      
     

	$('#button_ser_hrs').click(function(){ 
	    var post_data = new Object();
	    post_data.name = 'Design / Layout: Service Hours ($60 + gst per hours)';
	    post_data.qty = 1;
	    post_data.price = 60;	   
	    ajax_post(post_data);	  
	  });
	
	$('#button_typing').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Design / Layout: Text Typing (20 Mins)';
	    post_data.qty = 1;
	    post_data.price = 20;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_photo').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Design / Layout: Photo Editing (30 Mins)';
	    post_data.qty = 1;
	    post_data.price = 30;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_logo').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Design / Layout: Logo Design';
	    post_data.qty = 1;
	    post_data.price = 300;	   
	    ajax_post(post_data);		  
	  });	  
	  
	$('#button_file1').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Design / Layout: File Repair Services (10 Mins)';
	    post_data.qty = 1;
	    post_data.price = 10;	   
	    ajax_post(post_data);		  
	  });	  
	  
	$('#button_file').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Design / Layout: File Repair Services';
	    post_data.qty = 1;
	    post_data.price = 20;	   
	    ajax_post(post_data);		  
	  });	  
	  
	$('#button_file3').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Design / Layout: File Repair Services (30 Mins)';
	    post_data.qty = 1;
	    post_data.price = 30;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_bc1').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Design / Layout: Business Card Design 1 Sided';
	    post_data.qty = 1;
	    post_data.price = 30;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_bc2').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Design / Layout: Business Card Design 2 Sided';
	    post_data.qty = 1;
	    post_data.price = 50;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_dlb1').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Design / Layout: DL Brochure Design 1 Sided';
	    post_data.qty = 1;
	    post_data.price = 60;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_dlb2').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Design / Layout: DL Brochure Design 2 Sided';
	    post_data.qty = 1;
	    post_data.price = 120;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_invi').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Design / Layout: Invitation Design';
	    post_data.qty = 1;
	    post_data.price = 30;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_book').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Design / Layout: Booklet Design';
	    post_data.qty = 1;
	    post_data.price = 300;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_menu').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Design / Layout: Restaurant Menu';
	    post_data.qty = 1;
	    post_data.price = 200;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_sign1').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Design / Layout: Signage Design';
	    post_data.qty = 1;
	    post_data.price = 30;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_sign2').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Design / Layout: Signage Design';
	    post_data.qty = 1;
	    post_data.price = 60;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_sign3').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Design / Layout: Signage Design';
	    post_data.qty = 1;
	    post_data.price = 100;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_pkg1').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Design / Layout: Packaging Design';
	    post_data.qty = 1;
	    post_data.price = 30;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_pkg2').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Design / Layout: Packaging Design';
	    post_data.qty = 1;
	    post_data.price = 60;	   
	    ajax_post(post_data);		  
	  });
	  
	$('#button_pkg3').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Design / Layout: Packaging Design';
	    post_data.qty = 1;
	    post_data.price = 120;	   
	    ajax_post(post_data);		  
	  });
	  	  
 });

</script> 
 
<div class="ui_button font11px" style="margin-top:15px;width:540px;" id="cart">


</div>
<div class="ui_button font11px" style="margin-top:15px;width:540px;display:none;"  id="reset">
<?php
echo form_button($data = array(
    'name' => 'button',
    'id' => 'button',
     'style' => 'float:right;',  
    'content' => 'Reset Cart'
));
?>
</div>
<br style="clear:both;"/> 
<div  style="margin:0 0 30px 0; min-width:900px; float: left; width:100%;">

  
 <!--
    *******************************************************************************
     ***
     *******************************************************************************/
    -->
<div style="margin:15px 30px;width:900px;" class="flt" id="dp_price_pt">
<strong>General Services </strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_ser_hrs', 'style' => 'width: 280px;','content' => 'Service Hours $60 + gst per hour'));
?>
<?php
echo form_button($data = array('id' => 'button_file', 'style' => 'width: 280px;','content' => 'File Repair Services - $25 + gst'));
?>
<br style="clear:both;"/><br />
 
<?php
echo form_button($data = array('id' => 'button_typing', 'style' => 'width: 280px;','content' => 'Text Typing (20 Mins) - $20 + gst'));
?>
<?php
echo form_button($data = array('id' => 'button_photo', 'style' => 'width: 280px;','content' => 'Photo Editing (30 Mins) - $30 + gst'));
?>
 
 <br style="clear:both;"/><br />

<?php
echo form_button($data = array('id' => 'button_file1', 'style' => 'width: 280px;','content' => 'File Repair Services  (10 Mins) - $10 + gst'));
?>

<?php
echo form_button($data = array('id' => 'button_file3', 'style' => 'width: 280px;','content' => 'File Repair Services  (30 Mins) - $30 + gst'));
?>

 

</span> 

</div>
</div>
<!--
  /*******************************************************************************
   ***
   *******************************************************************************/		
-->
<div style="margin:15px 30px;width:900px;" class="flt">
<strong>Logo Design</strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_logo', 'style' => 'width: 280px;','content' => 'Logo Design - from $300 + gst'));
?>

 
</span> 
</div></div>

    <!--
    *******************************************************************************
     ***
     *******************************************************************************/
    -->
<!--
  /*******************************************************************************
   ***
   *******************************************************************************/		
-->
<div style="margin:15px 30px;width:900px;" class="flt">
<strong>Business Card Design</strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 

<?php
echo form_button($data = array('id' => 'button_bc1', 'style' => 'width: 280px;','content' => 'Business Card Design 1 Sided - from $30 + gst'));
?>
<?php
echo form_button($data = array('id' => 'button_bc2', 'style' => 'width: 280px;','content' => 'Business Card Design 2 Sided - from $50 + gst'));
?>
 

</span> 
</div></div>

    <!--
    *******************************************************************************
     ***
     *******************************************************************************/
    -->
<!--
  /*******************************************************************************
   ***
   *******************************************************************************/		
-->
<div style="margin:15px 30px;width:900px;" class="flt">
<strong>DL Brochure Design</strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 

<?php
echo form_button($data = array('id' => 'button_dlb1', 'style' => 'width: 280px;','content' => 'DL Brochure Design 1 Sided - from $60 + gst'));
?>
<?php
echo form_button($data = array('id' => 'button_dlb2', 'style' => 'width: 280px;','content' => 'DL Brochure Design 2 Sided - from $120 + gst'));
?>
 

</span> 
</div></div>

    <!--
    *******************************************************************************
     ***
     *******************************************************************************/
    -->
<!--
  /*******************************************************************************
   ***
   *******************************************************************************/		
-->
<div style="margin:15px 30px;width:900px;" class="flt">
<strong>Invitation Design</strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_invi', 'style' => 'width: 280px;','content' => 'Invitation Design - from $30 + gst'));
?>
 

</span> 
</div></div>

    <!--
    *******************************************************************************
     ***
     *******************************************************************************/
    -->
<!--
  /*******************************************************************************
   ***
   *******************************************************************************/		
-->
<div style="margin:15px 30px;width:900px;" class="flt">
<strong>Booklet Design</strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_book', 'style' => 'width: 280px;','content' => 'Booklet Design - from $300 + gst'));
?>
 

</span> 
</div></div>

    <!--
    *******************************************************************************
     ***
     *******************************************************************************/
    -->
<!--
  /*******************************************************************************
   ***
   *******************************************************************************/		
-->
<div style="margin:15px 30px;width:900px;" class="flt">
<strong>Restaurant Menu</strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_menu', 'style' => 'width: 280px;','content' => 'Restaurant Menu - from $200 + gst'));
?>
 

</span> 
</div></div>

    <!--
    *******************************************************************************
     ***
     *******************************************************************************/
    -->
<div style="margin:15px 30px;width:900px;" class="flt" id="dp_price_pt">
<strong>Signage Design </strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> 
<?php
echo form_button($data = array('id' => 'button_sign1', 'style' => 'width: 280px;','content' => 'Signage Design - from $30 + gst'));
?>
 
<?php
echo form_button($data = array('id' => 'button_sign2', 'style' => 'width: 280px;','content' => 'Signage Design - from $60 + gst'));
?>
 
<?php
echo form_button($data = array('id' => 'button_sign3', 'style' => 'width: 280px;','content' => 'Signage Design - from $100 + gst'));
?>

 

</span> 

</div>
</div>
    <!--	
  /*******************************************************************************
   ***
   *******************************************************************************/
    -->
<div style="margin:15px 30px;width:900px;" class="flt" id="dp_price_lf">
<strong>Packaging Design</strong><br /><br />
<div class="font11px flt">
<span class="ui_button font10px"> <?php
echo form_button($data = array('id' => 'button_pkg1', 'style' => 'width: 280px;','content' => 'Packaging Design - from $30 + gst'));
?>
 
<?php
echo form_button($data = array('id' => 'button_pkg2', 'style' => 'width: 280px;','content' => 'Packaging Design - from $60 + gst'));
?>
 
<?php
echo form_button($data = array('id' => 'button_pkg3', 'style' => 'width: 280px;','content' => 'Packaging Design - from $120 + gst'));
?>

</span> 
</div>
</div>
		
 <!--
  /*******************************************************************************
   ***
   *******************************************************************************/		
-->



<br style="clear:both;"/><br />
 
  </div>