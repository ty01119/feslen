<?php $this->load->view('quoting/menu_view'); ?>

<script type="text/javascript"> 
  var price_url = '<?php echo $price_url;?>'
</script> 

<script type="text/javascript"> 
  $(function() {
      $('select').selectmenu({wrapperElement: "<div class='font10px' />"});  

	$('#pt_button_add').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Digital Printing: ' + $('#pt_type :selected').text() + ' -- ' 
	      + $('#pt_size :selected').text()  + ' -- '
	      + $('#pt_weight :selected').text();
	    post_data.qty = $('#pt_qty').val();
	    post_data.price = $('#pt_unit').val();
	   
	    ajax_post(post_data);		  
	  });
	
	$('#lf_button_add').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Large Format Poster: ' + $('#lf_type :selected').text() + ' -- ' 
	      + $('#lf_size :selected').text();
	    post_data.qty = $('#lf_qty').val();
	    post_data.price = $('#lf_unit').val();
	   
	    ajax_post(post_data);		  
	  });
	
	$('#bc_button_add').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Business Cards: ' + $('#bc_type :selected').text() + ' -- ' 
	      + $('#bc_lam :selected').text() + ' -- ' + $('#bc_qty').val();
	    post_data.qty = 1;
	    post_data.price = $('#bc_total').val();
	   
	    ajax_post(post_data);		  
	  });
	
	$('#bd_button_add').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Finishing (Binding): ' + $('#bd_type :selected').text() + ' -- '
	    	+ $('#bd_size :selected').text();
	    post_data.qty = $('#bd_qty').val();
	    post_data.price = $('#bd_unit').val();
	   
	    ajax_post(post_data);		  
	  });
	
	$('#lm_button_add').click(function(){
	    var post_data = new Object();
	    post_data.name = 'Finishing (Lamination): ' + $('#lm_type :selected').text();
	    post_data.qty = $('#lm_qty').val();
	    post_data.price = $('#lm_unit').val();
	   
	    ajax_post(post_data);		  
	  });
	
	
 });

</script> 
<script type="text/javascript" src="<?php echo base_url(); ?>js/quoting_view.js"></script>


<!--
<div class="ui_button font11px" style="margin-top:15px;width:540px;" id="cart">


</div>
<div class="ui_button font11px" style="margin-top:15px;width:540px;display:none;"  id="reset">
<?php
echo form_button($data = array(
    'name' => 'button',
    'id' => 'button',
     'style' => 'float:right;',  
    'content' => 'Reset Cart'
));
?>
</div>
-->
<br style="clear:both;"/> 
<div  style="margin:0 0 30px 0; min-width:900px; float: left; width:100%;">

    <!--
    *******************************************************************************
     ***
     *******************************************************************************/
    -->
<div style="margin:30px 30px;width:900px;" class="flt" id="dp_price_pt">
<strong>Digital Printing</strong><br /><br />
<div class="font11px flt bgc">
<span class="quote_item">Color &amp; Side<br />
<?php 
$options = array('clr_sgl' => 'Color Single Side', 'clr_dbl' => 'Color Double Side', 'blk_sgl' => 'Black & White Single Side', 'blk_dbl' => 'Black & White Double Side', 'mix_dbl' => 'Color / Black & White Double Side');
echo form_dropdown('pt_type', $options, 'clr_sgl', 'class="pt_target font10px" id="pt_type" style="width:230px;"');
?>
</span>
<span class="quote_item">Size<br />
<?php
$options = array('sra3' => 'SRA3', 'a3' => 'A3', 'a4' => 'A4', 'a5' => 'A5', 'a6' => 'A6', 'dle' => 'DLE');
echo form_dropdown('pt_size', $options, 'a4', 'class="pt_target font10px" id="pt_size" style="width:80px;"');
?>
</span>
<span class="quote_item">Weight<br />
<?php
$options = array('80g' => '80gsm', '100g' => '100gsm', '128g' => '128gsm', '150g' => '150gsm', '170g' => '170gsm', '210g' => '210gsm', '256g' => '256gsm', '300g' => '300gsm', 'label' => 'Label');
echo form_dropdown('pt_weight', $options, '100g', 'class="pt_target font10px" id="pt_weight" style="width:100px;"');
?>
</span>
<br style="clear:both;"/>
<span class="quote_item">Total (Inc. GST)<br />
<?php
echo form_input(array('id' => 'pt_tot_gst', 'value' => '', 'class' => 'ui-corner-all quote', 'readonly' => 'readonly'));
?> 

</span>
<span class="quote_item">Unit (Inc. GST)<br />
<?php
echo form_input(array('id' => 'pt_unit_gst', 'value' => '', 'class' => 'ui-corner-all quote', 'readonly' => 'readonly'));
?> 
</span>
<span class="quote_item">Cutting Fee<br />
<?php
echo form_input(array('id' => 'pt_cut_fee', 'value' => '', 'class' => 'ui-corner-all quote', 'readonly' => 'readonly'));
?> 
</span>

</div>
<div class="font11px frt bgc">
<span class="quote_item">Quantity<br />
<?php
echo form_input(array('id' => 'pt_qty', 'value' => 1000, 'class' => 'ui-corner-all quote pt_target'));
?> 

</span>
<span class="quote_item">Total (Exc. GST)<br />
<?php
echo form_input(array('id' => 'pt_total', 'value' => '', 'class' => 'ui-corner-all quote', 'readonly' => 'readonly'));
?> 
</span>
<span class="quote_item ui_button font10px"><br />
<?php
echo form_button($data = array('id' => 'pt_button_calc', 'style' => 'width:70px;', 'content' => 'Calculate'));
?>
</span>
<br style="clear:both;"/>
<span class="quote_item">Service Fee<br />
<?php
echo form_input(array('id' => 'pt_ser', 'value' => '0.00', 'class' => 'ui-corner-all quote pt_target'));
?> 

</span>
<span class="quote_item">Unit (Exc. GST)<br />
<?php
echo form_input(array('id' => 'pt_unit', 'value' => '', 'class' => 'ui-corner-all quote', 'readonly' => 'readonly'));
?> 
</span>
<span class="quote_item ui_button font10px"><br />
<?php
echo form_button($data = array('id' => 'pt_button_add', 'style' => 'width:70px;','content' => 'Add'));
?>
</span>

</div></div>
    <!--	
  /*******************************************************************************
   ***
   *******************************************************************************/
    -->
<div style="margin:15px 30px;width:900px;" class="flt" id="dp_price_lf">
<strong style="float:left; padding:0 20px 0 0;" >Large Format Poster</strong>
<div class="ui-state-highlight ui-corner-all" style="float:left; padding:2px;" >
<div class="ui-icon ui-icon-info" style="float: left; margin-right:5px;"></div>
<div style="float: left;" id="dp_price_lf_msg"></div>
</div> 
<br /><br />
<div class="font11px flt bgc">
<span class="quote_item">Color<br />
<?php 
$options = array('clr' => 'Color', 'blk' => 'Black & White');
echo form_dropdown('lf_type', $options, 'clr', 'class="lf_target font10px" id="lf_type" style="width:130px;"');
?>
</span>
<span class="quote_item">Size<br />
<?php
$options = array('a0' => 'A0', 'a1' => 'A1', 'a2' => 'A2');
echo form_dropdown('lf_size', $options, 'a2', 'class="lf_target font10px" id="lf_size" style="width:80px;"');
?>
</span>
<span class="quote_item">Printing<br />
<?php
$options = array('ink' => 'Inkjet', 'laser_80gsm' => 'Laser on 80gsm', 'laser_120gsm' => 'Laser on 120gsm');
echo form_dropdown('lf_printing', $options, 'ink', 'class="lf_target font10px" id="lf_printing" style="width:160px;"');
?>
</span>
<br style="clear:both;"/>
<span class="quote_item">Total (Inc. GST)<br />
<?php
echo form_input(array('id' => 'lf_tot_gst', 'value' => '', 'class' => 'ui-corner-all quote', 'readonly' => 'readonly'));
?> 
</span>
<span class="quote_item">Unit (Inc. GST)<br />
<?php
echo form_input(array('id' => 'lf_unit_gst', 'value' => '', 'class' => 'ui-corner-all quote', 'readonly' => 'readonly'));
?> 
</span>

</div>
<div class="font11px frt bgc">
<span class="quote_item">Quantity<br />
<?php
echo form_input(array('id' => 'lf_qty', 'value' => '1', 'class' => 'ui-corner-all quote lf_target'));
?> 

</span>
<span class="quote_item">Total (Exc. GST)<br />
<?php
echo form_input(array('id' => 'lf_total', 'value' => '', 'class' => 'ui-corner-all quote', 'readonly' => 'readonly'));
?> 
</span>
<span class="quote_item ui_button font10px"><br />
<?php
echo form_button($data = array('id' => 'lf_button_calc', 'style' => 'width:70px;', 'content' => 'Calculate'));
?>
</span>
<br style="clear:both;"/>
<span class="quote_item">Service Fee<br />
<?php
echo form_input(array('id' => 'lf_ser', 'value' => '0.00', 'class' => 'ui-corner-all quote lf_target'));
?> 

</span>
<span class="quote_item">Unit (Exc. GST)<br />
<?php
echo form_input(array('id' => 'lf_unit', 'value' => '', 'class' => 'ui-corner-all quote', 'readonly' => 'readonly'));
?> 
</span>
<span class="quote_item ui_button font10px"><br />
<?php
echo form_button($data = array('name' => 'button', 'style' => 'width:70px;','id' => 'lf_button_add','content' => 'Add'));
?>
</span>

</div></div>
		
 <!--
  /*******************************************************************************
   ***
   *******************************************************************************/		
-->
<div style="margin:15px 30px;width:900px;" class="flt" id="dp_price_bc">
<strong>Business Cards</strong><br /><br />
<div class="font11px flt bgc">
<span class="quote_item">Color &amp; Side<br />
<?php 
$options = array('clr_sgl' => 'Color Single Side', 'clr_dbl' => 'Color Double Side', 'blk_sgl' => 'Black &amp; White Single Side', 'blk_dbl' => 'Black &amp; White Double Side');
echo form_dropdown('bc_type', $options, 'Color Single Side', 'class="bc_target font10px" id="bc_type" style="width:190px;"');
?>
</span>
<span class="quote_item" id="bc_lamin">Lamination<br />
<?php
$options = array('lam' => 'Laminating', 'nolam' => 'No laminating');
echo form_dropdown('bc_lam', $options, 'nolam', 'class="bc_target font10px" id="bc_lam" style="width:130px;"');
?>
</span>
<span class="quote_item">Quantity<br />
<?php
  $options = array('100' => '100', '200' => '200', '300' => '300', '400' => '400', '500' => '500', '600' => '600', '700' => '700', '800' => '800', '900' => '900', '1000' => '1000', '1200' => '1200', '1400' => '1400', '1600' => '1600', '1800' => '1800', '2000' => '2000', '2500' => '2500', '3000' => '3000', '3500' => '3500', '4000' => '4000', '4500' => '4500', '5000' => '5000');
echo form_dropdown('bc_qty', $options, '100', 'class="bc_target font10px" id="bc_qty" style="width:80px;"');
?>
</span>
<br style="clear:both;"/>
<span class="quote_item">Total (Inc. GST)<br />
<?php
echo form_input(array('id' => 'bc_tot_gst', 'value' => '', 'class' => 'ui-corner-all quote', 'readonly' => 'readonly'));
?> 

</span>
<span class="quote_item">Naming Fee<br />
<?php
echo form_input(array('id' => 'bc_name_fee', 'value' => '', 'class' => 'ui-corner-all quote', 'readonly' => 'readonly'));
?> 
</span>

</div>
<div class="font11px frt bgc">
<span class="quote_item">Names<br />
<?php
echo form_input(array('id' => 'bc_names', 'value' => '1', 'class' => 'ui-corner-all quote bc_target'));
?> 

</span>
<span class="quote_item">Total (Exc. GST)<br />
<?php
echo form_input(array('id' => 'bc_total', 'value' => '', 'class' => 'ui-corner-all quote', 'readonly' => 'readonly'));
?> 
</span>
<span class="quote_item ui_button font10px"><br />
<?php
echo form_button($data = array('id' => 'bc_button_calc', 'style' => 'width:70px;', 'content' => 'Calculate'));
?>
</span>
<br style="clear:both;"/>
<span class="quote_item">Service Fee<br />
<?php
echo form_input(array('id' => 'bc_ser', 'value' => '0.00', 'class' => 'ui-corner-all quote bc_target'));
?> 

</span>
<span class="quote_item" style="visibility:hidden;"><br />
<?php
echo form_input(array('class' => 'ui-corner-all quote', 'readonly' => 'readonly'));
?> 
</span>
<span class="quote_item ui_button font10px"><br />
<?php
echo form_button($data = array('name' => 'button', 'style' => 'width:70px;','id' => 'bc_button_add','content' => 'Add'));
?>
</span>

</div></div>

 

<div style="margin:15px 30px;width:900px;" class="flt" id="dp_price_bd">
<strong>Binding</strong><br /><br />
<div class="font11px flt bgc">
<span class="quote_item">Binding Type<br />
<?php
  $options = array('spb_6mm' => 'Spiral Plastic Binding 6mm', 
		   'spb_8_10mm' => 'Spiral Plastic Binding 8-10mm', 
		   'spb_12_14mm' => 'Spiral Plastic Binding 12-14mm', 
		   'spb_16mm' => 'Spiral Plastic Binding 16mm', 
		   'spb_20mm' => 'Spiral Plastic Binding 20mm', 
		   'spb_25mm' => 'Spiral Plastic Binding 25mm', 
		   'spb_32mm' => 'Spiral Plastic Binding 32mm', 
		   'spb_38mm' => 'Spiral Plastic Binding 38mm', 
		   'wb_6p4mm' => 'Wire Binding 6.4mm', 
		   'wb_7p9mm' => 'Wire Binding 7.9mm', 
		   'wb_9p5mm' => 'Wire Binding 9.5mm', 
		   'wb_11_12mm' => 'Wire Binding 11-12mm', 
		   'wb_14p3mm' => 'Wire Binding 14.3mm', 
		   'wb_16mm' => 'Wire Binding 16mm', 
		   'wb_19mm' => 'Wire Binding 19mm', 
		   'wb_22mm' => 'Wire Binding 22mm', 
		   'gpb_50p' => 'Glue Perfect Binding 50 Pages', 
		   'gpb_100p' => 'Glue Perfect Binding 100 Pages', 
		   'gpb_150p' => 'Glue Perfect Binding 150 Pages', 
		   'gpb_200p' => 'Glue Perfect Binding 200 Pages', 
		   'gpb_250p' => 'Glue Perfect Binding 250 Pages', 
		   'psm_2_5p' => 'Perfect Staple Machine 2-5 Pages', 
		   'psm_6_10p' => 'Perfect Staple Machine 6-10 Pages', 
		   'psm_gt11p' => 'Perfect Staple Machine 11+ Pages');
echo form_dropdown('bd_type', $options, 'spb_6mm', 'class="bd_target font10px" id="bd_type" style="width:230px;"');
?>
</span>
<span class="quote_item">Size<br />
<?php
$options = array('a4' => 'A4', 'a3' => 'A3');
echo form_dropdown('bd_size', $options, 'a4', 'class="bd_target font10px" id="bd_size" style="width:80px;"');
?>
</span>
<br style="clear:both;"/>
<span class="quote_item">Total (Inc. GST)<br />
<?php
echo form_input(array('id' => 'bd_tot_gst', 'value' => '', 'class' => 'ui-corner-all quote', 'readonly' => 'readonly'));
?> 

</span>
<span class="quote_item">Unit (Inc. GST)<br />
<?php
echo form_input(array('id' => 'bd_unit_gst', 'value' => '', 'class' => 'ui-corner-all quote', 'readonly' => 'readonly'));
?> 
</span>

</div>
<div class="font11px frt bgc">
<span class="quote_item">Quantity<br />
<?php
echo form_input(array('id' => 'bd_qty', 'value' => '1', 'class' => 'ui-corner-all quote bd_target'));
?> 

</span>
<span class="quote_item">Total (Exc. GST)<br />
<?php
echo form_input(array('id' => 'bd_total', 'value' => '', 'class' => 'ui-corner-all quote', 'readonly' => 'readonly'));
?> 
</span>
<span class="quote_item ui_button font10px"><br />
<?php
echo form_button($data = array('id' => 'bd_button_calc', 'style' => 'width:70px;', 'content' => 'Calculate'));
?>
</span>
<br style="clear:both;"/>
<span class="quote_item">Service Fee<br />
<?php
echo form_input(array('id' => 'bd_ser', 'value' => '0.00', 'class' => 'ui-corner-all quote bd_target'));
?> 

</span>
<span class="quote_item">Unit (Exc. GST)<br />
<?php
echo form_input(array('id' => 'bd_unit', 'value' => '', 'class' => 'ui-corner-all quote', 'readonly' => 'readonly'));
?> 
</span>
<span class="quote_item ui_button font10px"><br />
<?php
echo form_button($data = array('name' => 'button', 'style' => 'width:70px;','id' => 'bd_button_add','content' => 'Add'));
?>
</span>

</div></div>


<div style="margin:15px 30px;width:900px;" class="flt" id="dp_price_lm">
<strong>Lamination</strong><br /><br />
<div class="font11px flt bgc">
<span class="quote_item">Size<br />
<?php
$options = array('lam_a0' => 'A0 Laminating', 'lam_a1' => 'A1 Laminating', 'lam_a2' => 'A2 Laminating', 'lam_sra3_dbl' => 'SRA3 Double Laminating', 'lam_sra3_sgl' => 'SRA3 Single Laminating', 'lam_a3' => 'A3 Laminating', 'lam_a4' => 'A4 Laminating', 'lam_a5' => 'A5 Laminating');
echo form_dropdown('lm_type', $options, 'a4', 'class="lm_target font10px" id="lm_type" style="width:180px;"');
?>
</span>
<br style="clear:both;"/>
<span class="quote_item">Total (Inc. GST)<br />
<?php
echo form_input(array('id' => 'lm_tot_gst', 'value' => '', 'class' => 'ui-corner-all quote', 'readonly' => 'readonly'));
?> 

</span>
<span class="quote_item">Unit (Inc. GST)<br />
<?php
echo form_input(array('id' => 'lm_unit_gst', 'value' => '', 'class' => 'ui-corner-all quote', 'readonly' => 'readonly'));
?> 
</span>

</div>
<div class="font11px frt bgc">
<span class="quote_item">Quantity<br />
<?php
echo form_input(array('id' => 'lm_qty', 'value' => '1', 'class' => 'ui-corner-all quote lm_target'));
?> 

</span>
<span class="quote_item">Total (Exc. GST)<br />
<?php
echo form_input(array('id' => 'lm_total', 'value' => '', 'class' => 'ui-corner-all quote', 'readonly' => 'readonly'));
?> 
</span>
<span class="quote_item ui_button font10px"><br />
<?php
echo form_button($data = array('id' => 'lm_button_calc', 'style' => 'width:70px;', 'content' => 'Calculate'));
?>
</span>
<br style="clear:both;"/>
<span class="quote_item">Service Fee<br />
<?php
echo form_input(array('id' => 'lm_ser', 'value' => '0.00', 'class' => 'ui-corner-all quote lm_target'));
?> 

</span>
<span class="quote_item">Unit (Exc. GST)<br />
<?php
echo form_input(array('id' => 'lm_unit', 'value' => '', 'class' => 'ui-corner-all quote', 'readonly' => 'readonly'));
?> 
</span>
<span class="quote_item ui_button font10px"><br />
<?php
echo form_button($data = array('name' => 'button', 'style' => 'width:70px;','id' => 'lm_button_add','content' => 'Add'));
?>
</span>

</div></div>
 



<!-- start block-->
<div style="margin:15px 30px;width:900px;" class="flt">
<strong>Staple ( On corner / Right side  etc. )</strong><br /><br />

<div class="font12px flt">
<table border="0" cellpadding="5" cellspacing="0">
<tbody>
<tr>
<th>Amount</th><th>1 Staple</th><th>2 Staple</th>
</tr>

<tr class="bgc">
<td>1-20</td><td>0.08</td><td>0.12</td>
</tr>

<tr class="bgc">
<td>21-50</td><td>0.07</td><td>0.10</td>
</tr>

<tr class="bgc">
<td>50-100</td><td>0.06</td><td>0.09</td>
</tr>

<tr class="bgc">
<td>Over 100</td><td>0.05</td><td>0.07</td>
</tr>

</tbody></table>
</div></div>
<!-- end block-->
<!-- start block-->
<div style="margin:15px 30px;width:900px;" class="flt">
<strong>Saddle Stitched ( For booklet) </strong><br /><br />

<div class="font12px flt">
<table border="0" cellpadding="5" cellspacing="0">
<tbody>
<tr>
<th>Amount</th><th>2-5 Double Pages</th><th>6-10 Double Page</th><th>Above 10</th>
</tr>

<tr class="bgc">
<td>1-50</td><td>0.50</td><td>0.55</td><td>0.60</td>
</tr>

<tr class="bgc">
<td>51-100</td><td>0.40</td><td>0.45</td><td>0.50</td>
</tr>

<tr class="bgc">
<td>101-200</td><td>0.35</td><td>0.38</td><td>0.40</td>
</tr>

<tr class="bgc">
<td>201-300</td><td>0.30</td><td>0.33</td><td>0.35</td>
</tr>

<tr class="bgc">
<td>301-400</td><td>0.25</td><td>0.28</td><td>0.30</td>
</tr>
<tr class="bgc">
<td>Over 500</td><td>0.20</td><td>0.23</td><td>0.25</td>
</tr>
<tr class="bgc">
<td>Over 800</td><td>0.15</td><td>0.18</td><td>0.20</td>
</tr>
</tbody></table>
</div></div>
<!-- end block-->
<!-- start block-->
<div style="margin:15px 30px;width:900px;" class="flt">
<strong>Folding </strong><br /><br />

<div class="font12px flt">
<table border="0" cellpadding="10" cellspacing="0">
<tbody>
<tr>
<th>Folding</th><th colspan="2">A4 (80-100gsm)</th><th colspan="2">A3 (80-100gsm)</th>
</tr>
 

<tr class="bgc">
<td></td><td>Half</td><td>2 Folding</td><td>Half</td><td>2 Folding</td>
</tr>

<tr class="bgc">
<td>1-50</td><td>0.03</td><td>0.05</td><td colspan="2" rowspan="5">A4's rate x 150%</td>
</tr>

<tr class="bgc">
<td>50-100</td><td>0.025</td><td>0.04</td>
</tr>

<tr class="bgc">
<td>Over 100</td><td>0.02</td><td>0.03</td>
</tr>
<tr class="bgc">
<td>Over 500</td><td>0.015</td><td>0.025</td>
</tr>
<tr class="bgc">
<td>Over 1000</td><td>0.012</td><td>0.02</td>
</tr>
</tbody></table>

(130-170gsm paper’s price = 200 % X 80-100gsm's rates)

</div></div>
<!-- end block-->


<!-- start block-->
<div style="margin:15px 30px;width:900px;" class="flt">
<strong>Coloured Paper Extra Pricing</strong><br /><br />

<div class="font12px flt">
<table border="0" cellpadding="5" cellspacing="0">
<tbody>
<tr>
<th>Amount</th><th>Price</th>
</tr>

<tr class="bgc">
<td>1-500</td><td>0.02</td>
</tr>

<tr class="bgc">
<td>Over 500</td><td>0.015</td>
</tr>

<tr class="bgc">
<td>Over 1000</td><td>0.01</td>
</tr>

</tbody></table>
</div></div>
<!-- end block-->


<!-- start block-->
<div style="margin:15px 30px;width:900px;" class="flt">
<strong>Creasing</strong><br /><br />
The price is based on the number for creasing. If 2 A5 are lay-outed on A4 and one creasing can go through A4, the price for A5 is half of A4. If one A4 needs Creasing twice, the price will be double.
<br style="clear:both;"/>
<div class="font12px flt">
<table border="0" cellpadding="5" cellspacing="0">
<tbody>
<tr>
<th>Amount</th><th>Price</th>
</tr>

<tr class="bgc">
<td>1-20</td><td>0.30</td>
</tr>

<tr class="bgc">
<td>21-50</td><td>0.25</td>
</tr>

<tr class="bgc">
<td>51-100</td><td>0.15</td>
</tr>

<tr class="bgc">
<td>Over 100</td><td>0.10</td>
</tr>

</tbody></table>
</div></div>
<!-- end block-->






<!-- start block-->
<div style="margin:15px 30px;width:900px;" class="flt">
<strong>Perforation</strong><br /><br />

<div class="font12px flt">
Creasing's price x 1.50
</div></div>
<!-- end block-->


<!-- start block-->
<div style="margin:15px 30px;width:900px;" class="flt">
<strong>Scanning </strong><br /><br />

<div class="font12px flt">
<table border="0" cellpadding="5" cellspacing="0">
<tbody>
<tr>
<th>Amount</th><th colspan="2">A4 & A3</th><th colspan="2">A2</th><th colspan="2">A1</th>
</tr>

<tr class="bgc">
<td></td><td>black</td><td>color</td><td>black</td><td>colour</td><td>black</td><td>color</td>
</tr>

<tr class="bgc">
<td>1-5</td><td>1.00</td><td>1.50</td><td>2.00</td><td>8.00</td><td>3.00</td><td>10.00</td>
</tr>

<tr class="bgc">
<td>6-10</td><td>0.80</td><td>1.20</td><td>1.60</td><td>5.00</td><td>2.50</td><td>8.00</td>
</tr>

<tr class="bgc">
<td>11-20</td><td>0.60</td><td>1.00</td><td>1.50</td><td>4.50</td><td>2.00</td><td>7.00</td>
</tr>

<tr class="bgc">
<td>21-50</td><td>0.50</td><td>0.70</td><td>1.20</td><td>4.00</td><td>1.80</td><td>6.00</td>
</tr>
<tr class="bgc">
<td>Over 100</td><td>0.30</td><td>0.50</td><td>1.00</td><td>...</td><td>1.30</td><td>...</td>
</tr>

</tbody></table>
</div></div>
<!-- end block-->



<!-- start block-->
<div style="margin:15px 30px;width:900px;" class="flt">
<strong>Service & Others</strong><br /><br />
<div class="font12px">
* Folder: The price for digital printing and Creasing，plus the price for attachment(小耳朵） as follows:
<br /> <br />
<table border="0" cellpadding="5" cellspacing="0">
<tbody>
<tr>
<th>Amount</th><th>Price</th>
</tr>

<tr class="bgc">
<td>1-20</td><td>0.80</td>
</tr>
<tr class="bgc">
<td>21-50</td><td>0.70</td>
</tr>
<tr class="bgc">
<td>51-100</td><td>0.60</td>
</tr>
<tr class="bgc">
<td>101-200</td><td>0.55</td>
</tr>
<tr class="bgc">
<td>201-300</td><td>0.50</td>
</tr>
<tr class="bgc">
<td>301-400</td><td>0.45</td>
</tr>
<tr class="bgc">
<td>Over 500</td><td>0.40</td>
</tr>
</tbody></table>

  
  <br /> <br />
</div>


<div class="font12px">
* Burning CD/DVD      <br /> <br />

CD:<br />1-5: $3.50 <br />6-10: $3.00<br />Above 20: $2.50<br />
DVD:<br />1-5: $5.00<br />6-10: $4.50<br />Above 20: $3.50  <br /> <br />
</div>

<div class="font12px">
* Numbering / Mail Merging
<br /> <br />
Minimum Charge : $5.00<br />

First 300 : $35.00  Then  $15.00 / Per 300
  <br /> <br />
</div>


</div>
<!-- end block-->


<br style="clear:both;"/><br />

  </div> 