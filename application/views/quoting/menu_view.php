<span class="ui_buttonset font10px">
<?php 

$attr =  ($this->uri->segment(2) == 'digitalprint') ? array('class' => 'white') : array();
echo anchor(site_url('quoting/digitalprint'), 'Digital Print',$attr);

$attr =  ($this->uri->segment(2) == 'designexpress') ? array('class' => 'white') : array();
echo anchor(site_url('quoting/designexpress'), 'Graphic Design',$attr);


$attr =  ($this->uri->segment(2) == 'websolution') ? array('class' => 'white') : array();
echo anchor(site_url('quoting/websolution'), 'Web Solution',$attr);

$attr =  ($this->uri->segment(2) == 'energysigns') ? array('class' => 'white') : array();
echo anchor(site_url('quoting/energysigns'), 'Energy Signs',$attr);
?>
</span>

<script type="text/javascript"> 
$(".ui_buttonset").buttonset(); 
</script>

<script type="text/javascript"> 
      var span1 = '<span style="width:400px;display:inline-block;text-align:left">';
      var span2 = '</span><span style="width:60px;display:inline-block;text-align:right">';
      var span3 = '</span><span style="width:60px;display:inline-block;text-align:right">' ;
      var span4 = '</span><br />';
      var nbsp8 = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';	
	function ajax_post(post_data){  
	  $.post("<?php echo base_url(); ?>quoting/cart", post_data,
		 function(data){
 
		 var cart_data = $.parseJSON(data);
 		 $('#cart').html('');
 		 var subtotal = 0;
		 for (i in cart_data){
		 var html =  span1
		     + cart_data[i].product + span2
		     + cart_data[i].quantity + span3
		     + cart_data[i].unit_price + span4;
		     
		     subtotal += cart_data[i].quantity * cart_data[i].unit_price;
		     
		   $('#cart').append(html).fadeIn('slow');
		 }
		 var html_total =  'Subtotal: ' + (subtotal).toFixed(2) 
		 + nbsp8 + 'GST: ' + (subtotal * 0.15).toFixed(2)  
		 + nbsp8 + 'Total: ' + (subtotal * 1.15).toFixed(2);
		 
		 $('#cart').fadeIn('slow');
 		 $('#cart-total').html(html_total);	 
 		 	  
		 });	 	  
	}

  $(function() {
      
       ajax_post('');
        
      
     
	$('#reset').click(function(){
		 $.post("<?php echo base_url(); ?>quoting/reset", 
		 function(){  ajax_post(''); });	 
	});
	
	$('#hide-cart').click(function(){
	   if ($('#cart').is(":visible")){
		 $('#cart').stop(true,true).hide(111, function(){$('#hide-cart').html('[ Show Cart ]');}); 	 
           }
           else {
		 $('#cart').stop(true,true).show(111 , function(){$('#hide-cart').html('[ Hide Cart ]');});  
	   }
	});
	
        $('#cart-total').click(function(){
          $('#hide-cart').click();
        }).dblclick(function(){
          $('#reset').click();
        });
	
       $('.quote-noty')
       	.mouseenter(function() {
 			$(this).stop(true,true).fadeTo('normal', 1);
       		})
       	.mouseleave(function() {
        		$(this).stop(true,true).fadeTo('normal', 0.7);
       		});
	

 });
 
 
 

</script> 

<style>
.quote-noty {
opacity: 0.7;
display:block;
left:10px; 
   position:fixed; 
   text-align:left; 
   bottom:5px; 
         background:#000000;
         color:#ffffff; 
   padding:15px;
    border-radius: 6px;
     box-shadow: 0 4px 10px #000000;
z-index:999;

}
</style>

<div class="quote-noty">
<div class="font11px" style="width:540px;" id="cart"></div>
<div class="font11px" style="width:540px;margin-top:5px;font-weight:700;">
<a class="white font10px" id="reset" href="javascript:void(0)">[ Empty Cart ]</a>
<a class="white font10px" id="hide-cart"  style="margin-left:50px;" href="javascript:void(0)">[ Hide Cart ]</a>
<span id="cart-total"  style="margin-left:50px;"></span>
</div>
</div>