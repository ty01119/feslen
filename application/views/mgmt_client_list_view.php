  <script>
  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#customer" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/cust_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
			


		      });

      
    });
</script>

<?php echo form_open('mgmt_client/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="ui_button font10px">
<?php 
$data = array('content' => 'Add Customer', 'style' => 'margin:5px 0;', 'onclick'=>"openView('".site_url("customer/add")."','del');");
echo form_button($data);
?>
</td><td align="left" class="ui_button font10px">

</td><td align="left" class="ui_button font10px">

</td><td align="left" class="ui_button font10px">

</td></tr>

<tr><td align="left" class="font10px">
<span class="ui_button">
<?php
echo form_input(
		array(
		      'name'        => 'customer',
		      'id'        => 'customer',
		      'value'	  => $customer,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:5px;'
		      ) 
		);
?>
</span>
</td><td align="left" class="font10px">

</td><td align="left" class="ui_button font10px">
<?php 
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => $search,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		 );
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td></tr>
</table>
<?php echo form_close(); ?>



 

		
<table border="0" cellpadding="5" cellspacing="0" style="margin:30px 0; min-width:900px; float: left; width:100%;" >

<tr>
<th colspan="7" align="left"><?php echo $table_title;?></th>
</tr>

 

<tr class="bgc">
<td colspan="7" align="right" class="ui_button font9px">
<?php 
echo $this->pagination->create_links(); 
$attr = ($per_page == 25) ? array() : array('class' => 'white');
$attr['style'] = 'margin:0 10px;';
echo anchor("mgmt_client/filter/per_page/$total", 'Display All', $attr);
?></td>
</tr>

<tr class="bgc">
<th align="left" valign="middle" width="60">
<?php echo anchor("mgmt_client/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle">
<?php echo anchor("mgmt_client/orderby/name", 'Customer', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'name'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="120">
<?php echo anchor("mgmt_client/orderby/contact", 'Contact', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'contact'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="70">
<?php echo anchor("mgmt_client/orderby/phone", 'Phone', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'phone'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle">
<?php echo anchor("mgmt_client/orderby/email", 'Email', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'email'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="70">
<?php echo anchor("mgmt_client/orderby/city", 'City', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'city'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="center" valign="middle" width="140">Operations</th>
</tr>

<?php 



foreach($query->result() as $row):

  if (strlen($row->email) > 20) {
    $row->email = substr($row->email, 0, 17).'...';
  }
if (strlen($row->name) > 40) {
  $row->name = substr($row->name, 0, 37).'...';
}
?> 
<tr class="bgc">
<td align="left" valign="middle"><?php echo $row->id;?></td>
<td align="left" valign="middle"><?php echo $row->name;?></td>
<td align="left" valign="middle"><?php echo $row->contact;?></td>
<td align="left" valign="middle"><?php echo $row->phone;?></td>
<td align="left" valign="middle"><?php echo $row->email;?></td>
<td align="left" valign="middle"><?php echo $row->city;?></td>
<td align="center" valign="middle" class="ui_button font9px">
<?php 
$data = array('class' => 'invo-lib-btn','content' => 'Edit', 'onclick'=>"openView('".site_url("customer/edit/$row->id")."','edit-".$row->id."');");
echo form_button($data);
$data = array('class' => 'invo-lib-btn','content' => 'View', 'onclick'=>"openView('".site_url("customer/view/$row->id")."','view-".$row->id."');");
echo form_button($data);
$data = array('class' => 'invo-lib-btn','content' => 'Del', 'onclick'=>"openView('".site_url("mgmt_client/del/$row->id")."','del-".$row->id."');");
echo form_button($data);
?>
</td></tr>
<?php endforeach;?>

<tr><td colspan="7" align="left"></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"><?php echo 'Total Data Returned: ';echo $total;?></th>
<th align="left" valign="middle"></th>
<th align="right" valign="middle"></th>
<th align="right" valign="middle"></th>
<th align="right" valign="middle"></th>
<th align="center" valign="middle" class="ui_button font9px"></th>
</tr>
</table>

  