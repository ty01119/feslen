<h3>Branches</h3>
<div id="radio_menu3" class="ui_radio" style="float:left;margin-right: 10px;">
<?php 
echo form_radio(array('name'=>'radio_menu3', 'id'=>'radio_menu3_1', 'onclick'=>"window.location.href='".site_url('panel/set_branch/penrose')."'"));
echo form_label('Penrose Branch', 'radio_menu3_1');

echo form_radio(array('name'=>'radio_menu3', 'id'=>'radio_menu3_2', 'onclick'=>"window.location.href='".site_url('panel/set_branch/websolution')."'"));
echo form_label('3A Web Solution', 'radio_menu3_2');
 
echo form_radio(array('name'=>'radio_menu3', 'id'=>'radio_menu3_3', 'onclick'=>"window.location.href='".site_url('panel/set_branch/epsom')."'"));
echo form_label('Epsom Branch', 'radio_menu3_3');

echo form_radio(array('name'=>'radio_menu3', 'id'=>'radio_menu3_4', 'onclick'=>"window.location.href='".site_url('panel/set_branch/city')."'"));
echo form_label('City Branch', 'radio_menu3_4');

echo form_radio(array('name'=>'radio_menu3', 'id'=>'radio_menu3_6', 'onclick'=>"window.location.href='".site_url('panel/set_branch/energysigns')."'"));
echo form_label('3A Signs', 'radio_menu3_6');

echo form_radio(array('name'=>'radio_menu3', 'id'=>'radio_menu3_7', 'onclick'=>"window.location.href='".site_url('panel/set_branch/albany')."'"));
echo form_label('Albany Branch', 'radio_menu3_7');

echo form_radio(array('name'=>'radio_menu3', 'id'=>'radio_menu3_8', 'onclick'=>"window.location.href='".site_url('panel/set_branch/manukau')."'"));
echo form_label('Manukau Branch', 'radio_menu3_8');

?>
</div>  

<br style="clear: both;"/><br />

<div id="radio_menu3a" class="ui_radio" style="float:left;margin-right: 10px;">
<h3>Head Office</h3>
<?php 
echo form_radio(array('name'=>'radio_menu3a', 'id'=>'radio_menu3a_1', 'onclick'=>"window.location.href='".site_url('panel/set_branch/holdings')."'"));
echo form_label('3A HOLDINGS', 'radio_menu3a_1');
echo form_radio(array('name'=>'radio_menu3a', 'id'=>'radio_menu3a_2', 'onclick'=>"window.location.href='".site_url('hq_meeting')."'"));
echo form_label('Meeting Minutes', 'radio_menu3a_2');
?>
</div>

<div id="radio_menu3c" class="ui_radio" style="float:left;margin-right: 10px;">
<h3>Production </h3>
<?php 
echo form_radio(array('name'=>'radio_menu3c', 'id'=>'radio_menu3c_1', 'onclick'=>"window.location.href='".site_url('panel/set_branch/production')."'"));
echo form_label('Production Centre', 'radio_menu3c_1');
?>
</div>

<div id="radio_menu4d" class="ui_radio" style="float:left;margin-right: 10px;">
<h3>Client</h3>
<?php 
echo form_radio(array('name'=>'radio_menu4d', 'id'=>'radio_menu4d_1', 'onclick'=>"window.location.href='".site_url('mgmt_client')."'"));
echo form_label('Client Management', 'radio_menu4d_1');
?>
</div>
 
<div id="radio_menu4" class="ui_radio" style="float:left;margin-right: 10px;">
<h3>HR Management</h3>
<?php 
echo form_radio(array('name'=>'radio_menu4', 'id'=>'radio_menu4_1', 'onclick'=>"window.location.href='".site_url('mgmt_staff')."'"));
echo form_label('Staff Management', 'radio_menu4_1');
echo form_radio(array('name'=>'radio_menu4', 'id'=>'radio_menu4_2', 'onclick'=>"window.location.href='".site_url('mgmt_attn')."'"));
echo form_label('Attendance Management', 'radio_menu4_2');
?>
</div>


<br style="clear: both;"/><br />
<div id="radio_menu6" class="ui_radio" style="float:left;margin-right: 10px;">
<h3>Financial Management</h3>
<?php 
echo form_radio(array('name'=>'radio_menu6', 'id'=>'radio_menu6_1', 'onclick'=>"window.location.href='".site_url('stat_intertrans')."'"));
echo form_label('Internal Transaction Table', 'radio_menu6_1');
?>
</div>
<div id="radio_menu6a" class="ui_radio" style="float:left;margin-right: 10px;">
<h3>&nbsp;</h3>
<?php 
echo form_radio(array('name'=>'radio_menu6a', 'id'=>'radio_menu6a_1', 'onclick'=>"window.location.href='".site_url('mgmt_cost')."'"));
echo form_label('Cost Management', 'radio_menu6a_1'); 
echo form_radio(array('name'=>'radio_menu6a', 'id'=>'radio_menu6a_2', 'onclick'=>"window.location.href='".site_url('mgmt_cost_cate')."'"));
echo form_label('Category Management', 'radio_menu6a_2');
?>
</div>
<div id="radio_menu6b" class="ui_radio" style="float:left;margin-right: 10px;">
<h3>&nbsp;</h3>
<?php 
echo form_radio(array('name'=>'radio_menu6b', 'id'=>'radio_menu6b_1', 'onclick'=>"window.location.href='".site_url('stat_cost')."'"));
echo form_label('Cost Statistics Table', 'radio_menu6b_1');
?>
</div>

<br style="clear: both;"/><br />
<div id="radio_menu4a" class="ui_radio"  style="float:left;margin-right: 10px;">
<h3>Inventory &amp; Purchase Management</h3>
<?php 
echo form_radio(array('name'=>'radio_menu4a', 'id'=>'radio_menu4a_0', 'onclick'=>"window.location.href='".site_url('mgmt_inventory/order')."'"));
echo form_label('Inventory', 'radio_menu4a_0');

echo form_radio(array('name'=>'radio_menu4a', 'id'=>'radio_menu4a_1', 'onclick'=>"window.location.href='".site_url('mgmt_inventory')."'"));
echo form_label('Inventory Management', 'radio_menu4a_1');

echo form_radio(array('name'=>'radio_menu4a', 'id'=>'radio_menu4a_2', 'onclick'=>"window.location.href='".site_url('mgmt_inventory_cate')."'"));
echo form_label('Category Management', 'radio_menu4a_2');
 
echo form_radio(array('name'=>'radio_menu4a', 'id'=>'radio_menu4a_3', 'onclick'=>"window.location.href='".site_url('mgmt_bol')."'"));
echo form_label('Bill Of Lading', 'radio_menu4a_3');
?>
</div>


<div id="radio_menu4b" class="ui_radio"  style="float:left;margin-right: 10px;">
<h3> &nbsp;</h3>
<?php 
echo form_radio(array('name'=>'radio_menu4b', 'id'=>'radio_menu4b_1', 'onclick'=>"window.location.href='".site_url('mgmt_purchase')."'"));
echo form_label('Purchase Management', 'radio_menu4b_1');
 
echo form_radio(array('name'=>'radio_menu4b', 'id'=>'radio_menu4b_2', 'onclick'=>"window.location.href='".site_url('mgmt_vendor')."'"));
echo form_label('Vendors', 'radio_menu4b_2');
?>
</div>

 

<br style="clear: both;"/><br />
<div id="radio_menu5" class="ui_radio" style="float:left;margin-right: 10px;">

<h3>Web Solution Management</h3>
<?php 
echo form_radio(array('name'=>'radio_menu5', 'id'=>'radio_menu5_1', 'onclick'=>"window.location.href='".site_url('web_app_task')."'"));
echo form_label('Task Management', 'radio_menu5_1');
 
echo form_radio(array('name'=>'radio_menu5', 'id'=>'radio_menu5_2', 'onclick'=>"window.location.href='".site_url('web_app_domain')."'"));
echo form_label('Domain Management', 'radio_menu5_2');

?>
</div>

<br style="clear: both;"/><br />

<div id="radio_menu15" class="ui_radio" style="float:left;margin-right: 10px;">

<h3>Statistics</h3>

<?php 
echo form_radio(array('name'=>'radio_menu15', 'id'=>'radio_menu15_1', 'onclick'=>"window.location.href='".site_url('stat_turnover')."'"));
echo form_label('Turnover', 'radio_menu15_1');
 
echo form_radio(array('name'=>'radio_menu15', 'id'=>'radio_menu15_2', 'onclick'=>"window.location.href='".site_url('stat_turnover_charts')."'"));
echo form_label('Yearly Turnover Charts', 'radio_menu15_2');
 
echo form_radio(array('name'=>'radio_menu15', 'id'=>'radio_menu15_2a', 'onclick'=>"window.location.href='".site_url('stat_turnover_month')."'"));
echo form_label('Monthly Bar Chart by Year', 'radio_menu15_2a');

echo form_radio(array('name'=>'radio_menu15', 'id'=>'radio_menu15_3', 'onclick'=>"window.location.href='".site_url('stat_turnover_division')."'"));
echo form_label('Turnover by Division', 'radio_menu15_3');

echo form_radio(array('name'=>'radio_menu15', 'id'=>'radio_menu15_4', 'onclick'=>"window.location.href='".site_url('stat_turnover_clients')."'"));
echo form_label('Turnover by Clients', 'radio_menu15_4');
?>
</div>


<br style="clear: both;"/><br />

<div id="radio_menu3b" class="ui_radio" style="float:left;margin-right: 10px;">

<h3>Previous Branches</h3> 
<?php 
echo form_radio(array('name'=>'radio_menu3b', 'id'=>'radio_menu3b_1', 'onclick'=>"window.location.href='".site_url('panel/set_branch/newmarket')."'"));
echo form_label('Newmarket Branch', 'radio_menu3b_1');

echo form_radio(array('name'=>'radio_menu3b', 'id'=>'radio_menu3b_5', 'onclick'=>"window.location.href='".site_url('panel/set_branch/onehunga')."'"));
echo form_label('Onehunga Branch', 'radio_menu3b_5');
?>
</div>

<br style="clear: both;"/><br />

<script type="text/javascript"> 
$(".ui_radio, .ui_buttonset").buttonset(); 
</script>

<div class="ui-widget-content ui-corner-all" style="float: none; clear:both;margin:0px auto; width: 460px; align:left;padding:2px"> 
<div style="margin:5px;" >
  <div id="service" class="black12ptbold"> 
		<p>Technical Support: <a href="http://3awebsites.co.nz" target="_blank" class="white">3A web solution</a></p> 
                <p>Software Version:  <?PHP echo $cf_feslen['name'].' '.$cf_feslen['build']; ?></p>

        </div>
</div>
</div>