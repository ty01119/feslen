<?php

class Cron_model extends CI_Model {


  function __construct() {
    parent::__construct();
 
  }


  function attn_list()
  {

    $this->db->where('status', 'active');
    $this->db->where('full_time', 'yes');		
    $this->db->order_by('name', 'asc');
    $query = $this->db->get('co_staffs');
    foreach ($query->result() as $row) {

      $this->db->where('working_id', $row->working_id);
      $this->db->where('date', date("Y-m-d"));
      if (!$this->db->get('co_attn')->result()) {
	$data = array(
		      'id' => NULL,
		      'date' => date("Y-m-d"),
		      'working_id' => $row->working_id,
		      'name' => $row->name,
		      'working_time' => $row->working_time
		      );
	
       	$this->db->insert('co_attn', $data); 
	
      }

    } // end for each
  }

 
}
