<?php

class Invoice_model extends CI_Model {
  
  var $branch;
  var $db_name;
  function __construct() {
    parent::__construct();
    $this->branch = $this->session->userdata('branch');
    $this->db_name = $this->branch.'_invoices';
  }


  function invoice_list($date = "", $cust_name = "", 
			$filter = "", $search = "", 
			$orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;
 
    $this->_invoice_list_sql($date, $cust_name, $search, $filter, $orderby);
    
    $list->total = $this->db->get($this->branch.'_invoices')->num_rows();

    $this->_invoice_list_sql($date, $cust_name, $search, $filter, $orderby);
   
    if (!$per_page) $per_page = $list->total;
    
    $list->query = $this->db->get($this->branch.'_invoices', $per_page, $offset);
   //    echo  $this->db->last_query();
    
    return $list;


  }
	

  function _invoice_list_sql($date = "", $cust_name = "", 
			     $search = "", $filter = "", $orderby = ""){
    $this->db->where('deleted', 0);
   
    if ($date)
      {
	$this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	$this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
      }

    if ($cust_name && $cust_name != 'Customer..')
      {
	$this->db->where('cust_name', $cust_name);  
      }
    
    if ($orderby)
      {      
	if (strstr($orderby['order'], '-sp-')) 
	  {
	    $this->db->order_by(str_replace('-sp-', ' ', $orderby['order']), 
				$orderby['sort']); 
	  }
	else
	  {
	    $this->db->order_by($orderby['order'], $orderby['sort']);
	  }
      }
    
    if ($filter)
      {   

	foreach ($filter as $flr_num => $keys):
	  $this->db->where('( 1=', '1', false);
	  
	  $first = TRUE;
	  foreach ($keys as $key => $value):
	  if ($key == 'sql') 
	    {	 	      
	      $data = str_replace('-eq-', ' = ',
				  str_replace('-ne-', ' <> ', $value));
	    }
	  else 
	    {  
	      $data = $key.' = "'.$value.'"';
	    }
	  if ($first)
	    {
	      $this->db->where($data); 
	      $first = FALSE;
	    }
	  else 
	    {
	      $this->db->or_where($data);
	    }
	  endforeach;

	  $this->db->where('1', '1 )', false);
	  endforeach;
      }
    
    if ($search && $search != 'Search..')
      {   
	$search = $this->db->escape_like_str($search);
	$this->db->where("(`id` LIKE '%$search%' OR `date` LIKE '%$search%' OR `subtotal` LIKE '%$search%' OR `tax` LIKE '%$search%' OR `total` LIKE '%$search%' OR `discount` LIKE '%$search%' OR `paid` LIKE '%$search%' OR `cust_name` LIKE '%$search%' OR `quote_id` LIKE '%$search%' OR `logs` LIKE '%$search%' OR `info` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 


  function load_invoice($id)
  {
    $this->db->where('id', $id);
    $result = $this->db->get($this->branch.'_invoices')->result();
    $invoice =  $result[0];
    return $invoice;		
  }

  function load_by_id($id)
  {
    $this->db->where('id', $id);
    $query = $this->db->get($this->db_name);
    
    if ($query->num_rows() > 0)
      {
	$result = $query->result();
	return $result[0];
      }
    else
      {
	return false;
      }
  }
  

  function load_invo_cons($id)
  {
    $this->db->where('invoice_id', $id);
    $this->db->order_by("id", "asc"); 
    //$this->db->select('product', 'quantity', 'unit_price', 'discount');
    $result = $this->db->get($this->branch.'_invo_cons')->result();
    
    return $result;
		
  }


  function invoice_update($invoice, $contents)
  {

    // record payment
    $prev = $this->load_by_id($invoice->id);
    
    $payment = new stdClass;
    if(($payment->paid = ($invoice->paid - $prev->paid)) > 0)
      {
	$payment->id = NULL;
	$payment->date = date("Y-m-d H:i:s");
	$payment->invoice_id = $invoice->id;
	$payment->detail = 'Client: '.$invoice->cust_name;
	
	$array_curr = explode(';', $invoice->payment_meth);
	$array_prev = explode(';', $prev->payment_meth);
	$pay_meth = implode(';', array_diff($array_curr, $array_prev));

	$payment->payment_meth = $pay_meth ? $pay_meth : $invoice->payment_meth;
	
	$this->db->insert($this->branch.'_payments', $payment);
      }

    // update invoice
    $this->db->where('id', $invoice->id);

    $operator = $this->session->userdata('user_name').' ('.$this->session->userdata('login_id').') ';
    $invoice->logs = br(2).date("d/m/Y H:i:s").' Saved by: '.$operator.$invoice->logs;
    
    $this->db->update($this->branch.'_invoices', $invoice);
   
    $this->db->where('invoice_id', $invoice->id);
    $this->db->delete($this->branch.'_invo_cons');
    
    foreach($contents as $item) {
      $item->id = NULL;
      $item->invoice_id = $invoice->id;
      $this->db->insert($this->branch.'_invo_cons', $item); 
    }

  }


  function invoice_add($invoice, $contents)
  {

    // add payment
    $invoice->id = NULL;
    
    $operator = $this->session->userdata('user_name').' ('.$this->session->userdata('login_id').') ';
    $invoice->logs = br(2).date("d/m/Y H:i:s").' added by: '.$operator.$invoice->logs;

    $this->db->insert($this->branch.'_invoices', $invoice);
    
    $invoice->id = $this->db->insert_id();
    $this->db->where('invoice_id', $invoice->id);
    $this->db->delete($this->branch.'_invo_cons');
    
    foreach($contents as $item) {
      $item->id = NULL;
      $item->invoice_id = $invoice->id;
      $this->db->insert($this->branch.'_invo_cons', $item); 
    }

    // record payment    
    $payment = new stdClass;
    if(($payment->paid = $invoice->paid) > 0)
      {
	$payment->id = NULL;
	$payment->date = date("Y-m-d H:i:s");
	$payment->invoice_id = $invoice->id;
	$payment->detail = 'Client: '.$invoice->cust_name;
	$payment->payment_meth = $invoice->payment_meth;
	
	$this->db->insert($this->branch.'_payments', $payment);
      }
  }

  


  function invoice_del($id){

    $this->db->where('id', $id);
    $result = $this->db->get($this->branch.'_invoices')->result();
    $invoice =  $result[0];

    $invoice->deleted = 1;

    $this->db->where('id', $invoice->id);

    $this->db->update($this->branch.'_invoices', $invoice);
  
  }

 

  }