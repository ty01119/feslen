<?php

class Web_app_task_model extends CI_Model {

  var $db_name;

  function __construct() {
    parent::__construct();
    $this->db_name = 'web_app_task';  
  }


  function web_app_task_list($date = "", $developer = "", $designer = "",
			$filter = "", $search = "", 
			$orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->_web_app_task_list_sql($date, $developer, $designer, $search, $filter, $orderby);
    
    $list->total = $this->db->get($this->db_name)->num_rows();

    $this->_web_app_task_list_sql($date, $developer, $designer, $search, $filter, $orderby);
   
    $list->query = $this->db->get($this->db_name, $per_page, $offset);
    //   echo  $this->db->last_query();
    
    return $list;


  }
	

  function _web_app_task_list_sql($date = "", $developer = "", $designer = "", 
			     $search = "", $filter = "", $orderby = ""){
    //  $this->db->where('deleted', 0);
   
    if ($date)
      {
	$this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	$this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
      }

    if ($developer && $developer != 'Developer..')
      {
	$this->db->where('developer', $developer);  
      }

    if ($designer && $designer != 'Designer..')
      {
	$this->db->where('designer', $designer);  
      }
    
    if ($orderby)
      {      
	if (strstr($orderby['order'], '-sp-')) 
	  {
	    $this->db->order_by(str_replace('-sp-', ' ', $orderby['order']), 
				$orderby['sort']); 
	  }
	else
	  {
	    $this->db->order_by($orderby['order'], $orderby['sort']);
	  }
      }
    
    if ($filter)
      {   

	foreach ($filter as $flr_num => $keys):
	  $this->db->where('( 1=', '1', false);
	  
	  $first = TRUE;
	  foreach ($keys as $key => $value):
	  if ($key == 'sql') 
	    {	 	      
	      $data = str_replace('-eq-', ' = ',
				  str_replace('-ne-', ' <> ', $value));
	    }
	  else 
	    {  
	      $data = $key.' = "'.$value.'"';
	    }
	  if ($first)
	    {
	      $this->db->where($data); 
	      $first = FALSE;
	    }
	  else 
	    {
	      $this->db->or_where($data);
	    }
	  endforeach;

	  $this->db->where('1', '1 )', false);
	  endforeach;
      }
    
    if ($search && $search != 'Search..')
      {   
	$search = $this->db->escape_like_str($search);
	$this->db->where("(`id` LIKE '%$search%' OR `date` LIKE '%$search%' OR `subtotal` LIKE '%$search%' OR `tax` LIKE '%$search%' OR `total` LIKE '%$search%' OR `discount` LIKE '%$search%' OR `paid` LIKE '%$search%' OR `cust_name` LIKE '%$search%' OR `quote_id` LIKE '%$search%' OR `logs` LIKE '%$search%' OR `info` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 


  function load_web_app_task($id)
  {
    $this->db->where('id', $id);
    $result = $this->db->get($this->db_name)->result();
    $web_app_task =  $result[0];
    return $web_app_task;
		
  }



  function update($data)
  {
    $this->db->where('id', $data->id);

    $operator = $this->session->userdata('user_name').' ('.$this->session->userdata('login_id').') ';
    $data->logs = br(2).date("d/m/Y H:i:s").' Saved by: '.$operator.$data->logs;
    
    $this->db->update($this->db_name, $data);
   
  }


  function add($data)
  {
    $data->id = NULL;
    
    $operator = $this->session->userdata('user_name').' ('.$this->session->userdata('login_id').') ';
    $data->logs = br(2).date("d/m/Y H:i:s").' added by: '.$operator.$data->logs;
    
    $this->db->insert($this->db_name, $data);
    
    return ($this->db->affected_rows() == 1) ? TRUE : FALSE;
    }
  
  


  function web_app_task_del($id){

    $this->db->where('id', $id);
    $result = $this->db->get($this->db_name)->result();
    $web_app_task =  $result[0];

    $web_app_task->deleted = 1;

    $this->db->where('id', $web_app_task->id);

    $this->db->update($this->db_name, $web_app_task);
  
    return ($this->db->affected_rows() == 1) ? TRUE : FALSE;
  }

 

  }
