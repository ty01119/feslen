<?php

class hq_meeting_model extends CI_Model {

  var $db_name;

  function __construct() {
    parent::__construct();
    $this->db_name = 'co_meeting_minutes';  
  }


  function get_list($date = "", $filter = "", $search = "", 
			$orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->_list_sql($date, $search, $filter, $orderby);
    
    $list->total = $this->db->get($this->db_name)->num_rows();

    $this->_list_sql($date, $search, $filter, $orderby);
   
    $list->query = $this->db->get($this->db_name, $per_page, $offset);
    //   echo  $this->db->last_query();
    
    return $list;


  }
	

  function _list_sql($date = "",  $search = "", $filter = "", $orderby = ""){
    //  $this->db->where('deleted', 0);
   
    if ($date)
      {
	$this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	$this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
      }


    
    if ($orderby)
      {      
	if (strstr($orderby['order'], '-sp-')) 
	  {
	    $this->db->order_by(str_replace('-sp-', ' ', $orderby['order']), 
				$orderby['sort']); 
	  }
	else
	  {
	    $this->db->order_by($orderby['order'], $orderby['sort']);
	  }
      }
    
    if ($filter)
      {   

	foreach ($filter as $flr_num => $keys):
	  $this->db->where('( 1=', '1', false);
	  
	  $first = TRUE;
	  foreach ($keys as $key => $value):
	  if ($key == 'sql') 
	    {	 	      
	      $data = str_replace('-eq-', ' = ',
				  str_replace('-ne-', ' <> ', $value));
	    }
	  else 
	    {  
	      $data = $key.' = "'.$value.'"';
	    }
	  if ($first)
	    {
	      $this->db->where($data); 
	      $first = FALSE;
	    }
	  else 
	    {
	      $this->db->or_where($data);
	    }
	  endforeach;

	  $this->db->where('1', '1 )', false);
	  endforeach;
      }
    
    if ($search && $search != 'Search..')
      {   
	$search = $this->db->escape_like_str($search);
	$this->db->where("(`id` LIKE '%$search%' OR `date` LIKE '%$search%' OR `attendees` LIKE '%$search%' OR `purpose` LIKE '%$search%' OR `outcomes` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 

  function load_by_id($id)
  {
    $this->db->where('id', $id);
    $query = $this->db->get($this->db_name);
    
    if ($query->num_rows() > 0)
      {
	$result = $query->result();
	return $result[0];
      }
    else
      {
	return false;
      }
  }

  function update($data)
  {
    $this->db->where('id', $data->id);

    $operator = $this->session->userdata('user_name').' ('.$this->session->userdata('login_id').') ';
    $data->logs = br(2).date("d/m/Y H:i:s").' Saved by: '.$operator.$data->logs;
    
    if ($data->approve) $data->status .= 'Approved by: '.$operator.date("d/m/Y H:i:s").br();
    $this->db->update($this->db_name, $data);
   
  }


  function add($data)
  {
    $data->id = NULL;
    
    $operator = $this->session->userdata('user_name').' ('.$this->session->userdata('login_id').') ';
    $data->logs = br(2).date("d/m/Y H:i:s").' added by: '.$operator.$data->logs;
        
    if ($data->approve) $data->status .= 'Approved by: '.$operator.date("d/m/Y H:i:s").br();
    $this->db->insert($this->db_name, $data);
    
    return ($this->db->affected_rows() == 1) ? TRUE : FALSE;
    }
  
  


  function del($id){

    $this->db->where('id', $id);
    $result = $this->db->get($this->db_name)->result();
    $data =  $result[0];

    $data->deleted = 1;

    $this->db->where('id', $data->id);

    $this->db->update($this->db_name, $data);
  
    return ($this->db->affected_rows() == 1) ? TRUE : FALSE;
  }

 

  }
