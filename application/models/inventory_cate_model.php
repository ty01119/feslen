<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Inventory_cate_model extends CI_Model {

  var $db_name;

  function __construct()
  {
    parent::__construct();
    $this->db_name = 'co_inventory_cate';
    $this->db_item = 'co_inventory';
  }


  function load_by_url($url)
  {
    $this->db->where('url_title', $url);
    $query = $this->db->get($this->db_name);
    
    if ($query->num_rows() > 0)
      {
	$result = $query->result();
	return $result[0];
      }
    else
      {
	return false;
      }    
  }

  function load_tree()
  {
    
    $rta = array();
    $this->db->where('parent', '(none)'); 
    $this->db->order_by("order", "asc");
    $query =  $this->db->get($this->db_name);
    
    foreach ($query->result() as $row)
      { 
	$this->db->where('parent', $row->name); 
	$this->db->order_by("name", "asc");
	$query = $this->db->get($this->db_name);
	if ($query->num_rows) {
	  $tmp = array(); 
	  array_push($tmp, $row);
	  foreach ($query->result() as $row)
	    {
	      array_push($tmp, $row);
	    }
	  array_push($rta, $tmp);
	} else {
	  array_push($rta, $row);
	}
      }
    
    return $rta;
  }
  



  function get_list($search = "", $filter = "", $orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->_get_list_sql($search, $filter, $orderby);
    
    $list->total = $this->db->get($this->db_name)->num_rows();

    $this->_get_list_sql($search, $filter, $orderby);
   
    $list->query = $this->db->get($this->db_name, $per_page, $offset);
 
    return $list;

  }
	

  function _get_list_sql($search = "", $filter = "", $orderby = ""){
    if ($orderby)
      {      
	$this->db->order_by($orderby['order'], $orderby['sort']); 
      }    
    if ($filter)
      {   
	foreach ($filter as $key => $value):
	  $this->db->where($key, $value); 
	  endforeach;
      }
    if ($search && $search != 'Search..')
      {   
	// $this->db->where("(`id` LIKE '%$search%' OR `name` LIKE '%$search%' OR `parent` LIKE '%$search%')", NULL, FALSE);  
	$cols = array('id','name','parent');
     	
	$this->db->where('( 1=', '1', false);    
	$first = TRUE;
	foreach ($cols as $col)
	  {
	    if ($first)
	      {
		$this->db->like($col, $search); 
		$first = FALSE;
	      }
	    else 
	      {
		$this->db->or_like($col, $search); 
	      }	
	  }
	$this->db->where('1', '1 )', false);

      }
  }
 
  function load_parent_names($term)
  {
    $this->db->where('parent', '(none)'); 
    $this->db->select('name'); 
    $this->db->order_by('name', 'asc');
    $this->db->like('name', $term);
    $result = $this->db->get($this->db_name)->result();
    $array = array();
    foreach($result as $row) array_push($array, $row->name);
    return $array;
  }
 
  function load_category_names($term)
  {
    $this->db->select('name'); 
    $this->db->order_by('name', 'asc');
    $this->db->like('name', $term);
    $result = $this->db->get($this->db_name)->result();
    $array = array();
    foreach($result as $row) array_push($array, $row->name);
    return $array;
  }
  

  function load_by_id($id)
  {
    $this->db->where('id', $id);
    $query = $this->db->get($this->db_name);
    
    if ($query->num_rows() > 0)
      {
	$result = $query->result();
	return $result[0];
      }
    else
      {
	return false;
      }
  }


  function update($id)
  {
    if ($prev = $this->load_by_id($id))
      {
	$prev_cate = $prev->name;
      }
    else 
      {
	return FALSE;
      }
    
    $this->db->where('id', $id);    
    $cols = array('name', 'parent', 'order');
    foreach ($cols as $col) $data[$col] = $this->input->post($col);
    $data['url_title'] = url_title($data['name'], 'dash', TRUE);

    $this->db->update($this->db_name, $data);

    if ($this->db->affected_rows() == 1)
      {
	// update product
	$this->db->where('category', $prev_cate);   
	$query = $this->db->get($this->db_item);
	foreach($query->result() as $item) {      
	  $item->category = $data['name'];
	  $this->db->where('id', $item->id);
	  $this->db->update($this->db_item, $item);
	}
	// update sub category
	$this->db->where('parent', $prev_cate);
	$query = $this->db->get($this->db_name);
	foreach($query->result() as $cate) {      
	  $cate->parent = $data['name'];
	  $this->db->where('id', $cate->id);
	  $this->db->update($this->db_name, $cate);
	}
	return TRUE;
      }
    else
      {
	return FALSE;
      }    
  }


  function add() {

    $data['id'] = NULL;

    $cols = array('name','parent', 'order');
    foreach ($cols as $col) $data[$col] = $this->input->post($col);

    $data['url_title'] = url_title($data['name'], 'dash', TRUE);

    $this->db->insert($this->db_name, $data); 
    return ($this->db->affected_rows() == 1) ? TRUE : FALSE;

  }



  function del($id){

    $this->db->where('id', $id);
    $this->db->delete($this->db_name);
    return ($this->db->affected_rows() == 1) ? TRUE : FALSE;
  }




}