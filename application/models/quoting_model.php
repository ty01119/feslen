<?php

class Quoting_model extends CI_Model {

 

  function Quoting_model() {
    parent::__construct();
  }
 
  
  function dp_price_pt()
  { 
    $prices = $this->db->get('dp_price_pt')->result();
    $dp_price_pt = array();    
    
    foreach($prices as $row) {
      $dp_price_pt[$row->type] 
	= array(
		'qty10' => $row->qty10,			       
		'qty20' => $row->qty20,			       
		'qty30' => $row->qty30,
		'qty50' => $row->qty50,	
		'qty80' => $row->qty80,	
		'qty100' => $row->qty100,			       
		'qty300' => $row->qty300,			       
		'qty500' => $row->qty500,			       
		'qty800' => $row->qty800,			       
		'qty1000' => $row->qty1000,			       
		'qty1500' => $row->qty1500,	
		'qty2000' => $row->qty2000,			       
		'qty4000' => $row->qty4000,			       
		'qty6000' => $row->qty6000,			       
		'qty8000' => $row->qty8000,	
		'qty10000' => $row->qty10000
		);
    }
    return $dp_price_pt;
  }

  function dp_price_lf(){
    $prices = $this->db->get('dp_price_lf')->result();

    $dp_price_lf = array();        
   
    foreach($prices as $row) {
      
      $dp_price_lf[$row->type] 
	= array(
		'qty5' => $row->qty5,			       
		'qty10' => $row->qty10,			       
		'qty20' => $row->qty20,			       
		'qty30' => $row->qty30,			       
		'qty50' => $row->qty50,			       
		'qty100' => $row->qty100
		);
    }
    return $dp_price_lf;
  } 

  function dp_price_lm(){
    $prices = $this->db->get('dp_price_lm')->result();
    
    $dp_price_lm = array();        
    
    foreach($prices as $row) {
      
      $dp_price_lm[$row->type] 
	= array(
		'qty5' => $row->qty5,			       
		'qty10' => $row->qty10,			       
		'qty20' => $row->qty20,			       
		'qty30' => $row->qty30,			       
		'qty50' => $row->qty50,			       
		'qty100' => $row->qty100
		);
    }
    return $dp_price_lm;
  } 
 
 

  function dp_price_bd(){
    $prices = $this->db->get('dp_price_bd')->result();

    $array = '';
    $dp_price_bd = array();    
    
   
    foreach($prices as $row) {
      
      $dp_price_bd[$row->type] 
	= array(
		'qty5' => $row->qty5,			       
		'qty10' => $row->qty10,				       
		'qty20' => $row->qty20,			       
		'qty30' => $row->qty30,			       
		'qty50' => $row->qty50,			       
		'qty100' => $row->qty100,			       
		'qty200' => $row->qty200,			       
		'qty300' => $row->qty300,			       
		'qty500' => $row->qty500
		);
    }
    return $dp_price_bd;
  } 

  function dp_price_bc(){
    $prices = $this->db->get('dp_price_bc')->result();
    
    $dp_price_bc = array();    
    
    foreach($prices as $row) {
      $dp_price_bc[$row->type] 
	= array(
		'qty100' => $row->qty100,			       
		'qty200' => $row->qty200,			       
		'qty300' => $row->qty300,			       
		'qty400' => $row->qty400,			       
		'qty500' => $row->qty500,			       
		'qty600' => $row->qty600,			       
		'qty700' => $row->qty700,			       
		'qty800' => $row->qty800,			       
		'qty900' => $row->qty900,			       
		'qty1000' => $row->qty1000,			       
		'qty1200' => $row->qty1200,			       
		'qty1400' => $row->qty1400,			       
		'qty1600' => $row->qty1600,			       
		'qty1800' => $row->qty1800,			       
		'qty2000' => $row->qty2000,			       
		'qty2500' => $row->qty2500,
		'qty3000' => $row->qty3000,
		'qty3500' => $row->qty3500,			       
		'qty4000' => $row->qty4000,			       
		'qty4500' => $row->qty4500,			       
		'qty5000' => $row->qty5000	
		);
    }
    return $dp_price_bc;
  }
}