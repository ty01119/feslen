<?php

class Customer_model extends CI_Model {

  var $customer;
  var $db_name;

  function Customer_model() {
    parent::__construct();
    $this->db_name = 'co_customers';
    $this->customer = new stdClass;
    $this->customer->id = '';
    $this->customer->name 
      = $this->customer->address 
      = $this->customer->city 
      = $this->customer->ship_name 
      = $this->customer->ship_address 
      = $this->customer->ship_city
      = $this->customer->pricing
      = $this->customer->discount 
      = $this->customer->phone 
      = $this->customer->cellphone 
      = $this->customer->fax 
      = $this->customer->email 
      = $this->customer->url
      = $this->customer->contact
	= $customer->billing_contact 
	= $customer->billing_email 
      = $this->customer->notes = '';
  }


  function load_names($term)
  {
    $this->db->select('name');
    
    $this->db->like('name', $term);
    $result = $this->db->get('co_customers')->result();
		
    $array = array();
    foreach($result as $row) array_push($array, $row->name);
 
    return $array;
    
  }

  function load_by_id($id)
  {
    $this->db->where('id', $id);
    $query = $this->db->get($this->db_name);
    
    if ($query->num_rows() > 0)
      {
	$result = $query->result();
	return $result[0];
      }
    else
      {
	return false;
      }
  }
  
  function load_customer($id)
  {
    $this->db->where('id', $id);
    if ($result = $this->db->get('co_customers')->result()) {
      $customer =  $result[0];
    } else {
      $customer = $this->customer;
    }	
    return $customer;
  }

  function load_by_name($name)
  {
    $this->db->where('name', $name);
    if ($result = $this->db->get('co_customers')->result()) {
      $customer = $result[0];
    } else { 
      $customer = $this->customer;
    }
    return $customer;
  }

  function customer_list($cust_name = "", 
			 $filter = "", $search = "", 
			 $orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->_customer_list_sql( $cust_name, $search, $filter, $orderby);
    
    $list->total = $this->db->get('co_customers')->num_rows();
    
    $this->_customer_list_sql($cust_name, $search, $filter, $orderby);
    
    $list->query = $this->db->get('co_customers', $per_page, $offset);
    //   echo  $this->db->last_query();
    
    return $list;


  }

  function _customer_list_sql($cust_name = "", 
			      $search = "", $filter = "", $orderby = ""){
    
    if ($cust_name && $cust_name != 'Customer..')
      {
	$this->db->where('name', $cust_name);  
      }
   
    if ($orderby)
      {      
	$this->db->order_by($orderby['order'], $orderby['sort']);

      }
        
    if ($search && $search != 'Search..')
      {   
	$search = $this->db->escape_like_str($search);
	$this->db->where("(`id` LIKE '%$search%' OR `name` LIKE '%$search%' OR `address` LIKE '%$search%' OR `city` LIKE '%$search%' OR `ship_name` LIKE '%$search%' OR `ship_address` LIKE '%$search%' OR `ship_city` LIKE '%$search%' OR `pricing` LIKE '%$search%' OR `discount` LIKE '%$search%' OR `phone` LIKE '%$search%' OR `cellphone` LIKE '%$search%' OR `fax` LIKE '%$search%' OR `email` LIKE '%$search%' OR `url` LIKE '%$search%' OR `contact` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 
  function customer_update($customer)
  {
  
    if ($customer->id == 10001) return;
    if ($customer->id == 10002) return; 
    if ($customer->id == 10003) return;
      
    // $this->db->select('name');
    $this->db->where('id', $customer->id);
    if ($result = $this->db->get('co_customers')->result()) {
      $prev_data = $result[0];
     
      if($prev_data->name != $customer->name){

	$this->db->select('branch');
	$query = $this->db->get('co_branches');
	foreach ($query->result() as $row)
	  {
	    $branch = $row->branch;		    
	    $this->db->where('cust_name', $prev_data->name);
	    $invo_query = $this->db->get($branch.'_invoices');
	    foreach ($invo_query->result() as $invoice) {
	      //change cust name in invoices
	      $invoice->cust_name = $customer->name;
 
	      $this->db->where('id', $invoice->id);
	      $this->db->update($branch.'_invoices', $invoice);  
	      
	    }

	  }	
      }
      
      $this->db->where('id', $customer->id);
      $this->db->update('co_customers', $customer);
      
    }
   
  }

  function has_name($name)
  {
    $this->db->where('name', $name);
    $query = $this->db->get($this->db_name);    
    return ($query->num_rows == 1) ? true : false; 
  }

  function has_dup_name($id, $name)
  {
    $pre_data = $this->load_by_id($id); 
    
    return $this->has_name($name) && (strcasecmp($name, $pre_data->name) != 0);
	   
  }

  function customer_add($customer)
  {
    $customer->id = NULL;
    $customer->date= date("Y-m-d");
    $this->db->insert('co_customers', $customer);
    
  }
  

  function customer_del($id, $receiver)
  {
    $this->db->where('id', $id);

    if ($result = $this->db->get('co_customers')->result()) {
      $target = $result[0];

      if ($receiver != 'Customer..') {
	$this->db->select('branch');
	$query = $this->db->get('co_branches');

	foreach ($query->result() as $row)
	  {
	    $branch = $row->branch;		    
	    $this->db->where('cust_name', $target->name);
	    $invo_query = $this->db->get($branch.'_invoices');

	    foreach ($invo_query->result() as $invoice) {
	      $invoice->cust_name = $receiver;
	      $this->db->where('id', $invoice->id);
	      $this->db->update($branch.'_invoices', $invoice);  	      
	    }
	    
	  }	
      }
      
      $this->db->where('id', $id);
      $this->db->delete('co_customers'); 
      
    }
   
  }


  }