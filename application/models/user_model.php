<?php

class User_model extends CI_Model {

	function login()
	{
	  $this->db->where('working_id', $this->input->post('login_id'));
	  $this->db->where('status', 'active');
	  $this->db->where('password', sha1($this->input->post('password')));
	  
	  if ($result = $this->db->get('co_staffs')->result()) {
	    $user = $result[0];
	  } else {
	    $user = FALSE;
	  }	
	  return $user;
	  
	}
	
	

  	function login_cookie($uid, $passwd)
  	{ 
    
 	  $this->db->where('working_id', $uid);
	  $this->db->where('status', 'active');
	  $this->db->where('password', $passwd);
	  
	  if ($result = $this->db->get('co_staffs')->result()) {
	    $user = $result[0];
	  } else {
	    $user = FALSE;
	  }	
	  return $user;		
	  
 	}


	function check_in(){

	  if (!$this->check_attn($this->session->userdata('login_id'))) {
	    
	    $this->db->where('working_id', $this->session->userdata('login_id'));
	    $this->db->where('date', date("Y-m-d"));
	    
	    $this->db->set('check_in_time', date("H:i:s"));
	    $this->db->set('check_in_ip', $this->input->ip_address());

	    $this->db->update('co_attn');
	    
	    return TRUE;
	  }
	}
	
	function check_out(){
	  $this->db->where('working_id', $this->session->userdata('login_id'));
	  $this->db->where('date', date("Y-m-d"));

	  $this->db->set('check_out_time', date("H:i:s"));
	  $this->db->set('check_out_ip', $this->input->ip_address());

	  $this->db->update('co_attn'); 
	  return TRUE;
	}
	
	function check_attn($wid)
	{
	  $this->db->where('working_id', $wid);
	  $this->db->where('date', date("Y-m-d"));
	  $this->db->where('check_in_time', '00:00:00');
	  if ($this->db->get('co_attn')->result()) {
	    return FALSE; // not checked in
	  } else {
	    return TRUE;
	  }
	}

	function change_password()
	{
		$this->db->where('working_id', $this->input->post('login_id'));
		$this->db->where('password', sha1($this->input->post('password_old')));

		$data = array(
			'password' => sha1($this->input->post('password_new'))
		);
 
		$this->db->update('co_staffs', $data);

		if($this->db->affected_rows() == 1)
		{
			return true;
		}

	}
	
 
}
