<?php

class Vendor_model extends CI_Model {

  var $vendor;
 
  function Vendor_model() {
    parent::__construct();
    $this->vendor = new stdClass;
    $this->vendor->id = '';
    $this->vendor->name 
      = $this->vendor->address 
      = $this->vendor->city 
      = $this->vendor->ship_name 
      = $this->vendor->ship_address 
      = $this->vendor->ship_city
      = $this->vendor->pricing
      = $this->vendor->discount 
      = $this->vendor->phone 
      = $this->vendor->cellphone 
      = $this->vendor->fax 
      = $this->vendor->email 
      = $this->vendor->url
      = $this->vendor->contact = '';
  }


  function load_names($term)
  {
    $this->db->select('name');
    
    $this->db->like('name', $term);
    $result = $this->db->get('co_vendors')->result();
		
    $array = array();
    foreach($result as $row) array_push($array, $row->name);
 
    return $array;
    
  }

  function load_vendor($id)
  {
    $this->db->where('id', $id);
    if ($result = $this->db->get('co_vendors')->result()) {
      $vendor =  $result[0];
    } else {
      $vendor = $this->vendor;
    }	
    return $vendor;
  }

  function load_by_name($name)
  {
    $this->db->where('name', $name);
    if ($result = $this->db->get('co_vendors')->result()) {
      $vendor = $result[0];
    } else { 
      $vendor = $this->vendor;
    }
    return $vendor;
    
  }

  function vendor_list($cust_name = "", 
			 $filter = "", $search = "", 
			 $orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->_vendor_list_sql( $cust_name, $search, $filter, $orderby);
    
    $list->total = $this->db->get('co_vendors')->num_rows();
    
    $this->_vendor_list_sql($cust_name, $search, $filter, $orderby);
    
    $list->query = $this->db->get('co_vendors', $per_page, $offset);
    //   echo  $this->db->last_query();
    
    return $list;


  }

  function _vendor_list_sql($cust_name = "", 
			      $search = "", $filter = "", $orderby = ""){
    
    if ($cust_name && $cust_name != 'Vendor..')
      {
	$this->db->where('name', $cust_name);  
      }
   
    if ($orderby)
      {      
	$this->db->order_by($orderby['order'], $orderby['sort']);

      }
        
    if ($search && $search != 'Search..')
      {   
	$search = $this->db->escape_like_str($search);
	$this->db->where("(`id` LIKE '%$search%' OR `name` LIKE '%$search%' OR `address` LIKE '%$search%' OR `city` LIKE '%$search%' OR `ship_name` LIKE '%$search%' OR `ship_address` LIKE '%$search%' OR `ship_city` LIKE '%$search%' OR `pricing` LIKE '%$search%' OR `discount` LIKE '%$search%' OR `phone` LIKE '%$search%' OR `cellphone` LIKE '%$search%' OR `fax` LIKE '%$search%' OR `email` LIKE '%$search%' OR `url` LIKE '%$search%' OR `contact` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 
  function vendor_update($vendor)
  {
      
      $this->db->where('id', $vendor->id);
      $this->db->update('co_vendors', $vendor);
   
  }


  function vendor_add($vendor)
  {
    $vendor->id = NULL;
    
    $this->db->insert('co_vendors', $vendor);
    
  }
  

  function vendor_del($id, $receiver = '')
  {
     
      $this->db->where('id', $id);
      $this->db->delete('co_vendors'); 
      

  }


  }
