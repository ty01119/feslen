<?php

class Doc_model extends CI_Model {


  var $db_name;
 
  function Doc_model() {
    parent::__construct();

    $this->db_name = 'co_doc';
  }


  function load_names($term)
  {
    $this->db->select('name');
    
    $this->db->like('name', $term);
    $result = $this->db->get('co_doc')->result();
		
    $array = array();
    foreach($result as $row) array_push($array, $row->name);
 
    return $array;
    
  }

  function load_doc($id)
  {
    $this->db->where('id', $id);
    if ($result = $this->db->get('co_doc')->result()) {
      $doc =  $result[0];
    } else {
      $doc = $this->doc;
    }	
    return $doc;
  }

  function load_by_name($name)
  {
    $this->db->where('name', $name);
    if ($result = $this->db->get('co_doc')->result()) {
      $doc = $result[0];
    } else { 
      $doc = $this->doc;
    }
    return $doc;
    
  }
  
  
  function load_by_id($id)
  {
    $this->db->where('id', $id);
    $query = $this->db->get($this->db_name);
    
    if ($query->num_rows() > 0)
      {
	$result = $query->result();
	return $result[0];
      }
    else
      {
	return false;
      }
  }



  function doc_list($cust_name = "", 
			 $filter = "", $search = "", 
			 $orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->_doc_list_sql( $cust_name, $search, $filter, $orderby);
    
    $list->total = $this->db->get('co_doc')->num_rows();
    
    $this->_doc_list_sql($cust_name, $search, $filter, $orderby);
    
    $list->query = $this->db->get('co_doc', $per_page, $offset);
    //   echo  $this->db->last_query();
    
    return $list;


  }

  function _doc_list_sql($cust_name = "", 
			      $search = "", $filter = "", $orderby = ""){
        $this->db->where('deleted', 0);
    if ($cust_name && $cust_name != 'Name..')
      {
	$this->db->where('name', $cust_name);  
      }
   
    if ($orderby)
      {      
	$this->db->order_by($orderby['order'], $orderby['sort']);

      }
        
    if ($search && $search != 'Search..')
      {   
	$search = $this->db->escape_like_str($search);
	$this->db->where("(`id` LIKE '%$search%' OR `name` LIKE '%$search%' OR `dept` LIKE '%$search%' OR `order` LIKE '%$search%' OR `cnfd` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 
  function doc_update($doc)
  {
      
      $this->db->where('id', $doc->id);
      $this->db->update('co_doc', $doc);
   
  }


  function doc_add($doc)
  {
    $doc->id = NULL;
    
    $this->db->insert('co_doc', $doc);
    
  }
  
/*
  function doc_del($id, $receiver = '')
  {
     
      $this->db->where('id', $id);
      $this->db->delete('co_doc'); 
      

  }
*/

  function del($id){

    
    $doc = $this->load_by_id($id);  

    $doc->deleted = 1;

    $this->db->where('id', $doc->id);

    $this->db->update('co_doc', $doc);
  
  }


}
