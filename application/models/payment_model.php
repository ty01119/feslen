<?php

class Payment_model extends CI_Model {

  var $branch;

  function __construct() {
    parent::__construct();
    $this->branch = $this->session->userdata('branch');
    $this->db_name = $this->branch.'_payments';
  }


  function payment_list($date = "", $cust_name = "", 
			$filter = "", $search = "", 
			$orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->_payment_list_sql($date, $cust_name, $search, $filter, $orderby);
    
    $list->total = $this->db->get($this->db_name)->num_rows();

    $this->_payment_list_sql($date, $cust_name, $search, $filter, $orderby);
   
    $list->query = $this->db->get($this->db_name, $per_page, $offset);
    //   echo  $this->db->last_query();
    
    return $list;


  }
	

  function _payment_list_sql($date = "", $cust_name = "", 
			     $search = "", $filter = "", $orderby = ""){
    // $this->db->where('deleted', 0);
   
    if ($date)
      {
	$this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	$this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
      }

    if ($cust_name && $cust_name != 'Customer..')
      {
	$this->db->where('cust_name', $cust_name);  
      }
    
    if ($orderby)
      {      
	if (strstr($orderby['order'], '-sp-')) 
	  {
	    $this->db->order_by(str_replace('-sp-', ' ', $orderby['order']), 
				$orderby['sort']); 
	  }
	else
	  {
	    $this->db->order_by($orderby['order'], $orderby['sort']);
	  }
      }
    
    if ($filter)
      {   

	foreach ($filter as $flr_num => $keys):
	  $this->db->where('( 1=', '1', false);
	  
	  $first = TRUE;
	  foreach ($keys as $key => $value):
	  if ($key == 'sql') 
	    {	 	      
	      $data = str_replace('-eq-', ' = ',
				  str_replace('-ne-', ' <> ', $value));
	    }
	  else 
	    {  
	      $data = $key.' = "'.$value.'"';
	    }
	  if ($first)
	    {
	      $this->db->where($data); 
	      $first = FALSE;
	    }
	  else 
	    {
	      $this->db->or_where($data);
	    }
	  endforeach;

	  $this->db->where('1', '1 )', false);
	  endforeach;
      }
    
    if ($search && $search != 'Search..')
      {   
	$search = $this->db->escape_like_str($search);
	$this->db->where("(`id` LIKE '%$search%' OR `date` LIKE '%$search%' OR `detail` LIKE '%$search%' OR `paid` LIKE '%$search%' OR `payment_meth` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 


  function load_by_id($id)
  {
    $this->db->where('id', $id);
    $query = $this->db->get($this->db_name);
    
    if ($query->num_rows() > 0)
      {
	$result = $query->result();
	return $result[0];
      }
    else
      {
	return false;
      }
  }
  
  

  function update($payment)
  {
    $this->db->where('id', $payment->id);
   
    $this->db->update($this->db_name, $payment);
    
  }


  function add($payment)
  {
    $payment->id = NULL;
       
    $this->db->insert($this->db_name, $payment);
    
  }

  

 

  }
