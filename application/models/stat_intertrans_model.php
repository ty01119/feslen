<?php

class Stat_intertrans_model extends CI_Model {

  function __construct() {
    parent::__construct();
  }

  /*
  function get_list($date = "", $cust_name = "", 
			$filter = "", $search = "", 
			$orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->_get_list_sql($date, $cust_name, $search, $filter, $orderby);
    
    $list->total = $this->db->get($this->branch.'_inter_trans')->num_rows();

    $this->_get_list_sql($date, $cust_name, $search, $filter, $orderby);
   
    $list->query = $this->db->get($this->branch.'_inter_trans', $per_page, $offset);
    //   echo  $this->db->last_query();
    
    return $list;


  }
	
  */

  function _get_list_sql($date = "", $cust_name = "", 
			     $search = "", $filter = "", $orderby = ""){
    $this->db->where('deleted', 0);
   
    if ($date)
      {
	$this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	$this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
      }

    if ($cust_name && $cust_name != 'Customer..')
      {
	$this->db->where('cust_name', $cust_name);  
      }
    
    if ($orderby)
      {      
	if (strstr($orderby['order'], '-sp-')) 
	  {
	    $this->db->order_by(str_replace('-sp-', ' ', $orderby['order']), 
				$orderby['sort']); 
	  }
	else
	  {
	    $this->db->order_by($orderby['order'], $orderby['sort']);
	  }
      }
    
    if ($filter)
      {   

	foreach ($filter as $flr_num => $keys):
	  $this->db->where('( 1=', '1', false);
	  
	  $first = TRUE;
	  foreach ($keys as $key => $value):
	  if ($key == 'sql') 
	    {	 	      
	      $data = str_replace('-eq-', ' = ',
				  str_replace('-ne-', ' <> ', $value));
	    }
	  else 
	    {  
	      $data = $key.' = "'.$value.'"';
	    }
	  if ($first)
	    {
	      $this->db->where($data); 
	      $first = FALSE;
	    }
	  else 
	    {
	      $this->db->or_where($data);
	    }
	  endforeach;

	  $this->db->where('1', '1 )', false);
	  endforeach;
      }
    
    if ($search && $search != 'Search..')
      {   
	$search = $this->db->escape_like_str($search);
	$this->db->where("(`id` LIKE '%$search%' OR `date` LIKE '%$search%' OR `subtotal` LIKE '%$search%' OR `tax` LIKE '%$search%' OR `total` LIKE '%$search%' OR `discount` LIKE '%$search%' OR `paid` LIKE '%$search%' OR `cust_name` LIKE '%$search%' OR `contents` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 



  
  function branch_for_branch($branch, $cust_name, $date = "", $filter = "", $search = "")
  {   
    $this->_get_list_sql($date, $cust_name, $search, $filter);
    $this->db->select_sum('total'); 
    $this->db->select_sum('subtotal');
    $this->db->select_sum('tax');
    $result = $this->db->get($branch.'_inter_trans')->result();
    $data = $result[0];
        
    //   echo  $this->db->last_query();
    
    return $data;    
  }

  function load_table ($date)
  {
    $query = $this->db->get('co_branches');
    $branches = $query->result();
    $table = array();

    foreach ($branches as $row)
      {
	$branch = $row->branch;
	$table2 = array();

	foreach ($branches as $row)
	  {
	    $branch_name = $row->branch_name;
	    $data = $this->branch_for_branch($branch, $branch_name, $date);
	    $table2[$row->branch] =  $data->total ? $data->total : 0;
	  }
	$table[$branch] = $table2;
      }

    foreach ($branches as $row)
      {    	
	$branch1 = $row->branch;	 
	$sum = 0;
          foreach ($branches as $row)
     	 {    	
 	  $sum += $table[$row->branch][$branch1];
 
     	 }
	$table['total'][$branch1] = $sum;      
      }
      
    //  print_r($table); die;
    return $table;
  }

}