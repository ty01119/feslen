<?php

class Branch_model extends CI_Model {

 

  function Branch_model() {
    parent::__construct();
  }


  function load_by_name($name)
  {
    $this->db->where('branch', $name);
    $result = $this->db->get('co_branches')->result();
    $data =  $result[0];
    return $data;
    
  }

  
  function load_names($term)
  {
    $this->db->select('branch');
    $this->db->like('branch', $term);
    $result = $this->db->get('co_branches')->result();
    $array = array();
    foreach($result as $row) array_push($array, $row->branch);
    return $array;
    
  }
  
}
