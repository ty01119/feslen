<?php

class Attendance_model extends CI_Model {

 

  function __construct() {
    parent::__construct();
 
  }


  function attendance_list($date = "", $cust_name = "", 
			   $filter = "", $search = "", 
			   $orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->_attendance_list_sql($date, $cust_name, $search, $filter, $orderby);
    
    $list->total = $this->db->get('co_attn')->num_rows();

    $this->_attendance_list_sql($date, $cust_name, $search, $filter, $orderby);
   
    $list->query = $this->db->get('co_attn', $per_page, $offset);
    //   echo  $this->db->last_query();
    
    return $list;


  }
	

  function _attendance_list_sql($date = "", $cust_name = "", 
				$search = "", $filter = "", $orderby = ""){
 
   
    if ($date)
      {
	$this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	$this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
      }

    if ($cust_name && $cust_name != 'Name..')
      {
	$this->db->where('name', $cust_name);  
      }
    
    if ($orderby)
      {      
	if (strstr($orderby['order'], '-sp-')) 
	  {
	    $this->db->order_by(str_replace('-sp-', ' ', $orderby['order']), 
				$orderby['sort']); 
	  }
	else
	  {
	    $this->db->order_by($orderby['order'], $orderby['sort']);
	  }
      }
    
    if ($filter)
      {   

	foreach ($filter as $flr_num => $keys):
	  $this->db->where('( 1=', '1', false);
	  
	  $first = TRUE;
	  foreach ($keys as $key => $value):
	  if ($key == 'sql') 
	    {	 	      
	      $data = str_replace('-eq-', ' = ',
				  str_replace('-ne-', ' <> ', $value));
	    }
	  else 
	    {  
	      $data = $key.' = "'.$value.'"';
	    }
	  if ($first)
	    {
	      $this->db->where($data); 
	      $first = FALSE;
	    }
	  else 
	    {
	      $this->db->or_where($data);
	    }
	  endforeach;

	  $this->db->where('1', '1 )', false);
	  endforeach;
      }
    
    if ($search && $search != 'Search..')
      {   
	$search = $this->db->escape_like_str($search);
	$this->db->where("(`id` LIKE '%$search%' OR `working_id` LIKE '%$search%' OR `name` LIKE '%$search%' OR `check_in_time` LIKE '%$search%' OR `check_in_ip` LIKE '%$search%' OR `check_out_time` LIKE '%$search%' OR `check_out_ip` LIKE '%$search%')", NULL, FALSE);  
      }
  }
  
  function update($id){
    
    $this->db->where('id', $id);
    
    $cols = array('working_id', 'name', 'check_in_time', 'check_in_ip', 'check_out_time', 'check_out_ip');
    
    foreach ($cols as $col) $data[$col] = $this->input->post($col);
    $this->db->update('co_attn', $data);
  }
  	
  function add(){

    $data = array(
		  'id' => NULL,
		  'date' => date("Y-m-d"),
		  'working_id' => '',
		  'name' => '',
		  'check_in_time' => date("H:i:s"),
		  'check_out_time' => date("H:i:s"),
		  'check_in_ip' => $this->input->ip_address(),
		  'check_out_ip' => $this->input->ip_address()
		  );
    
    $this->db->insert('co_attn', $data); 
  }
 

 

}
