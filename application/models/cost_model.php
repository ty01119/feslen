<?php

class Cost_model extends CI_Model {
  
  
  function __construct() {
    parent::__construct();
  }


  function getlist($date = "", $branch = "", 
		       $filter = "", $search = "", 
		       $orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->_list_sql($date, $branch, $search, $filter, $orderby);
    
    $list->total = $this->db->get('co_mgmt_cost')->num_rows();

    $this->_list_sql($date, $branch, $search, $filter, $orderby);
   
    $list->query = $this->db->get('co_mgmt_cost', $per_page, $offset);
    //   echo  $this->db->last_query();
    
    return $list;


  }
	

  function _list_sql($date = "", $branch = "", 
			    $search = "", $filter = "", $orderby = ""){
 
    if ($date)
      {
	$this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	$this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
      }

    if ($branch && $branch != 'Branch..')
      {
	$this->db->where('branch', $branch);  
      }
    
    if ($orderby)
      {      
	if (strstr($orderby['order'], '-sp-')) 
	  {
	    $this->db->order_by(str_replace('-sp-', ' ', $orderby['order']), 
				$orderby['sort']); 
	  }
	else
	  {
	    $this->db->order_by($orderby['order'], $orderby['sort']);
	  }
      }
    
    if ($filter)
      {   

	foreach ($filter as $flr_num => $keys):
	$this->db->where('( 1=', '1', false);
	  
	$first = TRUE;
	foreach ($keys as $key => $value):
	if ($key == 'sql') 
	  {	 	      
	    $data = str_replace('-eq-', ' = ',
				str_replace('-ne-', ' <> ', $value));
	  }
	else 
	  {  
	    $data = $key.' = "'.$value.'"';
	  }
	if ($first)
	  {
	    $this->db->where($data); 
	    $first = FALSE;
	  }
	else 
	  {
	    $this->db->or_where($data);
	  }
	endforeach;

	$this->db->where('1', '1 )', false);
	endforeach;
      }
    
    if ($search && $search != 'Search..')
      {   
	$search = $this->db->escape_like_str($search);
	$this->db->where("(`id` LIKE '%$search%' OR `branch` LIKE '%$search%' OR `date` LIKE '%$search%' OR `category` LIKE '%$search%' OR `notes` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 


  function load($id)
  {
    $this->db->where('id', $id);
    $result = $this->db->get('co_mgmt_cost')->result();
    $domain =  $result[0];
    return $domain;
		
  }


  function update($data, $contents)
  {
    $this->db->where('id', $data->id);
    
    $data->contents = json_encode($contents);
    $this->db->update('co_mgmt_cost', $data);
   
  }


  function add($data, $contents)
  {
    $data->id = NULL;
    $data->contents = json_encode($contents);
    $this->db->insert('co_mgmt_cost', $data);
    
  }

 
  function del($id){
 
    $this->db->where('id', $id);
    $result = $this->db->get('co_mgmt_cost')->result();
    $data =  $result[0];

    $data->deleted = 1;

    $this->db->where('id', $data->id);

    $this->db->update('co_mgmt_cost', $data);
  }
  
  
  function get_sum($branch = "", $date = "", 
		       $filter = "", $search = "", 
		       $orderby = "")
  {
 
    $this->_list_sql($date, $branch, $search, $filter, $orderby);
     $this->db->where('deleted', 0);
     $this->db->select('category');
     $this->db->select('branch');

     $this->db->select_sum('total');
    $this->db->group_by(array("category", "branch"));
    $result = $this->db->get('co_mgmt_cost')->result();
    
    
     //echo  $this->db->last_query();die;
    
    return $result;
  }
	

  function load_table ($date)
  { 
 
    $query = $this->db->get('co_branches');
    $branches = $query->result();
    $table = array();

    foreach ($branches as $row)
      {
	$branch = $row->branch;
	$table2 = array();
	$sums = $this->get_sum($branch, $date);
	$sum = 0;	

	foreach ($sums as $row)
	  {
	    array_push($table2, array($row->category, $row->total));
	    $sum += $row->total;
	  }
	array_unshift($table2, array($branch, $sum));
	$table[$branch] = $table2;
      }

      
    return $table;
  }

 

  }
