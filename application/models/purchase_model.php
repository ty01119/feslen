<?php

class Purchase_model extends CI_Model {

  var $branch;

  function __construct() {
    parent::__construct();
    $this->branch = $this->session->userdata('branch');
  }


  function purchase_list($date = "", $cust_name = "", 
			 $filter = "", $search = "", 
			 $orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->_purchase_list_sql($date, $cust_name, $search, $filter, $orderby);
    
    $list->total = $this->db->get('co_purchase')->num_rows();

    $this->_purchase_list_sql($date, $cust_name, $search, $filter, $orderby);
   
    $list->query = $this->db->get('co_purchase', $per_page, $offset);
    //   echo  $this->db->last_query();
    
    return $list;


  }
	

  function _purchase_list_sql($date = "", $cust_name = "", 
			      $search = "", $filter = "", $orderby = ""){
    $this->db->where('deleted', 0);
   
    if ($date)
      {
	$this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	$this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
      }

    if ($cust_name && $cust_name != 'Vendor..')
      {
	$this->db->where('cust_name', $cust_name);  
      }
    
    if ($orderby)
      {      
	if (strstr($orderby['order'], '-sp-')) 
	  {
	    $this->db->order_by(str_replace('-sp-', ' ', $orderby['order']), 
				$orderby['sort']); 
	  }
	else
	  {
	    $this->db->order_by($orderby['order'], $orderby['sort']);
	  }
      }
    
    if ($filter)
      {   

	foreach ($filter as $flr_num => $keys):
	  $this->db->where('( 1=', '1', false);
	  
	  $first = TRUE;
	  foreach ($keys as $key => $value):
	  if ($key == 'sql') 
	    {	 	      
	      $data = str_replace('-eq-', ' = ',
				  str_replace('-ne-', ' <> ', $value));
	    }
	  else 
	    {  
	      $data = $key.' = "'.$value.'"';
	    }
	  if ($first)
	    {
	      $this->db->where($data); 
	      $first = FALSE;
	    }
	  else 
	    {
	      $this->db->or_where($data);
	    }
	  endforeach;

	  $this->db->where('1', '1 )', false);
	  endforeach;
      }
    
    if ($search && $search != 'Search..')
      {   
	$search = $this->db->escape_like_str($search);
	$this->db->where("(`id` LIKE '%$search%' OR `date` LIKE '%$search%' OR `subtotal` LIKE '%$search%' OR `tax` LIKE '%$search%' OR `total` LIKE '%$search%' OR `discount` LIKE '%$search%' OR `paid` LIKE '%$search%' OR `cust_name` LIKE '%$search%' OR `branch` LIKE '%$search%' OR `contents` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 


  function load_purchase($id)
  {
    $this->db->where('id', $id);
    $result = $this->db->get('co_purchase')->result();
    $purchase =  $result[0];
    return $purchase;
		
  }

  function combine_item($contents) {
    $tmp = array();
    while ($contents) {
      $curr = array_pop($contents);
      foreach ($contents as $i => $item) {
	if ($curr->product == $item->product) {
	  $curr->quantity += $item->quantity;
	  unset($contents[$i]);
	}
      }
      array_push($tmp, $curr);  
    }
    return $tmp;     
  }
  
  
  function purchase_update($purchase, $contents)
  {
    
    $contents = $this->combine_item($contents);
    
    $prev = $this->load_purchase($purchase->id);
    $prev_cnt = json_decode($prev->contents);
    
    foreach ($contents as $item) {
      $dec = $item->quantity;
//      $prev_p = 0;
//      $prev_qty = 0;
      foreach ($prev_cnt as $prev_i){
	if($item->product == $prev_i->product){
	  $dec = $dec - $prev_i->quantity;
//	  $prev_p = $prev_i->unit_price;
//          $prev_qty = $prev_i->quantity;
	}
      } 
      
      if (substr($item->product,0,6) == 'Stock:'){ 
	$this->db->where('name', substr($item->product,7));
	$query = $this->db->get('co_inventory');
	
	if ($query->num_rows() > 0)
	  {  
	    $result = $query->result();
	    $stock  = $result[0];
	    // change price only if the previous price is 0 and current price is not 0
	    /*
	    if (!$prev_p && $item->unit_price) {
	      $stock->price =  
		(($stock->qty - $prev_qty) * $stock->price + $item->quantity * $item->unit_price * 1.05) 
		/ ($stock->qty + $dec);
	    }
	    */
	    $stock->qty += $dec;
		  
	    $this->db->where('id', $stock->id);
	    $this->db->update('co_inventory', $stock);	   
	  }
      }
    }    


    $this->db->where('id', $purchase->id);
    
    $operator = $this->session->userdata('user_name').' ('.$this->session->userdata('login_id').') ';
    $purchase->logs = br(2).date("d/m/Y H:i:s").' Saved by: '.$operator.$purchase->logs;
    
    $purchase->contents = json_encode($contents);

    $this->db->update('co_purchase', $purchase);
    
  }

  function purchase_add($purchase, $contents)
  {
    $contents = $this->combine_item($contents);
    foreach ($contents as $item) {     

      if (substr($item->product,0,6) == 'Stock:'){ 
	$this->db->where('name', substr($item->product,7));
	$query = $this->db->get('co_inventory');
	if ($query->num_rows() > 0)
	  {  
	    $result = $query->result();
	    $stock  = $result[0];	
	    /*
   	    if ($item->unit_price) {
	      $stock->price =  ($stock->qty * $stock->price + $item->quantity * $item->unit_price * 1.05) / ($stock->qty + $item->quantity);
	    }
	    */
	    $stock->qty += $item->quantity;
	    $this->db->where('id', $stock->id);
	    $this->db->update('co_inventory', $stock);	   
	  }
      } 
    }
    $purchase->id = NULL;
    $operator = $this->session->userdata('user_name').' ('.$this->session->userdata('login_id').') ';
    $purchase->logs = br(2).date("d/m/Y H:i:s").' added by: '.$operator.$purchase->logs;
     
    $purchase->contents = json_encode($contents);
    
    $this->db->insert('co_purchase', $purchase);
    
  }

  


  function purchase_del($id){

    $this->db->where('id', $id);
    $result = $this->db->get('purchase')->result();

    if ($result[0]->image_small) unlink($this->image_path.'/'.$result[0]->image_small);  
    if ($result[0]->image_big) unlink($this->image_path.'/'.$result[0]->image_big);
  
    $this->db->where('id', $id);
    $this->db->delete('purchase');
  }

 

}
