<?php

class Stat_model extends CI_Model {
  
  
  function __construct() {
    parent::__construct();
  }
  
  
  function turnover($date = "", $cust_name = "", $filter = "", $search = "")
  {   
    $turnover = array();
    $query = $this->db->get('co_branches');
    foreach ($query->result() as $row)
      {
	$branch = new stdClass;
	$branch->name = $row->branch;
	$branch->detail = $row;
	$branch->turnover = $this->turnover_by_branch($row->branch,
						      $date,
						      $cust_name,
						      $filter,
						      $search);						      
	
	array_push($turnover, $branch);
      } 
    
    return $turnover;
        
  }

  function turnover_by_biz($biz, $date = "", $cust_name = "", $filter = "", $search = "")
  {   
    $turnover = array();
    
    $this->db->where('biz_name', $biz); 
    $this->db->where('status', 'active'); 
    $query = $this->db->get('co_branches');
    foreach ($query->result() as $row)
      {
	$branch = new stdClass;
	$branch->name = $row->branch;
	$branch->detail = $row;
	$branch->turnover = $this->turnover_by_branch($row->branch,
						      $date,
						      $cust_name,
						      $filter,
						      $search);						      
	
	array_push($turnover, $branch);
      } 
    
    return $turnover;
        
  }
  
  
  function turnover_by_branch($branch, $date = "", $cust_name = "", 
		    $filter = "", $search = "")
  {   
    $this->db->where('outsourced', 0);
    $this->_invoice_list_sql($date, $cust_name, $search, $filter);
    $this->db->select_sum('total'); 
    $this->db->select_sum('subtotal');
    $this->db->select_sum('tax');
    $this->db->select_sum('paid');
    $result = $this->db->get($branch.'_invoices')->result();
    $data = $result[0];
    
    $this->db->where('outsourced', 1);
    $this->_invoice_list_sql($date, $cust_name, $search, $filter);
    $this->db->select_sum('total'); 
    $this->db->select_sum('subtotal');
    $this->db->select_sum('tax');
    $this->db->select_sum('paid');
    $result = $this->db->get($branch.'_invoices')->result();
    $outsourced = $result[0];
    
    $data->total += $outsourced->total * 0.25;
    $data->subtotal += $outsourced->subtotal * 0.25;
    $data->tax += $outsourced->tax * 0.25;
    $data->paid += $outsourced->paid * 0.25;
    
    //  echo  $this->db->last_query();
   
    return $data;    
  }
  //////////////////////
  function division($date = "", $cust_name = "", $filter = "", $search = "") {

    $date_fr = date("Y-m-d", $date['fr']).' 00:00:00';
    $date_to = date("Y-m-d", $date['to']).' 23:59:50'; 

    $branchs = array('epsom', 'city', 'penrose', 'websolution', 'albany', 'manukau', 'energysigns');
 
    $branch_table = array();
    
    foreach ($branchs as $branch) { 

      $sql = 'SELECT *  FROM `'.$branch.'_invoices` WHERE date >= "'.$date_fr.'" AND date <= "'.$date_to.'" AND `deleted` = 0 ORDER BY `id`';

      $query = $this->db->query($sql);
      $first = true;
      $table = array();
     
      $total = new stdClass;
      $total->dp = $total->bc = $total->lf = $total->fi = $total->de = $total->se = $total->si 
      = $total->wh = $total->wp = $total->wds  = $total->wdv = $total->wc = $total->wa = $total->cu = $total->ms = $total->en = 0;
      
      $sql3 = 'SELECT sum(`total`) AS total  FROM `'.$branch.'_invoices` WHERE date >= "'.$date_fr.'" AND date <= "'.$date_to.'" AND `deleted` = 0 ORDER BY `id`';
      $array = $this->db->query($sql3)->result();
      $total->total = $array[0]->total;


      foreach ($query->result() as $row)
	{
	  $id = $row->id;
	  $sql2 = 'SELECT * FROM `'.$branch.'_invo_cons` WHERE `invoice_id` = "'.$id.'" ';
	  $query2 = $this->db->query($sql2);
	  foreach ($query2->result() as $row2)
	    { 
	    
	      if ($row2->unit_price < 0 ) continue;	     
	      if (substr($row2->product,0,17) == 'Digital Printing:'){
		$total->dp += ($row2->quantity * $row2->unit_price * 1.15);
	      }
	      if (substr($row2->product,0,15) == 'Business Cards:'){	     
		$total->bc += ($row2->quantity * $row2->unit_price * 1.15);
	      }
	      if (substr($row2->product,0,20) == 'Large Format Poster:'){	     
		$total->lf += ($row2->quantity * $row2->unit_price * 1.15);
	      }
	      if (substr($row2->product,0,9) == 'Finishing'){	     
		$total->fi += ($row2->quantity * $row2->unit_price * 1.15);
	      }
	      if (substr($row2->product,0,6) == 'Design'){	     
		$total->de += ($row2->quantity * $row2->unit_price * 1.15);
	      }
	      if (substr($row2->product,0,7) == 'Service'){	     
		$total->se += ($row2->quantity * $row2->unit_price * 1.15);  
	      }
	      if (substr($row2->product,0,7) == 'Signage'){	     
		$total->si += ($row2->quantity * $row2->unit_price * 1.15);
	      }
	      if (substr($row2->product,0,11) == 'Web Hosting'){	     
		$total->wh += ($row2->quantity * $row2->unit_price * 1.15);
	      }
	      if (substr($row2->product,0,11) == 'Web Package'){
		$total->wp += ($row2->quantity * $row2->unit_price * 1.15);
	      }
	      if (substr($row2->product,0,10) == 'Web Design'){	     
		$total->wds += ($row2->quantity * $row2->unit_price * 1.15);
	      }
	      if (substr($row2->product,0,17) == 'Custom Web Design'){	     
		$total->wds += ($row2->quantity * $row2->unit_price * 1.15);
	      }
	      if (substr($row2->product,0,15) == 'Web Development'){	     
		$total->wdv += ($row2->quantity * $row2->unit_price * 1.15);
	      }
	      if (substr($row2->product,0,22) == 'Custom Web Development'){	     
		$total->wdv += ($row2->quantity * $row2->unit_price * 1.15);
	      }
	      if (substr($row2->product,0,7) == 'Web CMS'){	     
		$total->wc += ($row2->quantity * $row2->unit_price * 1.15);
	      }
	      if (substr($row2->product,0,11) == 'Web Add-ons'){	     
		$total->wa += ($row2->quantity * $row2->unit_price * 1.15);
	      }
	      if (substr($row2->product,0,10) == 'Customised'){	     
		$total->cu += ($row2->quantity * $row2->unit_price * 1.15);
	      }
	      if (substr($row2->product,0,22) == 'Maintenance & Services'){	     
		$total->ms += ($row2->quantity * $row2->unit_price * 1.15);
	      }
	      if (substr($row2->product,0,16) == 'Email Newsletter'){	     
		$total->en += ($row2->quantity * $row2->unit_price * 1.15);
	      }
	    }
	}
  
      $total_aa = $total->dp + $total->bc + $total->lf + $total->fi + $total->de + $total->se + $total->si 
      + $total->wh + $total->wp + $total->wds  + $total->wdv + $total->wc + $total->wa + $total->cu + $total->ms + $total->en;
      
      $info = array('Not Analysed', $total->total - $total_aa);
      if (($total->total - $total_aa) > 0) array_push($table, $info); 
      else array_push($table, array('Not Analysed', 0 , 0));
      
      $info = array('Service', $total->se);
      if ($total->se) array_push($table, $info);  
            
      $info = array('Digital Printing', $total->dp);
      if ($total->dp) array_push($table, $info);  

      $info = array('Business Cards', $total->bc);
      if ($total->bc) array_push($table, $info);  

      $info = array('Large Format Poster', $total->lf);
      if ($total->lf) array_push($table, $info);  

      $info = array('Finishing', $total->fi);
      if ($total->fi) array_push($table, $info);    

      $info = array('Design/Layout', $total->de);
      if ($total->de) array_push($table, $info);       
      
      $info = array('Web Hosting', $total->wh);
      if ($total->wh) array_push($table, $info);         
      
      $info = array('Web Package', $total->wp);
      if ($total->wp) array_push($table, $info);        
      
      $info = array('Web Design', $total->wds);
      if ($total->wds) array_push($table, $info);          
      
      $info = array('Web Development', $total->wdv);
      if ($total->wdv) array_push($table, $info);          
      
      $info = array('Web CMS', $total->wc);
      if ($total->wc) array_push($table, $info);          
      
      $info = array('Web Add-ons', $total->wa);
      if ($total->wa) array_push($table, $info);          
      
      $info = array('Customised Development', $total->cu);
      if ($total->cu) array_push($table, $info);          
      
      $info = array('Maintenance & Services', $total->ms);
      if ($total->ms) array_push($table, $info);          
      
      $info = array('Email Newsletter', $total->en);
      if ($total->en) array_push($table, $info);          
        
      $branch_table[$branch] = $table;
        
    }
    
  return $branch_table;
  }
  /////////////////////////
  //////////////////////
  function turnover_clients($date = "", $branch = 'epsom') {

    $date_fr = date("Y-m-d", $date['fr']).' 00:00:00';
    $date_to = date("Y-m-d", $date['to']).' 23:59:50'; 

    //$branchs = array('epsom', 'city', 'newmarket', 'albany', 'onehunga');
 
  //  $branch_table = array();        
  //  foreach ($branchs as $branch) { 
    
    $sql = 'SELECT distinct `cust_name` FROM `'.$branch.'_invoices`
WHERE date >= "'.$date_fr.'" AND date <= "'.$date_to.'" AND `deleted` = 0';
    $query = $this->db->query($sql);
    $first = true;
    $table = array();
   
    foreach ($query->result() as $row)
      {

	$name = $row->cust_name;
	$row = array($name); 

	$sql2 = 'SELECT sum( `total` ) AS total, count( `cust_name` ) AS times
FROM `'.$branch.'_invoices`
WHERE `cust_name` = '.$this->db->escape($name).' and date >= "'.$date_fr.'" AND date <= "'.$date_to.'" AND `deleted` = 0 
GROUP BY cust_name';
 
	$query2 = $this->db->query($sql2);
 
	if ($valc = $query2->result()) 
	  {
	    array_push($row, $valc[0]->total, $valc[0]->times);	

	  }
	else
	{
		array_push($row, 0, 0);	
	}

	    array_push($table, $row);
      }

    // fillter 1
    $tmp = array();
    $cash = array('Cash Sales', 0, 0);
    foreach ($table as $item)
      {
	$this->db->where('name', $item[0]);
	$query = $this->db->get('co_customers');    
	if ($query->num_rows > 0) 
	  {
	    if ($item[0] == 'Cash Sales' 
		|| $item[0] == 'Daily Cash Sales' 
		|| $item[0] == 'Daily Eftpos Sales'
		|| $item[0] == 'Epsom Branch--Cash'
		|| $item[0] == 'Epsom Branch--Eftpos and cheque'
		|| $item[0] == 'city branch'
		)
	      {
		$cash[1] += $item[1];
		$cash[2] += $item[2];
	      }
	    else 
	      {		
		$item[0] = url_title($item[0]);
		array_push($tmp, $item);	    
	      }
	  }
	else 
	  {
	    $cash[1] += $item[1];
	    $cash[2] += $item[2];
	  }
      }
    array_push($tmp, $cash);	
    $table = $tmp;
    // end filter 1

    $data['query'] = $table;
 
    $table2 = array();

    foreach ($table as $item)
      {
	$i = array();
	$i['name'] = $item[0];
	$i['total'] = $item[1];
	$i['times'] = $item[2];
	array_push($table2, $i);
      }
    $total = array();
    foreach ($table2 as $key => $row) {
      $total[$key] = $row['total'];
    }
    
    array_multisort( $total, SORT_DESC, $table2);
  
    $branch_table[$branch] = $table2;
    
    //    }  
  //return $branch_table;
  
  return $table2;
  }
  /////////////////////////
  
  function _invoice_list_sql($date = "", $cust_name = "", 
			     $search = "", $filter = ""){
    
    $this->db->where('deleted', 0);
    
    if ($date)
      {
	$this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	$this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
      }

    if ($cust_name && $cust_name != 'Customer..')
      {
	$this->db->where('cust_name', $cust_name);  
      }
    
    if ($filter)
      {   

	foreach ($filter as $flr_num => $keys):
	  $this->db->where('( 1=', '1', false);
	  
	  $first = TRUE;
	  foreach ($keys as $key => $value):
	  if ($key == 'sql') 
	    {	 	      
	      $data = str_replace('-eq-', ' = ',
				  str_replace('-ne-', ' <> ', $value));
	    }
	  else 
	    {  
	      $data = $key.' = "'.$value.'"';
	    }
	  if ($first)
	    {
	      $this->db->where($data); 
	      $first = FALSE;
	    }
	  else 
	    {
	      $this->db->or_where($data);
	    }
	  endforeach;

	  $this->db->where('1', '1 )', false);
	  endforeach;
      }
    
    if ($search && $search != 'Search..')
      {   
	$search = $this->db->escape_like_str($search);
	$this->db->where("(`id` LIKE '%$search%' OR `date` LIKE '%$search%' OR `subtotal` LIKE '%$search%' OR `tax` LIKE '%$search%' OR `total` LIKE '%$search%' OR `discount` LIKE '%$search%' OR `paid` LIKE '%$search%' OR `cust_name` LIKE '%$search%' OR `quote_id` LIKE '%$search%' OR `logs` LIKE '%$search%' OR `info` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 

  }