<?php

class Mgmt_task_signs_model extends CI_Model {

  var $db_name;

  function __construct() {
    parent::__construct();
    $this->db_name = 'co_tasks_signs';  
  }


  function get_list($date = "", $staff_pre_press = "", $staff_production = "", $task_branch = "",
			$filter = "", $search = "", 
			$orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->_list_sql($date, $staff_pre_press, $staff_production, $task_branch, $search, $filter, $orderby);
    
    $list->total = $this->db->get($this->db_name)->num_rows();

    $this->_list_sql($date, $staff_pre_press, $staff_production, $task_branch, $search, $filter, $orderby);
   
    $list->query = $this->db->get($this->db_name, $per_page, $offset);
 // echo $this->db->last_query();die;
    return $list;


  }
	

  function _list_sql($date = "", $staff_pre_press = "", $staff_production = "", $task_branch = "",
			     $search = "", $filter = "", $orderby = ""){
    //  $this->db->where('deleted', 0);
   
    if ($date)
      {
	$this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	$this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
      }

    if ($staff_pre_press && $staff_pre_press != 'Pre-press Staff...')
      {
	$this->db->where('staff_pre_press', $staff_pre_press);  
      }

    if ($staff_production && $staff_production != 'Production Staff...')
      {
	$this->db->where('staff_production', $staff_production);  
      }

    if ($task_branch && $task_branch != 'Branch...')
      {
	$this->db->where('branch', $task_branch);  
      }
    
    if ($orderby)
      {      
	if (strstr($orderby['order'], '-sp-')) 
	  {
	    $this->db->order_by(str_replace('-sp-', ' ', $orderby['order']), 
				$orderby['sort']); 
	  }
	else
	  {
	    $this->db->order_by($orderby['order'], $orderby['sort']);
	  }
      }
    
    if ($filter)
      {   

	foreach ($filter as $flr_num => $keys):
	  $this->db->where('( 1=', '1', false);
	  
	  $first = TRUE;
	  foreach ($keys as $key => $value):
	  if ($key == 'sql') 
	    {	 	      
	      $data = str_replace('-eq-', ' = ',
				  str_replace('-ne-', ' <> ', $value));
	    }
	  else 
	    {  
	      $data = $key.' = "'.$value.'"';
	    }
	  if ($first)
	    {
	      $this->db->where($data); 
	      $first = FALSE;
	    }
	  else 
	    {
	      $this->db->or_where($data);
	    }
	  endforeach;

	  $this->db->where('1', '1 )', false);
	  endforeach;
      }
    
    if ($search && $search != 'Search..')
      {   
	$search = $this->db->escape_like_str($search);
	$this->db->where("(`id` LIKE '%$search%' OR `invo_id` LIKE '%$search%' OR `branch` LIKE '%$search%' OR `date` LIKE '%$search%' OR `date_due` LIKE '%$search%' OR `cust_name` LIKE '%$search%' OR `staff_pre_press` LIKE '%$search%' OR `staff_production` LIKE '%$search%' OR `file_name` LIKE '%$search%' OR `status` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 



  function load_by_id($id)
  {
    $this->db->where('id', $id);
    $query = $this->db->get($this->db_name);
    
    if ($query->num_rows() > 0)
      {
	$result = $query->result();
	return $result[0];
      }
    else
      {
	return false;
      }
  }

  function update($input)
  {
    $this->db->where('id', $input->id);
    $data->invo_id = $input->invo_id;
    $data->branch = $input->branch;
    $data->date = $input->date;
    $data->date_due = $input->date_due;
    $data->urgent = $input->urgent;
    $data->cust_name = $input->cust_name;
    $data->staff_pre_press = $input->staff_pre_press;
    $data->file_name = $input->file_name;
    $data->staff_production = $input->staff_production;
    $data->status = $input->status_btn;
    $data->json = json_encode($input); 

    $this->db->update($this->db_name, $data);
   
  }


  function add($input)
  {
    $data->id = NULL;

    $data->invo_id = $input->invo_id;
    $data->branch = $input->branch;
    $data->date = $input->date;
    $data->date_due = $input->date_due;
    $data->urgent = $input->urgent;
    $data->cust_name = $input->cust_name;
    $data->staff_pre_press = $input->staff_pre_press;
    $data->file_name = $input->file_name;
    $data->staff_production = $input->staff_production;
    $data->status = $input->status_btn;
    $data->json = json_encode($input); 
        
    $this->db->insert($this->db_name, $data);
    
    return ($this->db->affected_rows() == 1) ? TRUE : FALSE;
   
  }
  
  


  function web_app_task_signs_del($id){

    $this->db->where('id', $id);
    $result = $this->db->get($this->db_name)->result();
    $web_app_task_signs =  $result[0];

    $web_app_task_signs->deleted = 1;

    $this->db->where('id', $web_app_task_signs->id);

    $this->db->update($this->db_name, $web_app_task_signs);
  
    return ($this->db->affected_rows() == 1) ? TRUE : FALSE;
  }

 

  }