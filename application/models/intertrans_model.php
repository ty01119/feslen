<?php

class Intertrans_model extends CI_Model {

  var $branch;

  function __construct() {
    parent::__construct();
    $this->branch = $this->session->userdata('branch');
  }


  function intertrans_list($date = "", $cust_name = "", 
			$filter = "", $search = "", 
			$orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->_intertrans_list_sql($date, $cust_name, $search, $filter, $orderby);
    
    $list->total = $this->db->get($this->branch.'_inter_trans')->num_rows();

    $this->_intertrans_list_sql($date, $cust_name, $search, $filter, $orderby);
   
    $list->query = $this->db->get($this->branch.'_inter_trans', $per_page, $offset);
    //   echo  $this->db->last_query();
    
    return $list;


  }
	

  function _intertrans_list_sql($date = "", $cust_name = "", 
			     $search = "", $filter = "", $orderby = ""){
    $this->db->where('deleted', 0);
   
    if ($date)
      {
	$this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	$this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
      }

    if ($cust_name && $cust_name != 'Customer..')
      {
	$this->db->where('cust_name', $cust_name);  
      }
    
    if ($orderby)
      {      
	if (strstr($orderby['order'], '-sp-')) 
	  {
	    $this->db->order_by(str_replace('-sp-', ' ', $orderby['order']), 
				$orderby['sort']); 
	  }
	else
	  {
	    $this->db->order_by($orderby['order'], $orderby['sort']);
	  }
      }
    
    if ($filter)
      {   

	foreach ($filter as $flr_num => $keys):
	  $this->db->where('( 1=', '1', false);
	  
	  $first = TRUE;
	  foreach ($keys as $key => $value):
	  if ($key == 'sql') 
	    {	 	      
	      $data = str_replace('-eq-', ' = ',
				  str_replace('-ne-', ' <> ', $value));
	    }
	  else 
	    {  
	      $data = $key.' = "'.$value.'"';
	    }
	  if ($first)
	    {
	      $this->db->where($data); 
	      $first = FALSE;
	    }
	  else 
	    {
	      $this->db->or_where($data);
	    }
	  endforeach;

	  $this->db->where('1', '1 )', false);
	  endforeach;
      }
    
    if ($search && $search != 'Search..')
      {   
	$search = $this->db->escape_like_str($search);
	$this->db->where("(`id` LIKE '%$search%' OR `date` LIKE '%$search%' OR `subtotal` LIKE '%$search%' OR `tax` LIKE '%$search%' OR `total` LIKE '%$search%' OR `discount` LIKE '%$search%' OR `paid` LIKE '%$search%' OR `cust_name` LIKE '%$search%' OR `contents` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 


  function load_intertrans($id)
  {
    $this->db->where('id', $id);
    $result = $this->db->get($this->branch.'_inter_trans')->result();
    $intertrans =  $result[0];
    return $intertrans;
		
  }

  

  function intertrans_update($intertrans, $contents)
  {
    $this->db->where('id', $intertrans->id);
    $intertrans->contents = json_encode($contents);

    $this->db->update($this->branch.'_inter_trans', $intertrans);
    
  }


  function intertrans_add($intertrans, $contents)
  {
    $intertrans->id = NULL;
    $intertrans->contents = json_encode($contents);
    
    $this->db->insert($this->branch.'_inter_trans', $intertrans);
    
  }

  


  function intertrans_del($id){

    $this->db->where('id', $id);
    $result = $this->db->get('intertrans')->result();

    if ($result[0]->image_small) unlink($this->image_path.'/'.$result[0]->image_small);  
    if ($result[0]->image_big) unlink($this->image_path.'/'.$result[0]->image_big);
  
    $this->db->where('id', $id);
    $this->db->delete('intertrans');
  }

 

  }
