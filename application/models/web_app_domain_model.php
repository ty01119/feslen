<?php

class Web_app_domain_model extends CI_Model {
  
  
  function __construct() {
    parent::__construct();
  }


  function domain_list($date = "", $cust_name = "", 
		       $filter = "", $search = "", 
		       $orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->_domain_list_sql($date, $cust_name, $search, $filter, $orderby);
    
    $list->total = $this->db->get('web_app_domains')->num_rows();

    $this->_domain_list_sql($date, $cust_name, $search, $filter, $orderby);
   
    $list->query = $this->db->get('web_app_domains', $per_page, $offset);
    //   echo  $this->db->last_query();
    
    return $list;


  }
	

  function _domain_list_sql($date = "", $cust_name = "", 
			    $search = "", $filter = "", $orderby = ""){
 
 

    if ($cust_name && $cust_name != 'Customer..')
      {
	$this->db->where('cust_name', $cust_name);  
      }
    
    if ($orderby)
      {      
	if (strstr($orderby['order'], '-sp-')) 
	  {
	    $this->db->order_by(str_replace('-sp-', ' ', $orderby['order']), 
				$orderby['sort']); 
	  }
	else
	  {
	    $this->db->order_by($orderby['order'], $orderby['sort']);
	  }
      }
    
    if ($filter)
      {   

	foreach ($filter as $flr_num => $keys):
	$this->db->where('( 1=', '1', false);
	  
	$first = TRUE;
	foreach ($keys as $key => $value):
	if ($key == 'sql') 
	  {	 	      
	    $data = str_replace('-eq-', ' = ',
				str_replace('-ne-', ' <> ', $value));
	  }
	else 
	  {  
	    $data = $key.' = "'.$value.'"';
	  }
	if ($first)
	  {
	    $this->db->where($data); 
	    $first = FALSE;
	  }
	else 
	  {
	    $this->db->or_where($data);
	  }
	endforeach;

	$this->db->where('1', '1 )', false);
	endforeach;
      }
    
    if ($search && $search != 'Search..')
      {   
	$search = $this->db->escape_like_str($search);
	$this->db->where("(`id` LIKE '%$search%' OR `url` LIKE '%$search%' OR `registrar` LIKE '%$search%' OR `host` LIKE '%$search%' OR `cust_name` LIKE '%$search%' OR `due_date` LIKE '%$search%' OR `passwords` LIKE '%$search%' OR `mgmt_notes` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 


  function load_domain($id)
  {
    $this->db->where('id', $id);
    $result = $this->db->get('web_app_domains')->result();
    $domain =  $result[0];
    return $domain;
		
  }


  function domain_update($domain)
  {
    $this->db->where('id', $domain->id);
    
    $this->db->update('web_app_domains', $domain);
   
  }


  function domain_add($domain)
  {
    $domain->id = NULL;
    
    $this->db->insert('web_app_domains', $domain);
    
  }

  


  function domain_del($id){
 
    $this->db->where('id', $id);
    $result = $this->db->get('web_app_domains')->result();
    $data =  $result[0];

    $data->deleted = 1;

    $this->db->where('id', $data->id);

    $this->db->update('web_app_domains', $data);
  }

 

  }
