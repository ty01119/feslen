<?php

class Bol_model extends CI_Model {

  var $branch;

  function __construct() {
    parent::__construct();
    $this->branch = $this->session->userdata('branch');
  }


  function bol_list($date = "", $cust_name = "", 
		    $filter = "", $search = "", 
		    $orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->_bol_list_sql($date, $cust_name, $search, $filter, $orderby);
    
    $list->total = $this->db->get('co_bol')->num_rows();

    $this->_bol_list_sql($date, $cust_name, $search, $filter, $orderby);
   
    $list->query = $this->db->get('co_bol', $per_page, $offset);
    //   echo  $this->db->last_query();
    
    return $list;


  }

  function load_price(){
    $prices = $this->db->get('co_inventory')->result();

    $array = array();

  foreach($prices as $row):
 
    $array[$row->name] = $row->price;
    endforeach;
    return $array;
  }


  function _bol_list_sql($date = "", $cust_name = "", 
			 $search = "", $filter = "", $orderby = ""){
    $this->db->where('deleted', 0);
   
    if ($date)
      {
	$this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	$this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
      }

    if ($cust_name && $cust_name != 'Customer..')
      {
	$this->db->where('cust_name', $cust_name);  
      }
    
    if ($orderby)
      {      
	if (strstr($orderby['order'], '-sp-')) 
	  {
	    $this->db->order_by(str_replace('-sp-', ' ', $orderby['order']), 
				$orderby['sort']); 
	  }
	else
	  {
	    $this->db->order_by($orderby['order'], $orderby['sort']);
	  }
      }
    
    if ($filter)
      {   

	foreach ($filter as $flr_num => $keys):
	$this->db->where('( 1=', '1', false);
	  
	$first = TRUE;
	foreach ($keys as $key => $value):
	if ($key == 'sql') 
	  {	 	      
	    $data = str_replace('-eq-', ' = ',
				str_replace('-ne-', ' <> ', $value));
	  }
	else 
	  {  
	    $data = $key.' = "'.$value.'"';
	  }
	if ($first)
	  {
	    $this->db->where($data); 
	    $first = FALSE;
	  }
	else 
	  {
	    $this->db->or_where($data);
	  }
	endforeach;

	$this->db->where('1', '1 )', false);
	endforeach;
      }
    
    if ($search && $search != 'Search..')
      {   
	$search = $this->db->escape_like_str($search);
	$this->db->where("(`id` LIKE '%$search%' OR `date` LIKE '%$search%' OR `subtotal` LIKE '%$search%' OR `tax` LIKE '%$search%' OR `total` LIKE '%$search%' OR `discount` LIKE '%$search%' OR `paid` LIKE '%$search%' OR `cust_name` LIKE '%$search%' OR `contents` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 


  function load_bol($id)
  {
    $this->db->where('id', $id);
    $result = $this->db->get('co_bol')->result();
    $bol =  $result[0];
    return $bol;
		
  }

 

  function bol_update($bol, $contents)
  {   
    $contents = $this->combine_item($contents);

    $prev = $this->load_bol($bol->id);
    $prev_cnt = json_decode($prev->contents);
    
    foreach ($contents as $item) {
      $dec = $item->quantity;
      
      foreach ($prev_cnt as $prev_i){
	if($item->product == $prev_i->product){
	  $dec = $dec - $prev_i->quantity;
	}
      }
      $this->db->where('name', $item->product);
      
      $query = $this->db->get('co_inventory');
      
      if ($query->num_rows() > 0)
	{  
	  $result = $query->result();
	  $stock  = $result[0];
	  $stock->qty = $stock->qty - $dec;		  
	  $this->db->where('id', $stock->id);
	  $this->db->update('co_inventory', $stock);	   
	}
    }    
   
    $this->db->where('id', $bol->id);
    $bol->contents = json_encode($contents);
    $this->db->update('co_bol', $bol);
            
  }

   function combine_item($contents) {
     $tmp = array();
     while ($contents) {
       $curr = array_pop($contents);
       foreach ($contents as $i => $item) {
	 if ($curr->product == $item->product) {
	   $curr->quantity += $item->quantity;
	   unset($contents[$i]);
	 }
       }
       array_push($tmp, $curr);  
     }
     return $tmp;     
   }

  function bol_add($bol, $contents)
  {  
    $contents = $this->combine_item($contents);
    foreach ($contents as $item) {      
      $this->db->where('name', $item->product);
      $query = $this->db->get('co_inventory');
      if ($query->num_rows() > 0)
	{  
	  $result = $query->result();
	  $stock  = $result[0];
	  $stock->qty = $stock->qty - $item->quantity;		  
	  $this->db->where('id', $stock->id);
	  $this->db->update('co_inventory', $stock);	   
	}
    } 
      
    $bol->id = NULL;
    $bol->contents = json_encode($contents);
    $this->db->insert('co_bol', $bol);
    
  }

  


  function bol_del($id){

    $this->db->where('id', $id);
    $result = $this->db->get('bol')->result();

    if ($result[0]->image_small) unlink($this->image_path.'/'.$result[0]->image_small);  
    if ($result[0]->image_big) unlink($this->image_path.'/'.$result[0]->image_big);
  
    $this->db->where('id', $id);
    $this->db->delete('bol');
  }

 

  }
