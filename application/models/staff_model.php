<?php

class Staff_model extends CI_Model {

 

  function Staff_model() {
    parent::__construct();
  }


  function load_personnel($id)
  {
    $this->db->where('working_id', $id);
    $result = $this->db->get('co_staffs')->result();
    $invoice =  $result[0];
    return $invoice;
		
  }
  
  function load_names($term)
  {
    

    $this->db->where('status', 'active');
    $this->db->where('full_time', 'yes');		
    $this->db->order_by('name', 'asc');
    
    $this->db->select('name');
    
    $this->db->like('name', $term);
    $result = $this->db->get('co_staffs')->result();
		
    $array = array();
    foreach($result as $row) array_push($array, $row->name);
 
    return $array;
    
  }
  
  function staff_list($filter = "", $search = "", 
			 $orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->_staff_list_sql($search, $filter, $orderby);
    
    $list->total = $this->db->get('co_staffs')->num_rows();
    
    $this->_staff_list_sql($search, $filter, $orderby);
    
    $list->query = $this->db->get('co_staffs', $per_page, $offset);
 
    
    return $list;


  }

  function _staff_list_sql($search = "", $filter = "", $orderby = ""){
    
    if ($orderby)
      {      
	$this->db->order_by($orderby['order'], $orderby['sort']);
	
      }
    if ($filter)
      {   

	foreach ($filter as $flr_num => $keys):
	  $this->db->where('( 1=', '1', false);
	  
	  $first = TRUE;
	  foreach ($keys as $key => $value):
	  if ($key == 'sql') 
	    {	 	      
	      $data = str_replace('-eq-', ' = ',
				  str_replace('-ne-', ' <> ', $value));
	    }
	  else 
	    {  
	      $data = $key.' = "'.$value.'"';
	    }
	  if ($first)
	    {
	      $this->db->where($data); 
	      $first = FALSE;
	    }
	  else 
	    {
	      $this->db->or_where($data);
	    }
	  endforeach;

	  $this->db->where('1', '1 )', false);
	  endforeach;
      }
  }
 
 function personnel_update($person)
  {

    if ($person->id != 1) {
    $this->db->where('id', $person->id);
   
    $this->db->update('co_staffs', $person);
   }
  }


  function personnel_add($person)
  {
    $person->id = NULL;
    
    $this->db->insert('co_staffs', $person);
    
  }
  

function staff_by_branch($branch)
  {
    
    

    $this->db->where('branch', $branch);
    $this->db->where('status', 'active');
    $this->db->where('full_time', 'yes');		
    $this->db->order_by('name', 'asc');
       
 
    
    return  $this->db->get('co_staffs');


  }


}