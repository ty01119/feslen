<?php

class Inventory_model extends CI_Model {

  var $branch;

  function __construct() {
    parent::__construct();
    $this->branch = $this->session->userdata('branch');
  }

  function load_names($term, $prefix = FALSE)
  {
    $this->db->select('name');
 
    $this->db->like('name', $term);
    $result = $this->db->get('co_inventory')->result();
		
    $array = array();
    if ($prefix) {
    foreach($result as $row) array_push($array, 'Stock: '.$row->name);
     }
     else {
    foreach($result as $row) array_push($array, $row->name);
     }

    return $array;
    
  }


  function inventory_list($cate = "", $name = "", 
			$filter = "", $search = "", 
			$orderby = "", $per_page, $offset)
  {
    $cates = $this->_get_cates($cate);
    
    $list = new stdClass;

    $this->_inventory_list_sql($cates, $name, $search, $filter, $orderby);
    
    $list->total = $this->db->get('co_inventory')->num_rows();

    $this->_inventory_list_sql($cates, $name, $search, $filter, $orderby);
   
    $list->query = $this->db->get('co_inventory', $per_page, $offset);
    //   echo  $this->db->last_query();
    
    return $list;


  }
	
  function _get_cates($category){

    $cates = array();    
    $this->db->where('url_title', $category);
    $query = $this->db->get('co_inventory_cate');
    foreach ($query->result() as $category) {
      if ($category->parent == '(none)') 
	{
	  array_push($cates, $category->name);
	  $this->db->where('parent', $category->name); 
	  $query = $this->db->get('co_inventory_cate');
	  if ($query->num_rows) {	 
	    foreach ($query->result() as $row)
	      {
		array_push($cates, $row->name);
	      }
	  }
	}
      else
	{	
	  array_push($cates, $category->name);
	}
    }
    
    return $cates;
  }

  function _inventory_list_sql($cates = "", $name = "", 
			     $search = "", $filter = "", $orderby = ""){
 //   $this->db->where('deleted', 0);
   if ($cates) {
      $this->db->where('( 1=', '1', false);    
      $first = TRUE;
      foreach ($cates as $cate){
	if ($first)
	  {
	    $this->db->where('category', $cate); 
	    $first = FALSE;
	  }
	else 
	  {
	    $this->db->or_where('category', $cate); 
	  }	
      }
      $this->db->where('1', '1 )', false);
    } // if cate 

    if ($name && $name != 'Name..')
      {
	$this->db->where('name', $name);  
      }
    
    if ($orderby)
      {      
	if (strstr($orderby['order'], '-sp-')) 
	  {
	    $this->db->order_by(str_replace('-sp-', ' ', $orderby['order']), 
				$orderby['sort']); 
	  }
	else
	  {
	    $this->db->order_by($orderby['order'], $orderby['sort']);
	  }
      }
    
    if ($filter)
      {   

	foreach ($filter as $flr_num => $keys):
	  $this->db->where('( 1=', '1', false);
	  
	  $first = TRUE;
	  foreach ($keys as $key => $value):
	  if ($key == 'sql') 
	    {	 	      
	      $data = str_replace('-eq-', ' = ',
				  str_replace('-ne-', ' <> ', $value));
	    }
	  else 
	    {  
	      $data = $key.' = "'.$value.'"';
	    }
	  if ($first)
	    {
	      $this->db->where($data); 
	      $first = FALSE;
	    }
	  else 
	    {
	      $this->db->or_where($data);
	    }
	  endforeach;

	  $this->db->where('1', '1 )', false);
	  endforeach;
      }
    
    if ($search && $search != 'Search..')
      {   
	$search = $this->db->escape_like_str($search);
	$this->db->where("(`id` LIKE '%$search%' OR `name` LIKE '%$search%' OR `price` LIKE '%$search%' OR `category` LIKE '%$search%' OR `qty` LIKE '%$search%' OR `branch` LIKE '%$search%' OR `prod_id` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 


  function load_inventory($id)
  {
    $this->db->where('id', $id);
    $result = $this->db->get('co_inventory')->result();
    $inventory =  $result[0];
    return $inventory;
		
  }

  

  function inventory_update($inventory)
  { 
    $this->db->where('id', $inventory->id);
 
    $this->db->update('co_inventory', $inventory);
    
  }


  function inventory_add($inventory)
  { 
    $inventory->id = NULL;
     
    $this->db->insert('co_inventory', $inventory);
    
  }

  


  function del($id){
  
    $this->db->where('id', $id);
    $this->db->delete('co_inventory');
  }

 

  }
