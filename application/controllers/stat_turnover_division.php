<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stat_turnover_division extends CI_Controller 
{

  var $branch;

  function __construct()
  {
    parent::__construct();
    $this->branch = $this->session->userdata('branch');
    $this->load->helper('auth');
    is_logged_in();
  
  }

  function test()
  {
    echo '123';die;
  }
  
  function index()
  {
  
    $this->session->unset_userdata('stat_turnover_division_orderby');
    $this->session->unset_userdata('stat_turnover_division_filter'); 
    
    $this->session->set_userdata('stat_turnover_division_customer', 'Customer..');
    $this->session->set_userdata('stat_turnover_division_search', 'Search..');
    $this->session->set_userdata('stat_turnover_division_date_fr', strtotime('last sunday'));
    $this->session->set_userdata('stat_turnover_division_date_to', strtotime('next sunday'));    
 
    $this->session->keep_flashdata('msg');
    redirect('stat_turnover_division/lib');

  }
     
  
  function lib()
  {
    $data['orderby'] = $this->session->userdata('stat_turnover_division_orderby');
    $data['filter'] = $this->session->userdata('stat_turnover_division_filter'); 
    $data['customer'] = $this->session->userdata('stat_turnover_division_customer');
    $data['search'] = $this->session->userdata('stat_turnover_division_search');
    $data['date']['fr'] = $this->session->userdata('stat_turnover_division_date_fr');
    $data['date']['to'] = $this->session->userdata('stat_turnover_division_date_to');
    $data['per_page'] = $this->session->userdata('stat_turnover_division_per_page');

    $this->load->model('stat_model');
      
    $stat_turnover_division = $this->stat_model->division($data['date'],
						 $data['customer'],
						 $data['filter'], 
						 $data['search']);  

 
 
    $data['division'] = $stat_turnover_division; 
     $this->load->library('table'); 
     
  
 
						 
    $data['title'] = 'Turnover by Division';
   
    $data['main_content'] = 'statistics/stat_turnover_division_view';  
    $data['table_title'] = 'Turnover by Division'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	
   
  }


  function filter() 
  { 
    
    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('stat_turnover_division_per_page') == 25)
	$this->session->set_userdata('stat_turnover_division_per_page', $per_page);
      else 
	$this->session->set_userdata('stat_turnover_division_per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('stat_turnover_division_filter');
	if (isset($filter[$flr_num][$key]) && $filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('stat_turnover_division_filter', $filter);	
      }
    redirect('stat_turnover_division/lib');
  }
  

  function search() 
  {
        check_auth('mgmt_statistics');
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('stat_turnover_division_search', $search);
    }
    if ($customer = $this->input->post('customer')) {      
      $this->session->set_userdata('stat_turnover_division_customer', $customer);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('stat_turnover_division_date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('stat_turnover_division_date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('stat_turnover_division/lib');
  }
  
  

  

}