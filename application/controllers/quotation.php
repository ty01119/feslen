<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quotation extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('info');
    $this->load->helper('auth');   
    keep_logged_in();
    is_logged_in();
  
  }

  function test()
  {

    echo '123';die;
  }
  
  function index()
  {
 
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    
    $this->session->set_userdata('customer', 'Customer..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('quotation/lib');

  }
     

  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['customer'] = $this->session->userdata('customer');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('quotation/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

    $this->load->model('quotation_model');
    
    $quotation_list = $this->quotation_model->quotation_list($data['date'],
						       $data['customer'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       $config['per_page'], 
						       $this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $quotation_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $quotation_list->query;
    
    $data['title'] = '';

    $data['main_content'] = 'quotation_list_view';  
    $data['table_title'] = 'Quotation list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }



  function report()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['customer'] = $this->session->userdata('customer');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = '';

   

    $this->load->model('quotation_model');
    
    $invoice_list = $this->quotation_model->quotation_list($data['date'],
						       $data['customer'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       '', 
						       '');
    
    $data['total'] = $invoice_list->total;
    $data['query'] = $invoice_list->query;
    $data['title'] = 'Quotation Statement';
    $data['table_title'] = 'Transaction list'; 
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('invoice_report_view', $data);	

  }

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('quotation/lib');
  }
  

  function filter() 
  { 

    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if ($filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('quotation/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($customer = $this->input->post('customer')) {      
      $this->session->set_userdata('customer', $customer);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('quotation/lib');
  }
  

  function ajax_quotation($id)
  { 
    $json_obj = new stdClass;

    if ($id == 'new') {
      $quotation = new stdClass;
      $quotation->id = 'new';
      $quotation->date = date("Y-m-d H:i:s");
      $quotation->subtotal 
	= $quotation->tax 
	= $quotation->total 
	= $quotation->discount 
	= $quotation->paid 
	= $quotation->deleted = 0;
      $quotation->cust_name = '';
    
      if ($cart = $this->session->userdata('cart')) {
	$contents = $cart;
      } else {
	$contents=array();
	$contents[0] 
	  = array('product' => '', 'quantity' => 1, 'unit_price' => 0, 'discount' => 0);
      }      

      $quotation->contents = json_encode($contents);

      $json_obj->transaction = $quotation;
      
      echo json_encode($json_obj);
      
    } else {
     
      $this->load->model('quotation_model');
      if ($quotation = $this->quotation_model->load_quotation($id)) {
	
	$json_obj->transaction = $quotation;
	
	echo json_encode($json_obj);

      }      
    }

  }


  function view($id)
  {
    $this->load->model('quotation_model');
    $quotation = $this->quotation_model->load_quotation($id);
    $contents = json_decode($quotation->contents);
    
    $this->load->model('customer_model');
    $customer = $this->customer_model->load_by_name($quotation->cust_name);
    
    
    $this->load->model('branch_model');
    $branch = $this->branch_model->load_by_name($this->session->userdata('branch'));

    $branch = refine_branch_data($branch, $quotation->date);
    $quotation->date = date("d/m/y", strtotime($quotation->date));
    
    
    $data['transaction'] = $quotation;
    $data['contents'] = $contents;
    $data['customer'] = $customer;
    $data['branch'] =  $branch;
    
    $data['terms']= '';
    
    if ($branch->branch != 'energysigns' && $branch->branch != 'websolution')
    {
    	$data['terms'] = '<strong>Conditions: </strong><br />Quotations are valid for 30 days from the date stipulated on the estimate.';
    	$data['terms'] .= '<br />Extra service fee may be charged if the files provided by customers are not in print ready state and require further adjustment in size, color, format, bleeding, etc.';
    }
    else if ($branch->branch == 'energysigns')
    {
    	$data['terms'] = '<strong>DEPOSIT: A 50% deposit is required up front to confirm the quote.</strong>';
    }
    else if ($branch->branch == 'websolution')
    {

    	$data['terms'] = '<strong>Conditions: </strong><br />';
    	$data['terms'] .= '3A web solution will require a 20% deposit ($200 as minimum) before we officially start the website production process.<br />';
        $data['terms'] .= 'The information contained herein is confidential and the property of 3A web solution.<br />';
        $data['terms'] .= 'This is subject to change without notice and quotations are valid for 30 days from issue.<br />';
        $data['terms'] .= 'Revisions may be issued to advise of such changes and/or additions and quote is subject to acceptance by 3A web solution.';     
 
    }
    
    $data['title'] = 'QUOTE';
    $this->load->view('transaction_html_view', $data);

  }

  

  function pdf($id)
  {
    $this->load->model('quotation_model');
    $quotation = $this->quotation_model->load_quotation($id);
    $contents = json_decode($quotation->contents);

    
    $this->load->model('customer_model');
    $customer = $this->customer_model->load_by_name($quotation->cust_name);
    
    $this->load->model('branch_model');
    $branch = $this->branch_model->load_by_name($this->session->userdata('branch'));

    $branch = refine_branch_data($branch, $quotation->date);
    $quotation->date = date("d/m/y", strtotime($quotation->date));
    
    
    $data['transaction'] = $quotation;
    $data['contents'] = $contents;
    $data['customer'] = $customer;
    $data['branch'] =  $branch;
    
    $data['terms']= '';
    
    if ($branch->branch != 'energysigns' && $branch->branch != 'websolution')
    {
    	$data['terms'] = '<strong>Conditions: </strong><br />Quotations are valid for 30 days from the date stipulated on the estimate.';
    	$data['terms'] .= '<br />Extra service fee may be charged if the files provided by customers are not in print ready state and require further adjustment in size, color, format, bleeding, etc.';
    }
    else if ($branch->branch == 'energysigns')
    {
    	$data['terms'] = '<strong>DEPOSIT: A 50% deposit is required up front to confirm the quote.</strong>';
    }
    else if ($branch->branch == 'websolution')
    {

    	$data['terms'] = '<strong>Conditions: </strong><br />';
    	$data['terms'] .= '3A web solution will require a 20% deposit ($200 as minimum) before we officially start the website production process.<br />';
        $data['terms'] .= 'The information contained herein is confidential and the property of 3A web solution.<br />';
        $data['terms'] .= 'This is subject to change without notice and quotations are valid for 30 days from issue.<br />';
        $data['terms'] .= 'Revisions may be issued to advise of such changes and/or additions and quote is subject to acceptance by 3A web solution.';     
 
    }
 
    $data['title'] = 'QUOTE';
	
	$this->load->helper('dompdf');
    $html = $this->load->view('transaction_pdf_view', $data, true);
    pdf_create($html, 'quote#'.$quotation->id); 

  }
  
  
  
  function edit()
  {
    if (!($id = $this->uri->segment(3)))
      {
	redirect('quotation/add');
      }

    $data['json_url'] = site_url('quotation/ajax_quotation/'.$id); 
    $data['post_url'] = site_url('quotation/save'); 
    
    $data['main_content'] = 'quotation_edit_view';  
    $data['frame_name'] = 'Edit Quotation';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
    
  }



 function add()
  {
    $data['json_url'] = site_url('quotation/ajax_quotation/new'); 
    $data['post_url'] = site_url('quotation/save'); 

    $data['main_content'] = 'quotation_edit_view';  
    $data['frame_name'] = 'Add Quotation';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	

  }
 function save()
 {
   $data['session_msg'] = 'Quotation Saved';
   $this->load->view('includes/session_msg', $data);

   $json_obj = json_decode($this->input->post('json')); 
   $this->load->model('quotation_model');

   if ($json_obj->transaction->id == 'new') {
     $this->quotation_model->quotation_add($json_obj->transaction
				       , $json_obj->contents);
   } else {
     $this->quotation_model->quotation_update($json_obj->transaction
					  , $json_obj->contents);
   }

 }



 
  function del(){
    echo 'Authorisation rejected.';
  }

}
