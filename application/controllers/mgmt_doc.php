<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mgmt_doc extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    is_logged_in();
      check_auth('mgmt_doc_v');
  }
 
    function index()
  {
 
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    
    $this->session->set_userdata('name', 'Name..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('mgmt_doc/lib');

  }
     

  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['name'] = $this->session->userdata('name');
    $data['search'] = $this->session->userdata('search');
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('mgmt_doc/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

    $this->load->model('doc_model');
    
    $invoice_list = $this->doc_model->doc_list($data['name'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       $config['per_page'], 
						       $this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $invoice_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $invoice_list->query;
    
    $data['title'] = '';

    $data['main_content'] = 'mgmt_doc_list_view';  
    $data['table_title'] = 'doc list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('mgmt_doc/lib');
  }
  

  function filter() 
  { 

    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if ($filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('mgmt_doc/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($doc = $this->input->post('doc')) {      
      $this->session->set_userdata('doc', $doc);
    }
  
    redirect('mgmt_doc/lib');
  }
  
 
   

  
 function add(){
   check_auth('mgmt_doc_ve');
     $doc = new stdClass;
   $doc->id = 'new';
   $doc->order = $doc->cnfd = 0; 
   $doc->dept = $doc->name = $doc->content = '';


    $data['doc'] = $doc;

 
    $data['main_content'] = 'mgmt_doc_edit_view';  
 
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	
    

  }

  
 function edit(){
    check_auth('mgmt_doc_ve');
     $id = $this->uri->segment(3);
 
    $this->db->where('id', $id);
    $result = $this->db->get('co_doc')->result();
    $doc = $result[0];


    $data['doc'] = $doc;

 
    $data['main_content'] = 'mgmt_doc_edit_view';  
 
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	
    

  }


 function save()
 {
   check_auth('mgmt_doc_ve');
   
   $doc = new stdClass;
   $doc->id = $this->input->post('id'); 
   $doc->dept = $this->input->post('dept'); 
   $doc->name = $this->input->post('name'); 
   $doc->url_title = url_title($this->input->post('name'), 'dash', TRUE);
   $doc->order = $this->input->post('order'); 
   $doc->cnfd = $this->input->post('cnfd'); 
   $doc->content = $this->input->post('content'); 

   $this->load->model('doc_model');



   if ($doc->id == 'new') {
     $this->doc_model->doc_add($doc);
      $msg = 'Information saved.';
      $this->session->set_flashdata('msg', $msg);
      redirect('mgmt_doc/lib');
   } else {
     $this->doc_model->doc_update($doc);
      $msg = 'Information saved.';
      $this->session->set_flashdata('msg', $msg);
      redirect('mgmt_doc/edit/'.$doc->id);
   }
   
 }
 

  function del_msg($id){
      $msg = 'Are you really want to delete [ DOC #'.$id.' ] ?&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('mgmt_doc/del/'.$id).'" class="black">Yes</a>';
      $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('mgmt_doc/lib').'" class="black">No</a>';
      $this->session->set_flashdata('msg', $msg);
      redirect('mgmt_doc/lib');	
  }

  function del($id){
    check_auth('mgmt_doc_ved');

    $this->load->model('doc_model');
    $this->doc_model->del($id);
    $msg = 'Doc Deleted';
    $this->session->set_flashdata('msg', $msg);
    redirect('mgmt_doc/lib');
  }
 
  
/*
  function submit(){

    if ($a = $this->input->post('text')){

      $b = $this->input->post('name');
      $d = $this->input->post('dept');

      $this->db->insert('co_doc', array('content' => $a, 'name' => $b, 'dept' => $d)); 
      echo $b;
    }
    else
      {
	echo form_open('sop/submit');

	$data = array('name'        => 'dept');
	echo form_input($data);

	echo '<br />';
	$data = array('name'        => 'name');
	echo form_input($data);

	echo '<br />';
	$data = array('name'        => 'text');
	echo form_textarea($data);
	echo form_submit('mysubmit', 'Submit Post!');
	echo form_close();
      }

  }
  */


}
