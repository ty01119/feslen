<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hq_Meeting extends CI_Controller 
{

  var $branch;

  function __construct()
  {
    parent::__construct();
    $this->branch = $this->session->userdata('branch');
    //$this->load->helper('auth');
    keep_logged_in();
    is_logged_in();
    check_auth('holdings_ved');check_auth('director');
  }

  function test()
  {
    echo '123';die;
  }
  
  function index()
  {
  
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');  
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-1 year"));
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('hq_meeting/lib');

  }
     
  
  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
 
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('hq_meeting/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

    $this->load->model('hq_meeting_model');
    
    $hq_meeting_list = $this->hq_meeting_model->get_list($data['date'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       $config['per_page'], 
						       $this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $hq_meeting_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $hq_meeting_list->query;
    
    $data['title'] = 'hq_meeting Statement';

    $data['main_content'] = 'hq_meeting/hq_meeting_list_view';  
    $data['table_title'] = 'hq_meeting list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('hq_meeting/lib');
  }
  

  function filter() 
  { 
    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if (isset($filter[$flr_num][$key]) && $filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('hq_meeting/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
     
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('hq_meeting/lib');
  }
  


  function ajax_hq_meeting($id)
  { 
    $json_obj = new stdClass;

    if ($id == 'new') {
      $hq_meeting = new stdClass;
      $hq_meeting->id = 'new';
      $hq_meeting->date = date("Y-m-d H:i:s");
      $hq_meeting->attendees = 'Jeffery, Sam, Weber, Ke';
      $hq_meeting->purpose = 'Fortnightly Board Meeting';
      $hq_meeting->outcomes
	= $hq_meeting->logs
	= $hq_meeting->status = '';

      $hq_meeting->approve = 0;
      
      $json_obj->data = $hq_meeting;

      echo json_encode($json_obj);
      
    } else {
     
      $this->load->model('hq_meeting_model');
      if ($hq_meeting = $this->hq_meeting_model->load_by_id($id)) {
	
	$json_obj->data = $hq_meeting;
	
	
	echo json_encode($json_obj);

      }      
    }

  }

  function view($id)
  {
    $this->load->model('hq_meeting_model');
    $hq_meeting = $this->hq_meeting_model->load_by_id($id);
    $contents = $this->hq_meeting_model->load_invo_cons($id);
    $hq_meeting->date = date("d/m/y", strtotime($hq_meeting->date));

 

    $data['hq_meeting'] = $hq_meeting;
    $data['contents'] = $contents;
    $data['customer'] = $customer;
    $data['branch'] =  $branch;
    
    $this->load->view('hq_meeting/hq_meeting_html_view', $data);

  }

  
  function edit()
  {
      check_auth('holdings_ve');
    if (!($id = $this->uri->segment(3)))
      {
	redirect('hq_meeting/add');
      }

    $data['json_url'] = site_url('hq_meeting/ajax_hq_meeting/'.$id); 
    $data['cart_url'] = site_url('hq_meeting/ajax_cart'); 
    $data['post_url'] = site_url('hq_meeting/save'); 
    
    $data['main_content'] = 'hq_meeting/hq_meeting_edit_view';  
    $data['frame_name'] = 'Edit hq_meeting';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
    
  }



  function add()
  {      check_auth('holdings_ve');
    $data['json_url'] = site_url('hq_meeting/ajax_hq_meeting/new'); 
    $data['cart_url'] = site_url('hq_meeting/ajax_cart'); 
    $data['post_url'] = site_url('hq_meeting/save'); 

    $data['main_content'] = 'hq_meeting/hq_meeting_edit_view';  
    $data['frame_name'] = 'Add hq_meeting';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	

  }


 
  function save()
  {

    $json_obj = json_decode($this->input->post('json')); 
    $this->load->model('hq_meeting_model');
   
    if ($json_obj->transaction->id == 'new') {
     
      $this->hq_meeting_model->add($json_obj->transaction);
      $ret->msg =  'hq_meeting Created';
      $ret->close = TRUE;
      
    } else {
      $this->hq_meeting_model->update($json_obj->transaction);
      $ret->msg =  'hq_meeting Detail Saved';
      $ret->close = TRUE;
      
    }
    
    echo json_encode($ret);
    
  }

 

  function del_msg($id){


      $msg = 'Are you really want to delete [ hq_meeting #'.$id.' ] ?&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('hq_meeting/del/'.$id).'" class="black">Yes</a>';
      $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('hq_meeting/lib').'" class="black">No</a>';
      $this->session->set_flashdata('msg', $msg);
      redirect('hq_meeting/lib');	
  }

  function del($id){
    check_auth('holdings_ve');

    $this->load->model('hq_meeting_model');
    $this->hq_meeting_model->del($id);
    $msg = 'hq_meeting Deleted';
    $this->session->set_flashdata('msg', $msg);
    redirect('hq_meeting/lib');
  }


}