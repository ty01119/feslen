<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Panel extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();	 
    $this->load->helper('auth');		           	
    is_logged_in(); 
  }

  function index()
  {
 
    $data['title'] = 'System Panel';

    $data['main_content'] = 'panel_view';  
    
    error_reporting(0);
//    $xmlremote = simplexml_load_file('http://3aweb.co.nz/support/xml/support.xml');
    $xmllocal = <<<XML
<?xml version="1.0" encoding="ISO-8859-1"?>
      <support>
      <company>3A web solution</company>
      <url>http://3aweb.co.nz/</url>
      <email>it@3a.co.nz</email>
      <telephone>09 975 1800</telephone>
      <address>67 Ororke RD, Penrose, Auckland</address>
      <workinghours>9:00am-6pm (Monday - Friday)</workinghours>
      <message>3aweb.co.nz</message>
      </support>
XML;
    $data['xml'] = $xmlremote ? $xmlremote : simplexml_load_string($xmllocal);


    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }
	
 function set_branch($branch)
  {
    $pos = strpos($this->session->userdata('authority'), $branch);
    
    if($pos === false) {
      $msg = 'Authorisation rejected.';
      $this->session->set_flashdata('msg', $msg);
      redirect('panel');
    }
    else {
      $this->session->set_userdata('branch', $branch); 
      $msg = 'Branch switched successfully.';
      $this->session->set_flashdata('msg', $msg);
      redirect('invoice');      
    }
  }

	
 function set_mgmt($option)
  {
    $pos = strpos($this->session->userdata('authority'), $option);
    
    if($pos === false) {
      $msg = 'Authorisation rejected.';
      $this->session->set_flashdata('msg', $msg);
      redirect('panel');
    }
    else {
      /* not implemented yet */
      $msg = 'Authorisation rejected.';
      $this->session->set_flashdata('msg', $msg);
      redirect('panel');
    }
  }

}
