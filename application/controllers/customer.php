<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer extends CI_Controller 
{  
	var $branch;
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    is_logged_in();
     $this->load->model('customer_model');
    $this->branch = $this->session->userdata('branch');
  }

  function test()
  {

    echo '123';die;
  }
  
  function index()
  {
 
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    
    $this->session->set_userdata('customer', 'Customer..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('customer/lib');

  }
     

  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['customer'] = $this->session->userdata('customer');
    $data['search'] = $this->session->userdata('search');
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('customer/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

    $this->load->model('customer_model');
    
    $invoice_list = $this->customer_model->customer_list($data['customer'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       $config['per_page'], 
						       $this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $invoice_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $invoice_list->query;
    
    $data['title'] = '';

    $data['main_content'] = 'customer_list_view';  
    $data['table_title'] = 'Customer list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('customer/lib');
  }
  

  function filter() 
  { 

    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if ($filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('customer/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($customer = $this->input->post('customer')) {      
      $this->session->set_userdata('customer', $customer);
    }
  
    redirect('customer/lib');
  }
  
  function ajax_customer($id)
  {
    $json_obj = new stdClass;

    if ($id == 'new') {
          
      $customer = new stdClass;
      $customer->id = 'new';
      $customer->discount = 0.000;
      $customer->name = $this->uri->segment(4) ? html_entity_decode(urldecode($this->uri->segment(4))) : '';
      $customer->address 
	= $customer->city 
	= $customer->ship_name 
	= $customer->ship_address 
	= $customer->ship_city
	= $customer->pricing
	= $customer->phone 
	= $customer->cellphone 
	= $customer->fax 
	= $customer->email 
	= $customer->url
	= $customer->contact 
	= $customer->billing_contact 
	= $customer->billing_email 
	= $customer->notes 
	= $customer->salesperson = '';
      $json_obj->customer = $customer;
      echo json_encode($json_obj);

    } else {


      $this->load->model('customer_model');
      
      if ($customer = $this->customer_model->load_customer($id)) {
	
	$json_obj->customer = $customer;
	
	echo json_encode($json_obj);
	
      }
    
    }
  }

  function edit()
  { 
    if (!($id = $this->uri->segment(3)))
      {
	redirect('customer/add');
      }
    if (!is_numeric($id)) 
      { 
        $tmp_name = html_entity_decode(urldecode($id));
        $tmp_data = $this->customer_model->load_by_name($tmp_name);
        $id = $tmp_data->id;
      }
    if (!$id)
      {
        redirect('customer/add/'.$this->uri->segment(3));
      }
      
    $data['json_url'] = site_url('customer/ajax_customer/'.$id); 
    $data['post_url'] = site_url('customer/save'); 
    $data['read_only'] = 0;
    
    $data['main_content'] = 'customer_edit_view';  
    $data['frame_name'] = 'Edit Customer';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
   
  }

 function add($name = '')
  {
   
    $data['json_url'] = site_url('customer/ajax_customer/new/'.$name); 
    $data['post_url'] = site_url('customer/save'); 
    $data['read_only'] = 0;

    $data['main_content'] = 'customer_edit_view';  
    $data['frame_name'] = 'Add Customer';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	

  }

 function save()
 {
   
   
   $json_obj = json_decode($this->input->post('json')); 
   $this->load->model('customer_model');
   
   if ($json_obj->customer->id == 'new') 
     {
       if ($this->customer_model->has_name($json_obj->customer->name))
	 {
	   $ret->msg = 'Customer name exists...';
	   $ret->close = FALSE;
	 }
       else 
	 {
	   $json_obj->customer->branch = $this->branch;
	   $this->customer_model->customer_add($json_obj->customer);
	   $ret->msg = 'Customer Information Saved';
	//   $ret->msg = 'system update..'.$json_obj->customer->branch ;
	   $ret->close = TRUE;
	 }
     }
   else 
     {
       if ($this->customer_model->has_dup_name($json_obj->customer->id, 
					     $json_obj->customer->name))
	 {
	   $ret->msg = 'Customer name exists...';
	   $ret->close = FALSE;
	 }
       else 
	 {
	   $this->customer_model->customer_update($json_obj->customer);
	   $ret->msg = 'Customer Information Saved';
	   $ret->close = TRUE;
	 }
     }

   echo json_encode($ret);
   
 }
 
   
 function view()
 {

   if (!($id = $this->uri->segment(3)))
     {
       redirect('customer/add');
     }
   
   $data['json_url'] = site_url('customer/ajax_customer/'.$id); 
   $data['post_url'] = site_url('customer/save'); 
   $data['read_only'] = 1;
   
   $data['main_content'] = 'customer_edit_view';  
   $data['frame_name'] = 'View Customer';
   $data['session_msg'] = $this->session->flashdata('msg');
   $data['cf_feslen'] = $this->config->item('cf_feslen');
   $this->load->view('includes/template_frame', $data);
   
 }
 
 
  function view_name()
 {

   if ($name = $this->input->get('name', TRUE))
     {
    $customer = $this->customer_model->load_by_name($name);
    $id = $customer->id;
    $data['json_url'] = site_url('customer/ajax_customer/'.$id); 
   $data['post_url'] = site_url('customer/save'); 
   $data['read_only'] = 1;
   
   $data['main_content'] = 'customer_edit_view';  
   $data['frame_name'] = 'View Customer';
   $data['session_msg'] = $this->session->flashdata('msg');
   $data['cf_feslen'] = $this->config->item('cf_feslen');
   $this->load->view('includes/template_frame', $data);
     }
 }
 
 
 function del()
 {

   if (!($id = $this->uri->segment(3)))
     {
       redirect('customer/add');
     }
   
   $data['json_url'] = site_url('customer/ajax_customer/'.$id); 
   $data['post_url'] = site_url('customer/del_db'); 
   $data['read_only'] = 1;
   
   $data['main_content'] = 'customer_del_view';  
   $data['frame_name'] = 'Delete Customer';
   $data['session_msg'] = $this->session->flashdata('msg');
   $data['cf_feslen'] = $this->config->item('cf_feslen');
   $this->load->view('includes/template_frame', $data);
   
 }



 function del_db()
 {
   $data['session_msg'] = 'Customer Deleted';
   $this->load->view('includes/session_msg', $data);
   
   $json_obj = json_decode($this->input->post('json')); 
   $this->load->model('customer_model');

   if ($id = $json_obj->customer->id) {
     $this->customer_model->customer_del($id, $json_obj->receiver);
   }  
   
 }


}