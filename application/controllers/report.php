<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller {

  function __construct()
  {
    parent::__construct();    
    $this->load->helper('auth'); 
    $this->load->helper('info');   
    is_logged_in();
  }
	
  function index()
  {
    
    $data['title'] = '';

    $data['main_content'] = 'report_view';  
    $data['table_title'] = 'reports'; 

 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	
  }


 function cust_report() {	
    $name = $this->input->post('customer');

    $this->load->model('invoice_model');
    $filter[3]['sql'] = 'paid-ne-total';

    $data['date']['fr'] = strtotime("1 January 2000");
    $data['date']['to'] = strtotime(date('Y-m-t', strtotime(str_replace('/', '-', $this->input->post('to')))));

//echo date('Y-m-d', $data['date']['to']);
    $invoice_list = $this->invoice_model->invoice_list($data['date'],$name, $filter,'','', '','');
    
    $data['query'] = $invoice_list->query;
 
 
 
    $this->load->model('customer_model');
    $customer = $this->customer_model->load_by_name($name);


    $this->load->model('branch_model');
    $branch = $this->branch_model->load_by_name($this->session->userdata('branch'));

 
    $branch = refine_branch_data($branch, date("Y-m-d H:i:s", $data['date']['to']));
    
    $data['customer'] = $customer;
    $data['branch'] =  $branch;
    
    $this->load->view('report_stmt_customer_view', $data);


 }
 


 function invoice_report() {	
 
    $data['customer'] = $this->input->post('customer');
 
    $data['date']['fr'] = strtotime(str_replace('/', '-', $this->input->post('from')));
    $data['date']['to'] = strtotime(str_replace('/', '-', $this->input->post('to')));

    $data['filter'] = '';
    if ($this->input->post('finished') == 'no') $data['filter'][1]['finished'] = 0;
    if ($this->input->post('finished') == 'yes') $data['filter'][1]['finished'] = 1;
    if ($this->input->post('delivered') == 'no') $data['filter'][2]['delivered'] = 0;
    if ($this->input->post('delivered') == 'yes') $data['filter'][2]['delivered'] = 1;
    if ($this->input->post('paid') == 'no') $data['filter'][3]['sql'] = 'paid-ne-total';
    if ($this->input->post('paid') == 'yes') $data['filter'][3]['sql'] = 'paid-eq-total';

    $this->load->model('invoice_model');
    $invoice_list = $this->invoice_model->invoice_list($data['date'],
						       $data['customer'],
						       $data['filter'], 
						       '', 
						       '', 
						       '', 
						       '');
    
    $data['query'] = $invoice_list->query;
 
 
 
    $this->load->model('customer_model');
    $customer = $this->customer_model->load_by_name($data['customer']);


    $this->load->model('branch_model');
    $branch = $this->branch_model->load_by_name($this->session->userdata('branch'));
    
    $branch = refine_branch_data($branch, date("Y-m-d H:i:s", $data['date']['to']));
  
    $data['customer'] = $customer;
    $data['branch'] =  $branch;
    
    $this->load->view('report_stmt_invoice_view', $data);


 }
 

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */