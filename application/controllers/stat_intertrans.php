<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stat_intertrans extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    // $this->load->helper('date');
    $this->load->helper('auth');  
    keep_logged_in();
    is_logged_in();
  
  }

  function test()
  {

    echo '123';die;
  }
  
  function index()
  {
 
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    
    $this->session->set_userdata('customer', 'Customer..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('stat_intertrans_date_fr', strtotime("-3 month"));
    $this->session->set_userdata('stat_intertrans_date_to', time());    
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('stat_intertrans/lib');

  }
     

  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['customer'] = $this->session->userdata('customer');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('stat_intertrans_date_fr');
    $data['date']['to'] = $this->session->userdata('stat_intertrans_date_to');
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->model('stat_intertrans_model');
  //  print_r( $this->stat_intertrans_model->branch_for_branch('websolution', '3A Copy & Design (Epsom)', $data['date'] ));
    
$this->load->library('table');
    $data['table'] = $this->stat_intertrans_model->load_table( $data['date']);
//print_r($data['table']);die;
    $data['title'] = '';
  
    $data['main_content'] = 'statistics/stat_intertrans_view';  
    $data['table_title'] = 'Internal Transaction table'; 
    
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }



  function search() 
  {
      check_auth('mgmt_statistics');
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('stat_intertrans_date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('stat_intertrans_date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('stat_intertrans/lib');
  }
  
 
  function del(){
    echo 'Authorisation rejected.';
  }

}
