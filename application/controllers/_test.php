<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class _test extends CI_Controller 
{

  function __construct()
  {    
    parent::__construct();	
    $this->load->model('user_model');
    $this->load->helper('auth');	
  }
  
  function index()
  { 
  keep_logged_in();
  echo 'testing...<br />';
    
 }
  
}