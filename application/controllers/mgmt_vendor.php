<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mgmt_vendor extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    is_logged_in();
  check_auth('mgmt_inventory_v');
  }

  function test()
  {

    echo '123';die;
  }
  
  function index()
  {
 
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    
    $this->session->set_userdata('vendor', 'Vendor..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('mgmt_vendor/lib');

  }
     

  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['vendor'] = $this->session->userdata('vendor');
    $data['search'] = $this->session->userdata('search');
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('mgmt_vendor/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

    $this->load->model('vendor_model');
    
    $invoice_list = $this->vendor_model->vendor_list($data['vendor'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       $config['per_page'], 
						       $this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $invoice_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $invoice_list->query;
    
    $data['title'] = '';

    $data['main_content'] = 'mgmt_vendor_list_view';  
    $data['table_title'] = 'Vendor list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('mgmt_vendor/lib');
  }
  

  function filter() 
  { 

    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if ($filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('mgmt_vendor/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($vendor = $this->input->post('vendor')) {      
      $this->session->set_userdata('vendor', $vendor);
    }
  
    redirect('mgmt_vendor/lib');
  }
  
  function ajax_vendor($id)
  {
    $json_obj = new stdClass;

    if ($id == 'new') {
      $vendor = new stdClass;
      $vendor->id = 'new';
      $vendor->discount = 0.000;
      $vendor->name 
	= $vendor->address 
	= $vendor->city 
	= $vendor->ship_name 
	= $vendor->ship_address 
	= $vendor->ship_city
	= $vendor->pricing
	= $vendor->phone 
	= $vendor->cellphone 
	= $vendor->fax 
	= $vendor->email 
	= $vendor->url
	= $vendor->contact = '';
      $json_obj->vendor = $vendor;
      echo json_encode($json_obj);

    } else {


      $this->load->model('vendor_model');
      
      if ($vendor = $this->vendor_model->load_vendor($id)) {
	
	$json_obj->vendor = $vendor;
	
	echo json_encode($json_obj);
	
      }
    
    }
  }

  function edit()
  {check_auth('mgmt_inventory_ve', FALSE);
    if (!($id = $this->uri->segment(3)))
      {
	redirect('mgmt_vendor/add');
      }
   
    $data['json_url'] = site_url('mgmt_vendor/ajax_vendor/'.$id); 
    $data['post_url'] = site_url('mgmt_vendor/save'); 
    $data['read_only'] = 0;
    
    $data['main_content'] = 'mgmt_vendor_edit_view';  
    $data['frame_name'] = 'Edit Vendor';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
   
  }

 function add()
  {check_auth('mgmt_inventory_ved', FALSE);
   
    $data['json_url'] = site_url('mgmt_vendor/ajax_vendor/new'); 
    $data['post_url'] = site_url('mgmt_vendor/save'); 
    $data['read_only'] = 0;

    $data['main_content'] = 'mgmt_vendor_edit_view';  
    $data['frame_name'] = 'Add Vendor';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	

  }

 function save()
 {
   $data['session_msg'] = 'Vendor Information Saved';
   $this->load->view('includes/session_msg', $data);
   
   $json_obj = json_decode($this->input->post('json')); 
   $this->load->model('vendor_model');

   if ($json_obj->vendor->id == 'new') {
     $this->vendor_model->vendor_add($json_obj->vendor);
   } else {
     $this->vendor_model->vendor_update($json_obj->vendor);
   }
   
 }


 function view()
 {

   if (!($id = $this->uri->segment(3)))
     {
       redirect('mgmt_vendor/add');
     }
   
   $data['json_url'] = site_url('mgmt_vendor/ajax_vendor/'.$id); 
   $data['post_url'] = site_url('mgmt_vendor/save'); 
   $data['read_only'] = 1;
   
   $data['main_content'] = 'mgmt_vendor_edit_view';  
   $data['frame_name'] = 'View Vendor';
   $data['session_msg'] = $this->session->flashdata('msg');
   $data['cf_feslen'] = $this->config->item('cf_feslen');
   $this->load->view('includes/template_frame', $data);
   
 }
 
 function del()
 {

   if (!($id = $this->uri->segment(3)))
     {
       redirect('mgmt_vendor/add');
     }
   
   $data['json_url'] = site_url('mgmt_vendor/ajax_vendor/'.$id); 
   $data['post_url'] = site_url('mgmt_vendor/del_db'); 
   $data['read_only'] = 1;
   
   $data['main_content'] = 'vendor_del_view';  
   $data['frame_name'] = 'Delete Vendor';
   $data['session_msg'] = $this->session->flashdata('msg');
   $data['cf_feslen'] = $this->config->item('cf_feslen');
   $this->load->view('includes/template_frame', $data);
   
 }



 function del_db()
 {
   $data['session_msg'] = 'Vendor Deleted';
   $this->load->view('includes/session_msg', $data);
   
   $json_obj = json_decode($this->input->post('json')); 
   $this->load->model('vendor_model');

   if ($id = $json_obj->vendor->id) {
     $this->vendor_model->vendor_del($id, $json_obj->receiver);
   }  
   
 }


}
