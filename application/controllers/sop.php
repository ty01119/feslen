<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sop extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    is_logged_in();
  
  }
 
  
  function index()
  {
  
    if (pass_auth('sop_cnfd')) redirect('sop/cnfd');
    
    $data['main_content'] = 'sop_list_view';  
    $data['table_title'] = 'Policy and Standard Operating Procedures (SOP)'; 

    $this->db->where('dept', '3aweb');
    $this->db->order_by("order", "asc"); 
    $data['doc_3aweb'] = $this->db->get('co_doc')->result();

    $this->db->where('dept', '3agroup');    
    $this->db->where('cnfd !=', 2);
    $this->db->order_by("order", "asc"); 
    $data['doc_3agroup'] = $this->db->get('co_doc')->result();

    $this->db->where('dept', '3acopy');
    $this->db->order_by("order", "asc"); 
    $data['doc_3acopy'] = $this->db->get('co_doc')->result();

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }
  
  function cnfd()
  {
     check_auth('sop_cnfd');
    $data['main_content'] = 'sop_list_view';  
    $data['table_title'] = 'Policy and Standard Operating Procedures (SOP)'; 

    $this->db->where('dept', '3aweb');
    $this->db->order_by("order", "asc"); 
    $data['doc_3aweb'] = $this->db->get('co_doc')->result();

    $this->db->where('dept', '3agroup');
    $this->db->order_by("order", "asc"); 
    $data['doc_3agroup'] = $this->db->get('co_doc')->result();

    $this->db->where('dept', '3acopy');
    $this->db->order_by("order", "asc"); 
    $data['doc_3acopy'] = $this->db->get('co_doc')->result();

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }
  
     
  function load()
  {

    $dept = $this->uri->segment(3);
    $url_title = $this->uri->segment(4);
    $this->db->where('dept', $dept);
    $this->db->where('url_title', $url_title);
    $result = $this->db->get('co_doc')->result();

    $doc = $result[0];

    $data['main_content'] = $doc->content;


    $search  = array('src="images/');
    $replace = array('src="'.base_url().'images/doc/');
    $subject = $data['main_content'];
    $data['main_content'] = str_replace($search, $replace, $subject);


    if ($doc->cnfd)
      $this->load->view('sop/template_in', $data);
    else
      $this->load->view('sop/template_ex', $data);
    
	
  }
  /*
  function submit(){

    if ($a = $this->input->post('text')){

      $b = $this->input->post('name');
      $d = $this->input->post('dept');

      $this->db->insert('co_doc', array('content' => $a, 'name' => $b, 'dept' => $d)); 
      echo $b;
    }
    else
      {
	echo form_open('sop/submit');

	$data = array('name'        => 'dept');
	echo form_input($data);

	echo '<br />';
	$data = array('name'        => 'name');
	echo form_input($data);

	echo '<br />';
	$data = array('name'        => 'text');
	echo form_textarea($data);
	echo form_submit('mysubmit', 'Submit Post!');
	echo form_close();
      }

  }
  */


}
