<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staff extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    is_logged_in();
  
  }
 
  
  function index()
  {
 
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    
    $this->session->set_userdata('staff', 'Staff..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'name';
    $orderby['sort'] = 'asc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('staff/lib');

  }
     

  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['staff'] = $this->session->userdata('staff');
    $data['search'] = $this->session->userdata('search');
    $data['per_page'] = $this->session->userdata('per_page');

   
    $this->load->model('staff_model');
    $this->load->model('branch_model');
    

    

    $query = $this->db->get('co_branches');

    foreach ($query->result() as $row)
   {
    $branch = $row;		    
    $query = $this->staff_model->staff_by_branch($row->branch);
    $branch->staff_list = $query->result();
    $data[$row->branch] = $branch;
    } 
 

 
    
    $data['title'] = '';

    $data['main_content'] = 'staff_list_view';  
    $data['table_title'] = '3A Phone Directory'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }

 

 
  

 
 

}
