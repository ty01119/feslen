<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invoice extends CI_Controller 
{

  var $branch;

  function __construct()
  {
    parent::__construct();
    $this->branch = $this->session->userdata('branch');
    $this->load->helper('auth');    
    $this->load->helper('info');    
    keep_logged_in();
    is_logged_in();
    
    $this->load->model('invoice_model');
  }

  
  function index()
  {
  
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    $this->session->set_userdata('customer', 'Customer..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('invoice/lib');

  }
     
  
  function lib()
  {
  
    $this->session->set_userdata('page_index', $this->uri->segment(3));
  
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['customer'] = $this->session->userdata('customer');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');
/*** menu ***/
    $data['menu_main1'] = array(); 
    array_push($data['menu_main1'], array('id' => 'menu-main1_01', 
    	'onclick' => "window.location.href='".site_url('invoice')."'", 
    	'label' => 'invoice', 'checked' => ($this->uri->segment(1) == 'invoice')));
    
    
    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('invoice/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

    $this->load->model('invoice_model');
    
    $invoice_list = $this->invoice_model->invoice_list($data['date'],
						       $data['customer'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       $config['per_page'], 
						       $this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $invoice_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $invoice_list->query;
    
    $data['title'] = 'Invoice Statement';

    $data['main_content'] = 'invoice_list_view';  
    $data['table_title'] = 'Invoice list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }

  function report()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['customer'] = $this->session->userdata('customer');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = '';

    $this->load->model('invoice_model');
    
    $invoice_list = $this->invoice_model->invoice_list($data['date'],
						       $data['customer'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       '', 
						       '');
    
    $data['total'] = $invoice_list->total;
    $data['query'] = $invoice_list->query;
    $data['title'] = 'Invoice Statement';
    $data['table_title'] = 'Invoice list'; 
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('invoice_report_view', $data);	

  }

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('invoice/lib');
  }
  

  function filter() 
  { 
    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if (isset($filter[$flr_num][$key]) && $filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('invoice/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($customer = $this->input->post('customer')) {      
      $this->session->set_userdata('customer', $customer);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('invoice/lib');
  }
  

  function ajax_cart()
  { 
    $json_obj = new stdClass;

      if ($cart = $this->session->userdata('cart')) {
	$contents = $cart; 
	$json_obj->cart = $contents;
      	echo json_encode($json_obj);
      }    
        
 }

  function ajax_invoice($id)
  { 
    $json_obj = new stdClass;

    if ($id == 'new') {
      $invoice = new stdClass;
      $invoice->id = 'new';
      $invoice->date = date("Y-m-d H:i:s");
      $invoice->subtotal 
	= $invoice->tax 
	= $invoice->tax_exempt
	= $invoice->total 
	= $invoice->discount 
	= $invoice->paid 
	= $invoice->deleted
	= $invoice->finished
	= $invoice->delivered 
	= $invoice->bad_debt
	= $invoice->outsourced = 0;
      $invoice->cust_name
	= $invoice->payment_meth
	= $invoice->quote_id
	= $invoice->info
	= $invoice->logs = '';
    
      $json_obj->invoice = $invoice;

      if ($cart = $this->session->userdata('cart')) {
	$contents = $cart;
      } else {
	$contents=array();
	$contents[0] 
	  = array('quantity' => 1, 'unit_price' => 0, 'discount' => 0);
      }      

      $json_obj->contents = $contents;
      echo json_encode($json_obj);
      
    } elseif ($id == 'trans') {
      $id = $this->uri->segment(4);

      $this->load->model('quotation_model');
      if ($quotation = $this->quotation_model->load_quotation($id)) {
	
	$invoice = new stdClass;
	$invoice->id = 'new';
	$invoice->date = date("Y-m-d H:i:s");
	$invoice->subtotal = $quotation->subtotal;
	$invoice->tax = $quotation->tax;
	$invoice->total = $quotation->total;
	$invoice->discount = $quotation->discount;
	$invoice->paid = $quotation->paid; 
	$invoice->tax_exempt 
	  = $invoice->deleted
	  = $invoice->finished
	  = $invoice->delivered  
	  = $invoice->bad_debt
	  = $invoice->outsourced = 0;
	$invoice->cust_name = $quotation->cust_name;
	$invoice->quote_id = $quotation->id;
	$invoice->payment_meth
	  = $invoice->info
	  = $invoice->logs = '';

	$json_obj->invoice = $invoice;
	$json_obj->contents = json_decode($quotation->contents);
	
	echo json_encode($json_obj);
	
      }      
      
    } else if ($id == 'dup') {
      $id = $this->uri->segment(4);
      
      $this->load->model('invoice_model');
      if ($invoice = $this->invoice_model->load_invoice($id)) {
	
	$json_obj->invoice = $invoice;
	
	$contents = $this->invoice_model->load_invo_cons($id);
	
	$json_obj->contents = $contents;
 
	$json_obj->invoice->id = 'new';
	$json_obj->invoice->date = date("Y-m-d H:i:s");
	$json_obj->invoice->paid 
	  = $json_obj->invoice->deleted
	  = $json_obj->invoice->finished
	  = $json_obj->invoice->delivered  
	  = $json_obj->invoice->bad_debt
	  = $json_obj->invoice->outsourced = 0;
	$json_obj->invoice->payment_meth
	  = $json_obj->invoice->info
	  = $json_obj->invoice->logs = '';
 
	echo json_encode($json_obj);
      }
      
    } else {
     
      $this->load->model('invoice_model');
      if ($invoice = $this->invoice_model->load_invoice($id)) {
	
	$json_obj->invoice = $invoice;
	
	$contents = $this->invoice_model->load_invo_cons($id);
	
	$json_obj->contents = $contents;
	
	echo json_encode($json_obj);

      }      
    }

  }

  function pdf($id)
  {
    $this->load->model('invoice_model');
    $invoice = $this->invoice_model->load_invoice($id);
    $contents = $this->invoice_model->load_invo_cons($id);
    
    $this->load->model('customer_model');
    $customer = $this->customer_model->load_by_name($invoice->cust_name);
    
    $this->load->model('branch_model');
    $branch = $this->branch_model->load_by_name($this->session->userdata('branch'));
    
    $branch = refine_branch_data($branch, $invoice->date);
    $invoice->date = date("d/m/y", strtotime($invoice->date));

    $data['invoice'] = $invoice;
    $data['contents'] = $contents;
    $data['customer'] = $customer;
    $data['branch'] =  $branch;

    $this->load->helper('dompdf');
    $html = $this->load->view('invoice_pdf_view', $data, true);
    pdf_create($html, 'invoice#'.$invoice->id);
    
  }
  
  function email_msg($id){

    check_auth($this->branch.'_ve');
      $invoice = $this->invoice_model->load_invoice($id);    
    $this->load->model('customer_model');
    $customer = $this->customer_model->load_by_name($invoice->cust_name);
    
      $msg = 'Are you really want to email [ INVOICE #'.$id.' ] to '. $customer->contact.' ('. $customer->email.') ?&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('invoice/email/'.$id).'" class="black">Yes</a>';
      $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('invoice/lib/'.$this->session->userdata('page_index')).'" class="black">No</a>';
    $msg .= '<form name="input" action="'.site_url('invoice/email/'.$id).'" method="post">
<br />Additional Message: <br /><textarea name="email_msg" style="width: 349px; "></textarea>
<input type="submit" value="Send" style="color: #030303;" class="ui-state-highlight ui-corner-all">
</form>';
      $this->session->set_flashdata('msg', $msg);
      redirect('invoice/lib/'.$this->session->userdata('page_index'));	
  }

  function email($id)
  {

    check_auth($this->branch.'_ve');
    
    $data['email_msg'] = $this->input->post('email_msg'); 
    
    $invoice = $this->invoice_model->load_invoice($id);
    $contents = $this->invoice_model->load_invo_cons($id);
    
    $this->load->model('customer_model');
    $customer = $this->customer_model->load_by_name($invoice->cust_name);
    
    $this->load->model('branch_model');
    $branch = $this->branch_model->load_by_name($this->session->userdata('branch'));

    $branch = refine_branch_data($branch, $invoice->date);
    
    
    $invoice->due_date = $this->branch == 'websolution' ? date('Y-m-d H:i:s', strtotime("+1 week", strtotime($invoice->date))) : date('Y-m-21 H:i:s', strtotime("+1 month", strtotime($invoice->date)));
   
    
    $data['invoice'] = $invoice;
    $data['contents'] = $contents;
    $data['customer'] = $customer;
    $data['branch'] =  $branch;

    $this->load->helper(array('dompdf', 'file', 'email'));
    $html = $this->load->view('invoice_pdf_view', $data, TRUE);
    
    $pdf = pdf_create($html, 'invoice#'.$invoice->id, FALSE);
    
    $file = 'files/INVOICE#'.$data['invoice']->id.'.pdf';
 
    file_put_contents($file, $pdf);
        
    $data['member'] = $data['customer']; 
    $data['sender'] = $branch->biz_name;
 
    $letter = $this->load->view('templates/email_invoice', $data, true);
        	    
    $this->load->library('email');
    $this->email->from($branch->email, $data['sender']);


    if (!valid_email($data['member']->email)) 
    {
    	$msg = 'Email address invalid, INVOICE could not be sent.';
    	$this->session->set_flashdata('msg', $msg);
    }
    else
    {
    $this->email->to($data['member']->email);
//    $this->email->to($branch->email); 
    $this->email->bcc($branch->email); 
    
    $this->email->subject($branch->biz_name.' | INVOICE#'.$data['invoice']->id);
   
    $this->email->message($letter);
    $this->email->attach($file);
    $this->email->send();
 
    $msg = 'INVOICE sent to '.$customer->name.' - '.$customer->contact.' ('. $customer->email.').';
     //   $msg = 'INVOICE did not send to customer but your branch email instead for testing purpose.';
    $msg .= '<style>#inv'.$data['invoice']->id.' {outline: yellow solid 1px;}</style>';
    $this->session->set_flashdata('msg', $msg);
    }
    unlink($file);
    redirect('invoice/lib/'.$this->session->userdata('page_index'));
    
  }


  function view($id)
  {
    $this->load->model('invoice_model');
    $invoice = $this->invoice_model->load_invoice($id);
    $contents = $this->invoice_model->load_invo_cons($id);

    $this->load->model('customer_model');
    $customer = $this->customer_model->load_by_name($invoice->cust_name);


    $this->load->model('branch_model');
    $branch = $this->branch_model->load_by_name($this->session->userdata('branch'));
     
    $branch = refine_branch_data($branch, $invoice->date);


    $invoice->date = date("d/m/y", strtotime($invoice->date));
    
    $data['invoice'] = $invoice;
    $data['contents'] = $contents;
    $data['customer'] = $customer;
    $data['branch'] =  $branch;
    
    $this->load->view('invoice_html_view', $data);

  }

  function prt($id)
  {
    $this->load->model('invoice_model');
    $invoice = $this->invoice_model->load_invoice($id);
    $contents = $this->invoice_model->load_invo_cons($id);

    $this->load->model('customer_model');
    $customer = $this->customer_model->load_by_name($invoice->cust_name);


    $this->load->model('branch_model');
    $branch = $this->branch_model->load_by_name($this->session->userdata('branch'));
    
    $branch = refine_branch_data($branch, $invoice->date);
    $invoice->date = date("d/m/y", strtotime($invoice->date));
    
    $data['invoice'] = $invoice;
    $data['contents'] = $contents;
    $data['customer'] = $customer;
    $data['branch'] =  $branch;
    
    $this->load->view('templates/invoice_print_view', $data);

  }
  
  function edit()
  {

   

    check_auth($this->branch.'_ve', FALSE);
    if (!($id = $this->uri->segment(3)))
      {
	redirect('invoice/add');
      }

    $data['tax_auth'] = pass_auth('mgmt_tax_exempt') ? TRUE : FALSE;

    $data['json_url'] = site_url('invoice/ajax_invoice/'.$id); 
    $data['cart_url'] = site_url('invoice/ajax_cart'); 
    $data['post_url'] = site_url('invoice/save'); 
    
    $data['main_content'] = 'invoice_edit_view';  
    $data['frame_name'] = 'Edit Invoice';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
    
  }



  function add()
  {

    

    check_auth($this->branch.'_ve', FALSE);
    if ($this->session->userdata('branch') == 'newmarket') check_auth('no-auth', FALSE);
    if ($this->session->userdata('branch') == 'onehunga') check_auth('no-auth', FALSE);
    
    $data['tax_auth'] = pass_auth('mgmt_tax_exempt') ? TRUE : FALSE;
    $data['json_url'] = site_url('invoice/ajax_invoice/new'); 
    $data['cart_url'] = site_url('invoice/ajax_cart'); 
    $data['post_url'] = site_url('invoice/save'); 

    $data['main_content'] = 'invoice_edit_view';  
    $data['frame_name'] = 'Add Invoice';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	

  }

  function dup($id)
  {

    

    check_auth($this->branch.'_ve', FALSE);
    $data['tax_auth'] = pass_auth('mgmt_tax_exempt') ? TRUE : FALSE;
    $data['json_url'] = site_url('invoice/ajax_invoice/dup').'/'.$id; 
    $data['cart_url'] = site_url('invoice/ajax_cart'); 
    $data['post_url'] = site_url('invoice/save'); 
   
    $data['main_content'] = 'invoice_edit_view';  
    $data['frame_name'] = 'Add Invoice';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
   
  }


  function trans($id)
  {

    

    check_auth($this->branch.'_ve', FALSE);
    $data['tax_auth'] = pass_auth('mgmt_tax_exempt') ? TRUE : FALSE;
    $data['json_url'] = site_url('invoice/ajax_invoice/trans').'/'.$id; 
    $data['cart_url'] = site_url('invoice/ajax_cart'); 
    $data['post_url'] = site_url('invoice/save'); 
   
    $data['main_content'] = 'invoice_edit_view';  
    $data['frame_name'] = 'Add Invoice';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
   
  }
 
  function save()
  {

    

    check_auth($this->branch.'_ve', FALSE);
    
    $json_obj = json_decode($this->input->post('json')); 
    $this->load->model('invoice_model');
    
    if ($json_obj->invoice->id == 'new') {
      if ($this->session->userdata('branch') == 'newmarket') check_auth('no-auth', FALSE);
      if ($this->session->userdata('branch') == 'onehunga') check_auth('no-auth', FALSE);
    
      $this->invoice_model->invoice_add($json_obj->invoice
					, $json_obj->contents);
      $ret->msg =  'Invoice Created';
      $ret->close = TRUE;
      
    } else {
      $this->invoice_model->invoice_update($json_obj->invoice
					   , $json_obj->contents);
      $ret->msg =  'Invoice Detail Saved';
      $ret->close = TRUE;
      
    }
    
    echo json_encode($ret);
    
  }

 

  function del_msg($id){
      $msg = 'Are you really want to delete [ INVOICE #'.$id.' ] ?&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('invoice/del/'.$id).'" class="black">Yes</a>';
      $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('invoice/lib').'" class="black">No</a>';
      $this->session->set_flashdata('msg', $msg);
      redirect('invoice/lib');	
  }

  function del($id){
    check_auth($this->branch.'_ved');

    $this->load->model('invoice_model');
    $this->invoice_model->invoice_del($id);
    $msg = 'Invoice Deleted';
    $this->session->set_flashdata('msg', $msg);
    redirect('invoice/lib');
  }
  


}