<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    // $this->load->helper('date');
    $this->load->helper('auth');   
    keep_logged_in();
    is_logged_in();
  
  }

  function index()
  {
 
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    
    $this->session->set_userdata('customer', 'Customer..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', time());
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('payment/lib');

  }
     

  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['customer'] = $this->session->userdata('customer');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('payment/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

    $this->load->model('payment_model');
    
    $payment_list = $this->payment_model->payment_list($data['date'],
						       $data['customer'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       $config['per_page'], 
						       $this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $payment_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $payment_list->query;
    
    $data['title'] = '';

    $data['main_content'] = 'payment_list_view';  
    $data['table_title'] = 'Payment list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }



  function report()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['customer'] = $this->session->userdata('customer');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = '';

   

    $this->load->model('payment_model');
    
    $invoice_list = $this->payment_model->payment_list($data['date'],
						       $data['customer'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       '', 
						       '');
    
    $data['total'] = $invoice_list->total;
    $data['query'] = $invoice_list->query;
    $data['title'] = 'Payment Statement';
    $data['table_title'] = 'Transaction list'; 
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('invoice_report_view', $data);	

  }

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('payment/lib');
  }
  

  function filter() 
  { 

    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if ($filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('payment/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($customer = $this->input->post('customer')) {      
      $this->session->set_userdata('customer', $customer);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('payment/lib');
  }
  

  function ajax_payment($id)
  { 
    $json_obj = new stdClass;

    if ($id == 'new') {
      $payment = new stdClass;
      $payment->id = 'new';
      $payment->date = date("Y-m-d H:i:s");
      $payment->invoice_id
	= $payment->detail 
	= $payment->payment_meth 
	= '';
      $payment->paid = 0;
    
      $json_obj->transaction = $payment;
      
      echo json_encode($json_obj);
      
    } else {
     
      $this->load->model('payment_model');
      if ($payment = $this->payment_model->load_by_id($id)) {
	
	$json_obj->transaction = $payment;
	
	echo json_encode($json_obj);

      }      
    }

  }


  function view($id)
  {
    $this->load->model('payment_model');
    $payment = $this->payment_model->load_payment($id);
    $contents = json_decode($payment->contents);
    $payment->date = date("d/m/y", strtotime($payment->date));
    
    $this->load->model('customer_model');
    $customer = $this->customer_model->load_by_name($payment->cust_name);
    
    
    $this->load->model('branch_model');
    $branch = $this->branch_model->load_by_name($this->session->userdata('branch'));

    $data['transaction'] = $payment;
    $data['contents'] = $contents;
    $data['customer'] = $customer;
    $data['branch'] =  $branch;
    
    $data['terms']= '';
    
       
    $data['title'] = 'QUOTE';
    $this->load->view('transaction_html_view', $data);

  }

  
  
  
  function edit()
  {
    if (!($id = $this->uri->segment(3)))
      {
	redirect('payment/add');
      }

    $data['json_url'] = site_url('payment/ajax_payment/'.$id); 
    $data['post_url'] = site_url('payment/save'); 
    $data['read_only'] = 0;
    
    $data['main_content'] = 'payment_edit_view';  
    $data['frame_name'] = 'Edit Payment';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
    
  }



 function add()
  {
    $data['json_url'] = site_url('payment/ajax_payment/new'); 
    $data['post_url'] = site_url('payment/save'); 
    $data['read_only'] = 0;
    

    $data['main_content'] = 'payment_edit_view';  
    $data['frame_name'] = 'Add Payment';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	

  }

 function save()
 {
     
   $json_obj = json_decode($this->input->post('json')); 
   $this->load->model('payment_model');
   
   if ($json_obj->transaction->id == 'new') {
     $this->payment_model->add($json_obj->transaction);	
     $ret->msg =  'Transaction Created';
      $ret->close = TRUE;		
   } else {
     $this->payment_model->update($json_obj->transaction);
     $ret->msg =  'Transaction Saved';
     $ret->close = TRUE;
   }
   
   echo json_encode($ret);
    
 }



 
  function del(){
    echo 'Authorisation rejected.';
  }

}
