<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mgmt_inventory_cate extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    $this->load->model('inventory_cate_model');
    is_logged_in();
    check_auth('mgmt_inventory_cate_ved');	
  }
 
  function lib()
  {       
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter');
    $data['search'] = $this->session->userdata('search');

    $this->load->library('pagination');
    
    $config['base_url'] = site_url('mgmt_inventory_cate/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = 25;
    $config['num_links'] = 5;

    
    $inventory_cate_list = $this->inventory_cate_model->get_list(
					      $data['search'], 
					      $data['filter'], 
					      $data['orderby'], 
					      $config['per_page'], 
					      $this->uri->segment(3)
					      );
    
    $config['total_rows'] = $data['total'] = $inventory_cate_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $inventory_cate_list->query;
    
    $data['title'] = 'Inventory_Cate Database';
    $data['table_title'] = 'Inventory_Cate List';
    $data['main_content'] = 'mgmt_inventory/mgmt_inventory_cate_lib_view';  
      
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }	


  function index()
  {
     
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');

    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('mgmt_inventory_cate/lib');
  }	
  

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('mgmt_inventory_cate/lib');
  }
  

  function filter() 
  {
    if (($key = $this->uri->segment(3))
	&& 
	($value = $this->uri->segment(4))) 
      {	
	$filter = $this->session->userdata('filter');
	if (isset($filter[$key])):
	  unset($filter[$key]);
	else:
	  $filter[$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('mgmt_inventory_cate/lib');
  }

  function filter_main_cates() 
  {
    $filter = $this->session->userdata('filter');
    
    if (isset($filter['parent'])):
      unset($filter['parent']);
    else:
      $filter['parent'] = '(none)';
    endif;
    
    $this->session->set_userdata('filter', $filter);	

    redirect('mgmt_inventory_cate/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    redirect('mgmt_inventory_cate/lib');
  }
  

  function add()
  {  
    check_auth('mgmt_inventory_cate_ved');
    $inventory_cate = new stdClass;
    $inventory_cate->id 
      = $inventory_cate->name
      = '';
    $inventory_cate->order = 0;
    $inventory_cate->parent = '(none)';

    $data['category'] = $inventory_cate;
    
    $data['title'] = 'Add Inventory_Cate';
    $data['table_title'] = 'Inventory_Cate Detail';
    $data['main_content'] = 'mgmt_inventory/mgmt_inventory_cate_edit_view';  
    
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	
    
  }

	
  function edit()
  {  

    check_auth('mgmt_inventory_cate_ved');
    if (!($id = $this->uri->segment(3)))
      {
	redirect('mgmt_inventory_cate/add');
      }

    $this->load->model('inventory_cate_model');
      
    if ($data['category'] = $this->inventory_cate_model->load_by_id($id)) {
	
      $data['title'] = 'Edit Inventory_Cate';
      $data['table_title'] = 'Inventory_Cate Detail';
      $data['main_content'] = 'mgmt_inventory/mgmt_inventory_cate_edit_view';   
	
      $data['session_msg'] = $this->session->flashdata('msg');
      $data['cf_feslen'] = $this->config->item('cf_feslen');
      $this->load->view('includes/template', $data);	
    }
    
    
    
  }	
  
  function inventory_cate_submit()
  { 
    $this->load->model('inventory_cate_model');	
    if($id = $this->input->post('id'))
      {
	$this->inventory_cate_model->update($id);
	$msg = 'Inventory_Cate updated successfully.';
	$this->session->set_flashdata('msg', $msg);
	redirect('mgmt_inventory_cate/edit/'.$id);
      }
    else
      {
	$this->inventory_cate_model->add();
	$msg = 'Inventory_Cate added successfully.';
	$this->session->set_flashdata('msg', $msg);
	redirect('mgmt_inventory_cate');
      }
  }

  function del()
  { 

    if ($id = $this->uri->segment(3))
      {
	$this->inventory_cate_model->del($id);
	$msg = 'Inventory_Cate deleted successfully.';
	$this->session->set_flashdata('msg', $msg);
      }

    redirect('mgmt_inventory_cate');
    
  }

 
  /* 
  function tran()
  {
    $result = $this->db->get('inventory_cates')->result();

    foreach ($result as $row){
      echo $row->title;
      echo '<br />';
      $row->title = str_replace('title','inventory_cate',$row->title);
      $row->url_title = url_title($row->title, 'dash', TRUE); 

      $this->db->where('id', $row->id);
      
      $this->db->update('inventory_cates', $row);
    } 
  }
  */


}

/* End of file inventory_cate.php */
/* Location: ./application/controllers/inventory_cate.php */