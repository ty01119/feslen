<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mgmt_task extends CI_Controller 
{

  var $branch;

  function __construct()
  {
    parent::__construct();
    $this->branch = $this->session->userdata('branch');
    //$this->load->helper('auth');
    keep_logged_in();
    is_logged_in();
//    check_auth('epsom_v');      
    $this->load->model('mgmt_task_model');
  }

  function test()
  {
    echo '123';die;
  }
  
  function index()
  {
  
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    $this->session->set_userdata('staff_pre_press', 'Pre-press Staff...');
    $this->session->set_userdata('staff_production', 'Production Staff...');
    $this->session->set_userdata('task_branch', 'Branch...');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('mgmt_task/lib');

  }

  
  function my()
  {
  
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    $this->session->set_userdata('staff_pre_press', 'Pre-press Staff...');
    $this->session->set_userdata('staff_production', 'Production Staff...');
    $this->session->set_userdata('task_branch', 'Branch...');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    
    
    $filter[10]['staff_production'] = $this->session->userdata('user_name');
    $filter[10]['staff_pre_press'] = $this->session->userdata('user_name');
    $this->session->set_userdata('filter', $filter);	
	
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('mgmt_task/lib');

  } 
  
  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['staff_pre_press'] = $this->session->userdata('staff_pre_press');
    $data['staff_production'] = $this->session->userdata('staff_production');
    $data['task_branch'] = $this->session->userdata('task_branch');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('mgmt_task/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

 
    
    $mgmt_task_list = $this->mgmt_task_model->get_list($data['date'],
						       $data['staff_pre_press'],
						       $data['staff_production'],
						       $data['task_branch'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       $config['per_page'], 
						       $this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $mgmt_task_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $mgmt_task_list->query;
    
    $data['title'] = 'mgmt_task Statement';

    $data['main_content'] = 'mgmt_task/list_view';  
    $data['table_title'] = 'mgmt_task list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('mgmt_task/lib');
  }
  

  function filter() 
  { 
    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if (isset($filter[$flr_num][$key]) && $filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('mgmt_task/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($task_branch = $this->input->post('task_branch')) {      
      $this->session->set_userdata('task_branch', $task_branch);
    }
    if ($staff_pre_press = $this->input->post('staff_pre_press')) {      
      $this->session->set_userdata('staff_pre_press', $staff_pre_press);
    }
    if ($staff_production = $this->input->post('staff_production')) {      
      $this->session->set_userdata('staff_production', $staff_production);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('mgmt_task/lib');
  }
  


  function ajax_data($id)
  { 
    $json_obj = new stdClass;

    if ($id == 'new') {
      $mgmt_task = new stdClass;
      $mgmt_task->id = 'new';
      $mgmt_task->date = $mgmt_task->date_due = date("Y-m-d H:i:s");
      $mgmt_task->urgent = 0;
      $mgmt_task->branch = $this->branch;
      $mgmt_task->cust_name 
	= $mgmt_task->invo_id
	= $mgmt_task->staff_pre_press
	= $mgmt_task->file_name
	= $mgmt_task->bleed_btn
	= $mgmt_task->bleed_notes
	= $mgmt_task->sample_btn
	= $mgmt_task->sample_notes
	= $mgmt_task->image_btn
	= $mgmt_task->image_notes
	= $mgmt_task->text_btn
	= $mgmt_task->text_notes
	= $mgmt_task->printing_btn
	= $mgmt_task->printing_notes
	= $mgmt_task->printer
	= $mgmt_task->printer_notes
	= $mgmt_task->paper_btn
	= $mgmt_task->paper_notes
	= $mgmt_task->print_qty
	= $mgmt_task->print_size
	= $mgmt_task->print_notes
	= $mgmt_task->finish_qty
	= $mgmt_task->finish_size
	= $mgmt_task->finish_notes
	= $mgmt_task->finishing_btn
	= $mgmt_task->finishing_notes
	= $mgmt_task->delivery_btn
	= $mgmt_task->delivery_notes
	= $mgmt_task->staff_production
	= $mgmt_task->staff_production_notes
	= $mgmt_task->qa_btn
	= $mgmt_task->qa
	= $mgmt_task->qa_notes
	= $mgmt_task->status_btn
	= $mgmt_task->status_notes
	= '';

      $mgmt_task->staff_pre_press = $this->session->userdata('user_name');
      $mgmt_task->invo_id =  $this->uri->segment(4, '');
      
      $mgmt_task->status_btn = 'queued'; 

      $json_obj->data = $mgmt_task;

      echo json_encode($json_obj);
      
    } else {
     

      if ($mgmt_task = $this->mgmt_task_model->load_by_id($id)) {
	
	$json_obj->data = json_decode($mgmt_task->json);
	$json_obj->data->id = $mgmt_task->id;
	
	echo json_encode($json_obj);

      }      
    }

  }

  function view($id)
  {
 
    $mgmt_task = $this->mgmt_task_model->load_by_id($id);
 
    $task = json_decode($mgmt_task->json);
    $task->id = $mgmt_task->id;
    
    $data['task'] = $task;
 
    
    $this->load->view('mgmt_task/print_html_view', $data);

  }

  
  function edit()
  {
    //  check_auth('websolution_ve');
    if (!($id = $this->uri->segment(3)))
      {
	redirect('mgmt_task/add');
      }

    $data['json_url'] = site_url('mgmt_task/ajax_data/'.$id); 
    $data['cart_url'] = site_url('mgmt_task/ajax_cart'); 
    $data['post_url'] = site_url('mgmt_task/save'); 
    
    $data['main_content'] = 'mgmt_task/edit_view';  
    $data['frame_name'] = 'Edit mgmt_task';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
    
  }



  function add($id = "")
  {     
    $data['json_url'] = site_url('mgmt_task/ajax_data/new/'.$id); 
    //    $data['cart_url'] = site_url('mgmt_task/ajax_cart'); 
    $data['post_url'] = site_url('mgmt_task/save'); 

    $data['main_content'] = 'mgmt_task/edit_view';  
    $data['frame_name'] = 'Add mgmt_task';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	

  }


 
  function save()
  {

    $json_obj = json_decode($this->input->post('json')); 
    
   
    //    $json_obj->transaction->date_expected = date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $json_obj->transaction->date_expected)));
      
      
    $this->load->model('mgmt_task_model');
   
    if ($json_obj->transaction->id == 'new') {
     
      $this->mgmt_task_model->add($json_obj->transaction);
      $ret->msg =  'mgmt_task Created';
      $ret->close = TRUE;
      
    } else {
      $this->mgmt_task_model->update($json_obj->transaction);
      $ret->msg =  'mgmt_task Detail Saved';
      $ret->close = TRUE;
      
    }
    
    echo json_encode($ret);
    
  }

 

  function del_msg($id){


      $msg = 'Are you really want to delete [ mgmt_task #'.$id.' ] ?&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('mgmt_task/del/'.$id).'" class="black">Yes</a>';
      $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('mgmt_task/lib').'" class="black">No</a>';
      $this->session->set_flashdata('msg', $msg);
      redirect('mgmt_task/lib');	
  }

  function del($id){
    check_auth($this->branch.'_ved');

    $this->load->model('mgmt_task_model');
    $this->mgmt_task_model->mgmt_task_del($id);
    $msg = 'mgmt_task Deleted';
    $this->session->set_flashdata('msg', $msg);
    redirect('mgmt_task/lib');
  }


}