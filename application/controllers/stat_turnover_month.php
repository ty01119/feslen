<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stat_turnover_month extends CI_Controller 
{

  var $branch;

  function __construct()
  {
    parent::__construct();
    $this->branch = $this->session->userdata('branch');
    $this->load->helper('auth');
    is_logged_in();
  
  }

  
  function index()
  {
  
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    $this->session->set_userdata('customer', 'Customer..');
    $this->session->set_userdata('stat_branch', $this->branch);
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime('2012-01-01'));
    $this->session->set_userdata('date_to', strtotime('next sunday'));    
     $this->session->set_userdata('chart', 'monthly');
    $this->session->keep_flashdata('msg');
    redirect('stat_turnover_month/lib');

  }
    function monthly()
  {
  
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    $this->session->set_userdata('customer', 'Customer..');
    $this->session->set_userdata('stat_branch', $this->session->userdata('stat_branch'));
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime('2012-01-01'));
    $this->session->set_userdata('date_to', strtotime('next sunday'));    
     $this->session->set_userdata('chart', 'monthly');
    $this->session->keep_flashdata('msg');
    redirect('stat_turnover_month/lib');

  }
    
  
  function lib()
  {
    check_auth($this->session->userdata('stat_branch').'_ved');
    
    $data['orderby'] = $this->session->userdata('orderby');
        $data['chart'] = $this->session->userdata('chart');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['branch'] = $this->session->userdata('stat_branch');
    $data['search'] = $this->session->userdata('search');    $data['customer'] = $this->session->userdata('customer');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');
    

    
    $this->load->model('stat_model');
 
    if ( $data['chart'] == 'monthly') {
 
    $data['series1']=json_encode($this->get_monthly($data['branch'], '2011'));
     
    $data['series2']=json_encode($this->get_monthly($data['branch'], '2012'));
     
    $data['series3']=json_encode($this->get_monthly($data['branch'], '2013'));
     
    $data['series4']=json_encode($this->get_monthly($data['branch'], '2014'));
    

    } 
 

    $data['title'] = 'Statistical Charts';
   
    $data['main_content'] = 'statistics/stat_turnover_month_view';  
    $data['table_title'] = 'Turnover'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	
   
  }

  function get_monthly($branch, $year ){
    
    $date['fr'] =  strtotime($year.'-01-01');
    $totals = array();
    for ($i = 1; $i <= 12; $i++) {

    $date['to'] = strtotime('+ 1 month', $date['fr']);
    $result = $this->stat_model->turnover_by_branch($branch, $date, '', '','');  
	array_push( $totals, $result->total);
    $date['fr'] = $date['to'];
    }
    $rta = array();
    $rta['name']=$branch.' '.$year;
    $rta['data']= $totals;
    return $rta;
  }
  
  
 

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($branch = $this->input->post('branch')) {      
      $this->session->set_userdata('stat_branch', $branch);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('stat_turnover_month/lib');
  }
 
  

  

}