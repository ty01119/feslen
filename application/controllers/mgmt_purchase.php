<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mgmt_purchase extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    // $this->load->helper('date');
    $this->load->helper('auth');
    is_logged_in();
  check_auth('mgmt_inventory_v');
  }

  function test()
  {

    echo '123';die;
  }
  
  function index()
  {
 
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    
    $this->session->set_userdata('vendor', 'Vendor..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('mgmt_purchase/lib');

  }
     

  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['vendor'] = $this->session->userdata('vendor');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('mgmt_purchase/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

    $this->load->model('purchase_model');
    
    $purchase_list = $this->purchase_model->purchase_list($data['date'],
						       $data['vendor'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       $config['per_page'], 
						       $this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $purchase_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $purchase_list->query;
    
    $data['title'] = '';

    $data['main_content'] = 'mgmt_purchase_list_view';  
    $data['table_title'] = '3A Group Purchase list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }

  function report()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['vendor'] = $this->session->userdata('vendor');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = '';

   

    $this->load->model('purchase_model');
    
    $invoice_list = $this->purchase_model->purchase_list($data['date'],
						       $data['vendor'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       '', 
						       '');
    
    $data['total'] = $invoice_list->total;
    $data['query'] = $invoice_list->query;
    $data['title'] = 'Internal Transaction Statement';
    $data['table_title'] = 'Transaction list'; 
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('invoice_report_view', $data);	

  }

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('mgmt_purchase/lib');
  }
  

  function filter() 
  { 

    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if ($filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('mgmt_purchase/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($vendor = $this->input->post('vendor')) {      
      $this->session->set_userdata('vendor', $vendor);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('mgmt_purchase/lib');
  }
  

  function ajax_purchase($id)
  { 
    $json_obj = new stdClass;

    if ($id == 'new') {
      $purchase = new stdClass;
      $purchase->id = 'new';
      $purchase->date = date("Y-m-d H:i:s");
      $purchase->subtotal 
	= $purchase->tax 
	= $purchase->total 
	= $purchase->discount 
	= $purchase->paid 
	= $purchase->deleted = 0;
      $purchase->cust_name = '';
    

	$contents=array();
	$contents[0] 
	  = array('product' => '', 'quantity' => 1, 'unit_price' => 0, 'discount' => 0);
     

      $purchase->contents = json_encode($contents);

      $json_obj->transaction = $purchase;
      
      echo json_encode($json_obj);
      
    } else {
     
      $this->load->model('purchase_model');
      if ($purchase = $this->purchase_model->load_purchase($id)) {
	
	$json_obj->transaction = $purchase;
	
	echo json_encode($json_obj);

      }      
    }

  }


  function view($id)
  {
    $this->load->model('purchase_model');
    $purchase = $this->purchase_model->load_purchase($id);
    $contents = json_decode($purchase->contents);
    $purchase->date = date("d/m/y", strtotime($purchase->date));
    
    $this->load->model('vendor_model');
    $vendor = $this->vendor_model->load_by_name($purchase->cust_name);
    
    $purchase->branch = ($purchase->branch == 'group') ? 'epsom' : $purchase->branch;
    $this->load->model('branch_model');
    $branch = $this->branch_model->load_by_name($purchase->branch);

    $data['transaction'] = $purchase;
    $data['contents'] = $contents;
    $data['customer'] = $vendor;
    $data['branch'] =  $branch;
    $data['title'] = 'PURCHASE';
    $this->load->view('mgmt_purchase_html_view', $data);

  }


  function edit()
  {check_auth('mgmt_inventory_ve', FALSE);
    if (!($id = $this->uri->segment(3)))
      {
	redirect('mgmt_purchase/add');
      }

    $data['json_url'] = site_url('mgmt_purchase/ajax_purchase/'.$id); 
    $data['post_url'] = site_url('mgmt_purchase/save'); 
    $data['ajax_url'] = site_url('ajax/stock_names');

    $data['main_content'] = 'mgmt_purchase_edit_view';  
    $data['frame_name'] = 'Edit Purchase';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
    
  }



 function add()
  {check_auth('mgmt_inventory_ved', FALSE);
    $data['json_url'] = site_url('mgmt_purchase/ajax_purchase/new'); 
    $data['post_url'] = site_url('mgmt_purchase/save'); 
    $data['ajax_url'] = site_url('ajax/stock_names');

    $data['main_content'] = 'mgmt_purchase_edit_view';  
    $data['frame_name'] = 'Add Purchase';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	

  }

 function save()
 {
   $data['session_msg'] = 'Data Saved';
   $this->load->view('includes/session_msg', $data);

   $json_obj = json_decode($this->input->post('json')); 
   $this->load->model('purchase_model');

   if ($json_obj->transaction->id == 'new') {
     $this->purchase_model->purchase_add($json_obj->transaction
				       , $json_obj->contents);
   } else {
     $this->purchase_model->purchase_update($json_obj->transaction
					  , $json_obj->contents);
   }

 }


 
  function del(){
    echo 'Authorisation rejected.';
  }

}
