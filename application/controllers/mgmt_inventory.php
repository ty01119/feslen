<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mgmt_inventory extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    is_logged_in();
    $this->branch = $this->session->userdata('branch');
    $this->load->model('inventory_cate_model');
  }

  function low_stock()
  {
 
    $this->session->set_userdata('low_stock', TRUE);
    redirect('mgmt_inventory/lib');

  }

  function index()
  {
     check_auth('mgmt_inventory_v');	
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    $this->session->unset_userdata('category');
    $this->session->unset_userdata('low_stock');
    
    $this->session->set_userdata('low_stock', FALSE);
    $this->session->set_userdata('order', FALSE);
    $this->session->set_userdata('name', 'Name..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('mgmt_inventory/lib');

  }

  function order()
  {
         check_auth($this->branch.'_ve', FALSE);
    $data['cart'] = $this->session->userdata('cart1');
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    $this->session->unset_userdata('category');
    
    $this->session->set_userdata('order', TRUE);
    $this->session->set_userdata('name', 'Name..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('mgmt_inventory/order_lib');

  }
     

  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['name'] = $this->session->userdata('name');
    $data['search'] = $this->session->userdata('search');
    $data['category'] = $this->session->userdata('category');
    $data['low_stock'] = $this->session->userdata('low_stock');

    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');


    // start-cate: current main category and sub category 
    $data['cate_parent'] = $data['cate_sub'] = FALSE;
    if ($cate_obj = $this->inventory_cate_model->load_by_url($data['category']) ) 
      { 
	$data['cate_parent'] = $cate_obj->parent == '(none)' ? $cate_obj->name : $cate_obj->parent;
	$data['cate_sub'] = $cate_obj->parent == '(none)' ? FALSE : $cate_obj->name;
      }
    else 
      {
	$data['cate_parent'] = $data['cate_sub'] = FALSE;
      }

    $data['cate_tree'] = $this->inventory_cate_model->load_tree();
    // end-cate


    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('mgmt_inventory/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

    $this->load->model('inventory_model');
    
    $inventory_list = $this->inventory_model->inventory_list($data['category'],
						       $data['name'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       $config['per_page'], 
						       $this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $inventory_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $inventory_list->query;
    
    $data['title'] = '';

    $data['main_content'] = 'mgmt_inventory/mgmt_inventory_list_view';  
    $data['table_title'] = 'Stock list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }

  
  function order_lib()
  {

    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['name'] = $this->session->userdata('name');
    $data['search'] = $this->session->userdata('search');
    $data['category'] = $this->session->userdata('category');

    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');
    
    
    // start-cate: current main category and sub category 
    $data['cate_parent'] = $data['cate_sub'] = FALSE;
    if ($cate_obj = $this->inventory_cate_model->load_by_url($data['category']) ) 
      { 
	$data['cate_parent'] = $cate_obj->parent == '(none)' ? $cate_obj->name : $cate_obj->parent;
	$data['cate_sub'] = $cate_obj->parent == '(none)' ? FALSE : $cate_obj->name;
      }
    else 
      {
	$data['cate_parent'] = $data['cate_sub'] = FALSE;
      }

    $data['cate_tree'] = $this->inventory_cate_model->load_tree();
    // end-cate
    
    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('mgmt_inventory/order_lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;
    
    $this->load->model('inventory_model');
    
    $inventory_list = $this->inventory_model->inventory_list($data['category'],
						       $data['name'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       $config['per_page'], 
						       $this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $inventory_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $inventory_list->query;
    
    $data['title'] = '';

    $data['main_content'] = 'mgmt_inventory/mgmt_inventory_order_view';  
    $data['table_title'] = 'Stock list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }

 
  function report()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['name'] = $this->session->userdata('name');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = '';

   

    $this->load->model('inventory_model');
    
    $invoice_list = $this->inventory_model->inventory_list($data['date'],
						       $data['name'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       '', 
						       '');
    
    $data['total'] = $invoice_list->total;
    $data['query'] = $invoice_list->query;
    $data['title'] = 'Internal Transaction Statement';
    $data['table_title'] = 'Transaction list'; 
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('invoice_report_view', $data);	

  }

  function order_submit()
  {
   echo $this->input->post('name');
   echo $this->input->post('price'); 
   echo $this->input->post('qty'); 
   
  }

  function cart()
  { 
    $cart =  $this->session->userdata('cart1');
    if ($this->input->post('name'))
    {
    $data = array(
		  'product'      => $this->input->post('name'),
		  'quantity'     => $this->input->post('qty'),
		  'unit_price'   => $this->input->post('price'),
		  'discount'    => 0
		  );
    
    array_push($cart, $data);
    $this->session->set_userdata('cart1', $cart);
    }
    
    echo json_encode($cart);
  }

  function reset()
  {

    $cart = array();
    $this->session->set_userdata('cart1', $cart);
	
  }


  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }

    if ($this->session->userdata('order'))      
      redirect('mgmt_inventory/order_lib');
    else 
      redirect('mgmt_inventory/lib');
  }
  
  function cate()
  {
  
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');
    $this->session->unset_userdata('category');    
  
    if ($url_title = $this->uri->segment(3))
      {
	$this->session->set_userdata('category', $url_title);
      }    
    if ($this->session->userdata('order'))      
      redirect('mgmt_inventory/order_lib');
    else 
      redirect('mgmt_inventory/lib');
  }
  




  function filter() 
  { 

    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if ($filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    if ($this->session->userdata('order'))      
      redirect('mgmt_inventory/order_lib');
    else 
      redirect('mgmt_inventory/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($name = $this->input->post('name')) {      
      $this->session->set_userdata('name', $name);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    if ($this->session->userdata('order'))      
      redirect('mgmt_inventory/order_lib');
    else 
      redirect('mgmt_inventory/lib');
  }
  

  function ajax_inventory($id)
  { 
    $json_obj = new stdClass;

    if ($id == 'new') {
      $inventory = new stdClass;
      $inventory->id = 'new';
      $inventory->name = '';
      $inventory->deleted 
	= $inventory->price = 0;
      $inventory->qty = 1;
      $inventory->low_stock = 100;
      $inventory->pur_id = $inventory->prod_id = '';
      $inventory->category = 'Others';
    $inventory->branch = 'epsom';

      $json_obj->transaction = $inventory;
      
      echo json_encode($json_obj);
      
    } else {
     
      $this->load->model('inventory_model');
      if ($inventory = $this->inventory_model->load_inventory($id)) {
	$json_obj->transaction = $inventory;
	
	echo json_encode($json_obj);

      }      
    }

  }


  function view($id)
  {
    if (!($id = $this->uri->segment(3)))
      {
	redirect('mgmt_inventory/add');
      }

    $data['json_url'] = site_url('mgmt_inventory/ajax_inventory/'.$id); 
    $data['post_url'] = site_url('mgmt_inventory/save'); 
    $data['read_only'] = 1;

    $data['main_content'] = 'mgmt_inventory/mgmt_inventory_edit_view';  
    $data['frame_name'] = 'Edit Stock';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	

  }


  function edit()
  {    
  check_auth('mgmt_inventory_ve', FALSE);
    if (!($id = $this->uri->segment(3)))
      {
	redirect('mgmt_inventory/add');
      }

    $data['json_url'] = site_url('mgmt_inventory/ajax_inventory/'.$id); 
    $data['post_url'] = site_url('mgmt_inventory/save'); 
    $data['read_only'] = 0;

    $data['main_content'] = 'mgmt_inventory/mgmt_inventory_edit_view';  
    $data['frame_name'] = 'Edit Stock';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
    
  }



 function add()
  {
      check_auth('mgmt_inventory_ved', FALSE);
    $data['json_url'] = site_url('mgmt_inventory/ajax_inventory/new'); 
    $data['post_url'] = site_url('mgmt_inventory/save'); 
    $data['read_only'] = 0;

    $data['main_content'] = 'mgmt_inventory/mgmt_inventory_edit_view';  
    $data['frame_name'] = 'Add Stock';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	

  }

 function save()
 { 
   $data['session_msg'] = 'Data Saved';
   $this->load->view('includes/session_msg', $data);

   $json_obj = json_decode($this->input->post('json')); 
   $this->load->model('inventory_model');

   if ($json_obj->transaction->id == 'new') {
     $this->inventory_model->inventory_add($json_obj->transaction);
   } else {
     $this->inventory_model->inventory_update($json_obj->transaction);
   }

 }



  function del_msg($id){
      $msg = 'Are you really want to delete [ STOCK #'.$id.' ] ?&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('mgmt_inventory/del/'.$id).'" class="black">Yes</a>';
      $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('mgmt_inventory/lib').'" class="black">No</a>';
      $this->session->set_flashdata('msg', $msg);
      redirect('mgmt_inventory/lib');	
  }

  function del($id){
      check_auth('mgmt_inventory_ved', FALSE);

    $this->load->model('inventory_model');
    $this->inventory_model->del($id);
    $msg = 'Stock Deleted';
    $this->session->set_flashdata('msg', $msg);
    redirect('mgmt_inventory/lib');
  }

}