<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Login extends CI_Controller
{

	var $cf_feslen;

  function __construct()
  {    
    parent::__construct();	
    $this->load->helper('cookie');    	
    session_start();
    $this->cf_feslen = $this->config->item('cf_feslen');  
  }
 
  function index()
  { 
    if ($this->session->userdata('is_logged_in')) redirect('panel');
    	
    $data['title'] = 'System Panel';

    $data['main_content'] = 'login_view';  

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);		
  }
  
 
  function validate_login()
  {
    $this->load->model('user_model');
    
    if ($user = $this->user_model->login())
      {

    	$office_ips = $this->cf_feslen['office_ips'];
 
    	$user_ip = $this->input->ip_address();
      // keep logged in save cookie
	if ($this->input->post('keepsignin') && in_array($user_ip, $office_ips)) 
	  {	     
	    $data = array(
				   'name'   => 'passwd',
				   'value'  => $user->password,
				   'expire' => 3600*24*14
				   );
	    set_cookie($data); 
	    $data = array(
				  'name'   => 'uid',
				  'value'  => $user->working_id,
				  'expire' => 3600*24*14
				  );
	    set_cookie($data); 
	  }
	
 

	$data = array(
		      'is_logged_in' => TRUE,
		      'check_in' => $this->user_model->check_attn($user->working_id),
		      'login_id'  => $user->working_id,
		      'authority' => $user->authority,
		      'branch'    => $user->branch,
		      'user_name' => $user->name,
		      'user_position' => $user->position,
		      'working_time' => $user->working_time,
		      'remote_login' => $user->remote_login,
		      'cart' => array(),	      
		      'cart1' => array()	      
		      );

	$this->session->set_userdata($data);

        $msg = 'Logged in successfully.';
	$this->session->set_flashdata('msg', $msg);
	redirect('panel');
      }
    else
      {
	$msg = 'Sorry, we could not log you in with that login ID and password.';
	$this->session->set_flashdata('msg', $msg);
	redirect('login');
      }
  }
  
 
  function logout()
  {
    $this->session->sess_destroy();
    session_destroy();
    delete_cookie('uid');    delete_cookie('passwd');
    redirect('login');	
  }  


}
