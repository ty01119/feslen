<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class User extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    is_logged_in();
  }
 
  function index()
  {
    $data['title'] = 'Administration Panel';

    $data['main_content'] = 'form_User';  

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);		
  }
  
  function check_in()
  {
	$this->load->model('user_model');		

	if($this->user_model->check_in())
	  {
	    $this->session->set_userdata('check_in', TRUE);
	    $msg = 'Checked in successfully, Have a nice day!';
	    $this->session->set_flashdata('msg', $msg);
	  }	
	 redirect('attendance');	
  }
  
   function check_out()
  {
	$this->load->model('user_model');		

	if($this->user_model->check_out())
	  {
	    $this->session->unset_userdata('is_logged_in');
	    $msg = 'Checked out successfully, Good Bye!';
	    $this->session->set_flashdata('msg', $msg);
	  }		
    redirect('login');
  }
  
  function change_password()
  {
    $data['title'] = 'Administration Panel';
    $data['main_content'] = 'login_password_view';  
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);		
  }

 
  
  function validate_change_password()
  {
    $this->load->library('form_validation');
		
    $this->form_validation->set_rules('password_old', 'Current password', 'trim|required|min_length[4]|max_length[32]');
    $this->form_validation->set_rules('password_new', 'New password', 'trim|required|min_length[4]|max_length[32]');
    $this->form_validation->set_rules('password_rep', 'Password Confirmation', 'trim|required|matches[password_new]');
    
    
    if(!$this->form_validation->run())
      {
	$msg = validation_errors();
	$this->session->set_flashdata('msg', $msg);
	redirect('user/change_password');
      }
    
    else
      {			
	
	$this->load->model('user_model');		

	if($this->user_model->change_password())
	  {
	    $msg = 'Password changed successfully.';
	    $this->session->set_flashdata('msg', $msg);
	    redirect('user/change_password');
	  }
	else
	  {
	    $msg = 'Password could not be changed.';
	    $this->session->set_flashdata('msg', $msg);
	    redirect('user/change_password');
	  }

      }
  }

  function edit_profile()
  {
    $data['json_url'] = site_url('user/ajax_detail'); 
    $data['post_url'] = site_url('user/save'); 
    $data['read_only'] = 0;
    
    $data['main_content'] = 'user_edit_view';  
    $data['frame_name'] = 'Edit Profile';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
    
  }

function ajax_detail()
  {

    $id = $this->session->userdata('login_id');
    $json_obj = new stdClass;
 
      $this->load->model('staff_model');
      
      if ($staff = $this->staff_model->load_personnel($id)) {
	
	$json_obj->staff = $staff;
	
	echo json_encode($json_obj);
      }
    
 
  }


  function save()
  {
    $data['session_msg'] = 'Information Saved';
    $this->load->view('includes/session_msg', $data);
   
    $json_obj = json_decode($this->input->post('json')); 

	 
    $json_obj->staff->date_of_cmnc 
      = date("Y-m-d", strtotime(str_replace('/', '-', $json_obj->staff->date_of_cmnc)));

    $json_obj->staff->date_of_birth 
      = date("Y-m-d", strtotime(str_replace('/', '-', $json_obj->staff->date_of_birth)));   
   
    $this->load->model('staff_model');

    $this->staff_model->personnel_update($json_obj->staff);
   
  }


}
