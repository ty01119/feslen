<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leads extends CI_Controller 
{

  var $branch;

  function __construct()
  {
    parent::__construct();
    $this->branch = $this->session->userdata('branch');
    //$this->load->helper('auth');
    keep_logged_in();
    is_logged_in();
    //
    /*
    if (!pass_auth('penrose_ve') && !pass_auth('manukau_ve')) {
	$msg = 'Authorisation rejected. not 4 you yet OvO';
      	$this->session->set_flashdata('msg', $msg);
      	redirect('panel');
      	}
      	*/
    //
    $this->load->model('leads_model');
  }

  function test()
  {
    echo '123';die;
  }
  
  function index()
  {
  
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    $this->session->set_userdata('lead_owner', 'Lead Owner...');
    $this->session->set_userdata('lead_follow', 'Lead Follow...');
  //  $this->session->set_userdata('task_branch', 'Branch...');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    
    $filter[1]['deleted'] = 0;
    $this->session->set_userdata('filter', $filter);

	
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('leads/lib');

  }

  
  function my()
  {
 
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    $this->session->set_userdata('lead_owner', 'Lead Owner...');
    $this->session->set_userdata('lead_follow', 'Lead Follow...');
 //   $this->session->set_userdata('task_branch', 'Branch...');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    
 
    $filter[1]['deleted'] = 0; 
    $filter[10]['follow'] = $this->session->userdata('user_name');
    $filter[10]['owner'] = $this->session->userdata('user_name');
    $this->session->set_userdata('filter', $filter);	
	
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('leads/lib');

  } 
  
  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['lead_owner'] = $this->session->userdata('lead_owner');
    $data['lead_follow'] = $this->session->userdata('lead_follow');
 //   $data['task_branch'] = $this->session->userdata('task_branch');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('leads/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

 
    
    $list = $this->leads_model->get_list($data['date'],
						       $data['lead_owner'],
						       $data['lead_follow'],
						      
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       $config['per_page'], 
						       $this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $list->query;
    
    $data['title'] = 'leads Statement';
    
    

    $data['main_content'] = 'leads/list_view';  
    $data['table_title'] = 'leads list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('leads/lib');
  }
  

  function filter() 
  { 
    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if (isset($filter[$flr_num][$key]) && $filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('leads/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    /*
    if ($task_branch = $this->input->post('task_branch')) {      
      $this->session->set_userdata('task_branch', $task_branch);
    }
    */
    if ($lead_owner = $this->input->post('lead_owner')) {      
      $this->session->set_userdata('lead_owner', $lead_owner);
    }
    if ($lead_follow = $this->input->post('lead_follow')) {      
      $this->session->set_userdata('lead_follow', $lead_follow);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('leads/lib');
  }
  


  function ajax_data($id)
  { 
    $json_obj = new stdClass;

    if ($id == 'new') {
      $leads = new stdClass;
      $leads->id = 'new';
      $leads->date = $leads->reminder_date = date("Y-m-d");
      
      $leads->owner 
	= $leads->follow
	=  $this->session->userdata('user_name');
	
	$leads->reminder_notes
	= $leads->status_btn
	= $leads->company
	= $leads->client_contact
	= $leads->client_email
	= $leads->client_phone
	= $leads->client_address
	= $leads->client_note
	= $leads->quote_id
	= $leads->invoice_id
	= $leads->customer_id
	= $leads->logs
	= $leads->json
	= '';
  
      $leads->deleted = 0;
      $leads->status_btn = 'new';  

      $json_obj->data = $leads;

      echo json_encode($json_obj);
      
    } else {
     

      if ($leads = $this->leads_model->load_by_id($id)) {
	
	$json_obj->data = json_decode($leads->json);
	$json_obj->data->id = $leads->id;
	
	echo json_encode($json_obj);

      }      
    }

  }

  function view($id)
  {
 
    $leads = $this->leads_model->load_by_id($id);
 
    $task = json_decode($leads->json);
    $task->id = $leads->id;
    
    $data['task'] = $task;
 
    
    $this->load->view('leads/print_html_view', $data);

  }

  
  function edit()
  {
    //  check_auth('websolution_ve');
    if (!($id = $this->uri->segment(3)))
      {
	redirect('leads/add');
      }

    $data['json_url'] = site_url('leads/ajax_data/'.$id); 
    $data['cart_url'] = site_url('leads/ajax_cart'); 
    $data['post_url'] = site_url('leads/save'); 
    
    $data['main_content'] = 'leads/edit_view';  
    $data['frame_name'] = 'Edit leads';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
    
  }



  function add($id = "")
  {     
    $data['json_url'] = site_url('leads/ajax_data/new/'.$id); 
    //    $data['cart_url'] = site_url('leads/ajax_cart'); 
    $data['post_url'] = site_url('leads/save'); 

    $data['main_content'] = 'leads/edit_view';  
    $data['frame_name'] = 'Add leads';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	

  }


 
  function save()
  {

    $json_obj = json_decode($this->input->post('json')); 
    
   
    //    $json_obj->transaction->date_expected = date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $json_obj->transaction->date_expected)));
      
      
    $this->load->model('leads_model');
   
    if ($json_obj->transaction->id == 'new') {
     
      $this->leads_model->add($json_obj->transaction);
      $ret->msg =  'leads Created';
      $ret->close = TRUE;
      
    } else {
      $this->leads_model->update($json_obj->transaction);
      $ret->msg =  'leads Detail Saved';
      $ret->close = TRUE;
      
    }
    
    echo json_encode($ret);
    
  }

 

  function del_msg($id){


      $msg = 'Are you really want to delete [ leads #'.$id.' ] ?&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('leads/del/'.$id).'" class="black">Yes</a>';
      $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('leads/lib').'" class="black">No</a>';
      $this->session->set_flashdata('msg', $msg);
      redirect('leads/lib');	
  }

  function del($id){
    check_auth($this->branch.'_ved');

    $this->load->model('leads_model');
    $this->leads_model->del($id);
    $msg = 'leads Deleted';
    $this->session->set_flashdata('msg', $msg);
    redirect('leads/lib');
  }


 
}