<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Files_sop extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    is_logged_in();
  
  }
 
  
  function index()
  {
  
    $this->load->helper('directory');
    
    $data['main_content'] = 'files/files_sop_view';  
    $data['table_title'] = 'Standard Operating Procedure Files'; 
    
    $array = directory_map('../feslen_files/SOP/');
    $this->_sort_tree($array);
   
    $data['files'] = $array;
    $data['base_url'] = dirname(base_url()).'/feslen_files/SOP/';

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }
  

  
    function _sort_tree( &$array )
  {
  	  if (!is_array($array)) return false; 
	 
	  ksort($array);
	  
	  $tmp = array();
	  foreach($array as $key => $value){
	       if (!is_array($value)) {
	          array_push($tmp, $value);
	          unset($array[$key]);
	       }
	  } 
	  
	  sort($tmp);
  
    	  foreach($tmp as $value) array_push($array, $value);
	   
	  foreach ($array as $k=>$v) $this->_sort_tree($array[$k]);
 
	  return true;
  }
  
   


}