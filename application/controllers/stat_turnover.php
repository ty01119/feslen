<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stat_turnover extends CI_Controller 
{

  var $branch;

  function __construct()
  {
    parent::__construct();
    $this->branch = $this->session->userdata('branch');
    $this->load->helper('auth');
    is_logged_in();
  
  }

  function test()
  {
    echo '123';die;
  }
  
  function index()
  {
  
    $this->session->unset_userdata('stat_turnover_orderby');
    $this->session->unset_userdata('stat_turnover_filter'); 
    
    $this->session->set_userdata('stat_turnover_customer', 'Customer..');
    $this->session->set_userdata('stat_turnover_search', 'Search..');
    $this->session->set_userdata('stat_turnover_date_fr', strtotime('last sunday'));
    $this->session->set_userdata('stat_turnover_date_to', strtotime('next sunday'));    
 
    $this->session->keep_flashdata('msg');
    redirect('stat_turnover/lib');

  }
     
  
  function lib()
  {
    $data['orderby'] = $this->session->userdata('stat_turnover_orderby');
    $data['filter'] = $this->session->userdata('stat_turnover_filter'); 
    $data['customer'] = $this->session->userdata('stat_turnover_customer');
    $data['search'] = $this->session->userdata('stat_turnover_search');
    $data['date']['fr'] = $this->session->userdata('stat_turnover_date_fr');
    $data['date']['to'] = $this->session->userdata('stat_turnover_date_to');
    $data['per_page'] = $this->session->userdata('stat_turnover_per_page');

    $this->load->model('stat_model');
      
    $stat_turnover = $this->stat_model->turnover($data['date'],
						 $data['customer'],
						 $data['filter'], 
						 $data['search']);  

    $data['turnover'] = $stat_turnover;
    
    $data['turnover_copy'] = $this->stat_model->turnover_by_biz('3A Copy & Design',
    						 $data['date'],
						 $data['customer'],
						 $data['filter'], 
						 $data['search']); 
						 
    $data['turnover_signs'] = $this->stat_model->turnover_by_biz('3A Signs LTD',
    						 $data['date'],
						 $data['customer'],
						 $data['filter'], 
						 $data['search']); 
						 
    $data['turnover_web'] = $this->stat_model->turnover_by_biz('3A web solution',
    						 $data['date'],
						 $data['customer'],
						 $data['filter'], 
						 $data['search']); 
						 
						 
    $data['title'] = 'Turnover';
   
    $data['main_content'] = 'statistics/stat_turnover_view';  
    $data['table_title'] = 'Turnover'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	
   
  }


  function filter() 
  { 
    
    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('stat_turnover_per_page') == 25)
	$this->session->set_userdata('stat_turnover_per_page', $per_page);
      else 
	$this->session->set_userdata('stat_turnover_per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('stat_turnover_filter');
	if (isset($filter[$flr_num][$key]) && $filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('stat_turnover_filter', $filter);	
      }
    redirect('stat_turnover/lib');
  }
  

  function search() 
  {
      check_auth('mgmt_statistics');
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('stat_turnover_search', $search);
    }
    if ($customer = $this->input->post('customer')) {      
      $this->session->set_userdata('stat_turnover_customer', $customer);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('stat_turnover_date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('stat_turnover_date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('stat_turnover/lib');
  }
  
  

  

}
