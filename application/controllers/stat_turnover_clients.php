<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stat_turnover_clients extends CI_Controller 
{

  var $branch;

  function __construct()
  {
    parent::__construct();
    $this->branch = $this->session->userdata('branch');
    $this->load->helper('auth');
    is_logged_in();
  
  }

  function test()
  {
    echo '123';die;
  }
  
  function index()
  {
  
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    
    $this->session->set_userdata('customer', 'Customer..');
    $this->session->set_userdata('stat_branch', $this->branch);
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime('2011-01-01'));
    $this->session->set_userdata('date_to', strtotime('next sunday'));    
 
    $this->session->keep_flashdata('msg');
    redirect('stat_turnover_clients/lib');

  }
     
  
  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['customer'] = $this->session->userdata('customer');
    $data['branch'] = $this->session->userdata('stat_branch');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->model('stat_model');
      
    $stat_turnover_clients = $this->stat_model->turnover_clients($data['date'],$data['branch'] );  


  //  $data['turnover_clients'] = $stat_turnover_clients; 
    $data['stat_data'] =  $stat_turnover_clients;
						 
    $data['title'] = 'Turnover by turnover_clients';
   
    $data['main_content'] = 'statistics/stat_turnover_clients_view';  
    $data['table_title'] = 'Turnover by turnover_clients'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
   $this->load->view('includes/template', $data);	
   
  }


  function filter() 
  { 
    
    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if (isset($filter[$flr_num][$key]) && $filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('stat_turnover_clients/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($branch = $this->input->post('branch')) {      
      $this->session->set_userdata('stat_branch', $branch);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('stat_turnover_clients/lib');
  }
  
  

  

}
