<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stat_cost extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    is_logged_in();
    check_auth('mgmt_fina_v');
  }

  
  function index()
  {
  
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    
    $this->session->set_userdata('obj_branch', 'Branch..');
    $this->session->set_userdata('search', 'Search..');
	
    $filter[1]['deleted'] = 0;
    $this->session->set_userdata('filter', $filter);
     $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());  
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'date';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('stat_cost/lib');

  }
     
  
  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['obj_branch'] = $this->session->userdata('obj_branch');
    $data['search'] = $this->session->userdata('search');

    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');

 

    $this->load->model('cost_model');
    
 
 
    $data['table'] = $this->cost_model->load_table( $data['date'] );
    
 
$this->load->library('table');

 
 
    
    $data['title'] = 'Domain List';

    $data['main_content'] = 'mgmt_cost/stat_cost_list_view';  
    $data['table_title'] = 'Domain list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }



  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('stat_cost/lib');
  }
  

  function filter() 
  { 

    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if (isset($filter[$flr_num][$key]) && $filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('stat_cost/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($obj_branch = $this->input->post('obj_branch')) {      
      $this->session->set_userdata('obj_branch', $obj_branch);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('stat_cost/lib');
  }
  
 

 
 
 
  
 
  

}
