<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Web_app_domain extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    is_logged_in();
    check_auth('mgmt_web_v');
  }

  function test()
  {

    echo '123';die;
  }
  
  function index()
  {
  
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    
    $this->session->set_userdata('customer', 'Customer..');
    $this->session->set_userdata('search', 'Search..');
	
    $filter[1]['deleted'] = 0;
    $this->session->set_userdata('filter', $filter);
 
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'due_date';
    $orderby['sort'] = 'asc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('web_app_domain/lib');

  }
     
  
  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['customer'] = $this->session->userdata('customer');
    $data['search'] = $this->session->userdata('search');
 
 
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('web_app_domain/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

    $this->load->model('web_app_domain_model');
    
    $domain_list = $this->web_app_domain_model->domain_list('',
							    $data['customer'],
							    $data['filter'], 
							    $data['search'], 
							    $data['orderby'], 
							    $config['per_page'], 
							    $this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $domain_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $domain_list->query;
    
    $data['title'] = 'Domain List';

    $data['main_content'] = 'web_app_domain_list_view';  
    $data['table_title'] = 'Domain list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }



  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('web_app_domain/lib');
  }
  

  function filter() 
  { 

    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if (isset($filter[$flr_num][$key]) && $filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('web_app_domain/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($customer = $this->input->post('customer')) {      
      $this->session->set_userdata('customer', $customer);
    }
 
    redirect('web_app_domain/lib');
  }
  
 

  function ajax_domain($id)
  { 
    $json_obj = new stdClass;

    if ($id == 'new') {
      $domain = new stdClass;
      $domain->id = 'new';
      $domain->due_date = date("Y-m-d", strtotime("+1 year"));
      $domain->url 
	= $domain->registrar
	= $domain->host
	= $domain->cust_name
	= $domain->passwords
	= $domain->mgmt_notes = '';
    
      $json_obj->domain = $domain;

      echo json_encode($json_obj);
      
    }  else {
     
      $this->load->model('web_app_domain_model');
      if ($domain = $this->web_app_domain_model->load_domain($id)) {
	$json_obj->domain = $domain;
		
	echo json_encode($json_obj); 
      }      
    }

  }



  function view($id)
  {

    $data['json_url'] = site_url('web_app_domain/ajax_domain/'.$id); 
    $data['post_url'] = site_url('web_app_domain/save'); 
    $data['read_only'] = 1;

    $data['main_content'] = 'web_app_domain_edit_view';  
    $data['frame_name'] = 'View Domain';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);

  }

  
  function edit()
  {
  check_auth('mgmt_web_ve');
    if (!($id = $this->uri->segment(3)))
      {
	redirect('web_app_domain/add');
      }

    $data['json_url'] = site_url('web_app_domain/ajax_domain/'.$id); 
    $data['post_url'] = site_url('web_app_domain/save'); 
    $data['read_only'] = 0;

    $data['main_content'] = 'web_app_domain_edit_view';  
    $data['frame_name'] = 'Edit Domain';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
    
  }



  function add()
  {
check_auth('mgmt_web_ve');
    $data['json_url'] = site_url('web_app_domain/ajax_domain/new'); 
    $data['post_url'] = site_url('web_app_domain/save'); 
    $data['read_only'] = 0;

    $data['main_content'] = 'web_app_domain_edit_view';  
    $data['frame_name'] = 'Add web_app_domain';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	

  }

 
  function save()
  {
check_auth('mgmt_web_ve');
    $data['session_msg'] = 'Detail Saved';
    $this->load->view('includes/session_msg', $data);

    $json_obj = json_decode($this->input->post('json')); 


    $json_obj->domain->due_date
      = date("Y-m-d", strtotime(str_replace('/', '-', $json_obj->domain->due_date)));

    $this->load->model('web_app_domain_model');

    if ($json_obj->domain->id == 'new') {
      $this->web_app_domain_model->domain_add($json_obj->domain);
    } else {
      $this->web_app_domain_model->domain_update($json_obj->domain);
    }

  }


 
  function del_msg($id){
check_auth('mgmt_web_ved');
    $this->load->model('web_app_domain_model');
    if ($domain = $this->web_app_domain_model->load_domain($id)) {
      $json_obj->domain = $domain;
      $msg = 'Are you really want to delete [ '.$domain->url.' ] ?&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('web_app_domain/del/'.$id).'" class="black">Yes</a>';
      $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('web_app_domain/lib').'" class="black">No</a>';
      $this->session->set_flashdata('msg', $msg);
      redirect('web_app_domain/lib');	
 
    }      


  }

  function del($id){
check_auth('mgmt_web_ved');
    $this->load->model('web_app_domain_model');
    $this->web_app_domain_model->domain_del($id);
    $msg = 'Item Deleted';
    $this->session->set_flashdata('msg', $msg);
    redirect('web_app_domain/lib');
  }

}
