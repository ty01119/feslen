<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Intertrans extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->branch = $this->session->userdata('branch');
    // $this->load->helper('date');
    $this->load->helper('auth');    
    keep_logged_in();
    is_logged_in();
  
  }

  function test()
  {

    echo '123';die;
  }
  
  function index()
  {
 
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    
    $this->session->set_userdata('customer', 'Customer..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('intertrans/lib');

  }
     

  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['customer'] = $this->session->userdata('customer');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('intertrans/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

    $this->load->model('intertrans_model');
    
    $intertrans_list = $this->intertrans_model->intertrans_list($data['date'],
						       $data['customer'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       $config['per_page'], 
						       $this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $intertrans_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $intertrans_list->query;
    
    $data['title'] = '';

    $data['main_content'] = 'intertrans_list_view';  
    $data['table_title'] = 'Internal Transaction list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }

  function report()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['customer'] = $this->session->userdata('customer');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = '';

   

    $this->load->model('intertrans_model');
    
    $invoice_list = $this->intertrans_model->intertrans_list($data['date'],
						       $data['customer'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       '', 
						       '');
    
    $data['total'] = $invoice_list->total;
    $data['query'] = $invoice_list->query;
    $data['title'] = 'Internal Transaction Statement';
    $data['table_title'] = 'Transaction list'; 
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('invoice_report_view', $data);	

  }

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('intertrans/lib');
  }
  

  function filter() 
  { 

    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if ($filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('intertrans/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($customer = $this->input->post('customer')) {      
      $this->session->set_userdata('customer', $customer);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('intertrans/lib');
  }
  

  function ajax_intertrans($id)
  { 
    $json_obj = new stdClass;

    if ($id == 'new') {
      $intertrans = new stdClass;
      $intertrans->id = 'new';
      $intertrans->date = date("Y-m-d H:i:s");
      $intertrans->subtotal 
	= $intertrans->tax 
	= $intertrans->total 
	= $intertrans->discount 
	= $intertrans->paid 
	= $intertrans->deleted = 0;
      $intertrans->cust_name = '';
    
      if ($cart = $this->session->userdata('cart')) {
	$contents = $cart;
      } else {
	$contents=array();
	$contents[0] 
	  = array('product' => '', 'quantity' => 1, 'unit_price' => 0, 'discount' => 0);
      }      

      $intertrans->contents = json_encode($contents);

      $json_obj->transaction = $intertrans;
      
      echo json_encode($json_obj);
      
    } else {
     
      $this->load->model('intertrans_model');
      if ($intertrans = $this->intertrans_model->load_intertrans($id)) {
	
	$json_obj->transaction = $intertrans;
	
	echo json_encode($json_obj);

      }      
    }

  }


  function view($id)
  {
    $this->load->model('intertrans_model');
    $intertrans = $this->intertrans_model->load_intertrans($id);
    $contents = json_decode($intertrans->contents);
    $intertrans->date = date("d/m/y", strtotime($intertrans->date));
    
    $this->load->model('customer_model');
    $customer = $this->customer_model->load_by_name($intertrans->cust_name);
    
    
    $this->load->model('branch_model');
    $branch = $this->branch_model->load_by_name($this->session->userdata('branch'));

    $data['transaction'] = $intertrans;
    $data['contents'] = $contents;
    $data['customer'] = $customer;
    $data['branch'] =  $branch;
    $data['title'] = 'INTERNEL TRANSACION';
    $this->load->view('transaction_html_view', $data);

  }



  function pdf($id)
  {
    $this->load->model('intertrans_model');
    $intertrans = $this->intertrans_model->load_intertrans($id);
    $contents = json_decode($intertrans->contents);
    $intertrans->date = date("d/m/y", strtotime($intertrans->date));
    
    $this->load->model('customer_model');
    $customer = $this->customer_model->load_by_name($intertrans->cust_name);
    
    
    $this->load->model('branch_model');
    $branch = $this->branch_model->load_by_name($this->session->userdata('branch'));

    $data['transaction'] = $intertrans;
    $data['contents'] = $contents;
    $data['customer'] = $customer;
    $data['branch'] =  $branch;
    $data['title'] = 'INTERNEL TRANSACION';
 
    
    	$this->load->helper('dompdf');
    $html = $this->load->view('transaction_pdf_view', $data, true);
    pdf_create($html, 'internal#'.$intertrans->id); 

  }
  
  
  function edit()
  {
    check_auth($this->branch.'_ve', FALSE);
    if (!($id = $this->uri->segment(3)))
      {
	redirect('intertrans/add');
      }

    $data['json_url'] = site_url('intertrans/ajax_intertrans/'.$id); 
    $data['post_url'] = site_url('intertrans/save'); 
    
    $data['main_content'] = 'intertrans_edit_view';  
    $data['frame_name'] = 'Edit Intertrans';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
    
  }



 function add()
  {
    check_auth($this->branch.'_ve', FALSE);
    $data['json_url'] = site_url('intertrans/ajax_intertrans/new'); 
    $data['post_url'] = site_url('intertrans/save'); 

    $data['main_content'] = 'intertrans_edit_view';  
    $data['frame_name'] = 'Add Internal Transaction';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	

  }
 function save()
 {
    check_auth($this->branch.'_ve', FALSE);
   $data['session_msg'] = 'Intertrans Saved';
   $this->load->view('includes/session_msg', $data);

   $json_obj = json_decode($this->input->post('json')); 
   $this->load->model('intertrans_model');

   if ($json_obj->transaction->id == 'new') {
     $this->intertrans_model->intertrans_add($json_obj->transaction
				       , $json_obj->contents);
   } else {
     $this->intertrans_model->intertrans_update($json_obj->transaction
					  , $json_obj->contents);
   }

 }


 
  function del(){
    echo 'Authorisation rejected.';
  }

}