<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mgmt_cost extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    is_logged_in();
    check_auth('mgmt_fina_v');
  }

  
  function index()
  {
  
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    
    $this->session->set_userdata('obj_branch', 'Branch..');
    $this->session->set_userdata('search', 'Search..');
	
    $filter[1]['deleted'] = 0;
    $this->session->set_userdata('filter', $filter);
     $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());  
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'date';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('mgmt_cost/lib');

  }
     
  
  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['obj_branch'] = $this->session->userdata('obj_branch');
    $data['search'] = $this->session->userdata('search');

    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('mgmt_cost/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

    $this->load->model('cost_model');
    
    $cost_list = $this->cost_model->getlist($data['date'],
						$data['obj_branch'],
						$data['filter'], 
						$data['search'], 
						$data['orderby'], 
						$config['per_page'], 
						$this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $cost_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $cost_list->query;
    
    $data['title'] = 'cost List';

    $data['main_content'] = 'mgmt_cost/mgmt_cost_list_view';  
    $data['table_title'] = 'cost list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }



  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('mgmt_cost/lib');
  }
  

  function filter() 
  { 

    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if (isset($filter[$flr_num][$key]) && $filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('mgmt_cost/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($obj_branch = $this->input->post('obj_branch')) {      
      $this->session->set_userdata('obj_branch', $obj_branch);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('mgmt_cost/lib');
  }
  
 

  function ajax_data($id)
  { 
    $json_obj = new stdClass;

    if ($id == 'new') {
      $obj_data = new stdClass;
      $obj_data->id = 'new';
      $obj_data->date = date("Y-m-d H:i:s");
      $obj_data->branch 
	= $obj_data->category
	= $obj_data->notes = '';
      $obj_data->total = 0;
      
      $contents=array();
	$contents[0] 
	  = array('product' => '', 'quantity' => 1, 'unit_price' => 0, 'discount' => 0);
      
      $obj_data->contents = json_encode($contents);
      
      $json_obj->obj_data = $obj_data;

      echo json_encode($json_obj);
      
    } else if ($id == 'dup') {
      
      $id = $this->uri->segment(4);
      $this->load->model('cost_model');
      if ($obj_data = $this->cost_model->load($id)) {
      
        $obj_data->id = 'new';
        $obj_data->date = date("Y-m-d H:i:s");
        
	$json_obj->obj_data = $obj_data;
		
	echo json_encode($json_obj); 
      }      

      
    } else {
     
      $this->load->model('cost_model');
      if ($obj_data = $this->cost_model->load($id)) {
      
	$json_obj->obj_data = $obj_data;
		
	echo json_encode($json_obj); 
      }      
    }

  }



  function view($id)
  {

    $data['json_url'] = site_url('mgmt_cost/ajax_data/'.$id); 
    $data['post_url'] = site_url('mgmt_cost/save'); 
    $data['read_only'] = 1;

    $data['main_content'] = 'mgmt_cost/mgmt_cost_edit_view';  
    $data['frame_name'] = 'View cost';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);

  }

  
  function edit()
  {
  check_auth('mgmt_fina_ve');
    if (!($id = $this->uri->segment(3)))
      {
	redirect('mgmt_cost/add');
      }

    $data['json_url'] = site_url('mgmt_cost/ajax_data/'.$id); 
    $data['post_url'] = site_url('mgmt_cost/save'); 
    $data['read_only'] = 0;

    $data['main_content'] = 'mgmt_cost/mgmt_cost_edit_view';  
    $data['frame_name'] = 'Edit cost';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
    
  }



  function dup($id)
  {
check_auth('mgmt_fina_ve');
    $data['json_url'] = site_url('mgmt_cost/ajax_data/dup').'/'.$id; 
    $data['post_url'] = site_url('mgmt_cost/save'); 
    $data['read_only'] = 0;

    $data['main_content'] = 'mgmt_cost/mgmt_cost_edit_view';  
    $data['frame_name'] = 'Add mgmt_cost';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	

  }

  function add()
  {
check_auth('mgmt_fina_ve');
    $data['json_url'] = site_url('mgmt_cost/ajax_data/new'); 
    $data['post_url'] = site_url('mgmt_cost/save'); 
    $data['read_only'] = 0;

    $data['main_content'] = 'mgmt_cost/mgmt_cost_edit_view';  
    $data['frame_name'] = 'Add mgmt_cost';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	

  }

 
  function save()
  {
check_auth('mgmt_fina_ve');
    $data['session_msg'] = 'Detail Saved';
    $this->load->view('includes/session_msg', $data);

    $json_obj = json_decode($this->input->post('json')); 


    $json_obj->obj_data->date
      = date("Y-m-d", strtotime(str_replace('/', '-', $json_obj->obj_data->date)));

    $this->load->model('cost_model');

    if ($json_obj->obj_data->id == 'new') {
      $this->cost_model->add($json_obj->obj_data, $json_obj->contents);
    } else {
      $this->cost_model->update($json_obj->obj_data, $json_obj->contents);
    }

  }


  function report()
  {
  
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['obj_branch'] = $this->session->userdata('obj_branch');
    $data['search'] = $this->session->userdata('search');

    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = '';
 

    $this->load->model('cost_model');
    
    $data_list = $this->cost_model->getlist($data['date'],
						$data['obj_branch'],
						$data['filter'], 
						$data['search'], 
						$data['orderby'], 
						$data['per_page'], 
						$this->uri->segment(3));
    
 
 

    $data['query'] = $data_list->query;
    
   
    
    $data['total'] = $data_list->total;
 
    $data['title'] = 'Cost Statement';
    $data['table_title'] = 'Cost list'; 
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('mgmt_cost/mgmt_cost_report_view.php', $data);	

  }
 
  function del_msg($id){
check_auth('mgmt_fina_ved');
    $this->load->model('cost_model');
    if ($obj_data = $this->cost_model->load($id)) {
      $json_obj->obj_data = $obj_data;
      $msg = 'Are you really want to delete [ '.$obj_data->id.' ] ?&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('mgmt_cost/del/'.$id).'" class="black">Yes</a>';
      $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('mgmt_cost/lib').'" class="black">No</a>';
      $this->session->set_flashdata('msg', $msg);
      redirect('mgmt_cost/lib');	
 
    }      


  }

  function del($id){
check_auth('mgmt_fina_ved');
    $this->load->model('cost_model');
    $this->cost_model->del($id);
    $msg = 'Item Deleted';
    $this->session->set_flashdata('msg', $msg);
    redirect('mgmt_cost/lib');
  }

/*
function trans(){

    $this->load->model('cost_model');
    $cost_list = $this->cost_model->getlist('','','','','','','');
     echo  $cost_list->total;
     $query = $cost_list->query;
    foreach($query->result() as $row): 
    $obj_data = $this->cost_model->load($row->id);
    $contents=array();
    $contents[0] = array('product' => $row->notes, 'quantity' => 1, 'unit_price' => $row->total, 'discount' => 0);   
    $this->cost_model->update($obj_data, $contents);

     endforeach;
}
*/


}
