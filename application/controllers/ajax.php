<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    is_logged_in();

  }

 
  function cust_names(){
    $term = $this->input->post('term'); 
    $this->load->model('customer_model');
    $data = $this->customer_model->load_names($term);
    echo json_encode($data);
  }

  function vendor_names(){
    $term = $this->input->post('term'); 
    $this->load->model('vendor_model');
    $data = $this->vendor_model->load_names($term);
    echo json_encode($data);
  }
  
  function product_names(){
    $term = $this->input->post('term'); 
    $this->load->model('inventory_model');
    $data = $this->inventory_model->load_names($term);
    echo json_encode($data);
  }
  
  function stock_names(){
    $term = $this->input->post('term'); 
    $this->load->model('inventory_model');
    $data = $this->inventory_model->load_names($term, TRUE);
    echo json_encode($data);
  }
  
  function staff_names(){
    $term = $this->input->post('term'); 
    $this->load->model('staff_model');
    $data = $this->staff_model->load_names($term);
    echo json_encode($data);
  }
  
  function branch_names(){
    $term = $this->input->post('term'); 
    $this->load->model('branch_model');
    $data = $this->branch_model->load_names($term);
    echo json_encode($data);
  }
  
  function cate_names(){
    $term = $this->input->post('term'); 
    $this->load->model('inventory_cate_model');
    $data = $this->inventory_cate_model->load_category_names($term);    
    echo json_encode($data);
  }
  
  
  function parent_cate_names(){
    $term = $this->input->post('term'); 
    $this->load->model('inventory_cate_model');
    $data = $this->inventory_cate_model->load_parent_names($term);      
    array_push($data, '(none)');
    echo json_encode($data);
  }

  function cost_cate_names(){
    $term = $this->input->post('term'); 
    $this->load->model('cost_cate_model');
    $data = $this->cost_cate_model->load_parent_names($term);      
    array_push($data, '(none)');
    echo json_encode($data);
  }
}
