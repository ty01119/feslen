<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mgmt_bol extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    // $this->load->helper('date');
    $this->load->helper('auth');
    is_logged_in();
    $this->branch = $this->session->userdata('branch');
     // check_auth('mgmt_inventory_v');
  }

  function load_price()
  {

    $this->load->model('bol_model');
    
    $array = $this->bol_model->load_price();
    echo json_encode($array);
  }
  
  function index()
  {
       check_auth($this->branch.'_ve', FALSE);
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    
    $this->session->set_userdata('customer', 'Customer..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('mgmt_bol/lib');

  }
     
  function load_inventory(){

    $result = $this->db->get('co_inventory')->result();
    echo json_encode($result);
  }

  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['customer'] = $this->session->userdata('customer');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('mgmt_bol/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

    $this->load->model('bol_model');
    
    $bol_list = $this->bol_model->bol_list($data['date'],
						       $data['customer'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       $config['per_page'], 
						       $this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $bol_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $bol_list->query;
    
    $data['title'] = '';

    $data['main_content'] = 'mgmt_bol_list_view';  
    $data['table_title'] = 'Bill Of Lading List'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }

  function report()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['customer'] = $this->session->userdata('customer');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = '';

   

    $this->load->model('bol_model');
    
    $invoice_list = $this->bol_model->bol_list($data['date'],
						       $data['customer'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       '', 
						       '');
    
    $data['total'] = $invoice_list->total;
    $data['query'] = $invoice_list->query;
    $data['title'] = 'Bill Of Lading Statement';
    $data['table_title'] = 'Bill Of Lading list'; 
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('invoice_report_view', $data);	

  }

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('mgmt_bol/lib');
  }
  

  function filter() 
  { 

    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if ($filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('mgmt_bol/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($customer = $this->input->post('customer')) {      
      $this->session->set_userdata('customer', $customer);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('mgmt_bol/lib');
  }
  

  function ajax_bol($id)
  { 
    $json_obj = new stdClass;

    if ($id == 'new') {
      $bol = new stdClass;
      $bol->id = 'new';
      $bol->date = date("Y-m-d H:i:s");
      $bol->subtotal 
	= $bol->tax 
	= $bol->total 
	= $bol->discount 
	= $bol->paid 
	= $bol->deleted = 0;
      $bol->cust_name = '';
       
      if ($cart = $this->session->userdata('cart1')) {
	$contents = $cart;
      } else {
	$contents=array();
$contents[0] = array('product' => '', 'quantity' => 1, 'unit_price' => 0, 'discount' => 0);
      }      


      $bol->contents = json_encode($contents);

      $json_obj->transaction = $bol;
      
      echo json_encode($json_obj);
      
    } else {
     
      $this->load->model('bol_model');
      if ($bol = $this->bol_model->load_bol($id)) {
	
	$json_obj->transaction = $bol;
	
	echo json_encode($json_obj);

      }      
    }

  }


  function view($id)
  {
    $this->load->model('bol_model');
    $bol = $this->bol_model->load_bol($id);
    $contents = json_decode($bol->contents);
    $bol->date = date("d/m/y", strtotime($bol->date));
    
    $this->load->model('customer_model');
    $customer = $this->customer_model->load_by_name($bol->cust_name);
    
    
    $this->load->model('branch_model');
    $branch = $this->branch_model->load_by_name('holdings');

    $data['transaction'] = $bol;
    $data['contents'] = $contents;
    $data['customer'] = $customer;
    $data['branch'] =  $branch;
    $data['title'] = 'BILL OF LADING';
    $this->load->view('transaction_html_view', $data);

  }


  function edit()
  {     
      check_auth($this->branch.'_ve', FALSE);
 //    check_auth('mgmt_inventory_ve', FALSE);
    if (!($id = $this->uri->segment(3)))
      {
	redirect('mgmt_bol/add');
      }

    $data['json_url'] = site_url('mgmt_bol/ajax_bol/'.$id); 
    $data['post_url'] = site_url('mgmt_bol/save'); 
    $data['ajax_url'] = site_url('ajax/product_names');
    $data['price_url'] = site_url('mgmt_bol/load_price');

    $data['main_content'] = 'mgmt_bol_edit_view';  
    $data['frame_name'] = 'Edit Bill Of Lading';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
    
  }



 function add()
  { 
       check_auth($this->branch.'_ve', FALSE);
  //check_auth('mgmt_inventory_ved', FALSE);
    $data['json_url'] = site_url('mgmt_bol/ajax_bol/new'); 
    $data['post_url'] = site_url('mgmt_bol/save'); 
    $data['ajax_url'] = site_url('ajax/product_names');
    $data['price_url'] = site_url('mgmt_bol/load_price');

    $data['main_content'] = 'mgmt_bol_edit_view';  
    $data['frame_name'] = 'Add Bill Of Lading';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	

  }

 function save()
 {
   $data['session_msg'] = 'bol Saved';
   $this->load->view('includes/session_msg', $data);

   $json_obj = json_decode($this->input->post('json')); 
   $this->load->model('bol_model');

   if ($json_obj->transaction->id == 'new') {
     $this->bol_model->bol_add($json_obj->transaction
				       , $json_obj->contents);
   } else {
     $this->bol_model->bol_update($json_obj->transaction
					  , $json_obj->contents);
   }

 }


 
  function del(){
    echo 'Authorisation rejected.';
  }

}
