<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Web_App_Task extends CI_Controller 
{

  var $branch;

  function __construct()
  {
    parent::__construct();
    $this->branch = $this->session->userdata('branch');
    //$this->load->helper('auth');
    keep_logged_in();
    is_logged_in();
    check_auth('websolution_v');
  }

  function test()
  {
    echo '123';die;
  }
  
  function index()
  {
  
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    $this->session->set_userdata('developer', 'Developer..');
    $this->session->set_userdata('designer', 'Designer..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('web_app_task/lib');

  }
     
  
  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['developer'] = $this->session->userdata('developer');
    $data['designer'] = $this->session->userdata('designer');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('web_app_task/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

    $this->load->model('web_app_task_model');
    
    $web_app_task_list = $this->web_app_task_model->web_app_task_list($data['date'],
						       $data['developer'],
						       $data['designer'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       $config['per_page'], 
						       $this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $web_app_task_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $web_app_task_list->query;
    
    $data['title'] = 'Web_App_Task Statement';

    $data['main_content'] = 'web_app_task_list_view';  
    $data['table_title'] = 'Web_App_Task list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('web_app_task/lib');
  }
  

  function filter() 
  { 
    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if (isset($filter[$flr_num][$key]) && $filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('web_app_task/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($developer = $this->input->post('developer')) {      
      $this->session->set_userdata('developer', $developer);
    }
    if ($designer = $this->input->post('designer')) {      
      $this->session->set_userdata('designer', $designer);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('web_app_task/lib');
  }
  


  function ajax_web_app_task($id)
  { 
    $json_obj = new stdClass;

    if ($id == 'new') {
      $web_app_task = new stdClass;
      $web_app_task->id = 'new';
      $web_app_task->date = date("Y-m-d H:i:s");
      $web_app_task->cust_name
	= $web_app_task->invo_id
	= $web_app_task->designer
	= $web_app_task->developer
	= $web_app_task->notes
	= $web_app_task->logs = '';

      $web_app_task->qa = 'Ke';

      $json_obj->data = $web_app_task;

      echo json_encode($json_obj);
      
    } else {
     
      $this->load->model('web_app_task_model');
      if ($web_app_task = $this->web_app_task_model->load_web_app_task($id)) {
	
	$json_obj->data = $web_app_task;
	
	
	echo json_encode($json_obj);

      }      
    }

  }

  function view($id)
  {
    $this->load->model('web_app_task_model');
    $web_app_task = $this->web_app_task_model->load_web_app_task($id);
    $contents = $this->web_app_task_model->load_invo_cons($id);
    $web_app_task->date = date("d/m/y", strtotime($web_app_task->date));

    $this->load->model('customer_model');
    $customer = $this->customer_model->load_by_name($web_app_task->cust_name);


    $this->load->model('branch_model');
    $branch = $this->branch_model->load_by_name($this->session->userdata('branch'));

    $data['web_app_task'] = $web_app_task;
    $data['contents'] = $contents;
    $data['customer'] = $customer;
    $data['branch'] =  $branch;
    
    $this->load->view('web_app_task_html_view', $data);

  }

  
  function edit()
  {
      check_auth('websolution_ve');
    if (!($id = $this->uri->segment(3)))
      {
	redirect('web_app_task/add');
      }

    $data['json_url'] = site_url('web_app_task/ajax_web_app_task/'.$id); 
    $data['cart_url'] = site_url('web_app_task/ajax_cart'); 
    $data['post_url'] = site_url('web_app_task/save'); 
    
    $data['main_content'] = 'web_app_task_edit_view';  
    $data['frame_name'] = 'Edit Web_App_Task';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
    
  }



  function add()
  {      check_auth('websolution_ve');
    $data['json_url'] = site_url('web_app_task/ajax_web_app_task/new'); 
    $data['cart_url'] = site_url('web_app_task/ajax_cart'); 
    $data['post_url'] = site_url('web_app_task/save'); 

    $data['main_content'] = 'web_app_task_edit_view';  
    $data['frame_name'] = 'Add Web_App_Task';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	

  }


 
  function save()
  {

    $json_obj = json_decode($this->input->post('json')); 
    $this->load->model('web_app_task_model');
   
    if ($json_obj->transaction->id == 'new') {
     
      $this->web_app_task_model->add($json_obj->transaction);
      $ret->msg =  'Web_App_Task Created';
      $ret->close = TRUE;
      
    } else {
      $this->web_app_task_model->update($json_obj->transaction);
      $ret->msg =  'Web_App_Task Detail Saved';
      $ret->close = TRUE;
      
    }
    
    echo json_encode($ret);
    
  }

 

  function del_msg($id){


      $msg = 'Are you really want to delete [ WEB_APP_TASK #'.$id.' ] ?&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('web_app_task/del/'.$id).'" class="black">Yes</a>';
      $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('web_app_task/lib').'" class="black">No</a>';
      $this->session->set_flashdata('msg', $msg);
      redirect('web_app_task/lib');	
  }

  function del($id){
    check_auth($this->branch.'_ved');

    $this->load->model('web_app_task_model');
    $this->web_app_task_model->web_app_task_del($id);
    $msg = 'Web_App_Task Deleted';
    $this->session->set_flashdata('msg', $msg);
    redirect('web_app_task/lib');
  }


}
