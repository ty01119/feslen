<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quoting extends CI_Controller {
  var $branch;
  function __construct()
  {
    parent::__construct();    
    $this->load->helper('auth');   
    keep_logged_in();
    is_logged_in();    
    $this->branch = $this->session->userdata('branch');
  }

  function index()
  {
  
    
    if ($this->branch == 'websolution')
    { 
    	redirect('quoting/websolution');
    }
    else if ($this->branch == 'energysigns')
    {
    	redirect('quoting/energysigns');
    }
    else 
    { 
    	redirect('quoting/digitalprint');
    }
  }
  
  function digitalprint()
  {
    
    $data['title'] = '';
    
    $data['price_url'] = site_url('quoting/load_price_js'); 
    
    $data['main_content'] = 'quoting/quoting_print_view.php';  
    $data['table_title'] = 'Quotings'; 

    $data['cart'] = $this->session->userdata('cart');

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	
  }


  function websolution (){
 
    $data['title'] = '';
    $data['main_content'] = 'quoting/quoting_web_view';  
    $data['table_title'] = 'Quotings'; 

    $data['cart'] = $this->session->userdata('cart');
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	
  
  }

  function designexpress (){
 
    $data['title'] = '';
    $data['main_content'] = 'quoting/quoting_design_view';  
    $data['table_title'] = 'Quotings'; 

    $data['cart'] = $this->session->userdata('cart');
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	
  
  }


  function energysigns(){
 
    $data['title'] = '';
    $data['main_content'] = 'quoting/quoting_signs_view';  
    $data['table_title'] = 'Quotings'; 

    $data['cart'] = $this->session->userdata('cart');
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	
  
  }

  function reset()
  {

    $cart = array();
    $this->session->set_userdata('cart', $cart);
	
  }

  function load_price_js($cate = '') {

    $this->load->model('quoting_model');
    
    switch ($cate) {
    case 'dp_price_pt':
      echo json_encode($this->quoting_model->dp_price_pt());
      break;
    case 'dp_price_lf':
      echo json_encode($this->quoting_model->dp_price_lf());
      break;
    case 'dp_price_bc':
      echo json_encode($this->quoting_model->dp_price_bc());
      break;
    case 'dp_price_bd':
      echo json_encode($this->quoting_model->dp_price_bd());
      break;
    case 'dp_price_lm':
      echo json_encode($this->quoting_model->dp_price_lm());
      break;
    }
		 
  }

  function cart()
  { 
    $cart =  $this->session->userdata('cart'); 
 
   
    if ($items = $this->input->post('array'))
    {
 
    	foreach ($items as $item) {
    	    $data = array(
		  'product'      => $item['name'],
		  'quantity'     => $item['qty'],
		  'unit_price'   => $item['price'],
		  'discount'    => 0
		  );
    		array_push($cart, $data);
    	}

    $this->session->set_userdata('cart', $cart);
    }
 
    else if ($this->input->post('name'))
    {
    $data = array(
		  'product'      => $this->input->post('name'),
		  'quantity'     => $this->input->post('qty'),
		  'unit_price'   => $this->input->post('price'),
		  'discount'    => 0
		  );
    array_push($cart, $data);
    $this->session->set_userdata('cart', $cart);
    }
 
    echo json_encode($cart);
  }

  function cart_t1()
  { //$this->session->unset_userdata('cart');
    echo "open<br />";
    
    $cart = $this->session->userdata('cart');
    if ($cart) echo 'ok';
  foreach($cart as $items):
    echo $items['product'];echo '<br />';
    endforeach;  

    

  }

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
