<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mgmt_staff extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    is_logged_in();
    check_auth('mgmt_staff_v');
  }

  function test()
  {

    echo '123';die;
  }
  
  function index()
  {
 
    $this->session->unset_userdata('orderby');
  //  $this->session->unset_userdata('filter'); 
    $filter[1]['status'] = 'active';
    $filter[2]['full_time'] = 'yes';
    $this->session->set_userdata('filter', $filter); 
    $this->session->set_userdata('staff', 'Staff..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('per_page', 50);
    $orderby['order'] = 'name';
    $orderby['sort'] = 'asc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('mgmt_staff/lib');

  }
     

  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['staff'] = $this->session->userdata('staff');
    $data['search'] = $this->session->userdata('search');
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('mgmt_staff/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

    $this->load->model('staff_model');
    
    $staff_list = $this->staff_model->staff_list($data['filter'], 
						 $data['search'], 
						 $data['orderby'], 
						 $config['per_page'], 
						 $this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $staff_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $staff_list->query;
    
    $data['title'] = '';

    $data['main_content'] = 'mgmt_staff_list_view';  
    $data['table_title'] = 'Staff Management'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('mgmt_staff/lib');
  }
  

  function filter() 
  { 

    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if (isset($filter[$flr_num][$key]) && $filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('mgmt_staff/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($staff = $this->input->post('staff')) {      
      $this->session->set_userdata('staff', $staff);
    }
  
    redirect('mgmt_staff/lib');
  }
  

  function ajax_staff($id)
  {
    $json_obj = new stdClass;

    if ($id == 'new') { 
      $staff = new stdClass;
      $staff->id = 'new';
      $staff->working_id = '1999';
      $staff->name 
	= $staff->position
	= $staff->branch
	= $staff->authority
	= $staff->full_name
	= $staff->cellphone
	= $staff->telephone
	= $staff->email 
	= $staff->address
	= $staff->gender
	= $staff->academic_bg
	= $staff->ird_num
	= $staff->bank_acc_num
	= $staff->pay_rate
	= $staff->working_hrs
	= $staff->mgmt_notes
	= $staff->password = '';
     $staff->working_time = '09:00:00';
     $staff->date_of_birth = $staff->date_of_cmnc = '2000-01-01';
     $staff->status = 'active';

      $json_obj->staff = $staff;
      echo json_encode($json_obj);
      
    } else {
      
      $this->load->model('staff_model');
      
      if ($staff = $this->staff_model->load_personnel($id)) {
	 $staff->password = '';
	$json_obj->staff = $staff;
	
	echo json_encode($json_obj);
      }
    
    }
  }

  function edit($id)
  {
    check_auth('mgmt_staff_ve', FALSE);
    if (!($id = $this->uri->segment(3)))
      {
	redirect('mgmt_staff/add');
      }
 
    $data['json_url'] = site_url('mgmt_staff/ajax_staff/'.$id); 
    $data['post_url'] = site_url('mgmt_staff/save'); 
    $data['read_only'] = 0;
    
    $data['main_content'] = 'mgmt_staff_edit_view';  
    $data['frame_name'] = 'Edit Staff';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
    
  }

  function add()
  {
     check_auth('mgmt_staff_ved', FALSE);
    $data['json_url'] = site_url('mgmt_staff/ajax_staff/new'); 
    $data['post_url'] = site_url('mgmt_staff/save'); 
    $data['read_only'] = 0;
    $data['mode'] = 'add';
    
    $data['main_content'] = 'mgmt_staff_edit_view';  
    $data['frame_name'] = 'Add Staff';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);
    
  }

  function save()
  {
     check_auth('mgmt_staff_ve', FALSE);
    $data['session_msg'] = 'Information Saved';
    $this->load->view('includes/session_msg', $data);
   
    $json_obj = json_decode($this->input->post('json')); 

    if ($json_obj->staff->password) 
	$json_obj->staff->password = sha1($json_obj->staff->password);
   else unset($json_obj->staff->password);

    $json_obj->staff->date_of_cmnc 
      = date("Y-m-d", strtotime(str_replace('/', '-', $json_obj->staff->date_of_cmnc)));

    $json_obj->staff->date_of_birth 
      = date("Y-m-d", strtotime(str_replace('/', '-', $json_obj->staff->date_of_birth)));
   
    $this->load->model('staff_model');

    if ($json_obj->staff->id == 'new') {
      $this->staff_model->personnel_add($json_obj->staff);
    } else {
      $this->staff_model->personnel_update($json_obj->staff);
    }
   
  }

  function view($id)
  {
 
    $data['json_url'] = site_url('mgmt_staff/ajax_staff/'.$id); 
    $data['post_url'] = site_url('mgmt_staff/save'); 
    $data['read_only'] = 1;
    
    $data['main_content'] = 'mgmt_staff_edit_view';  
    $data['frame_name'] = 'View Staff';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
    
  }
 
  function del(){
   check_auth('mgmt_STOP_ved', FALSE);// echo 'Authorisation rejected.';
  }

}
