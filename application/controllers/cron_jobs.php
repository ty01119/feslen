<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron_jobs extends CI_Controller 
{

  function __construct()
  {    
    parent::__construct();	
    $this->load->model('cron_model');
    $this->cf_site = $this->config->item('site');
    $this->load->helper('email');   
    $this->load->helper('file');
  }
  
  function index()
  {
  	$this->attn_list();
  	$this->email_backup();
  }
  
  
  function attn_list()
  { 
    $this->cron_model->attn_list();  
  }
 
   function attn_list_r()
  { 
    $this->attn_list();
    $msg = "Today's attn list added for full time staffs";
    $this->session->set_flashdata('msg', $msg);        
    redirect();   
  }

  function email_backup()
  { 
 	$time = '_'.date("Y-m-d");

	$this->load->dbutil();
	$prefs = array(
                'format'      => 'zip',             
                'filename'    => 'feslen_bk'.$time.'.sql',    
                'add_drop'    => TRUE,              
                'add_insert'  => TRUE,              
                'newline'     => "\n"               
              );

	$backup =& $this->dbutil->backup($prefs); 
            
 	$file = 'files/feslen_bk'.$time.'.zip';
 
        file_put_contents($file, $backup);
  
	$data['sender'] = $this->cf_site['sender'];
	$data['website'] = $this->cf_site['name'];
 	    	    
	$this->load->library('email');
	$this->email->from($this->cf_site['email'], $this->cf_site['sender']);
	$this->email->to($this->cf_site['email']);
	$this->email->to('ke@3a.co.nz');
	$this->email->subject('Feslen BMS backup');
	$this->email->message('Feslen BMS backup '.date('Y-m-d'));
	$this->email->attach($file);
	$this->email->send();	
 	unlink($file);

  }
  
  function email_backup_r()
  { 
  	$this->email_backup();
  	$msg = 'Database backup sent to '.$this->cf_site['email'];
	$this->session->set_flashdata('msg', $msg);
        redirect();  
  }
  
}
