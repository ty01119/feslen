<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statement extends CI_Controller {

  function __construct()
  {
    parent::__construct();   
    $this->branch = $this->session->userdata('branch'); 
    $this->load->helper('auth'); 
    $this->load->helper('info');   
    is_logged_in();
  }
	
  function index()
  {
  
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    $this->session->set_userdata('customer', 'Customer..');
 //   $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());  
    /*  
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    */
    $this->session->keep_flashdata('msg');
    redirect('statement/lib');

  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    } 
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('statement/lib');
  }
  
  function lib()
  {   	
    
    $data['customer'] = $this->session->userdata('customer'); 

    $this->load->model('invoice_model');
    $filter[3]['sql'] = 'paid-ne-total'; 
    $filter[1]['finished'] = 1;
 	

 //   $data['date']['fr'] = strtotime("1 January 2000");
 //   $data['date']['to'] = strtotime(date('Y-m-t', strtotime(str_replace('/', '-', $this->input->post('to')))));

    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    
    
//echo date('Y-m-d', $data['date']['to']);
    $invoice_list = $this->invoice_model->invoice_list($data['date'], $data['customer'], $filter,'','', '','');
    
 //   $data['query'] = $invoice_list->query;
 
 // combine invoice to customer list with debit
 $customers = array();
 foreach($invoice_list->query->result() as $row) { 
 	array_key_exists($row->cust_name, $customers) ?
 	$customers[$row->cust_name] += $row->total - $row->paid
 	: 	
 	$customers[$row->cust_name] = $row->total - $row->paid;
 }
  $data['customers'] = $customers;
// print_r($customers);
 //die;
         
    $data['title'] = 'Customer Statement';

    $data['main_content'] = 'statement/statement_list_view';  
    $data['table_title'] = 'Statement list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	
   
  }



 function view() {	
    $customer = $this->input->post('customer');

    $this->load->model('invoice_model');
    $filter[3]['sql'] = 'paid-ne-total';
    $filter[1]['finished'] = 1;

    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    
    $invoice_list = $this->invoice_model->invoice_list($data['date'],$customer, $filter,'','', '','');
    
    $data['query'] = $invoice_list->query;
 
    $this->load->model('customer_model');
    $customer = $this->customer_model->load_by_name($customer);

    $this->load->model('branch_model');
    $branch = $this->branch_model->load_by_name($this->branch);
 
    $branch = refine_branch_data($branch, date("Y-m-d H:i:s", $data['date']['to']));
    
    $data['customer'] = $customer;
    $data['branch'] =  $branch;
   
    $html = $this->load->view('statement/statement_pdf_view', $data, true);
 
     foreach($invoice_list->query->result() as $row) {
        $html .= '<div style="page-break-before: always;"></div>';
     	$html .= $this->load_invoice($row->id, $customer, $branch);
     }
    echo $html; 
   


 }
 


  function pdf()
  {

    $customer = $this->input->post('customer');

    $this->load->model('invoice_model');
    $filter[3]['sql'] = 'paid-ne-total';
    $filter[1]['finished'] = 1;

    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    
    $invoice_list = $this->invoice_model->invoice_list($data['date'],$customer, $filter,'','', '','');
    
    $data['query'] = $invoice_list->query;
 
    $this->load->model('customer_model');
    $customer = $this->customer_model->load_by_name($customer);


    $this->load->model('branch_model');
    $branch = $this->branch_model->load_by_name($this->branch);

 
    $branch = refine_branch_data($branch, date("Y-m-d H:i:s", $data['date']['to']));
    
    $data['customer'] = $customer;
    $data['branch'] =  $branch;
    
    $this->load->helper('dompdf6');   
    $html = $this->load->view('statement/statement_pdf_view', $data, true);

 
     foreach($invoice_list->query->result() as $row) {
        $html .= '<div style="page-break-before: always;"></div>';
     	$html .= $this->load_invoice($row->id, $customer, $branch);
     }
 
    pdf_create($html, 'statement_'.url_title($customer->name).'_'.date("Y-m-d"));
    
  }
  
  function load_invoice($id, $customer, $branch) {
  
    $this->load->model('invoice_model');
    $invoice = $this->invoice_model->load_invoice($id);
    $contents = $this->invoice_model->load_invo_cons($id);
 
    $invoice->date = date("d/m/y", strtotime($invoice->date));
    
    $data['invoice'] = $invoice;
    $data['contents'] = $contents;
    $data['customer'] = $customer;
    $data['branch'] =  $branch;
    
    return $this->load->view('statement/invoice_pdf_view', $data, true);

  }
  
  
  function email_msg(){

    check_auth($this->branch.'_ve');
       $customer = $this->input->post('customer');
    $this->load->model('customer_model');
    
    $customer = $this->customer_model->load_by_name($customer);
 
      $msg = 'Are you really want to email statement to '. $customer->billing_contact.' ('. $customer->billing_email.') ?&nbsp;&nbsp;';
      $msg .= '&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('invoice/lib/'.$this->session->userdata('page_index')).'" class="black">No</a>';
    $msg .= '<form name="input" action="'.site_url('statement/email/').'" method="post">
<br />Additional Message: <br /><textarea name="email_msg" style="width: 349px; "></textarea>
<input type="submit" value="Send" style="color: #030303;" class="ui-state-highlight ui-corner-all">
<input type="hidden" name="customer" value="'.$customer->name.'"  >
</form>';
      $this->session->set_flashdata('msg', $msg);
      redirect('statement/lib/');	
  }

  function email()
  {

    check_auth($this->branch.'_ve');
    
    $data['email_msg'] = $this->input->post('email_msg'); 
    
   $customer = $this->input->post('customer');

    $this->load->model('invoice_model');
    $filter[3]['sql'] = 'paid-ne-total';
    $filter[1]['finished'] = 1;

    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    
    $invoice_list = $this->invoice_model->invoice_list($data['date'],$customer, $filter,'','', '','');
    
    $data['query'] = $invoice_list->query;
 
    $this->load->model('customer_model');
    $customer = $this->customer_model->load_by_name($customer);

    $this->load->model('branch_model');
    $branch = $this->branch_model->load_by_name($this->branch);
 
    $branch = refine_branch_data($branch, date("Y-m-d H:i:s", $data['date']['to']));
    
    $data['customer'] = $customer;
    $data['branch'] =  $branch;
    
    $this->load->helper(array('file', 'email'));
    $this->load->helper('dompdf6');
    $html = $this->load->view('statement/statement_pdf_view', $data, true);
 
     foreach($invoice_list->query->result() as $row) {
        $html .= '<div style="page-break-before: always;"></div>';
     	$html .= $this->load_invoice($row->id, $customer, $branch);
     }
  
    $filename = 'statement_'.url_title($customer->name).'_'.date("Y-m-d");

    $pdf = pdf_create($html, $filename, FALSE);      
    
    $file = 'files/'.$filename.'.pdf';
 
    file_put_contents($file, $pdf);
        
 //   $data['member'] = $data['customer']; 
    $data['sender'] = $branch->biz_name;
 
    $letter = $this->load->view('templates/email_statement', $data, true);
        	    
    $this->load->library('email');
    $this->email->from($branch->email, $data['sender']);

 
    if (!valid_email($data['customer']->billing_email)) 
    {
    	$msg = 'Email address invalid, STATEMENT could not be sent.';
    	$this->session->set_flashdata('msg', $msg);
    }
    else
    {
    $this->email->to($data['customer']->billing_email);
//    $this->email->to($branch->email); 
    $this->email->bcc($branch->email); 
    
    $this->email->subject($branch->biz_name.' | STATEMENT');
   
    $this->email->message($letter);
    $this->email->attach($file);
    $this->email->send();
 
    $msg = 'STATEMENT sent to '.$customer->name.' - '.$customer->billing_contact.' ('. $customer->billing_email.').';
     //   $msg = 'INVOICE did not send to customer but your branch email instead for testing purpose.';
    $msg .= '<style>#cust-'.url_title($customer->name).'{outline: yellow solid 1px;}</style>';
    $this->session->set_flashdata('msg', $msg);
    }
    unlink($file);
    redirect('statement/lib/'.$this->session->userdata('page_index'));
    
  }


/////

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */