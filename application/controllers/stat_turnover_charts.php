<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stat_turnover_charts extends CI_Controller 
{

  var $branch;

  function __construct()
  {
    parent::__construct();
    $this->branch = $this->session->userdata('branch');
    $this->load->helper('auth');
    is_logged_in();
  
  }

  
  function index()
  {
  
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    $this->session->set_userdata('customer', 'Customer..');
    $this->session->set_userdata('stat_branch', $this->branch);
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime('2012-01-01'));
    $this->session->set_userdata('date_to', strtotime('next sunday'));    
     $this->session->set_userdata('chart', 'monthly');
    $this->session->keep_flashdata('msg');
    redirect('stat_turnover_charts/lib');

  }
    function monthly()
  {
  
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    $this->session->set_userdata('customer', 'Customer..');
    $this->session->set_userdata('stat_branch', $this->session->userdata('stat_branch'));
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime('2012-01-01'));
    $this->session->set_userdata('date_to', strtotime('next sunday'));    
     $this->session->set_userdata('chart', 'monthly');
    $this->session->keep_flashdata('msg');
    redirect('stat_turnover_charts/lib');

  }
     function weekly()
  {
  
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter'); 
    $this->session->set_userdata('customer', 'Customer..');
    $this->session->set_userdata('stat_branch', $this->session->userdata('stat_branch'));
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime('2012-01-01'));
    $this->session->set_userdata('date_to', strtotime('next sunday'));    
     $this->session->set_userdata('chart', 'weekly');
    $this->session->keep_flashdata('msg');
    redirect('stat_turnover_charts/lib');

  }
  
  function lib()
  {
    check_auth($this->session->userdata('stat_branch').'_ved');
    
    $data['orderby'] = $this->session->userdata('orderby');
        $data['chart'] = $this->session->userdata('chart');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['branch'] = $this->session->userdata('stat_branch');
    $data['search'] = $this->session->userdata('search');    $data['customer'] = $this->session->userdata('customer');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');
    

    
    $this->load->model('stat_model');
 
    if ( $data['chart'] == 'monthly') {
 
//    $data['series1']=json_encode($this->get_monthly($data['branch'], '2012'));
     
//    $data['series2']=json_encode($this->get_monthly($data['branch'], '2011'));
    
 $data['series1']=json_encode($this->get_all_monthly($data['branch']));
    } 
    if ( $data['chart'] == 'weekly') {
//    $data['series3']=json_encode($this->get_weekly($data['branch'], '2012'));
     
//    $data['series4']=json_encode($this->get_weekly($data['branch'], '2011'));
    
 $data['series3']=json_encode($this->get_all_weekly($data['branch']));
    }

    $data['title'] = 'Statistical Charts';
   
    $data['main_content'] = 'statistics/stat_turnover_charts2_view';  
    $data['table_title'] = 'Turnover'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	
   
  }

  function get_monthly($branch, $year ){
    
    $date['fr'] =  strtotime($year.'-01-01');
    $totals = array();
    for ($i = 1; $i <= 12; $i++) {

    $date['to'] = strtotime('+ 1 month', $date['fr']);
    $result = $this->stat_model->turnover_by_branch($branch, $date, '', '','');  
	array_push( $totals, $result->total);
    $date['fr'] = $date['to'];
    }
    $rta = array();
    $rta['name']=$branch.' '.$year;
    $rta['data']= $totals;
    return $rta;
  }
  
  
  function get_all_monthly($branch){
    
    $date['fr'] =  strtotime('2011-01-01');
    $totals = array();
    for ($i = 1; $i <= 48; $i++) {

    $date['to'] = strtotime('+ 1 month', $date['fr']);
    $result = $this->stat_model->turnover_by_branch($branch, $date, '', '','');  
	array_push( $totals, $result->total);
    $date['fr'] = $date['to'];
    }
    $rta = array();
    $rta['name'] = '( '.strtoupper($branch).' - since 2011.01.01 )';
    $rta['data'] = $totals;
    return $rta;
  }

  function get_all_weekly($branch ){
    
    $date['fr'] =  strtotime('next sunday', strtotime('2012-01-01'));
    $date['to'] = strtotime('+ 1 week', $date['fr']);
    $first = $this->stat_model->turnover_by_branch($branch, $date, '', '',''); 
    
    $totals = array();
    $avg = array($first, $first, $first, $first, $first, $first, $first, $first);
    
    for ($i = 1; $i <= 104; $i++) {
	    $date['to'] = strtotime('+ 1 week', $date['fr']);
	    if ($date['to'] >= time()) break;
	    $result = $this->stat_model->turnover_by_branch($branch, $date, '', '','');  
	    array_shift($avg);
	    array_push( $avg, $result->total);
	    
	    $curr_avg = array_sum($avg) / count($avg);
	    array_push( $totals, $curr_avg);
	    $date['fr'] = $date['to'];
    }
    
    $rta = array();
    $rta['name'] = '( '.strtoupper($branch).' - since 2012.01.01 )';
    $rta['data'] = $totals;
    return $rta;
  }
  
  function get_weekly($branch, $year ){
    
    $date['fr'] =  strtotime('next sunday', strtotime($year.'-01-01'));
    $date['to'] = strtotime('+ 1 week', $date['fr']);
    $first= $this->stat_model->turnover_by_branch($branch, $date, '', '',''); 
    
    $totals = array();
    $avg = array($first, $first, $first, $first, $first, $first, $first, $first);
    
    for ($i = 1; $i <= 52; $i++) {

	    $date['to'] = strtotime('+ 1 week', $date['fr']);
	    if ($date['to'] >= time()) break;
	    $result = $this->stat_model->turnover_by_branch($branch, $date, '', '','');   
	    array_shift($avg);
	    array_push($avg, $result->total);
	    $curr_avg = array_sum($avg) / count($avg);
	    array_push($totals, $curr_avg);
   	    $date['fr'] = $date['to'];
    }
    $rta = array();
    $rta['name']=$branch.' '.$year;
    $rta['data']= $totals;
    return $rta;
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($branch = $this->input->post('branch')) {      
      $this->session->set_userdata('stat_branch', $branch);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('stat_turnover_charts/lib');
  }
 
  

  

}